# Remember Me App

Bible memorization application [Remember Me][docs]

## Getting started

1. Set up the [Flutter development environment][flutter]
2. Check out the develop branch
3. Run the app (main_beta.dart). For Android add --flavor beta

## Need help?

Please use the [user forum][user forum] for help requests and how-to questions.
Please open Gitlab issues for bugs and enhancements only, not general help requests.

## Want to contribute?

If you want to contribute through code or documentation, the [Contributing
guide is the best place to start][contributing]. If you have questions, feel free
to ask.

## Want to support us?

If you want to support us financially, you can [help fund the project
through a Paypal donation][paypal].

[flutter]: https://flutter.dev/docs/get-started/install
[docs]: https://www.remem.me
[user forum]: https://forum.remem.me/
[contributing]: https://gitlab.com/remem-me/app/tree/master/CONTRIBUTING.md
[paypal]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NHMVF5RBNH3TE



