#!/usr/bin/env bash
# fail if any commands fails
set -e
# make pipelines' return status equal the last command to exit with a non-zero status, or zero if all commands exit successfully
set -o pipefail
# debug log
set -x

# echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
flutter build web -t lib/config/${MAIN} --web-renderer canvaskit
mkdir -p build/web/assets/download
mv $BITRISE_APK_PATH build/web/assets/download/remember_me.apk
rsync -rtz --del --progress build/web/ remem-me@ssh-remem-me.alwaysdata.net:/home/remem-me/${TARGET}/
#curl --basic --user "$DEPLOY_API_KEY account=remem-me:" --data '' --request POST --silent --output /dev/null --write-out '%{http_code}' "https://api.alwaysdata.com/v1/site/${SITE_ID}/restart/"
curl -X POST --basic --user "$DEPLOY_API_KEY:" "https://api.alwaysdata.com/v1/site/${SITE_ID}/restart/"