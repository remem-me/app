import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:integration_test/integration_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/config/main_config.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/widgets/box/box.dart';
import 'package:remem_me/widgets/zoom/zoom.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockClient extends Mock implements http.Client {}

class MockSettings extends Mock implements Settings {}

class MockRepository<T extends Entity> extends Mock
    implements EntitiesRepositoryHive<T> {}

class MockQueues extends Mock implements Queues {}

class MockHomeWidgetService extends Mock implements HomeWidgetService {}

class MockZoomCubit extends Mock implements ZoomCubit {}

void main() async {
  MockClient client = MockClient();
  MockSettings settings = MockSettings();
  MockRepository<AccountEntity> accountsRepo = MockRepository<AccountEntity>();
  MockRepository<VerseEntity> versesRepo = MockRepository<VerseEntity>();
  MockRepository<TagEntity> tagsRepo = MockRepository<TagEntity>();
  MockRepository<ScoreEntity> scoresRepo = MockRepository<ScoreEntity>();
  MockQueues queues = MockQueues();
  MockHomeWidgetService homeWidgetService = MockHomeWidgetService();
  MockZoomCubit zoomCubit = MockZoomCubit();

  setUpAll(() {
    final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
    registerFallbackValue(Uri());
    registerFallbackValue({});
    registerFallbackValue(Object());
    registerFallbackValue(Entity);
  });

  group('Sanity test', () {
    testWidgets('Test framework should be sane', (WidgetTester tester) async {
      expect(2 + 2, lessThan(5));
    });
  });

  group('Home test', () {
    setUp(() async {
      I.registerSingleton<Settings>(settings);
      I.registerSingleton<HomeWidgetService>(homeWidgetService);
      I.registerSingleton<http.Client>(client);
      I.registerSingleton<EntitiesRepositoryHive<AccountEntity>>(
          accountsRepo);
      I.registerSingleton<EntitiesRepositoryHive<VerseEntity>>(versesRepo);
      I.registerSingleton<EntitiesRepositoryHive<TagEntity>>(tagsRepo);
      I.registerSingleton<EntitiesRepositoryHive<ScoreEntity>>(scoresRepo);
      I.registerSingleton<Queues>(queues);
      I.registerSingleton<ZoomCubit>(zoomCubit);
      when(() => homeWidgetService.update(any()))
          .thenAnswer((_) async => true);
      when(() => settings.put(any(), any())).thenAnswer((_) async {});
      when(() => settings.putInt(any(), any())).thenAnswer((_) async {});
      when(() => settings.deviceId).thenReturn(1);
      when(() => settings.hasNewDbVersion).thenReturn(false);
      when(() => settings.get(Settings.ACCESS_TOKEN)).thenReturn('access');
      when(() => settings.get(Settings.REFRESH_TOKEN)).thenReturn('refresh');
      when(() => settings.get(Settings.CURRENT_ACCOUNT))
          .thenReturn(jsonEncode(Account('test', id: 1).toJson()));
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      when(() => accountsRepo.readList(
              accountId: any(named: 'accountId'), deleted: false))
          .thenAnswer((_) async => [AccountEntity('test-account', id: 1)]);
      when(() => versesRepo.readList(accountId: 1, deleted: false)).thenAnswer(
          (_) async => [VerseEntity('reference', 'passage', id: 1, accountId: 1)]);
      when(() => tagsRepo.readList(accountId: 1, deleted: false))
          .thenAnswer((_) async => []);
      when(() => scoresRepo.readList(accountId: 1, deleted: false))
          .thenAnswer((_) async => []);
      when(() => queues.get(any(), accountId: any(named: 'accountId')))
          .thenReturn({});
      when(() => queues.add(any(), any())).thenAnswer((_) async {});
      when(() => accountsRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => versesRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => tagsRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => scoresRepo.updateList(any())).thenAnswer((_) async => []);
      when(() => accountsRepo.entityType).thenReturn(AccountEntity);
      when(() => versesRepo.entityType).thenReturn(VerseEntity);
      when(() => tagsRepo.entityType).thenReturn(TagEntity);
      when(() => scoresRepo.entityType).thenReturn(ScoreEntity);
      when(() => zoomCubit.state).thenReturn(1.0);
      when(() => zoomCubit.stream).thenAnswer((_) => NeverStream());

      await registerServices();
    });

    tearDown(() {
      reset(client);
      reset(accountsRepo);
      reset(versesRepo);
      I.reset(dispose: false);
    });

    testWidgets('Authenticated user should see restored verses',
        (WidgetTester tester) async {
      await initApp();
      final accountsBloc = I<AccountsBloc>();
      final versesBloc = I<VersesBloc>();

      expectLater(
          accountsBloc.stream,
          emitsInOrder([
            isA<EntitiesLoadInProgress<AccountEntity>>(),
            isA<EntitiesLoadSuccess<AccountEntity>>(),
          ])).then((_) {
        verify(() => accountsRepo.readList(
            accountId: any(named: 'accountId'), deleted: false)).called(1);
      });

      expectLater(
          versesBloc.stream,
          emitsInOrder([
            isA<EntitiesLoadInProgress<VerseEntity>>(),
            isA<EntitiesLoadSuccess<Verse>>(),
          ])).then((_) {
        verify(() => versesRepo.readList(accountId: 1, deleted: false)).called(1);
        expectSync((versesBloc.state as EntitiesLoadSuccess<Verse>).items.length, equals(1));
      });

      await tester.pumpAndSettle();
      expect(find.byType(SelectableVerseTile), findsOneWidget);
    });

    testWidgets('Authenticated startup should trigger successful replication',
        (WidgetTester tester) async {
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      when(() => client.get(
              Uri.parse('https://rpl.remem.me/v1/accounts/?deleted=false'),
              headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response(
              '[{"id":1,"modified":1,"modified_by":"1","deleted":false,'
              '"name":"test","language":"en","lang_ref":null,'
              '"review_limit":0,"daily_goal":0,"review_frequency":2.0,"inverse_limit":0,'
              '"order_new":2,"order_due":1,"order_known":3,"order_all":1,'
              '"reference_included":false,"topic_preferred":false,'
              '"imported_decks":[],"default_source":"GNTD"}]',
              200));
      await initApp();

      final bloc = I<ReplicationBloc>();

      expectLater(
          bloc.stream,
          emitsInOrder([
            isA<ReplicationInProgress>(),
            isA<ReplicationSuccess>(),
          ])).then((_) {
        verify(() => client.get(any(), headers: any(named: 'headers')))
            .called(4);
      });

      await tester.pumpAndSettle();
    });

  });
}