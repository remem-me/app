import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:integration_test/integration_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/config/main_config.dart';
import 'package:remem_me/screens/screens.dart';
import 'package:repository/repository.dart';

class MockClient extends Mock implements http.Client {}

class MockSettings extends Mock implements Settings {}

void main() async {
  MockClient client = MockClient();
  MockSettings settings = MockSettings();

  setUpAll(() {
    final binding = IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    binding.framePolicy = LiveTestWidgetsFlutterBindingFramePolicy.fullyLive;
    when(() => settings.put(any(), any())).thenAnswer((_) async {});
    when(() => settings.putInt(any(), any())).thenAnswer((_) async {});
    when(() => settings.deviceId).thenReturn(0);
    when(() => settings.hasNewDbVersion).thenReturn(false);
    registerFallbackValue(Uri());
    registerFallbackValue({});
    registerFallbackValue(Object());
  });

  group('Sanity test', () {
    testWidgets('Test framework should be sane', (WidgetTester tester) async {
      expect(2 + 2, lessThan(5));
    });
  });

  group('Login test', () {
    setUp(() {
      I.registerSingleton<http.Client>(client);
      I.registerSingleton<Settings>(settings);
      registerServices();
    });

    tearDown(() {
      reset(client);
      I.reset(dispose: false);
    });

    testWidgets('Successful login should display home screen',
        (WidgetTester tester) async {
      when(() => client.post(any(),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async =>
              http.Response('{"refresh": "refresh", "access": "access"}', 200));
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      await initApp();
      await tester.pumpAndSettle();
      await _login(tester);

      expect(find.byKey(const ValueKey('homeBar')), findsOneWidget);
    });


    testWidgets('Login should trigger successful replication',
        (WidgetTester tester) async {
      when(() => client.post(any(),
              body: any(named: 'body'), headers: any(named: 'headers')))
          .thenAnswer((_) async =>
              http.Response('{"refresh": "refresh", "access": "access"}', 200));
      when(() => client.get(any(), headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response('[]', 200));
      when(() => client.get(
              Uri.parse('https://rpl.remem.me/v1/accounts/?deleted=false'),
              headers: any(named: 'headers')))
          .thenAnswer((_) async => http.Response(
              '[{"id":1,"modified":1,"modified_by":"1","deleted":false,'
              '"name":"test","language":"en","lang_ref":null,'
              '"review_limit":0,"daily_goal":0,"review_frequency":2.0,"inverse_limit":0,'
              '"order_new":2,"order_due":1,"order_known":3,"order_all":1,'
              '"reference_included":false,"topic_preferred":false,'
              '"imported_decks":[],"default_source":"GNTD"}]',
              200));
      await initApp();
      await tester.pumpAndSettle();

      BuildContext context = tester.element(find.byType(StartScreen));
      final bloc = BlocProvider.of<ReplicationBloc>(context);

      expectLater(
          bloc.stream,
          emitsInOrder([
            isA<ReplicationInProgress>(),
            isA<ReplicationSuccess>(),
          ])).then((_) {
        verify(() => client.get(any(), headers: any(named: 'headers')))
            .called(4);
      });

      await _login(tester);

      expect(find.byKey(const ValueKey('homeBar')), findsOneWidget);
    });


    testWidgets('Failed login should display error message',
            (WidgetTester tester) async {
          when(() => client.post(any(),
              body: any(named: 'body'), headers: any(named: 'headers')))
              .thenAnswer((_) async => http.Response('{"detail": "401"}', 401));

          await initApp();
          await tester.pumpAndSettle();
          await _login(tester);

          expect(find.byKey(const ValueKey('errorText')), findsOneWidget);
        });
  });
}

Future<void> _login(WidgetTester tester) async {
  await tester.tap(find.byKey(const ValueKey('loginButton')));
  await tester.pumpAndSettle();
  await tester.enterText(find.byType(TextFormField).first, 'test@mail.com');
  await tester.enterText(find.byType(TextFormField).last, 'password');
  await tester.tap(find.byKey(const ValueKey('submitButton')));
  await tester.pumpAndSettle();
}
