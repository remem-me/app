import 'config.dart';

/* https://www.mouritech.com/it-technical-articles/implementing-flutter-flavors-for-android-ios-applications/ */
Future<void> main() {
  AppConfig.appFlavor = Flavor.prod;
  return mainConfig();
}