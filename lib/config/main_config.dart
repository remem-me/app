import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:path_provider/path_provider.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/services/bible_service.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:remem_me/services/notification_service.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/services/playback_service.dart';
import 'package:remem_me/widgets/zoom/zoom_cubit.dart';
import 'package:repository/repository.dart';

import '../app.dart';
import '../models/models.dart';
import '../routers/routers.dart';
import '../services/start_service.dart';
import '../update_home_widget_task.dart';
import 'config.dart';

Future<void> mainConfig() async {
  usePathUrlStrategy();
  WidgetsFlutterBinding.ensureInitialized();
  setupLog();
  String? storagePath;
  if (!kIsWeb) {
    var appDir = await getApplicationDocumentsDirectory();
    storagePath = appDir.path;
    HiveUtil.init(storagePath);
    Hive.registerAdapter<Verse>(VerseAdapter()); // #10
    if (Platform.isAndroid) {
      HomeWidgetTask.schedule(storagePath);
      await _installCertificate('lets-encrypt-r3.pem');
      await _installCertificate('bskorea-or-kr.pem');
      HttpOverrides.global = AppHttpOverrides(); // replaces lets-encrypt-r3.pem
    }
  }
  I.registerSingleton<Settings>(await Settings().init());
  I.registerSingleton(PlaybackService());
  I.registerSingleton(TtsService());
  I.registerSingleton(await AudioService.init<TtsHandler>(
    builder: () => TtsHandler(),
    config: const AudioServiceConfig(
      androidNotificationChannelId: 'me.remem.app.channel.tts',
      androidNotificationChannelName: 'Remember Me Text-to-Speech',
      androidNotificationIcon: 'drawable/ic_notification',
      androidNotificationOngoing: true,
    ),
  ));
  if (!kIsWeb) {
    final notificationService = NotificationService();
    await notificationService.init();
    I.registerSingleton(notificationService);
  }
  I.registerFactory<http.Client>(() => http.Client());
  I.registerSingleton<HomeWidgetService>(HomeWidgetService());
  Bloc.observer = SimpleBlocObserver();
  I.registerSingleton(ZoomCubit());
  await registerServices();
  runApp(App(key: UniqueKey()));
}

Future<void> registerServices() async {
  I.registerSingleton(MessageService());
  I.registerSingleton(BibleService());
  registerConnectionBlocs();
  I.registerSingleton(AuthService());
  I.registerSingleton(StartService());
  I.registerSingleton(CollectionService());
  I.registerSingleton<UndoBloc>(UndoBloc());
  registerRepositoriesHttp();
  registerRepositoriesHive();
  registerEntitiesBlocs();
  if (!kIsWeb) registerReplicators();
  registerVersesBlocs();
  I.registerSingleton<HomeFlashcardBloc>(HomeFlashcardBloc());
  I.registerSingleton(VerseService());
  I<AuthBloc>().stream.listen((auth) {
    registerDependingOnAuth(auth);
  });
}

void registerDependingOnAuth(AuthState auth) {
  if(auth is! Access) return;   // RETURN
  // Keep the current dependencies on NoAuth for cleaning up
  unregisterDependingOnAuth();
  if (auth is AccessLocal) {
    registerRepositoriesLocal();
  } else if (kIsWeb) {
    registerRepositoriesWeb();
  } else {
    registerRepositoriesMobile();
  }
}

Future<void> initApp() async {
  runApp(App(key: UniqueKey()));
}

void registerConnectionBlocs() {
  I.registerSingleton<CurrentAccount>(CurrentAccount());
  I.registerSingleton<ConnectivityCubit>(ConnectivityCubit());
  I.registerSingleton<AuthBloc>(AuthBloc());
  I.registerSingleton<ReplicationBloc>(ReplicationBloc());
  I.registerSingleton<NavigationCubit>(NavigationCubit());
  I.registerSingleton<StartBloc>(StartBloc());
  I.registerSingleton<TabBloc>(TabBloc());
  I.registerSingleton<TranscriptionCubit>(TranscriptionCubit());
}

void registerEntitiesBlocs() {
  I.registerSingleton<AccountsBloc>(AccountsBloc());
  I.registerSingleton<VersesBloc>(VersesBloc());
  I.registerSingleton<TagsBloc>(TagsBloc());
  I.registerSingleton<ScoresBloc>(ScoresBloc());
}

void registerVersesBlocs() {
  I.registerSingleton<TaggedVersesBloc>(TaggedVersesBloc());
  I.registerSingleton<FilteredVersesBloc>(FilteredVersesBloc());
  I.registerSingleton<OrderedVersesNewBloc>(OrderedVersesNewBloc());
  I.registerSingleton<OrderedVersesDueBloc>(OrderedVersesDueBloc());
  I.registerSingleton<OrderedVersesKnownBloc>(OrderedVersesKnownBloc());
  I.registerSingleton<OrderedVersesAllBloc>(OrderedVersesAllBloc());
}

void registerRepositoriesHttp() {
  I.registerSingleton<EntitiesRepositoryHttp<AccountEntity>>(
      AccountsRepositoryHttp());
  I.registerSingleton<EntitiesRepositoryHttp<VerseEntity>>(
      VersesRepositoryHttp());
  I.registerSingleton<EntitiesRepositoryHttp<TagEntity>>(TagsRepositoryHttp());
  I.registerSingleton<EntitiesRepositoryHttp<ScoreEntity>>(
      ScoresRepositoryHttp());
}

void registerRepositoriesHive() {
  if (!I.isRegistered<Queues>()) I.registerSingleton<Queues>(Queues());
  if (!I.isRegistered<EntitiesRepositoryHive<AccountEntity>>()) {
    I.registerSingleton<EntitiesRepositoryHive<AccountEntity>>(
        AccountsRepositoryHive());
  }
  if (!I.isRegistered<EntitiesRepositoryHive<VerseEntity>>()) {
    I.registerSingleton<EntitiesRepositoryHive<VerseEntity>>(
        VersesRepositoryHive());
  }
  if (!I.isRegistered<EntitiesRepositoryHive<TagEntity>>()) {
    I.registerSingleton<EntitiesRepositoryHive<TagEntity>>(
        TagsRepositoryHive());
  }
  if (!I.isRegistered<EntitiesRepositoryHive<ScoreEntity>>()) {
    I.registerSingleton<EntitiesRepositoryHive<ScoreEntity>>(
        ScoresRepositoryHive());
  }
}

void registerRepositoriesLocal() {
  I.registerSingleton<EntitiesRepository<AccountEntity>>(
      I<EntitiesRepositoryHive<AccountEntity>>());
  I.registerSingleton<EntitiesRepository<VerseEntity>>(
      I<EntitiesRepositoryHive<VerseEntity>>());
  I.registerSingleton<EntitiesRepository<TagEntity>>(
      I<EntitiesRepositoryHive<TagEntity>>());
  I.registerSingleton<EntitiesRepository<ScoreEntity>>(
      I<EntitiesRepositoryHive<ScoreEntity>>());
}

void registerRepositoriesWeb() {
  I.registerSingleton<EntitiesRepository<AccountEntity>>(
      I<EntitiesRepositoryHttp<AccountEntity>>());
  I.registerSingleton<EntitiesRepository<VerseEntity>>(
      I<EntitiesRepositoryHttp<VerseEntity>>());
  I.registerSingleton<EntitiesRepository<TagEntity>>(
      I<EntitiesRepositoryHttp<TagEntity>>());
  I.registerSingleton<EntitiesRepository<ScoreEntity>>(
      I<EntitiesRepositoryHttp<ScoreEntity>>());
}

void registerRepositoriesMobile() {
  onUpdate() => I<ReplicationBloc>().add(const RplQueued());
  I.registerSingleton<EntitiesRepository<AccountEntity>>(
      AccountsRepositoryRpl(onUpdate: onUpdate));
  I.registerSingleton<EntitiesRepository<VerseEntity>>(
      VersesRepositoryRpl(onUpdate: onUpdate));
  I.registerSingleton<EntitiesRepository<TagEntity>>(
      TagsRepositoryRpl(onUpdate: onUpdate));
  I.registerSingleton<EntitiesRepository<ScoreEntity>>(ScoresRepositoryRpl());
}

void registerReplicators() {
  ReplicationService().register(
      AccountEntity,
      Replicator<AccountEntity, AccountEntity>(
          AccountEntity, I<AccountsBloc>()));
  ReplicationService().register(VerseEntity,
      Replicator<VerseEntity, Verse>(VerseEntity, I<VersesBloc>()));
  ReplicationService().register(
      TagEntity, Replicator<TagEntity, TagEntity>(TagEntity, I<TagsBloc>()));
  ReplicationService().register(ScoreEntity,
      Replicator<ScoreEntity, ScoreEntity>(ScoreEntity, I<ScoresBloc>()));
}

void unregisterDependingOnAuth() {
  if(I.isRegistered<EntitiesRepository<AccountEntity>>()) I.unregister<EntitiesRepository<AccountEntity>>();
  if(I.isRegistered<EntitiesRepository<VerseEntity>>()) I.unregister<EntitiesRepository<VerseEntity>>();
  if(I.isRegistered<EntitiesRepository<TagEntity>>()) I.unregister<EntitiesRepository<TagEntity>>();
  if(I.isRegistered<EntitiesRepository<ScoreEntity>>()) I.unregister<EntitiesRepository<ScoreEntity>>();
}

void setupLog() {
  Logger.root.level = kReleaseMode ? Level.INFO : Level.ALL; // defaults to Level.INFO
  final DateFormat logFormatter = DateFormat('HH:mm:ss.SSS');
  Logger.root.onRecord.listen((record) {
    debugPrint(
        '${record.level.name} | ${logFormatter.format(record.time)} | ${record.message}');
  });
}

Future<void> _installCertificate(String filename) async {
  // https://stackoverflow.com/questions/69511057/flutter-on-android-7-certificate-verify-failed-with-letsencrypt-ssl-cert-after-s
  ByteData data = await PlatformAssetBundle().load('assets/ca/$filename');
  SecurityContext.defaultContext
      .setTrustedCertificatesBytes(data.buffer.asUint8List());
}
