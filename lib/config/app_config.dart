import 'flavor.dart';

class AppConfig {
  static Flavor appFlavor = Flavor.beta;

  static String get googleId {
    switch (appFlavor) {
      case Flavor.beta:
        return 'me.remem.app';
      case Flavor.prod:
        return 'org.bible.remember_me';
    }
  }
}
