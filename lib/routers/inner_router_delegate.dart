import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/navigators/navigators.dart';
import 'package:remem_me/navigators/simple_navigator.dart';
import 'package:remem_me/routers/routers.dart';

import '../paths/paths.dart';

class InnerRouterDelegate extends RouterDelegate<AppRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {

  InnerRouterDelegate({required NavigationCubit nav});

  @override
  final GlobalKey<NavigatorState> navigatorKey = innerNavigatorKey;

  @override
  Widget build(BuildContext context) {
    final path = I<NavigationCubit>().state;
    if (path is CollectionsPath) {
      return CollectionsNavigator(navigatorKey, onNotify: notifyListeners);
    }
    if (path is HomePath) {
      return HomeNavigator(navigatorKey, onNotify: notifyListeners);
    }
    if (path is SettingsPath) {
      return SettingsNavigator(navigatorKey, onNotify: notifyListeners);
    }
    if (path is TagsPath) {
      return TagsNavigator(navigatorKey, onNotify: notifyListeners);
    }
    return SimpleNavigator(navigatorKey, onNotify: notifyListeners);
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath path) async {
    // This is not required for inner router delegate because it does not
    // parse route
    assert(false);
  }
}
