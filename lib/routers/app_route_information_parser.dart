import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
import 'package:repository/repository.dart';

import '../blocs/navigation/navigation_cubit.dart';
import '../paths/paths.dart';

class AppRouteInformationParser extends RouteInformationParser<AppRoutePath> {
  static final _log = Logger('AppRouteInformationParser');

  @override
  Future<AppRoutePath> parseRouteInformation(
      RouteInformation routeInformation) async {
    final uri = _removeTrailingSlash(routeInformation.uri);
    try {
      _log.fine('parseRouteInformation $uri');

      // Ignore home widget launches
      if (uri.scheme == 'app') {
        _log.fine('Ignoring home widget launch: $uri');
        return I<NavigationCubit>().state;
      }
      final path = AppRoutePath.fromUri(uri);
      if (I<AuthBloc>().state is AuthInitialState) {
        if (path is StartBasePath || path is! StartPath) {
          I<AuthBloc>().add(AuthLoadedFromSettings());
        } else {
          I<AuthBloc>().add(Revoked());
        }
      }
      return path;
    } on InvalidPathException catch (e) {
      return InvalidPath(e.path);
    }
  }

  @override
  RouteInformation restoreRouteInformation(AppRoutePath configuration) {
    _log.fine('restoreRouteInformation $configuration');
    if(configuration is StartPath) return RouteInformation(uri: Uri.parse('/'));
    return RouteInformation(uri: Uri.parse(configuration.toString()));
  }

  Uri _removeTrailingSlash(Uri uri) {
    String path = uri.path;
    if (path.endsWith('/')) {
      path = path.substring(0, path.length - 1);
    }
    return uri.replace(path: path);
  }
}
