import 'package:flutter/material.dart';

export '../paths/app_route_path.dart';
export 'inner_router_delegate.dart';
export 'outer_router_delegate.dart';
export 'app_route_information_parser.dart';
export 'url_strategy_noop.dart' if (dart.library.html) 'url_strategy_web.dart';

final GlobalKey<NavigatorState> outerNavigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> innerNavigatorKey = GlobalKey<NavigatorState>();
