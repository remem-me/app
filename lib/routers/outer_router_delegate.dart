import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/screens/screens.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../paths/paths.dart';
import '../services/home_widget_service.dart';

class OuterRouterDelegate extends RouterDelegate<AppRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutePath> {
  static final _log = Logger('OuterRouterDelegate');

  @override
  final GlobalKey<NavigatorState> navigatorKey = outerNavigatorKey;

  final NavigationCubit _nav = I<NavigationCubit>();

  OuterRouterDelegate() {
    _nav.stream.listen((event) =>
        notifyListeners()); // do not notify if route pushed by browser back button
  }

  @override
  AppRoutePath get currentConfiguration {
    return _nav.state;
  }

  @override
  Widget build(BuildContext context) {
    final path = _nav.state;
    return _topLevelProviders(
        child: BlocBuilder<AuthBloc, AuthState>(
            builder: (BuildContext ctx, AuthState auth) =>
                _providersDependingOnAuth(auth,
                    child: Navigator(
                      key: navigatorKey,
                      pages: [
                        if (auth is Access &&
                                // using 'is' because .runtimeType does not work with inheritance
                                (path is HomePath ||
                                    path is TagsPath ||
                                    path is SimplePath) ||
                            path is CollectionsPath ||
                            path is SettingsPath)
                          MaterialPage(child: BaseScreen(_nav))
                        else if (auth is NoAuth &&
                            (path is HomePath ||
                                path is TagsPath ||
                                path is SimplePath))
                          _redirectToLogin(path)
                        else if (path is StartPath)
                          _start(ctx, auth)
                        else
                          _notFound(ctx),
                      ],
                      onPopPage: (route, result) {
                        if (!route.didPop(result)) {
                          return false;
                        }
                        notifyListeners();
                        return true;
                      },
                    ))));
  }

  Page _redirectToLogin(AppRoutePath path) {
    // Redirect to LoginPath
    WidgetsBinding.instance.addPostFrameCallback((_) {
      I<StartBloc>().add(LogInInitiated());
      _nav.go(LoginPath(redirectTo: path), propagates: true);
    });
    // temporarily show StartScreen until navigation has completed
    return const MaterialPage(child: StartScreen());
  }

  @override
  Future<void> setInitialRoutePath(AppRoutePath configuration) {
    _log.fine('Initial route $configuration. Nav.state ${_nav.state}');
    return _nav.state is StartBasePath && configuration is! StartBasePath
        ? setNewRoutePath(configuration)
        : Future.value();
  }

  @override
  Future<void> setNewRoutePath(AppRoutePath configuration) async {
    _nav.go(configuration, propagates: true);
  }

  Page _start(BuildContext ctx, AuthState auth) {
    if (auth is Access) {
      if (kIsWeb || auth is AccessLocal) {
        BlocProvider.of<AuthBloc>(ctx)
            .add(const Revoked()); // fixme: not called on web
      } else {
        BlocProvider.of<ReplicationBloc>(ctx).add(const RplLoggedOut());
      }
    }
    if (!kIsWeb && Platform.isAndroid) {
      HomeWidgetService().update([]);
    }
    return const MaterialPage(child: StartScreen());
  }

  Page _notFound(BuildContext ctx) {
    return MaterialPage(
        child: MessageScreen(
            title: L10n.of(ctx).t8('Error.notFound.title'),
            messages: {
          'messages': [L10n.of(ctx).t8('Error.notFound.message')]
        }));
  }

  /// Provide top level blocs here, others in routes
  Widget _topLevelProviders({required Widget child}) {
    return MultiBlocProvider(providers: [
      BlocProvider<AuthBloc>.value(value: I<AuthBloc>()),
      BlocProvider<ConnectivityCubit>.value(value: I<ConnectivityCubit>()),
      BlocProvider<CurrentAccount>.value(value: I<CurrentAccount>()),
      BlocProvider<NavigationCubit>.value(value: _nav),
      BlocProvider<UndoBloc>.value(value: I<UndoBloc>()),
      if (!kIsWeb)
        BlocProvider<ReplicationBloc>.value(value: I<ReplicationBloc>()),
      BlocProvider<AccountsBloc>.value(value: I<AccountsBloc>()),
      BlocProvider<VersesBloc>.value(value: I<VersesBloc>()),
      BlocProvider<TagsBloc>.value(value: I<TagsBloc>()),
      BlocProvider<ScoresBloc>.value(value: I<ScoresBloc>()),
      BlocProvider<TaggedVersesBloc>.value(value: I<TaggedVersesBloc>()),
      BlocProvider<CollectionDetailBloc>(
          create: (ctx) => CollectionDetailBloc()),
    ], child: child);
  }

  /// Provide top auth level blocs here, others in routes
  Widget _providersDependingOnAuth(AuthState auth, {required Widget child}) {
    return auth is Access
        ? MultiRepositoryProvider(providers: [
            RepositoryProvider<AccountsRepository>.value(
                value: I<EntitiesRepository<AccountEntity>>()
                    as AccountsRepository),
            RepositoryProvider<VersesRepository>.value(
                value:
                    I<EntitiesRepository<VerseEntity>>() as VersesRepository),
            RepositoryProvider<TagsRepository>.value(
                value: I<EntitiesRepository<TagEntity>>() as TagsRepository),
          ], child: child)
        : child;
  }
}
