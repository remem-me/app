import 'package:flutter/services.dart';
import 'package:logging/logging.dart';

class DeepLinkHandler {
  static const platform = MethodChannel('me.remem.web');
  static final _log = Logger('_AppState');

  static Future<Uri?> initiallyLaunchedFromDeepLink() async {
    return platform
        .invokeMethod('initiallyLaunchedFromDeepLink')
        .then(_handleReceivedData);
  }

  static Uri? _handleReceivedData(dynamic value) {
    if (value != null) {
      if (value is String) {
        try {
          return Uri.parse(value);
        } on FormatException {
          _log.warning('Received Data($value) is not parsable into an Uri');
        }
      }
      return Uri();
    } else {
      return null;
    }
  }
}
