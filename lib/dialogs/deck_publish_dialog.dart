import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

class DeckPublishDialog extends EntityForm<DeckEntity> {
  final String tagText;
  const DeckPublishDialog(
      {super.key, required super.onSave, super.onDelete, required this.tagText, DeckEntity? deck})
      : super(entity: deck);

  @override
  State<StatefulWidget> createState() {
    return _DeckPublishDialogState();
  }
}

/// Dialog for editing the name of a deck
class _DeckPublishDialogState
    extends EntityFormState<DeckEntity, DeckPublishDialog> {
  String? _name;
  String? _description;
  String? _website;
  String? _image;

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
      insetPadding: const EdgeInsets.all(16.0),
      title: Text(L10n.of(ctx).t8('Deck.publish')),
      contentPadding: EdgeInsets.all(12.0),
      content: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Stack(children: [
            Form(
                key: formKey,
                child: SingleChildScrollView(
                    child: Column(children: [
                  Row(
                    children: [
                      SizedBox(width: 12.0),
                      Icon(Icons.label_rounded),
                      SizedBox(width: 12.0),
                      Text(widget.tagText),
                    ],
                  ),
                  SizedBox(height: 16.0),
                  ...fields(ctx),
                ]))),
            if (isUpdating) const Positioned.fill(child: LoadingIndicator()),
          ])),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (widget.entity != null)
              IconButton(
                tooltip: L10n.of(ctx).t8('Deck.retract'),
                icon: const Icon(Icons.public_off_rounded),
                onPressed: deleteEntity,
              ),
            Expanded(child: SizedBox.shrink()),
            IconButton(
              tooltip: L10n.of(ctx).t8('Deck.publish'),
              icon: Icon(
                Icons.public_rounded,
                color: Theme.of(ctx).colorScheme.primary,
              ),
              onPressed: saveForm,
            ),
          ],
        ),
      ],
    );
  }

  @override
  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
          l10nKey: 'Deck.name',
          iconData: Icons.public_rounded,
          initialValue: widget.entity?.name,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.error<String?>(errors['name'], _name),
            Validators.required<String?>(ctx),
            Validators.minLength(ctx, 3),
            Validators.maxLength(ctx, 500)
          ]),
          onSaved: (value) => _name = value,
          onFieldSubmitted: (_) => saveForm(),
        ),
        SizedBox(height: spacing),
        StringInput(
          l10nKey: 'Deck.summary',
          iconData: Icons.description_outlined,
          maxLines: 12,
          initialValue: widget.entity?.description,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.error<String?>(errors['description'], _description),
            Validators.required<String?>(ctx),
            Validators.minLength(ctx, 10),
            Validators.maxLength(ctx, 5000)
          ]),
          onSaved: (value) => _description = value,
          onFieldSubmitted: (_) => saveForm(),
        ),
        SizedBox(height: spacing),
        StringInput(
          l10nKey: 'Deck.website',
          iconData: Icons.link_rounded,
          initialValue: widget.entity?.website,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.url(ctx),
            Validators.error<String?>(errors['website'], _website),
            Validators.maxLength(ctx, 500)
          ]),
          onSaved: (value) => _website = value,
          onFieldSubmitted: (_) => saveForm(),
        ),
        /*SizedBox(height: spacing),
      StringInput(
          l10nKey: 'Deck.image',
          iconData: Icons.image_rounded,
          initialValue: widget.entity?.image,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.url(ctx),
            Validators.error<String>(errors['image'], _image),
            Validators.maxLength(ctx, 1500)
          ]),
          onSaved: (value) => _image = value),*/
      ]);
  }

  @override
  DeckEntity createdEntity() {
    return DeckEntity(
        name: _name!,
        description: _description!,
        website: _website,
        image: _image);
  }

  @override
  DeckEntity updatedEntity() {
    return widget.entity!.copyWith(
        name: _name,
        description: _description,
        website: _website,
        image: _image,
        nullValues: [
          if (_website == null) 'website',
          if (_image == null) 'image',
        ]);
  }
}
