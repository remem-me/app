import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/box.dart';
import 'package:repository/repository.dart';

import '../theme.dart';

class BoxOrderDialog extends StatelessWidget {
  const BoxOrderDialog({
    super.key,
    required this.tab, this.prevOrder,
  });

  final VerseBox tab;
  final VerseOrder? prevOrder;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: MediaQuery.of(context).platformBrightness == Brightness.light
          ? AppTheme.light
          : AppTheme.dark,
      child: SimpleDialog(
        title: Text(L10n.of(context).t8('VerseOrder.title')),
        children: <Widget>[
          _option(context, VerseOrder.ALPHABET, 'VerseOrder.alphabet'),
          _option(context, VerseOrder.CANON, 'VerseOrder.canon'),
          _option(context, VerseOrder.TOPIC, 'VerseOrder.topic'),
          _option(context, VerseOrder.DATE, 'VerseOrder.date'),
          _option(context, VerseOrder.LEVEL, 'VerseOrder.level'),
          _option(context, VerseOrder.RANDOM, 'VerseOrder.random'),
        ],
      ),
    );
  }

  Widget _option(BuildContext ctx, VerseOrder value, String l10nKey) {
    return RadioListTile<VerseOrder>(
          title: Text(L10n.of(ctx).t8(l10nKey)),
          value: value,
          groupValue: prevOrder,
          onChanged: (VerseOrder? value) {
            Navigator.pop(ctx, value);
          },
        );
  }
}
