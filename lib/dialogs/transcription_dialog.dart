import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/audio/transcription.dart';

class TranscriptionDialog extends StatefulWidget {
  final Transcription transcription;

  const TranscriptionDialog(this.transcription, {super.key});

  @override
  State<TranscriptionDialog> createState() => _TranscriptionDialogState();
}

class _TranscriptionDialogState extends State<TranscriptionDialog> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 8,
      alignment: Alignment.bottomCenter,
      insetPadding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 16.0),
      child: Stack(
        children: [
          Scrollbar(
            controller: _scrollController,
            child: SingleChildScrollView(
                controller: _scrollController,
                child: ConstrainedBox(
                  constraints: const BoxConstraints(minHeight: 48),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 16.0, 24.0, 16.0),
                    child: Text(widget.transcription.text.trim(),
                        style: TextStyle(
                            fontSize: 15,
                            fontFamily: widget.transcription.mode ==
                                    TranscriptionMode.script
                                ? I<CurrentAccount>().font
                                : 'Sans')),
                  ),
                )),
          ),
          Positioned(
              top: 0.0,
              right: 0.0,
              child: IconButton(
                  padding: const EdgeInsets.all(4.0),
                  constraints: const BoxConstraints(),
                  onPressed: () =>
                      I<TranscriptionCubit>().clear(widget.transcription.mode),
                  icon: const Icon(Icons.close_rounded)))
        ],
      ),
    );
  }
}
