import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';

class ConfirmDialog {
  final String titleL10nKey;
  final String confirmL10nKey;
  final String? summaryL10nKey;
  final void Function() onConfirm;

  const ConfirmDialog({
    required this.titleL10nKey,
    required this.confirmL10nKey,
    this.summaryL10nKey,
    required this.onConfirm,
  });

  Future<void> show(BuildContext ctx) {
    return showDialog(
        context: ctx,
        builder: (ctx) => AlertDialog(
                title: Text(L10n.of(ctx).t8(titleL10nKey)),
                content: SingleChildScrollView(
                    child: ListBody(
                  children: <Widget>[
                    Text(L10n.of(ctx)
                        .t8(summaryL10nKey ?? 'Dialog.confirm.summary')),
                  ],
                )),
                actionsAlignment: MainAxisAlignment.spaceBetween,
                actions: <Widget>[
                  TextButton(
                      style: TextButton.styleFrom(
                          foregroundColor:
                              Theme.of(ctx).textTheme.bodySmall!.color),
                      onPressed: () {
                        Navigator.of(ctx).pop();
                      },
                      child:
                          Text(L10n.of(ctx).t8('Button.cancel').toUpperCase())),
                  TextButton(
                      onPressed: () {
                        onConfirm();
                        Navigator.of(ctx).pop();
                      },
                      child:
                          Text(L10n.of(ctx).t8(confirmL10nKey).toUpperCase())),
                ]));
  }
}
