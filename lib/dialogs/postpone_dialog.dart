import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../services/verse_service.dart';
import '../widgets/widgets.dart';

class PostponeDialog extends BaseForm {
  final Map<int, Verse>? selection;

  const PostponeDialog({
    super.key,
    this.selection,
  });

  @override
  State<StatefulWidget> createState() => _PostponeDialogState();
}

class _PostponeDialogState extends BaseFormState<PostponeDialog> {
  int? _days;

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
        content: SizedBox(
            width: double.minPositive,
            child: Stack(children: [
              Form(
                key: formKey,
                child: Column(mainAxisSize: MainAxisSize.min, children: [
                  ListView(shrinkWrap: true, children: fields(ctx))
                  // don't use ListView for fields scrolling off screen
                ]),
              ),
              if (isUpdating) const Positioned.fill(child: LoadingIndicator()),
            ])),
        actions: <Widget>[
          TextButton(
              style: TextButton.styleFrom(padding: const EdgeInsets.all(16.0)),
              onPressed: _saveAndPop,
              child:
                  Text(L10n.of(ctx).t8('Verses.postpone').toUpperCase())),
        ]);
  }

  Future<void> _saveAndPop() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      final verses = _updatedVerses();
      try {
        setState(() {
          isUpdating = true;
        });
        await RepositoryProvider.of<VersesRepository>(context)
            .updateList(verses);
        Navigator.of(context).pop(verses);
      } on MessageException catch (e) {
        setState(() {
          errors = e.messages;
        });
      } finally {
        setState(() {
          isUpdating = false;
        });
      }
    }
  }

  @override
  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        IntegerInput(
          l10nKey: 'Verses.postpone.title',
          l10nKeySummary: 'Verses.postpone.summary',
          iconData: Icons.edit_calendar,
          signed: true,
          initialValue: 7,
          onSaved: (value) => _days = value,
          onFieldSubmitted: (_) => _saveAndPop(),
        ),
      ]);
  }

  List<Verse> _updatedVerses() {
    return widget.selection!.values.map((verse) {
      return VerseService().postponed(verse, _days!);
    }).toList();
  }
}
