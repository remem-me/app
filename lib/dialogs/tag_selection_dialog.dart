import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

import 'tag_edit_dialog.dart';

class TagSelectionDialog extends StatefulWidget {
  final List<TagEntity>? tags;
  final Map<int, Verse>? selection;

  const TagSelectionDialog({
    super.key,
    this.tags,
    this.selection,
  });

  @override
  State<StatefulWidget> createState() => _TagSelectionDialogState();
}

class _TagSelectionDialogState extends State<TagSelectionDialog> {
  Map<TagEntity, bool?>? values;
  Map<String, List<String>> errors = {};
  bool isUpdating = false;

  _TagSelectionDialogState();

  @override
  void initState() {
    super.initState();
    values = Map.fromEntries(widget.tags!
        .map((tag) => MapEntry(tag, _checkBoxValue(tag, widget.selection!))));
  }

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
        contentPadding: const EdgeInsets.only(top: 16),
        content: Stack(children: [
          SingleChildScrollView(
              child: ListBody(
                  children: _errorMessages(ctx) +
                      (_tiles(ctx, widget.tags!, widget.selection)
                          as List<Widget>))),
          if (isUpdating) const Positioned.fill(child: LoadingIndicator()),
        ]),
        actions: <Widget>[
          IconButton(
              tooltip: L10n.of(ctx).t8('Tag.add'),
              icon: const Icon(Icons.new_label_outlined),
              onPressed: () async {
                await showDialog<TagEntity>(
                    context: ctx,
                    builder: (BuildContext context) =>
                        TagEditDialog(onSave: (context, tag) async {
                          await RepositoryProvider.of<TagsRepository>(ctx)
                              .create(tag);
                          BlocProvider.of<TagsBloc>(ctx).add(
                              EntitiesAdded<TagEntity>([tag], persist: false));
                          setState(() {
                            widget.tags!.add(tag);
                            values![tag] = true;
                          });
                        }));
              }),
          IconButton(
              tooltip: L10n.of(ctx).t8('Button.save'),
              icon: Icon(
                Icons.save_rounded,
                color: Theme.of(ctx).colorScheme.primary,
              ),
              onPressed: () async {
                if (values != null) {
                  await _saveAndPop(ctx);
                } else {
                  Navigator.of(ctx).pop();
                }
              }),
        ]);
  }

  Future<void> _saveAndPop(BuildContext ctx) async {
    final verses = _updatedVerses();
    try {
      setState(() {
        isUpdating = true;
      });
      await RepositoryProvider.of<VersesRepository>(ctx).updateList(verses);
      Navigator.of(ctx).pop(verses);
    } on MessageException catch (e) {
      setState(() {
        errors = e.messages;
      });
    } finally {
      setState(() {
        isUpdating = false;
      });
    }
  }

  List<Verse> _updatedVerses() {
    return widget.selection!.values.map((verse) {
      final tags = Map.of(verse.tags);
      values!.forEach((tag, value) {
        if (value == true && !tags.containsKey(tag.id)) {
          tags[tag.id] = tag.text;
        } else if (value == false && tags.containsKey(tag.id)) {
          tags.remove(tag.id);
        }
      });
      return verse.copyWith(tags: tags);
    }).toList();
  }

  List<Widget> _errorMessages(BuildContext ctx) {
    return errors['messages'] != null
        ? [
            ListTile(
                title: Text(errors['messages']!.join('\n'),
                    style: Theme.of(ctx)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: Theme.of(ctx).colorScheme.error))),
          ]
        : [];
  }

  Iterable<Widget> _tiles(
      BuildContext ctx, List<TagEntity> tags, Map<int, Verse>? selection) {
    return tags
        .map((tag) => CheckboxListTile(
              title: Text(tag.text),
              tristate: true,
              value: values![tag],
              onChanged: (bool? value) => setState(() => values![tag] = value),
            ))
        .toList();
  }

  bool? _checkBoxValue(TagEntity tag, Map<int, Verse> selection) {
    if (selection.values.every((verse) => verse.tags.containsKey(tag.id))) {
      return true;
    }
    if (!selection.values.any((verse) => verse.tags.containsKey(tag.id))) {
      return false;
    }
    return null;
  }
}
