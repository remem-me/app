import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTheme {
  static ThemeData light = ThemeData.from(
          useMaterial3: true,
          colorScheme: AppColorScheme.light,
          textTheme: textLight)
      .copyWith(
    pageTransitionsTheme: pageTransitions,
    appBarTheme: appBar,
    iconTheme: IconThemeData(color: AppColorScheme.light.outline),
    listTileTheme: listTileLight,
    drawerTheme: drawer,
    floatingActionButtonTheme: fab,
    dialogTheme: dialogLight,
    cardTheme: CardTheme(color: AppColorScheme.light.surfaceContainerLowest),
    inputDecorationTheme: inputDecorationLight,
    scrollbarTheme: scrollBar,
  );

  static ThemeData dark = ThemeData.from(
          useMaterial3: true,
          colorScheme: AppColorScheme.dark,
          textTheme: textDark)
      .copyWith(
    pageTransitionsTheme: pageTransitions,
    appBarTheme: appBar,
    iconTheme: IconThemeData(color: AppColorScheme.dark.outline),
    listTileTheme: listTileDark,
    drawerTheme: drawer,
    floatingActionButtonTheme: fab,
    dialogTheme: dialogDark,
    cardTheme: CardTheme(color: AppColorScheme.dark.surfaceContainerLowest),
    inputDecorationTheme: inputDecorationDark,
    scrollbarTheme: scrollBar,
  );

  static ThemeData twilight = AppTheme.dark.copyWith(
      iconTheme: appBar.iconTheme,
      tabBarTheme: tabBar,
      textSelectionTheme: TextSelectionThemeData(
          cursorColor: AppTheme.dark.colorScheme.secondary),
      inputDecorationTheme: inputDecorationTwilight);

  static AppBarTheme appBar = const AppBarTheme(
      elevation: 4.0,
      surfaceTintColor: Colors.transparent,
      shadowColor: Colors.black,
      foregroundColor: Colors.white,
      backgroundColor: AppColor.dark,
      iconTheme: IconThemeData(color: Colors.white),
      systemOverlayStyle: SystemUiOverlayStyle.light,
      titleTextStyle: TextStyle(fontSize: 18.0, color: Colors.white),
      titleSpacing: 0.0,
      centerTitle: false);

  static const tabBar = TabBarTheme(
      dividerHeight: 0.0,
      indicatorColor: Flat.amber,
      unselectedLabelColor: Colors.white70);

  static final iconButton = IconButtonThemeData(
      style: ButtonStyle(iconColor: WidgetStateProperty.all(Colors.yellow)));

  static final listTileLight = ListTileThemeData(
    iconColor: AppColorScheme.light.outline,
    tileColor: Colors.transparent,
    //AppColorScheme.light.surfaceContainerLowest,
    selectedTileColor: AppColorScheme.light.surfaceContainerHigh,
  );

  static final listTileDark = ListTileThemeData(
    iconColor: AppColorScheme.dark.outline,
    tileColor: Colors.transparent,
    selectedTileColor: AppColorScheme.dark.surfaceContainerHigh,
  );

  static final FloatingActionButtonThemeData fab =
      FloatingActionButtonThemeData(
          foregroundColor: Colors.white,
          backgroundColor: AppColorScheme.dark.outlineVariant,
          shape: const CircleBorder());

  static const PageTransitionsTheme pageTransitions =
      PageTransitionsTheme(builders: {
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.android: ZoomPageTransitionsBuilder()
  });

  static const drawer = DrawerThemeData(shape: Border(), endShape: Border());

  static final scrollBar = ScrollbarThemeData(
    thumbVisibility: WidgetStateProperty.all<bool>(true),
    radius: const Radius.circular(8),
  );

  static final dialogLight = DialogTheme(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      shadowColor: AppColorScheme.light.outline,
      backgroundColor: AppColorScheme.light.surfaceContainerLow);

  static final dialogDark = DialogTheme(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      shadowColor: AppColorScheme.dark.surface,
      backgroundColor: AppColorScheme.dark.surfaceContainerLow);

  static final inputDecorationLight = InputDecorationTheme(
    isDense: true,
    filled: true,
    fillColor: AppColorScheme.light.surfaceContainer,
    // default: ColorScheme.surfaceContainerHighest
    prefixIconColor: AppColorScheme.light.outline,
    suffixIconColor: AppColorScheme.light.outline,
    border: InputBorder.none,
  );

  static final inputDecorationDark = InputDecorationTheme(
    isDense: true,
    filled: true,
    // fillColor default: ColorScheme.surfaceContainerHighest
    prefixIconColor: AppColorScheme.dark.outline,
    suffixIconColor: AppColorScheme.dark.outline,
    border: InputBorder.none,
  );

  static const inputDecorationTwilight = InputDecorationTheme(
    hintStyle: TextStyle(color: Colors.white70),
    fillColor: Colors.transparent,
    border: InputBorder.none,
  );

  static final textLight =
      Typography.blackMountainView.apply(fontFamily: 'Sans');

  static final textDark =
      Typography.whiteMountainView.apply(fontFamily: 'Sans');
}
