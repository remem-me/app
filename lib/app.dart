import 'dart:async';
import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:hive/hive.dart';
import 'package:home_widget/home_widget.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/routers/deep_link_handler.dart';
import 'package:remem_me/services/rating_service.dart';
import 'package:remem_me/widgets/zoom/zoom.dart';
import 'package:repository/repository.dart';

import 'paths/paths.dart';
import 'routers/routers.dart';
import 'theme.dart';

class App extends StatefulWidget {
  const App({super.key});

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  static final _log = Logger('_AppState');

  StreamSubscription? _homeWidgetSubscription;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    if (!kIsWeb) {
      RatingService().countDownToRating();
      HomeWidget.setAppGroupId('me.remem.app');
    }
  }

  @override
  Widget build(BuildContext ctx) {
    return ZoomDetector(
      child: ValueListenableBuilder<Box>(
          valueListenable:
              I<Settings>().listenable(keys: [Settings.THEME_MODE]),
          builder: (context, bx, wdg) {
            return MaterialApp.router(
              debugShowCheckedModeBanner: false,
              themeMode: I<Settings>()
                      .getEnum(Settings.THEME_MODE, ThemeMode.values) ??
                  ThemeMode.system,
              theme: AppTheme.light,
              darkTheme: AppTheme.dark,
              supportedLocales: TextUtil.appLocales,
              localizationsDelegates: const [
                L10n.delegate,
                LocaleNamesLocalizationsDelegate(),
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              onGenerateTitle: (BuildContext ctx) =>
                  L10n.of(ctx).t8('App.name'),
              scaffoldMessengerKey: scaffoldMessengerKey,
              routerDelegate: OuterRouterDelegate(),
              routeInformationParser: AppRouteInformationParser(),
            );
          }),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    _log.fine('App.didChangeAppLifecycleState: $state');
    if (state == AppLifecycleState.resumed) {
      DateService().updateToday();
      if (!kIsWeb) RatingService().countDownToRating();
    }
  }

  @override
  void dispose() {
    _homeWidgetSubscription?.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!kIsWeb && Platform.isAndroid) {
      _checkForWidgetLaunch();
      _checkForDeepLinkLaunch();
      HomeWidget.widgetClicked.listen((uri) {
        _log.fine('_AppState.widgetClicked $uri');
        _launchedFromWidget(uri);
      });
    }
  }

  void _checkForWidgetLaunch() {
    HomeWidget.initiallyLaunchedFromHomeWidget().then((uri) {
      _log.fine('_AppState.initiallyLaunchedFromHomeWidget $uri');
      _launchedFromWidget(uri);
    });
  }

  void _launchedFromWidget(Uri? uri) {
    if (uri != null && uri.scheme == 'app') {
      _log.fine('_launchedFromWidget $uri');
      if (HomePath.matches(uri)) {
        I<NavigationCubit>().go(HomePath.fromUri(uri), propagates: true);
      }
    }
  }

  void _checkForDeepLinkLaunch() {
    DeepLinkHandler.initiallyLaunchedFromDeepLink().then((uri) {
      _log.fine('_AppState.initiallyLaunchedFromHomeWidget $uri');
      _launchedFromDeepLink(uri);
    });
  }

  void _launchedFromDeepLink(Uri? uri) {
    if (uri != null && uri.authority == 'web.remem.me') {
      _log.fine('_launchedFromDeepLink $uri');
      I<NavigationCubit>().go(AppRoutePath.fromUri(uri), propagates: true);
    }
  }
}
