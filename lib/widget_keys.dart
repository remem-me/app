import 'package:flutter/widgets.dart';

class RememMeKeys {
  static const extraActionsPopupMenuButton =
      Key('__extraActionsPopupMenuButton__');
  static const extraActionsEmptyContainer =
      Key('__extraActionsEmptyContainer__');
  static const filteredVersesEmptyContainer =
      Key('__filteredVersesEmptyContainer__');
  static const scoresLoadInProgressIndicator =
      Key('__scoresLoadingIndicator__');
  static const emptyScoresContainer = Key('__emptyScoresContainer__');
  static const emptyDetailsContainer = Key('__emptyDetailsContainer__');
  static const detailsScreenCheckBox = Key('__detailsScreenCheckBox__');
}
