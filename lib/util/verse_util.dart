import 'dart:math';
import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:repository/repository.dart';

import '../models/book.dart';
import '../models/verse.dart';

class FromEntitiesArgs {
  final Iterable<VerseEntity> entities;
  final Map<String, Book> books;
  final int reviewLimit;
  final double reviewFrequency;
  final int deviceId;

  FromEntitiesArgs(this.entities, this.books, this.reviewLimit,
      this.reviewFrequency, this.deviceId);
}

abstract class VerseUtil {
  static const String _delimiter = r'[:.,。：、．]';

  static List<Verse> fromEntities(FromEntitiesArgs args) {
    return args.entities
        .map<Verse>((VerseEntity entity) => Verse.fromEntity(
              entity,
              due: due(
                  commit: entity.commit,
                  review: entity.review,
                  level: entity.level,
                  reviewLimit: args.reviewLimit,
                  reviewFrequency: args.reviewFrequency),
              color: color(entity.level),
              rank: rank(entity.reference, args.books),
              modifiedBy: args.deviceId,
            ))
        .toList();
  }

  static List<Verse> mapVersesToBoxAll(List<Verse> verses, VerseOrder order) {
    // move to service
    final result = verses.where((verse) {
      return true;
    }).toList();
    result.sort(VerseUtil.compare(order));
    return result;
  }

  static List<Verse> mapVersesToBoxNew(List<Verse> verses, VerseOrder order) {
    final result = verses.where((verse) {
      return verse.due == null;
    }).toList();
    result.sort(VerseUtil.compare(order));
    return result;
  }

  static List<Verse> mapVersesToBoxDue(List<Verse> verses, VerseOrder order) {
    final today = DateService().today;
    final result = verses.where((verse) {
      return verse.due != null && today >= verse.due!;
    }).toList();
    result.sort(VerseUtil.compare(order));
    return result;
  }

  static List<Verse> mapVersesToBoxKnown(List<Verse> verses, VerseOrder order) {
    final today = DateService().today;
    final result = verses.where((verse) {
      return verse.due != null && verse.due! > today;
    }).toList();
    result.sort(VerseUtil.compare(order));
    return result;
  }

  static Color color(int level) {
    // todo: get default color from account settings
    return level < 0
        ? AppColor.base
        : FlatDark.wheel[level % Flat.wheel.length];
  }

  static LocalDate? due({
    LocalDate? commit,
    LocalDate? review,
    int? level,
    required int reviewLimit,
    required double reviewFrequency,
  }) {
    if (review == null || level == null || level < 0) {
      return commit; // RETURN
    }
    final offset = dueOffset(level, reviewFrequency: reviewFrequency);
    return review +
        (reviewLimit == 0 || reviewLimit >= offset ? offset : reviewLimit);
  }

  static int dueOffset(int level, {required double reviewFrequency}) {
    return level < 1 ? 0 : min(pow(reviewFrequency, level - 1).truncate(), 999);
  }

  static int rank(String reference, Map<String, Book> books) {
    return book(reference, books).index * 1000000 +
        chapter(reference) * 1000 +
        first(reference);
  }

  static Book book(String reference, Map<String, Book> books) {
    final book = books.entries
        .firstWhereOrNull(
            (entry) => reference.toLowerCase().startsWith(_bookPattern(entry)))
        ?.value;
    return book ?? Book(null, 0, null, null);
  }

  static RegExp _bookPattern(MapEntry<String, Book> entry) {
    return RegExp(r'(' +
        entry.key.toLowerCase() +
        r'\.?|' +
        entry.value.name!.toLowerCase() +
        r')\s*(?=\d+\s*(' +
        _delimiter +
        r'|$))');
  }

  static int chapter(String reference) {
    final pattern = RegExp(r'(?<=\D+)\d+');
    // does not work in Safari (https://stackoverflow.com/questions/51568821/works-in-chrome-but-breaks-in-safari-invalid-regular-expression-invalid-group)
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  static int first(String reference) {
    final pattern = RegExp(r'(?<=\d+\s*' + _delimiter + r'\s*)\d+');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  static int Function(Verse, Verse) compare(VerseOrder order) {
    // move to service
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => _or(a, b, order, VerseOrder.TOPIC, VerseOrder.DATE);
      case VerseOrder.CANON:
        return (a, b) => _or(a, b, order, VerseOrder.ALPHABET, VerseOrder.DATE);
      case VerseOrder.TOPIC:
        return (a, b) =>
            _or(a, b, order, VerseOrder.CANON, VerseOrder.ALPHABET);
      case VerseOrder.DATE:
        return (a, b) => _or(a, b, order, VerseOrder.LEVEL, VerseOrder.CANON,
            VerseOrder.ALPHABET);
      case VerseOrder.LEVEL:
        return (a, b) => _or(a, b, order, VerseOrder.DATE, VerseOrder.CANON,
            VerseOrder.ALPHABET);
      default:
        return _compare(order);
    }
  }

  static int Function(Verse, Verse) _compare(VerseOrder order) {
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => a.reference.compareTo(b.reference);
      case VerseOrder.CANON:
        return (a, b) => (a.rank ?? 0).compareTo(b.rank ?? 0);
      case VerseOrder.TOPIC:
        return (a, b) => _compareInitialNumbers(a.topic ?? '', b.topic ?? '');
      case VerseOrder.DATE:
        final zero = LocalDate(2001, 1, 1);
        return (a, b) => a.due == null && b.due == null
            ? -a.id.compareTo(b.id)
            : (a.due ?? zero).compareTo(b.due ?? zero);
      case VerseOrder.LEVEL:
        return (a, b) => a.level.compareTo(b.level);
      case VerseOrder.RANDOM:
        return (a, b) => Random().nextInt(2) * 2 - 1;
      case VerseOrder.ID:
        return (a, b) => -a.id.compareTo(b.id);
    }
  }

  static int _or(Verse a, Verse b, VerseOrder order1, VerseOrder order2,
      [VerseOrder? order3, VerseOrder? order4]) {
    final result1 = _compare(order1)(a, b);
    if (result1 != 0) return result1;
    final result2 = _compare(order2)(a, b);
    if (result2 != 0 || order3 == null) return result2;
    final result3 = _compare(order3)(a, b);
    if (result3 != 0 || order4 == null) return result3;
    return _compare(order4)(a, b);
  }

  /// Compares two strings. If both strings start with a number,
  /// and both numbers are different, the numbers are compared.
  /// Otherwise, the strings are compared alphabetically.
  static int _compareInitialNumbers(String a, String b) {
    final pattern = RegExp(r'^\d+');
    final matchA = pattern.firstMatch(a);
    final matchB = pattern.firstMatch(b);

    if (matchA != null && matchB != null) {
      final numberA = int.parse(matchA.group(0)!);
      final numberB = int.parse(matchB.group(0)!);
      if (numberA != numberB) {
        return numberA.compareTo(numberB);  // RETURN
      }
      // fall through and compare alphabetically
    }
    return a.compareTo(b);
  }
}
