import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> Function() launchDocumentation(BuildContext ctx,
    {String doc = 'docs/', String path = ''}) {
  return () async {
    final server = Settings().getOrDefault(Settings.DOC_SERVER);
    final base = L10n.of(ctx).t8('Documentation.path');
    final uri = Uri.parse('$server/$base$doc$path');
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri, mode: LaunchMode.externalApplication);
    }
  };
}

ScreenSize screenSize(BuildContext ctx) {
  final screenWidth = MediaQuery.of(ctx).size.width;
  if (screenWidth <= 960) return ScreenSize.small;
  if (screenWidth <= 1280) return ScreenSize.medium;
  return ScreenSize.large;
}

String localeNameOf(BuildContext ctx, String locale) {
  return LocaleNames.of(ctx)?.nameOf(locale) ??
      L10n.of(ctx).t8('Locale.$locale');
}

Map<String, String> languageMap(BuildContext ctx,
    {bool includingCurrentLocales = false}) {
  final locales = BIBLE_DEFAULTS.keys.toSet();
  if (includingCurrentLocales) {
    _addSystemLocale(ctx, locales);
    locales.add(I<CurrentAccount>().language.toString());
  } else {
    locales.removeWhere((locale) => locale.startsWith('zh'));
    locales.add('zh');
  }
  final result = _mapLocales(ctx, locales);
  result.addEntries([MapEntry('ot', L10n.of(ctx).t8('Locale.ot'))]);
  return result;
}

Map<String, String> langRefMap(BuildContext ctx,
    {bool includingCurrentLocales = false}) {
  final locales = REFERENCE_DEFAULTS.keys.toSet();
  if (includingCurrentLocales) {
    _addSystemLocale(ctx, locales);
    locales.add(I<CurrentAccount>().langRef.toString());
  }
  return _mapLocales(ctx, locales);
}

void _addSystemLocale(BuildContext ctx, Set<String> locales) {
  final Locale systemLocale = View.of(ctx).platformDispatcher.locale;
  if (locales.contains(systemLocale.languageCode)) {
    // specific locale of available language
    locales.add(systemLocale.toString());
  }
}

Map<String, String> _mapLocales(BuildContext ctx, Set<String> locales) {
  return Map.fromEntries(locales
      .map((locale) => MapEntry(locale, localeNameOf(ctx, locale)))
      .sortedBy((a) => a.value));
}
