import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:repository/repository.dart';
import 'package:workmanager/workmanager.dart';

import 'config/config.dart';
import 'models/verse.dart';


/// Used for Background Updates using Workmanager Plugin
@pragma('vm:entry-point')
void callbackDispatcher() {
  Workmanager().executeTask(HomeWidgetTask.update);
}


abstract class HomeWidgetTask {
  static final _log = Logger('HomeWidgetTask');

  static void schedule(String? storagePath) {
    Workmanager().initialize(
      callbackDispatcher,
      isInDebugMode: kDebugMode,
    );
    final initialDelay = DateService().durationUntilDayChanged();
    final DateFormat formatter = DateFormat('yyyyMMdd.HHmm');
    final taskName = 'me.remem.app.update.${formatter.format(DateTime.now())}';
    Workmanager().registerOneOffTask(
      taskName,
      taskName,
      inputData: {
        'storagePath': storagePath,
      },
      initialDelay: initialDelay,
    );
    _log.fine('HomeWidgetTask.schedule() in $initialDelay');
  }

  static Future<bool> update(
      String taskName, Map<String, dynamic>? inputData) async {
    _log.fine('HomeWidgetTask.update()');
    _log.fine('taskName: $taskName');
    _log.fine('inputData: ${jsonEncode(inputData)}');
    String storagePath = inputData?['storagePath'];
    setupLog();
    HiveUtil.init(storagePath);
    Hive.registerAdapter<Verse>(VerseAdapter()); // #10
    I.registerSingleton<Settings>(await Settings().init());
    schedule(storagePath);
    return HomeWidgetService().updateFromStorage();
  }
}
