import 'package:flutter/material.dart';
import 'package:remem_me/models/book.dart';
import 'package:repository/repository.dart';

class Account extends AccountEntity {
  final Map<String, Book>? books;

  Account(
    super.name, {
    super.language,
    super.langRef,
    super.reviewFrequency,
    super.reviewLimit,
    super.dailyGoal,
    super.reviewBatch,
    super.inverseLimit,
    super.referenceIncluded,
    super.topicPreferred,
    super.defaultSource,
    super.canon,
    super.fontType,
    super.orderNew,
    super.orderDue,
    super.orderKnown,
    super.orderAll,
    super.importedDecks,
    super.id,
    super.modified,
    super.modifiedBy,
    super.deleted,
    this.books, // transient
  });

  @override
  Account copyWith({
    String? name,
    Locale? language,
    Locale? langRef,
    double? reviewFrequency,
    int? reviewLimit,
    int? dailyGoal,
    int? reviewBatch,
    int? inverseLimit,
    bool? referenceIncluded,
    bool? topicPreferred,
    String? defaultSource,
    Canon? canon,
    FontType? fontType,
    VerseOrder? orderNew,
    VerseOrder? orderDue,
    VerseOrder? orderKnown,
    VerseOrder? orderAll,
    List<int>? importedDecks,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    Map<String, Book>? books,
    List<String> nullValues = const [],
  }) {
    return Account(
      name ?? this.name,
      language: language ?? this.language,
      langRef: langRef ?? this.langRef,
      reviewFrequency: reviewFrequency ?? this.reviewFrequency,
      reviewLimit: reviewLimit ?? this.reviewLimit,
      dailyGoal: dailyGoal ?? this.dailyGoal,
      reviewBatch: reviewBatch ?? this.reviewBatch,
      inverseLimit: inverseLimit ?? this.inverseLimit,
      referenceIncluded: referenceIncluded ?? this.referenceIncluded,
      topicPreferred: topicPreferred ?? this.topicPreferred,
      defaultSource: defaultSource ??
          (nullValues.contains('defaultSource') ? null : this.defaultSource),
      canon: canon ?? this.canon,
      fontType: fontType ?? this.fontType,
      orderNew: orderNew ?? this.orderNew,
      orderDue: orderDue ?? this.orderDue,
      orderKnown: orderKnown ?? this.orderKnown,
      orderAll: orderAll ?? this.orderAll,
      importedDecks: importedDecks ?? this.importedDecks,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      modifiedBy: modifiedBy ?? this.modifiedBy,
      deleted: deleted ?? this.deleted,
      books: books ?? this.books,
    );
  }

  Account.fromEntity(AccountEntity entity, {this.books = const {}})
      : super(entity.name,
            language: entity.language,
            langRef: entity.langRef,
            reviewFrequency: entity.reviewFrequency,
            reviewLimit: entity.reviewLimit,
            dailyGoal: entity.dailyGoal,
            reviewBatch: entity.reviewBatch,
            inverseLimit: entity.inverseLimit,
            referenceIncluded: entity.referenceIncluded,
            topicPreferred: entity.topicPreferred,
            defaultSource: entity.defaultSource,
            canon: entity.canon,
            fontType: entity.fontType,
            orderNew: entity.orderNew,
            orderDue: entity.orderDue,
            orderKnown: entity.orderKnown,
            orderAll: entity.orderAll,
            importedDecks: entity.importedDecks,
            id: entity.id,
            modified: entity.modified,
            modifiedBy: entity.modifiedBy,
            deleted: entity.deleted);
}
