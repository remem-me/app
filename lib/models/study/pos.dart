import 'package:equatable/equatable.dart';

class Pos extends Equatable implements Comparable<Pos> {
  final int line;
  final int word;

  const Pos(this.line, this.word);

  @override
  List<Object?> get props => [line, word];

  @override
  String toString() => 'Pos { line: $line, word: $word }';

  @override
  int compareTo(Pos? other) {
    return other == null
        ? 1
        : line != other.line
            ? line.compareTo(other.line)
            : word.compareTo(other.word);
  }

  bool operator >(Pos? other) => compareTo(other) > 0;
  bool operator >=(Pos? other) => compareTo(other) >= 0;
  bool operator <(Pos? other) => compareTo(other) < 0;
  bool operator <=(Pos? other) => compareTo(other) <= 0;
}
