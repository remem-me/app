import 'package:equatable/equatable.dart';

class Choice extends Equatable {
  final List<String> options;
  final int solution;

  const Choice(this.options, this.solution);

  @override
  List<Object?> get props => [options, solution];

  @override
  String toString() => 'Choice { options: $options, solution: $solution }';
}