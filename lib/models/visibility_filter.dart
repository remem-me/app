import 'package:app_core/app_core.dart';
import 'package:equatable/equatable.dart';

import 'models.dart';

class QueryFilter extends Equatable {
  final String query;

  const QueryFilter([this.query = '']);

  @override
  String toString() => 'QueryFilter { query: $query }';

  @override
  bool appliesTo(Verse verse) {
    return query.isEmpty ||
        verse.reference.toLowerCase().contains(query.toLowerCase()) ||
        verse.passage.toLowerCase().contains(query.toLowerCase()) ||
        verse.topic != null &&
            verse.topic!.toLowerCase().contains(query.toLowerCase());
  }

  @override
  List<Object?> get props => [query];
}

class ReviewedTodayFilter extends Equatable {
  final bool? reviewedToday;

  const ReviewedTodayFilter([this.reviewedToday]);

  @override
  String toString() => 'ReviewedTodayFilter { reviewedToday: $reviewedToday }';

  bool isIncluded(Verse verse) {
    return reviewedToday == true && verse.review == DateService().today;
  }

  bool isExcluded(Verse verse) {
    return reviewedToday == false && verse.review == DateService().today;
  }

  @override
  List<Object?> get props => [reviewedToday];
}
