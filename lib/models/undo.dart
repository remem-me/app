import 'package:equatable/equatable.dart';

enum EntityAction { create, update, delete }

class Undo extends Equatable {
  final EntityAction action;
  final List entities;

  Undo(this.action, this.entities) {
    assert(entities.isNotEmpty);
  }

  @override
  List<Object?> get props => [action, entities];

  @override
  String toString() => 'Undone { undo: $action, '
      'entities: ${entities.runtimeType}(${entities.length}) }';
}
