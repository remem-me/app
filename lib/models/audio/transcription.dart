import 'package:equatable/equatable.dart';

enum TranscriptionMode {
  caption, script
}

class Transcription extends Equatable {
  final String text;
  final TranscriptionMode mode;

  const Transcription(this.text, {required this.mode});

  @override
  List<Object?> get props => [text, mode];
}
