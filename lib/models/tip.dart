import 'package:flutter/material.dart';

class Tip {
  final String settingKey;
  final Widget icon;
  final String l10nKey;

  const Tip({required this.settingKey, required this.icon, required this.l10nKey});
}