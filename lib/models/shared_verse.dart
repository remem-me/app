import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/services/verse_service.dart';

class SharedVerse {
  static final _log = Logger('SharedVerse');

  String? reference;
  String? source;
  String? passage;

  SharedVerse(String data) {
    try {
      final separator = !kIsWeb && Platform.isIOS ? '\n' : '\n\n';
      final i = data.lastIndexOf(separator);
      String? text;
      var url = data;
      if (i > -1) {
        text = data.substring(0, i);
        url = data.substring(i + separator.length);
      }
      if (url.contains('//bible.com/')) {
        _parseYouVersion(text, url);
      } else if (url.contains('//olivetree.com/')) {
        _parseOliveTree(text, url);
      } else if (data.contains('//bibleis.app.link/')) {
        _parseBibleIs(data);
      } else if (url.contains('https://ref.ly/')) {
        _parseLogos(text, url);
      } else {
        passage = data;
      }
    } catch (e) {
      _log.warning(e.toString());
      passage = data;
    }
    _log.fine(toString());
  }

  // Ein Lied von David. Der Herr ist mein Hirte, nichts wird mir fehlen. Er weidet mich auf saftigen Wiesen und führt mich zu frischen Quellen. Er gibt mir neue Kraft. Er leitet mich auf sicheren Wegen und macht seinem Namen damit alle Ehre. Auch wenn es durch dunkle Täler geht, fürchte ich kein Unglück, denn du, Herr, bist bei mir. Dein Hirtenstab gibt mir Schutz und Trost. Du lädst mich ein und deckst mir den Tisch vor den Augen meiner Feinde. Du begrüßt mich wie ein Hausherr seinen Gast und füllst meinen Becher bis zum Rand. Deine Güte und Liebe begleiten mich Tag für Tag; in deinem Haus darf ich bleiben mein Leben lang.
  // Psalm 23:1‭-‬6 HFA
  //
  // https://bible.com/bible/73/psa.23.1-6.HFA

  // „Little by little I will drive them out before you, until you have increased enough to take possession of the land.“
  // ‭‭Exodus‬ ‭23‬:‭30‬ ‭NIV‬‬
  // https://bible.com/bible/111/exo.23.30.NIV
  void _parseYouVersion(String? text, String url) {
    _log.fine('$runtimeType._parseYouVersion');
    if (text != null) {
      String contentData;
      String metaData;
      var segments = text.split(RegExp(r'\s*\n\s*'));
      if (segments[0].startsWith(RegExp(r'([“„\[])'))) {
        contentData = segments[0];
        metaData = segments[1];
      } else {
        contentData = segments[1];
        metaData = segments[0];
      }
      metaData = metaData.replaceAll(RegExp(r'[\u202D,\u202C]'), '');
      passage = contentData.replaceAll(RegExp(r'(^[“„]|[”“]$)', unicode: true), '');
      final i = metaData.lastIndexOf(' ');
      reference = metaData.substring(0, i);
      source = metaData.substring(i + 1).toUpperCase();
    } else {
      final i = url.lastIndexOf('.');
      reference = RegExp(r'(?<=https://bible\.com/bible/\d+/)\S+')
          .firstMatch(url.substring(0, i))
          ?.group(0);
      if (reference != null) {
        reference = VerseService().formattedReference(reference!);
      }
      source = url.substring(i + 1).toUpperCase();
    }
  }

  // Psalmen 23:1-2 (ESV) " The LORD Is My Shepherd
  // A PSALM OF DAVID.
  // ​1 The LORD is my shepherd; I shall not want.
  // 2 He makes me lie down in green pastures.
  // He leads me beside still waters.
  // "
  //
  // Sent from Bible Study (http://olivetree.com/b3/Ps.23.1-2.ESV)
  void _parseOliveTree(String? text, String url) {
    final i = url.lastIndexOf('.');
    source = url.substring(i + 1);
    if(!kIsWeb && Platform.isIOS) source = source?.toUpperCase();
    if(text != null) {
      var segments = text.split('"');
      passage = segments[1].trim();
      if (segments[0].contains('(')) {
        source = RegExp(r'(?<=\().*(?=\))').firstMatch(segments[0])?.group(0)?.trim();
        reference =
            RegExp(r'.*(?=\()').firstMatch(segments[0])?.group(0)?.trim();
      } else {
        reference = segments[0].trim();
      }
    } else {
      reference = RegExp(r'(?<=//olivetree\.com/b2/)\S+')
          .firstMatch(url.substring(0, i))
          ?.group(0);
      if (reference != null) {
        reference = VerseService().formattedReference(reference!);
      }
    }
  }

  // Yesu Krisita Kibaru Duman min sɛbɛnna MATIYU fɛ 5:3-4
  //
  // «Minnu y'a dɔn ko u ye faantanw ye
  //   Ala ta fan fɛ, olu ye dubadenw ye,
  //   katuguni sankolo masaya ye u ta ye. «Minnu dusu kasilen don, olu ye dubadenw ye,
  //   katuguni u dusuw na saalo.
  //
  // https://bibleis.app.link/ojCwvBheusb - Freier Zugang zur Bibel in Video, Audio und Text in 1652 Sprachen:
  void _parseBibleIs(String data) {
    final segments = data.split('\n\n');
    passage = segments[1];
    reference = segments[0];
  }

  // “All things came into being through him, and apart from him not one thing came into being that has come into being. In him was life, and the life was the light of humanity.”
  //
  // https://ref.ly/Jn1.3-4 via the Faithlife Study Bible Android app.

  // "This one was in the beginning with God.",https://ref.ly/Jn1.2;leb
  void _parseLogos(String? text, String url) {
    _log.fine('$runtimeType._parseLogos');
    passage = text != null
        ? text.replaceAll(RegExp(r'(^["“]|[”"]$)', unicode: true), '')
        : '';
    final i = !kIsWeb && Platform.isIOS ? url.lastIndexOf(';') : url.indexOf(' ');
    reference = RegExp(r'(?<=https://ref\.ly/)\S+')
        .firstMatch(url.substring(0, i))
        ?.group(0);
    if (reference != null) {
      reference = VerseService().formattedReference(reference!);
    }
    source = url.substring(i + 1);
    if(!kIsWeb && Platform.isIOS) source = source?.toUpperCase();
  }

  @override
  String toString() {
    return 'SharedVerse { reference: $reference, source: $source, passage: $passage }';
  }
}
