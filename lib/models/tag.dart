import 'package:repository/repository.dart';

class Tag extends TagEntity {
  final int? size;

  Tag(
    text, {
    included,
    required super.published,
    super.accountId,
    super.id,
    super.modified,
    super.modifiedBy,
    super.deleted,
    this.size, // transient
  }) : super(text,
            included: included);

  Tag.fromEntity(TagEntity entity, {this.size})
      : super(entity.text,
            included: entity.included,
            accountId: entity.accountId,
            published: entity.published,
            id: entity.id,
            modified: entity.modified,
            modifiedBy: entity.modifiedBy,
            deleted: entity.deleted);

  @override
  Tag copyWith({
    String? text,
    bool? included,
    int? accountId,
    bool? published,
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
    int? size,
    List<String> nullValues = const [],
  }) {
    return Tag(
      text ?? this.text,
      included:
          included ?? (nullValues.contains('included') ? null : this.included),
      accountId: accountId ?? this.accountId,
      published: published ?? this.published,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      modifiedBy: modifiedBy ?? this.modifiedBy,
      deleted: deleted ?? this.deleted,
      size: size ?? (nullValues.contains('size') ? null : this.size),
    );
  }
}
