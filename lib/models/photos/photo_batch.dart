import 'package:remem_me/models/photos/photos.dart';

class PhotoBatch {
  final List<Photo> photos;
  final int? total;

  const PhotoBatch(this.photos, {this.total});

  PhotoBatch.fromJson(Map<String, dynamic> json)
      : photos = (json['results'] as List<dynamic>)
            .map((item) => Photo.fromJson(item))
            .toList(),
        total = json['total'] as int;
}
