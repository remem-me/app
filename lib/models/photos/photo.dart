class Photo {
  final String color;
  final String small;
  final String regular;
  final String download;
  final String author;
  final String profile;

  Photo(this.color, this.small, this.regular, this.download, this.author, this.profile);

  static Photo fromJson(Map<String, dynamic> json) {
    return Photo(
      '${json['color']}',
      '${json['urls']['small']}&utm_source=remember_me&utm_medium=referral',
      '${json['urls']['regular']}&utm_source=remember_me&utm_medium=referral',
      '${json['links']['download_location']}&utm_source=remember_me&utm_medium=referral',
      '${json['user']['name']}',
      '${json['user']['links']['html']}?utm_source=remember_me&utm_medium=referral',
    );
  }
}