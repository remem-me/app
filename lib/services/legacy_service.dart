import 'dart:convert';
import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:repository/repository.dart';


class LegacyService {
  static LegacyService? _instance;

  factory LegacyService() {
    _instance ??= LegacyService._internal();
    return _instance!;
  }

  LegacyService._internal();

  Future<AccountEntity> attachAccount({
    required String? name,
    required String? password,
  }) async {
    final email = Settings().get(Settings.USER_EMAIL);
    final credentials =
        jsonEncode({'email': email, 'name': name, 'password': password});
    try {
      final response = await http.post(await legacyUri('/attach/'),
          body: credentials, headers: _headers());
      if ([200].contains(response.statusCode)) {
        return AccountEntity.fromJson(json.decode(utf8.decode(response.body.codeUnits)));
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.title')]
        };
        if (response.statusCode < 500 && response.statusCode != 404) {
          messages = _parseMessages(response);
        }
        throw MessageException(messages);
      }
    } on SocketException {
      throw MessageException({
        'messages': [L10n.current.t8('Connection.error.title')]
      });
    } on http.ClientException {
      throw MessageException({
        'messages': [L10n.current.t8('Connection.error.title')]
      });
    }
  }

  Map<String, String> _headers() {
    final headers = <String, String>{
      HttpHeaders.acceptLanguageHeader: L10n.current.locale.toLanguageTag(),
      HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
    };
    return headers;
  }

  Map<String, List<String>> _parseMessages(Response response) {
    final obj = json.decode(utf8.decode(response.body.codeUnits));
    final result = <String, List<String>>{};
    for (var key in obj.keys) {
      Iterable list = obj[key];
      result[key] = list.map((msg) => msg.toString()).toList();
    }
    return result;
  }
}
