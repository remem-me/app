import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class Replicator<E extends Entity, T extends E> {
  static final _log = Logger('Replicator');
  final Type entityType;
  final EntitiesBloc<E, T> bloc;

  Replicator(this.entityType, this.bloc);

  EntitiesRepositoryHive<E> get hiveRepo =>
      (bloc.repository as EntitiesRepositoryRpl<E>).hiveRepo;

  EntitiesRepositoryHttp<E> get httpRepo =>
      (bloc.repository as EntitiesRepositoryRpl<E>).httpRepo;

  Future<void> sendQueued(List<AccountEntity> accounts) async {
    final entities = <E>[];
    if (entityType == AccountEntity) {
      final queue = I<Queues>().get(entityType);
      for (var id in queue) {
        entities.add((await hiveRepo.read(id))!);
      }
    } else {
      for (var account in accounts) {
        final queue = I<Queues>().get(entityType, accountId: account.id);
        for (var id in queue) {
          entities.add((await hiveRepo.read(id, accountId: account.id))!);
        }
      }
    }
    await _sendAndClear(entities, accounts);
  }

  Future<void> sendAll(List<AccountEntity> accounts) async {
    final entities = <E>[];
    if (entityType == AccountEntity) {
      try {
        entities.addAll(await hiveRepo.readList());
      } on NotFoundException catch (e) {
        debugPrint(e.toString());
      }
    } else {
      for (var account in accounts) {
        try {
          entities.addAll(await hiveRepo.readList(accountId: account.id));
        } on NotFoundException catch (e) {
          debugPrint(e.toString());
        }
      }
    }
    await _sendAndClear(entities, accounts);
  }

  Future<void> _sendAndClear(
      List<E> entities, List<AccountEntity> accounts) async {
    if (entities.isNotEmpty) {
      await httpRepo.updateList(entities, isAlreadyStamped: true);
      if (entityType == AccountEntity) {
        await I<Queues>().delete(entityType);
      } else
        for (var account in accounts) {
          await I<Queues>().delete(entityType, accountId: account.id);
        }
      for (var entity in entities) {
        if (entity.deleted) await hiveRepo.delete(entity);
      }
    }
  }

  Future<void> clearLocalStorage(List<AccountEntity> accounts) async {
    if (entityType == AccountEntity) {
      hiveRepo.clear();
    }
    for (var account in accounts) {
      hiveRepo.clear(account.id);
    }
  }

  /// Receives items since timestamp if entities are loaded locally (EntitiesLoadSuccess)
  /// Receives all items if loading local entities has failed (EntitiesLoadFailure)
  /// Receives items from all accounts, but ui blocs are updated with visible ones only
  /// (depending on current account)
  Future<void> receiveWhenReady(int? since, int? client) async {
    var state = await bloc.stream.startWith(bloc.state).firstWhere((state) =>
        state is InitialEntitiesState<E> ||
        state is EntitiesLoadSuccess<T> ||
        state is EntitiesLoadFailure<E>);
    return _receive(since, client, state);
  }

  Future<void> _receive(int? since, int? client, EntitiesState<E> state) async {
    final received =
        await httpRepo.readList(deleted: null, since: since, client: client);
    final added = <E>[];
    final updated = <T>[];
    for (var remoteItem in received) {
      final localItem = await hiveRepo.read(remoteItem.id,
          accountId: remoteItem is Accountable ? remoteItem.accountId : null);
      if (localItem == null) {
        if (!remoteItem.deleted) {
          hiveRepo.create(remoteItem);
          if (_inAppState(remoteItem)) {
            added.add(remoteItem);
          }
        }
      } else if (localItem.modified < remoteItem.modified) {
        if (remoteItem.deleted) {
          hiveRepo.delete(localItem);
        } else {
          hiveRepo.update(remoteItem);
        }
        if (_inAppState(remoteItem)) {
          if (state is EntitiesLoadSuccess<T>) {
            updated.addAll(await bloc.toModels([remoteItem]));
          } else {
            added.add(remoteItem);
          }
        }
      }
    }
    if (updated.isNotEmpty) {
      bloc.add(EntitiesUpdated<T>(updated, persist: false));
    }
    if (added.isNotEmpty) {
      bloc.add(EntitiesAdded<E>(added, persist: false));
    }
  }

  bool _inAppState(E item) {
    final currentAccount = I<CurrentAccount>();
    return item is! Accountable || item.accountId == currentAccount.id;
  }
}
