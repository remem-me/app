import 'package:app_core/app_core.dart';
import 'package:diacritic/diacritic.dart';
import 'package:flutter/cupertino.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tiny_segmenter.dart';

// RegExp:
// (?<!y)x    negative lookbehind
// x(?!y)     negative lookahead
// \p{P}      any kind of punctuation character (unicode: true)
// \p{Letter} unicode letter (without numbers, unicode: true)

// Chinese:
// Block                                   Range       Comment
// --------------------------------------- ----------- ----------------------------------------------------
// CJK Unified Ideographs                  4E00-9FFF   Common
// CJK Unified Ideographs Extension A      3400-4DBF   Rare
// CJK Unified Ideographs Extension B      20000-2A6DF Rare, historic
// CJK Unified Ideographs Extension C      2A700–2B73F Rare, historic
// CJK Unified Ideographs Extension D      2B740–2B81F Uncommon, some in current use
// CJK Unified Ideographs Extension E      2B820–2CEAF Rare, historic
// CJK Compatibility Ideographs            F900-FAFF   Duplicates, unifiable variants, corporate characters
// CJK Compatibility Ideographs Supplement 2F800-2FA1F Unifiable variants
// CJK Symbols and Punctuation             3000-303F

class TextService {
  static final TextService _instance = TextService._internal();
  final ja = TinySegmenter();

  //static final delimiter = RegExp(r'(?<!^\p{P})\s+(?!\p{P}+(\s+|$))', unicode: true);
  @visibleForTesting
  static final wordDelimiterEn =
      RegExp(r'(?<=[\p{Letter}\d].*\s+)(?=\S*[\p{Letter}\d])', unicode: true);
  @visibleForTesting
  static final wordDelimiterZh = RegExp(
      r'(?<=[\u3400-\u4DBF\u4E00-\u9FFF\d].*\s*)(?=[\u3000-\u303F]*[\u3400-\u4DBF\u4E00-\u9FFF\d])',
      unicode: true);
  @visibleForTesting
  static final lineBreak =
      RegExp(r'(\r\n|\n|\x0b|\f|\r|\x85|\u2028|\u2029)', unicode: true);
  static final lineDelimiter = RegExp(r'(?<=\n+)(?=.*\S)', unicode: true);
  @visibleForTesting
  static final referenceDelimiterEn = RegExp(
      r'((?<=\d[\p{P}\s]+)(?=[\p{Letter}\d])|(?<=[\p{Letter}\d].*\s+)(?=\S*[\p{Letter}\d]))',
      unicode: true); // todo: separate book from chapter in zh
  @visibleForTesting
  static final referenceDelimiterZh = RegExp(
      r'((?<=\d[\u3000-\u303F\s]+)(?=[\u3400-\u4DBF\u4E00-\u9FFF\d])|(?<=[\u3400-\u4DBF\u4E00-\u9FFF\d].*\s*)(?=[\u3000-\u303F]*[\u3000-\u303F]*[\u3400-\u4DBF\u4E00-\u9FFF\d]))',
      unicode: true); // todo: separate book from chapter in zh

  // todo: make language dependent, e.g. inject localized TextService using Provider
  final language = 'en';

  factory TextService() {
    return _instance;
  }

  TextService._internal();

  /// Splits the given text into lines based on the line delimiter.
  ///
  /// This method uses the `lineDelimiter` regular expression to split the input
  /// text into a list of lines.
  List<String> lines(String text) {
    return text.replaceAll(lineBreak, '\n').split(lineDelimiter);
  }

  /// Splits the given text into words based on the word delimiter.
  ///
  /// \return A list of lists, where each inner list contains the words of a line.
  List<List<String>> words(String text) {
    return lines(text).map((line) => splitText(line)).toList();
  }

  @visibleForTesting
  List<String> splitText(String text) {
    return languageCode == 'ja' ? (_splitJa(text)) : text.split(wordDelimiter);
  }

  /// Splits the given reference into parts based on the reference delimiter.
  ///
  /// This method uses the `referenceDelimiter` regular expression to split the input
  /// reference into a list of parts.
  ///
  /// \return A list of strings representing the parts of the reference.
  List<String> splitReference(String reference) {
    return languageCode == 'ja'
        ? _splitJa(reference)
        : reference.trim().split(referenceDelimiter);
  }

  List<String> _splitJa(String text) {
    return ja.tokenize(text).fold(<String>[], (tokens, item) {
      if (tokens.isNotEmpty &&
          (RegExp(r'^[\p{P}\s]+$', unicode: true).hasMatch(item) ||
              RegExp(r'^[0-9]+$').hasMatch(tokens.last) &&
                  RegExp(r'^[0-9]+$').hasMatch(item))) {
        tokens.last += item;
      } else if (!RegExp(r'^\s+$').hasMatch(item)) {
        tokens.add(item);
      }
      return tokens;
    });
  }

  /// Converts a list of lines and a starting position into a text string.
  String wordsToText(List<List<String>> lines, Pos start, bool tillEndOfLine) {
    if (tillEndOfLine) {
      return lines[start.line].sublist(start.word).join();
    } else {
      return lines[start.line][start.word];
    }
  }

  /// Strips punctuation from the given word.
  ///
  /// This method removes leading and trailing punctuation characters
  /// from the input word and returns the cleaned word. If the result
  /// is empty, it returns a string with three spaces.
  String stripPunctuation(String word) {
    final result = word
        .toLowerCase()
        .replaceAll(RegExp(r'^[\p{P}\s]+', unicode: true), '')
        .replaceAll(RegExp(r'[\p{P}\s]+$', unicode: true), '');
    return result.isEmpty ? '   ' : result;
  }

  /// Strips line breaks from the given word.
  ///
  /// This method removes all newline (`\n`) and carriage return (`\r`) characters
  /// from the input word and returns the cleaned word.
  String stripLinebreaks(String word) {
    return word.replaceAll(RegExp('[\n\r]+'), '');
  }

  int punctuationBefore(String word) {
    return RegExp(r'^\p{P}+([^\S\n\r]+|(?=[\p{Letter}\d]))', unicode: true)
            .firstMatch(word)
            ?.group(0)
            ?.length ??
        0;
  }

  int punctuationAfter(String word) {
    return RegExp(r'((?<=(\s|^))(\p{P}+\s*)|(?<=[\p{Letter}\d])[\p{P}\s]+)$',
                unicode: true)
            .firstMatch(word)
            ?.group(0)
            ?.length ??
        0;
  }

  Pos lastPos(List<List<String>> lines) {
    return Pos(lines.length - 1, lines[lines.length - 1].length - 1);
  }

  List<List<bool>> wordsToMarks(List<List<String>> lines) {
    final List<List<bool>> marks = [];
    for (var i = 0; i < lines.length; i++) {
      final line = <bool>[];
      for (var j = 0; j < lines[i].length; j++) {
        line.add(false);
      }
      marks.add(line);
    }
    return marks;
  }

  List<List<T>> copyItems<T>(List<List<T>> rows) {
    return rows.map((row) => [...row]).toList();
  }

  int countWordsOfText(String? text) {
    return countWordsOfRows(words(text!));
  }

  int countWordsOfRows(List<List<String>> rows) {
    return rows.fold(0, (count, row) => count + row.length);
  }

  int countMarks(List<List<bool>> rows) {
    return rows.fold(
        0,
        (count, line) =>
            count + line.fold(0, ((count, mark) => mark ? count + 1 : count)));
  }

  bool startsWith(String? word, String letters) {
    return letters.isNotEmpty &&
        removeDiacritics(stripPunctuation(word!).toLowerCase())
            .startsWith(removeDiacritics(letters.toLowerCase()));
  }

  /// Returns the beginning of the given text.
  String beginning(String text) {
    final firstWords = splitText(lines(text)[0]);
    switch (firstWords.length) {
      case > 5:
        return '${firstWords.getRange(0, 3).join()}…';
      case > 3:
        return '${firstWords.getRange(0, 2).join()}…';
      case > 1:
        return '${firstWords.first}…';
      case 1:
        return '${firstWords.first.substring(0, 1)}…';
      default:
        return '';
    }
  }

  /// Marks the word at the specified index in the `marked` list.
  ///
  /// This method iterates through the `marked` list and sets the value to `true`
  /// at the specified index, indicating that the word at that position is marked.
  ///
  /// \param index The index of the word to mark.
  /// \param marked A list of lists of booleans representing the marked words.
  void mark(int index, List<List<bool>> marked) {
    var current = 0;
    for (var i = 0; i < marked.length; i++) {
      for (var j = 0; j < marked[i].length; j++) {
        if (marked[i][j]) continue; // not counting 'true'
        if (current == index) {
          marked[i][j] = true;
          return;
        }
        current += 1;
      }
    }
  }

  RegExp get wordDelimiter {
    return languageCode == 'zh' ? wordDelimiterZh : wordDelimiterEn;
  }

  RegExp get referenceDelimiter {
    return languageCode == 'zh' ? referenceDelimiterZh : referenceDelimiterEn;
  }

  @visibleForTesting
  String get languageCode {
    return I<CurrentAccount>().language.languageCode;
  }

  /// Generates a list of new words from the difference between the previous and next text.
  List<String> newText(String prev, String next) {
    return splitText(next.substring(differingPos(prev, next)))
        .map((word) => stripPunctuation(word))
        .toList();
  }

  int differingPos(String a, String b) {
    int i = 0;
    while (a.length > i && b.length > i && a[i] == b[i]) {
      i++;
    }
    return i;
  }

  /// Removes Hebrew vowel points (niqqud) from the given string.
  ///
  /// This method uses a regular expression to match and remove
  /// Hebrew vowel points, which are Unicode characters in the range
  /// U+0591 to U+05C7.
  ///
  /// Example:
  /// ```dart
  /// removeVowels("שָׁלוֹם"); // Returns: "שלום"
  /// ```
  String removeVowels(String s) {
    return s.replaceAll(RegExp(r'[\u0591-\u05C7]'), '');
  }

  /// Wraps the differences between the original and extended strings in square brackets.
  ///
  /// The additional parts (without surrounding spaces) in the extended string are wrapped in square brackets.
  ///
  /// Example en:
  /// original = 'abc 11 def 12 ghi',
  /// extended = '1 abc 11 def 12 12 ghi 11'
  /// Result: '[1] abc 11 def [12] 12 ghi [11]'
  ///
  /// Example zh:
  /// original = '这是一个测试。这是另一行。',
  /// extended = '15这是一个测试。6 这是另一行。'
  /// Result: '[15]这是一个测试。[6] 这是另一行。'
  String wrapDifferences(String original, String extended) {
    final originalChars = original.split('');
    final extendedChars = extended.split('');

    final wrappedChars = <String>[];
    var i = 0;
    var j = 0;
    var inBracket = false;
    var bracketContent = <String>[];
    var leadingSpace = '';
    var trailingSpace = '';

    while (j < extendedChars.length) {
      if (i < originalChars.length && originalChars[i] == extendedChars[j]) {
        if (inBracket) {
          wrappedChars.add(leadingSpace);
          wrappedChars.add('[${bracketContent.join('').trim()}]');
          wrappedChars.add(trailingSpace);
          bracketContent = [];
          inBracket = false;
          leadingSpace = '';
          trailingSpace = '';
        }
        wrappedChars.add(extendedChars[j]);
        i++;
        j++;
      } else {
        if (!inBracket) {
          inBracket = true;
          if (extendedChars[j].trim().isEmpty) {
            leadingSpace = extendedChars[j];
          } else {
            bracketContent.add(extendedChars[j]);
          }
        } else if (bracketContent.isNotEmpty &&
            extendedChars[j].trim().isEmpty) {
          trailingSpace = extendedChars[j];
        } else {
          bracketContent.add(extendedChars[j]);
        }
        j++;
      }
    }

    if (inBracket) {
      wrappedChars.add(leadingSpace);
      wrappedChars.add('[${bracketContent.join('').trim()}]');
      wrappedChars.add(trailingSpace);
    }

    return wrappedChars.join('').replaceAll('[]', ''); // remove empty brackets
  }

  /// Converts verse numbers in the given text to Markdown format.
  ///
  /// This method replaces spaces after verse numbers with `&nbsp;`
  /// and adds parentheses around verse numbers.
  ///
  /// Example:
  /// ```dart
  /// verseNumbersToMarkdown("[1] In the beginning [2] was the Word");
  /// // Returns: "[1]()&nbsp;In the beginning [2]()&nbsp;was the Word"
  /// ```
  String verseNumbersToMarkdown(String text) {
    return text.replaceAllMapped(
      RegExp(r'(?<=\[\S+\]) ?'),
      (Match match) {
        if (match.group(0) == ' ') {
          return '()&nbsp;';
        } else {
          return '()';
        }
      },
    );
  }

  /// Converts line breaks in the given text to Markdown format.
  String lineBreaksToMarkdown(String text) {
    // Use RegExp to match groups of consecutive '\n'
    // replacements:
    // '\n' -> '  \n'
    // '\n\n' -> '&nbsp;\n\n'
    // '\n\n\n' -> '  \n&nbsp;\n\n'
    // '\n\n\n\n' -> '&nbsp;\n\n&nbsp;\n\n'
    // '\n\n\n\n\n' -> '  \n&nbsp;\n\n&nbsp;\n\n'
    return text.replaceAllMapped(
      RegExp(r'(\n)+'), // Match one or more consecutive '\n'
      (match) {
        int count = match.group(0)!.length; // Count the number of '\n'
        StringBuffer replacement = StringBuffer();
        for (int i = 1; i <= count; i++) {
          if (i == 1 && count % 2 == 1) {
            replacement.write('  \n');
          } else if ((count - i) % 2 == 1) {
            replacement.write('&nbsp;\n');
          } else {
            replacement.write('\n');
          }
        }
        return replacement.toString();
      },
    );
  }

  /// Strips verse numbers from the given text.
  ///
  /// This method removes any verse numbers enclosed in square brackets
  /// and any optional space following them.
  ///
  /// Example:
  /// ```dart
  /// stripVerseNumbers("[1] In the beginning [2] was the Word");
  /// // Returns: "In the beginning was the Word"
  /// ```
  String stripVerseNumbers(String text) {
    return text.replaceAll(RegExp(r'\[\S+\]\s?'), '');
  }

  /// Converts verse numbers in the given text to a spoken format.
  ///
  /// This method replaces any space following verse numbers enclosed in square brackets
  /// with an ellipsis character.
  ///
  /// Example:
  /// ```dart
  /// spokenVerseNumbers("[1] In the beginning [2] was the Word");
  /// // Returns: "[1]… In the beginning [2]… was the Word"
  /// ```
  String spokenVerseNumbers(String text) {
    return text.replaceAll(RegExp(r'(?<=\[\S+\])\s?'), ' — ');
  }

  /// Strips Markdown formatting from the given text.
  ///
  /// This method removes bold and italic markers, square brackets around numbers,
  /// and heading markers from the beginning of lines.
  ///
  /// Example:
  /// ```dart
  /// stripMarkdown("**bold** _italic_ [1] # Heading");
  /// // Returns: "bold italic 1 Heading"
  /// ```
  String stripMarkdown(String text) {
    // Remove bold and italic markers
    text = text.replaceAllMapped(
        RegExp(r'\*{1,2}(.*?)\*{1,2}|_{1,2}(.*?)_{1,2}'),
        (Match m) => m.group(1) ?? m.group(2) ?? '');
    // Remove square brackets around numbers
    text = text.replaceAllMapped(
        RegExp(r'\[(\S+)\]'), (Match m) => m.group(1) ?? '');
    // Remove heading markers from beginning of line
    text = text.replaceAllMapped(
        RegExp(r'^(#+\s+|>)', multiLine: true), (Match m) => '');
    return text;
  }
}
