import 'dart:async';
import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

const _fetchLimit = 20;

class CollectionService {
  static CollectionService? _instance;
  late http.Client httpClient;

  factory CollectionService() {
    _instance ??= CollectionService._internal();
    return _instance!;
  }

  CollectionService._internal() {
    httpClient = I<http.Client>();
  }

  Future<List<Collection>> fetchCollections(Query query,
      {offset = 0, Account? account}) async {
    final params = {
      'search': query.search,
      'language': query.language,
      'ordering': _ordering(query.order),
      'limit': _fetchLimit.toString(),
      'offset': offset.toString()
    };
    if (query.importedOnly && account != null) {
      params['imported_by'] = account.id.toString();
    }
    final uri = await replicationUri('/v1/collections/', params);
    final response = await httpClient.get(uri);
    if (response.statusCode == 200) {
      final body = json.decode(utf8.decode(response.body.codeUnits));
      return Batch.fromJson(body).results;
    }
    throw FetchException(
        response.statusCode, 'Error fetching collections');
  }

  Future<List<Collection>> fetchImported(Account account) async {
    final params = {
      'imported_by': account.id.toString(),
      'ordering': 'name',
    };
    final uri = await replicationUri('/v1/collections/', params);
    final response = await httpClient.get(uri);
    if (response.statusCode == 200) {
      final body = json.decode(utf8.decode(response.body.codeUnits));
      return (body as List).map((dynamic json) {
        return Collection.fromJson(json);
      }).toList();
    }
    throw FetchException(
        response.statusCode, 'Error fetching imported collections');
  }

  Future<Collection> fetchCollection(int id) async {
    final uri = await replicationUri('/v1/collections/${id.toString()}/');
    final response = await httpClient.get(uri);
    if (response.statusCode == 200) {
      final body = json.decode(utf8.decode(response.body.codeUnits));
      return Collection.fromJson(body);
    }
    throw FetchException(
        response.statusCode, 'Error fetching collection with id: $id');
  }

  bool hasReachedMax(int collectionsCount) => collectionsCount < _fetchLimit;

  _ordering(CollectionOrder order) {
    switch (order) {
      case CollectionOrder.featured:
        return '-${describeEnum(CollectionOrder.featured)},-${describeEnum(CollectionOrder.downloads)}';
      case CollectionOrder.downloads:
      case CollectionOrder.created:
        return '-${describeEnum(order)}';
      default:
        return describeEnum(order);
    }
  }
}

class Batch {
  final int? count;
  final String? next;
  final String? previous;
  final List<Collection> results;

  Batch(this.count, this.next, this.previous, this.results);

  static Batch fromJson(Map<String, dynamic> json) {
    return Batch(
        json['count'] as int?,
        json['next'] as String?,
        json['previous'] as String?,
        (json['results'] as List).map((dynamic json) {
          return Collection.fromJson(json);
        }).toList());
  }
}

class FetchException implements Exception {
  final int statusCode;
  final String message;

  FetchException(this.statusCode, this.message);

  @override
  String toString() => 'CollectionFetchException: $message (Status code: $statusCode)';
}
