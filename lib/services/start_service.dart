import 'dart:async';
import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:http/http.dart' as http;
import 'package:repository/repository.dart';

import '../blocs/start/start_bloc.dart';

class StartService {
  static StartService? _instance;

  factory StartService() {
    _instance ??= StartService._internal();
    return _instance!;
  }

  StartService._internal();

  /// Sends email always as lowercase
  Future<StartState> register({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final client = I<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/register/'),
          body: credentials,
          headers: AuthService.headers());
      if ([201].contains(response.statusCode)) {
        return StartInfo(
          {'email': email, 'password': password},
          dialogId: 'register-sent',
          title: L10n.current.t8('User.register.title'),
          message: AuthService.parseMessages(response)['messages']!.join('/n'),
        );
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500) {
          messages = AuthService.parseMessages(response);
        }
        return SignUpFeedback({'email': email, 'password': password},
            messages: messages);
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return SignUpFeedback({'email': email, 'password': password},
          messages: messages);
    } finally {
      client.close();
    }
  }

  /// Sends email always as lowercase
  Future<StartState> login({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final client = I<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/token/'),
          body: credentials,
          headers: AuthService.headers());
      if ([200].contains(response.statusCode)) {
        final accessToken = jsonDecode(response.body)['access'];
        final refreshToken = jsonDecode(response.body)['refresh'];
        return LogInSuccess(Granted(email,
            AccessRemote(accessToken: accessToken, refreshToken: refreshToken)));
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500) {
          messages = AuthService.parseMessages(response);
        }
        return LogInFeedback({'email': email, 'password': password},
            messages: messages);
      }
    } on Exception catch (e) {
      var messages = {
        'messages': [
          '${L10n.current.t8('Connection.error.title')} (${e.runtimeType}: $e)'
        ]
      };
      return LogInFeedback({'email': email, 'password': password},
          messages: messages);
    } finally {
      client.close();
    }
  }

  /// Sends email always as lowercase
  Future<StartState> forgotPassword({
    required String email,
  }) async {
    final credentials = jsonEncode({'email': email.toLowerCase()});
    final client = I<http.Client>();
    try {
      final response = await client.post(
          await replicationUri('/v1/access/forgot-password/'),
          body: credentials,
          headers: AuthService.headers());
      if ([200].contains(response.statusCode)) {
        return StartInfo(
          {'email': email},
          dialogId: 'forgot-password-sent',
          title: L10n.current.t8('User.resetPassword.title'),
          message: AuthService.parseMessages(response)['messages']!.join('/n'),
        );
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500) {
          messages = AuthService.parseMessages(response);
        }
        return ForgotPasswordFeedback({'email': email}, messages: messages);
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return ForgotPasswordFeedback({'email': email}, messages: messages);
    } finally {
      client.close();
    }
  }

  /// Submits the password and returns a StartState
  Future<StartState> submitPassword({
    required String password,
    required String uidb64,
    required String token,
  }) async {
    final body =
        jsonEncode({'password': password, 'uidb64': uidb64, 'token': token});
    try {
      final response = await http.post(
          await replicationUri('/v1/access/reset-password/'),
          body: body,
          headers: AuthService.headers());
      if ([200].contains(response.statusCode)) {
        return StartInfo(
          {'password': password},
          dialogId: 'reset-password-success',
          title: L10n.current.t8('User.resetPassword.title'),
          message: AuthService.parseMessages(response)['messages']!.join('/n'),
        );
      } else {
        var messages = {
          'messages': [L10n.current.t8('Server.error.summary')]
        };
        if (response.statusCode < 500) {
          messages = AuthService.parseMessages(response);
        }
        return ResetPasswordFeedback({'password': password},
            messages: messages, uidb64: uidb64, token: token);
      }
    } on Exception {
      var messages = {
        'messages': [L10n.current.t8('Connection.error.title')]
      };
      return ResetPasswordFeedback({'password': password},
          messages: messages, uidb64: uidb64, token: token);
    }
  }

  /// Sends email always as lowercase
  Future<Map<String, dynamic>> changeEmail({
    required String email,
    required String password,
  }) async {
    final credentials =
        jsonEncode({'email': email.toLowerCase(), 'password': password});
    final uri = await replicationUri('/v1/access/change-email/');
    final response = await I<AuthService>()
        .call((headers) => http.put(uri, body: credentials, headers: headers));
    return json.decode(response.body) as Map<String, dynamic>;
  }

  /// Deletes user access
  Future<void> deleteUser({required String password}) async {
    final credentials = jsonEncode({'password': password});
    final uri = await replicationUri('/v1/access/delete-user/');
    await I<AuthService>()
        .call((headers) => http.put(uri, body: credentials, headers: headers));
  }
}
