import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';
import 'package:repository/repository.dart';

import '../models/models.dart';
import 'default_art.dart';

class PlaybackService {

  static PlaybackService? _instance;
  late Future<void> init;
  late DefaultArt _defaultArt;


  factory PlaybackService({DefaultArt? defaultArt}) {
    _instance ??= PlaybackService._internal();
    return _instance!;
  }

  PlaybackService._internal({DefaultArt? defaultArt}) {
    _defaultArt = defaultArt ?? DefaultArt();
    init = _defaultArt.init;
  }

  List<MediaItem> createMediaItems(List<Verse> verses) {
    return verses
        .map((verse) =>
        MediaItem(
          id: '${verse.id}',
          title: verse.reference,
          album: verse.topic != null && verse.topic!.isNotEmpty
              ? verse.source
              : null,
          artist: verse.topic != null && verse.topic!.isNotEmpty
              ? verse.topic
              : verse.source,
          artUri: verse.image != null
              ? Uri.parse(verse.image!)
              : _defaultArt.uri,
        ))
        .toList();
  }

  Future<bool> activateSession() async {
    final musicOff = Settings().getBool(Settings.SPEECH_MUSIC_OFF) ?? true;
    final session = await AudioSession.instance;
    await session.configure(AudioSessionConfiguration(
      avAudioSessionCategory: AVAudioSessionCategory.playback,
      avAudioSessionMode: AVAudioSessionMode.spokenAudio,
      avAudioSessionCategoryOptions: musicOff
          ? AVAudioSessionCategoryOptions.none
          : AVAudioSessionCategoryOptions.interruptSpokenAudioAndMixWithOthers,
      androidAudioAttributes: const AndroidAudioAttributes(
        contentType: AndroidAudioContentType.speech,
        usage: AndroidAudioUsage.media,
      ),
      androidAudioFocusGainType: AndroidAudioFocusGainType.gainTransientMayDuck,
      androidWillPauseWhenDucked: true,
    ));
    return await session.setActive(true);
  }


  PlaybackState played(PlaybackState playbackState, bool willPause, bool repeat, bool playlist) {
    return playbackState.copyWith(
        controls: [
          MediaControl.skipToPrevious,
          MediaControl.pause,
          MediaControl.skipToNext
        ],
        processingState: AudioProcessingState.ready,
        playing: true,
        repeatMode: willPause
            ? AudioServiceRepeatMode.group
            : repeat
            ? playlist
            ? AudioServiceRepeatMode.all
            : AudioServiceRepeatMode.one
            : AudioServiceRepeatMode.none);
  }

  PlaybackState stopped(PlaybackState playbackState) {
    return playbackState.copyWith(
      controls: [],
      processingState: AudioProcessingState.idle,
      playing: false,
    );
  }

  PlaybackState paused(PlaybackState playbackState) {
    return playbackState.copyWith(
      controls: [MediaControl.stop, MediaControl.play, MediaControl.skipToNext],
      processingState: AudioProcessingState.ready,
      playing: false,
    );
  }
}