import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';


class DefaultArt {
  static final DefaultArt _instance = DefaultArt._internal();
  late Future<void> init;
  late Uri _uri;

  factory DefaultArt() {
    return _instance;
  }

  DefaultArt._internal() {
    init = _init();
  }

  Future<void> _init() async {
    _uri = await _createUri();
  }

  Uri get uri {
    return _uri;
  }

  Future<Uri> _createUri() async {
    if (kIsWeb) return Uri.parse('assets/icon/app.png');
    Directory appDir = await getApplicationDocumentsDirectory();
    ByteData bytes = await rootBundle.load('assets/icon/app.png');
    File file = File(join(appDir.path, 'app.png'));
    File newFile = await file.writeAsBytes(bytes.buffer.asUint8List());
    return Uri.file(newFile.path);
  }

}
