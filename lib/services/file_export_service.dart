import 'package:app_core/app_core.dart';
import 'package:csv/csv.dart';
import 'package:remem_me/services/file/file.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';

class FileExportService {
  static final FileExportService _instance = FileExportService._internal();

  factory FileExportService() {
    return _instance;
  }

  FileExportService._internal();

  Future<bool> writeCsv(List<VerseEntity> verses) async {
    final csv = encode(verses);
    return FileWriter().write(csv, _fileName);
  }

  String get _fileName {
    return '${I<CurrentAccount>().name.toLowerCase().replaceAll(RegExp(r'[\s.]+'), '_')}.csv';
  }

  String encode(List<VerseEntity> verses) {
    final rows = [_header, ...verses.map((v) => encodeVerse(v))];
    return const ListToCsvConverter().convert(rows);
  }

  List<String> encodeVerse(VerseEntity v) {
    return [
      v.reference,
      v.passage,
      v.source ?? '',
      v.topic ?? '',
      v.commit == null ? '' : v.commit!.formatIso(),
      v.review == null ? '' : v.review!.formatIso(),
      v.level.toString(),
      v.tags.values.join('; '),
      v.image ?? '',
    ];
  }

  List<String> get _header {
    return [
      L10n.current.t8('Verse.reference'),
      L10n.current.t8('Verse.passage'),
      L10n.current.t8('Verse.source'),
      L10n.current.t8('Verse.topic'),
      L10n.current.t8('Verse.commit'),
      L10n.current.t8('Verse.review'),
      L10n.current.t8('Verse.level'),
      L10n.current.t8('Tags.title'),
      L10n.current.t8('Verse.image'),
    ];
  }
}
