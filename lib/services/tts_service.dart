import 'dart:async';
import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../util/util.dart';

enum EngineState { started, paused, cancelled, completed }

class TtsService {
  static final _log = Logger('TtsService');
  final _flutterTts = FlutterTts();
  late Future<void> _isInitialised;
  String _currentEngine = '';
  bool _isStopRequested = false;
  String? _key;
  final completeHandlers = <MapEntry<String, Function>>[];
  final cancelHandlers = <MapEntry<String, Function>>[];
  final errorHandlers = <MapEntry<String, Function>>[];
  List<String> _chunks = const [];
  List<Voice> _voices = const [];

  static final TtsService _instance = TtsService._internal();

  factory TtsService() {
    return _instance;
  }

  TtsService._internal() {
    _isInitialised = _init();
  }

  Future<void> _init() async {
    await _updateEngine();
    await _flutterTts.awaitSpeakCompletion(true);
    _flutterTts.setCompletionHandler(() {
      I<TranscriptionCubit>().clear(TranscriptionMode.caption);
      if (_isStopRequested) {
        _notify(cancelHandlers);
      } else {
        if (_chunks.isNotEmpty) _chunks.removeAt(0);
        _chunks.isEmpty ? _notify(completeHandlers) : _speakNextChunk();
      }
    });
    _flutterTts.setCancelHandler(() {
      I<TranscriptionCubit>().clear(TranscriptionMode.caption);
      _notify(cancelHandlers);
    });
    _flutterTts.setErrorHandler((e) {
      _log.warning('TtsService error: $e');
      I<TranscriptionCubit>().clear(TranscriptionMode.caption);
      _notify(errorHandlers);
    });
    _updateVoices();
    _flutterTts.setVoicesChangedHandler(() {
      _updateVoices();
    });
  }

  _updateVoices() async {
    if (isVoiceSelectable) {
      _voices = (await _flutterTts.getVoices as List<dynamic>)
          .where((item) =>
              item is Map && item['name'] != null && item['locale'] != null)
          .map((item) => Voice(
                name: item['name']!,
                idiom: item['locale']!,
              ))
          .sorted((a, b) => a.idiom.compareTo(b.name) != 0
              ? a.idiom.compareTo(b.idiom)
              : a.name.compareTo(b.name))
          .toList();
    }
  }

  _notify(List<MapEntry<String, Function>> handlers) {
    handlers.where((item) => item.key == _key).forEach((item) => item.value());
  }

  void registerCompleteHandler(String key, Function fn) {
    completeHandlers.add(MapEntry(key, fn));
  }

  void registerCancelHandler(String key, Function fn) {
    cancelHandlers.add(MapEntry(key, fn));
  }

  void registerErrorHandler(String key, Function fn) {
    errorHandlers.add(MapEntry(key, fn));
  }

  Future<void> stop() async {
    await _isInitialised;
    _isStopRequested = true;
    var result = await _flutterTts.stop();
    _log.fine('TtsService.stop result: $result');
  }

  Future<void> speakText(String key, String text, Locale locale,
      {bool isResumed = false}) async {
    await _isInitialised;
    if (locale.languageCode == 'he') text = TextService().removeVowels(text);
    _isStopRequested = false;
    _key = key;
    await _updateEngine();
    await _updateVoice(locale);
    if (!isResumed || _chunks.isEmpty) _chunks = TextService().lines(text);
    await _speakNextChunk();
  }

  Iterable<Voice> get voices => _voices;

  Future<void> _updateEngine() async {
    if (!kIsWeb && Platform.isAndroid) {
      _log.fine('TtsService._init retrieve default engine');
      var defaultEngine = await _flutterTts.getDefaultEngine;
      if (_currentEngine != defaultEngine) {
        _currentEngine = defaultEngine;
        _log.info('TtsService._init set default engine to $defaultEngine');
        await _flutterTts.setEngine(defaultEngine);
        _log.fine('TtsService._init default engine set');
      }
    }
  }

  Future<void> _updateVoice(Locale locale) async {
    if (isVoiceSelectable && _voices.isNotEmpty) {
      Voice voice = _voices
              .firstWhereOrNull((v) => v == I<Settings>().getVoice(locale)) ??
          (await defaultVoice(locale))!;
      return await _flutterTts
          .setVoice({'name': voice.name, 'locale': voice.idiom});
    } else {
      var idiom = await _availableLang(locale);
      if (await _flutterTts.isLanguageAvailable(idiom)) {
        await _flutterTts.setLanguage(idiom);
      }
    }
  }

  Future<void> _speakNextChunk() async {
    assert(_chunks.isNotEmpty);
    final chunk = _chunks.first;
    I<TranscriptionCubit>()
        .flash(Transcription(chunk, mode: TranscriptionMode.caption));
    final rate = Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5;
    final volume = Settings().getDouble(Settings.SPEECH_VOLUME) ?? 1.0;
    await _flutterTts.setSpeechRate(rate);
    await _flutterTts.setVolume(volume);
    _log.fine('TtsService._speakNextChunk: $chunk');
    final result = await _flutterTts.speak(chunk);
    _log.fine('TtsService._speakNextChunk result: $result');
  }

  Future<String> _availableLang(Locale intended) async {
    var available = await _intendedLang(intended);
    if (available == null) {
      _messageLanguageNotAvailable(intended.toString());
      available = await _intendedLang(L10n.current.locale);
    }
    available ??= 'en';
    return available;
  }

  Future<String?> _intendedLang(Locale intended) async {
    var lang = intended.languageCode;
    var ctry = intended.countryCode;
    if (ctry != null) {
      if (await _flutterTts.isLanguageAvailable('$lang-$ctry')) {
        return '$lang-$ctry';
      }
      if (lang == 'zh') lang = '${_chineseLang(ctry)}';
    }
    if (await _flutterTts.isLanguageAvailable(lang)) return lang;
    List<dynamic> locales = await _flutterTts.getLanguages;
    final alternative = locales.firstWhereOrNull(
        (locale) => locale is String && locale.startsWith(lang));
    return alternative;
  }

  bool get isVoiceSelectable {
    return kIsWeb &&
        [TargetPlatform.windows, TargetPlatform.macOS]
            .contains(defaultTargetPlatform);
  }

  Future<Voice?> defaultVoice(Locale locale) async {
    if (_voices.isEmpty) return null; // RETURN
    final lang = await _availableLang(locale);
    return _voices.firstWhereOrNull(
            (v) => RegExp('^$lang(-|\$)').hasMatch(v.idiom)) ??
        _voices
            .firstWhereOrNull((v) => RegExp('^$lang(-|\$)').hasMatch(v.name)) ??
        _voices.first;
  }

  void _messageLanguageNotAvailable(String locale) {
    final ctx = outerNavigatorKey.currentContext; // basic BuildContext
    if (ctx != null && LocaleNames.of(ctx) != null) {
      MessageService().warn(L10n.current.t8('Speech.languageNotAvailable',
          [localeNameOf(ctx, locale.toString())]));
    }
  }

  _chineseLang(String ctry) {
    switch (ctry) {
      case 'HK':
        return 'yue';
      default: // CN, TW
        return 'cmn';
    }
  }
}
