import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:csv/csv.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_charset_detector/flutter_charset_detector.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';

class FileImportService {
  static final FileImportService _instance = FileImportService._internal();

  factory FileImportService() {
    return _instance;
  }

  FileImportService._internal();

  Future<List<VerseEntity>> read(List<TagEntity> newTags) async {
    final import = await _read();
    late List<VerseEntity> list;
    if (import.importText == null) {
      list = [];
    } else if (import.isLegacyImport) {
      list = decodeLegacy(import.importText.toString(), newTags);
    } else {
      list = decodeCsv(import.importText.toString(), newTags);
    }
    return list;
  }

  @visibleForTesting
  List<VerseEntity> decodeCsv(String text, List<TagEntity> newTags) {
    final rows = const CsvToListConverter()
        .convert<String>(text, shouldParseNumbers: false);
    final verses =
        rows.skip(1).map((row) => decodeVerse(row, newTags)).toList();
    return verses;
  }

  @visibleForTesting
  List<VerseEntity> decodeLegacy(String text, List<TagEntity> newTags) {
    final List<String> blocks = text.split(RegExp(r'\n(?=R:)'));
    List<VerseEntity> verses = [];
    if (blocks.isNotEmpty) {
      VerseEntity? verse;
      for (String thisV in blocks) {
        verse = _decodeLegacyVerse(thisV, newTags);
        if (verse != null) {
          verses.add(verse);
        }
      }
    } else {
      verses = [];
    }
    return verses;
  }

  @visibleForTesting
  VerseEntity decodeVerse(List<String> row, List<TagEntity> newTags) {
    return VerseEntity(
      row[0],
      row[1],
      source: row.length < 3 || row[2].isEmpty ? null : row[2],
      topic: row.length < 4 || row[3].isEmpty ? null : row[3],
      commit: row.length < 5 || row[4].isEmpty ? null : LocalDate.parse(row[4]),
      review: row.length < 6 || row[5].isEmpty ? null : LocalDate.parse(row[5]),
      level: row.length < 7 || row[6].isEmpty ? -1 : int.parse(row[6]),
      tags: row.length < 8 ? {} : _decodeTags(row[7], newTags),
      image: row.length < 9 || row[8].isEmpty ? null : row[8],
      accountId: I<CurrentAccount>().id,
    );
  }

  VerseEntity? _decodeLegacyVerse(String block, List<TagEntity> newTags) {
    List<String> lines = block.split(RegExp(r'(\r?\n)'));
    const delimiterPattern = r'^(?<delimiter>[RQTPVLDCS]):\s(?<text>.+)$';
    final delimiterExp = RegExp(delimiterPattern);
    String? reference;
    String? source;
    String? topic;
    String? passage;
    LocalDate? commit;
    LocalDate? review;
    int? level;
    String? tags;
    late final VerseEntity? verse;
    for (final line in lines) {
      if (delimiterExp.hasMatch(line)) {
        RegExpMatch? match = delimiterExp.firstMatch(line);
        switch (match?.namedGroup('delimiter')) {
          case 'R':
            reference = match?.namedGroup('text');
            break;
          case 'Q':
            source = match?.namedGroup('text');
            break;
          case 'T':
            topic = match?.namedGroup('text');
            break;
          case 'P':
            passage = match?.namedGroup('text');
            break;
          case 'V':
            review = LocalDate.parse(match?.namedGroup('text') as String);
            break;
          case 'L':
            level = int.parse((match)?.namedGroup('text') as String);
            break;
          case 'C':
            commit = LocalDate.parse(match?.namedGroup('text') as String);
            break;
          case 'S':
            tags = match?.namedGroup('text');
            break;
          default:
            debugPrint('Unknown legacy import: $line');
        }
      } else {
        // it's a following line of a multi-line passage
        passage = "$passage\n$line";
      }
    }
    if (reference != null && passage != null) {
      verse = VerseEntity(
        reference,
        passage,
        source: source,
        topic: topic,
        commit: commit,
        review: review,
        level: level ?? -1,
        tags: (tags == null ? const {} : _decodeTags(tags, newTags)),
        accountId: I<CurrentAccount>().id,
      );
    } else {
      verse = null;
    }
    return verse;
  }

  Map<int, String?> _decodeTags(String text, List<TagEntity> newTags) {
    final state = I<TagsBloc>().state;
    if (state is! EntitiesLoadSuccess<TagEntity>) return {}; // RETURN
    final tags = <int, String?>{};
    text.split(RegExp(r';\s*')).forEach((item) {
      var tag = (state.items + newTags).firstWhereOrNull(
          (entity) => entity.label == item && !entity.deleted);
      if (tag == null && item.trim().isNotEmpty) {
        tag = TagEntity(item, accountId: I<CurrentAccount>().id);
        newTags.add(tag);
      }
      if (tag != null) {
        tags[tag.id] = tag.label;
      }
    });
    return tags;
  }

  Future<({bool isLegacyImport, String? importText})> _read() async {
    const XTypeGroup typeGroup = XTypeGroup(
      label: 'text',
      extensions: <String>['txt', 'csv'],
    );
    final XFile? file = await openFile(
      acceptedTypeGroups: <XTypeGroup>[typeGroup],
    );
    if (file == null) {
      // Operation was canceled by the user.
      return (isLegacyImport: false, importText: null);
    }
    bool isLegacyImport = file.name.endsWith('.txt');

    // Read file as bytes
    final bytes = await file.readAsBytes();

    // Detect charset and decode
    DecodingResult result = await CharsetDetector.autoDecode(bytes);
    String text = result.string;
    return (isLegacyImport: isLegacyImport, importText: text);
  }
}
