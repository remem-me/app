import 'dart:math';

import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';

class StudyService {
  static final StudyService _instance = StudyService._internal();
  static const OBFUSCATE_FRACTION = 12;

  factory StudyService() {
    return _instance;
  }

  StudyService._internal();

  List<List<bool>> obfuscate(
      List<List<String>> lines, List<List<bool>> obfuscated) {
    final numWords = TextService().countWordsOfRows(lines);
    final numObfuscated = TextService().countMarks(obfuscated);
    final result = TextService().copyItems<bool>(obfuscated);
    final batchSize = numWords / OBFUSCATE_FRACTION;
    for (var i = 0;
        i < batchSize && i < numWords - numObfuscated;
        i++) {
      final index = Random().nextInt(numWords - numObfuscated - i);
      TextService().mark(index, result);
    }
    return result;
  }

  List<List<bool>> reveal(List<List<bool>> revealed, Pos pos) {
    final result = TextService().copyItems(revealed);
    result[pos.line][pos.word] = true;
    return result;
  }

  Choice puzzle(List<List<String>> lines, Pos pos, int puzzleSize) {
    var repo = _wordRepo(lines, pos);
    final optionSet = <String>{};
    // add the correct word as option
    final correctWord = TextService().stripPunctuation(lines[pos.line][pos.word]);
    optionSet.add(correctWord);
    final rnd = Random();
    // build repo from remaining words
    while (optionSet.length < puzzleSize && repo.isNotEmpty) {
      optionSet.add(repo.removeAt(rnd.nextInt(repo.length)));
    }
    // add some earlier words if necessary
    int missing = puzzleSize - optionSet.length;
    if (missing > 0) {
      repo = _wordRepo(lines, const Pos(0, 0));
      while (optionSet.length < puzzleSize && repo.isNotEmpty) {
        optionSet.add(repo.removeAt(rnd.nextInt(repo.length)));
      }
    }
    // randomize the options' order
    final options = optionSet.toList();
    options.shuffle();
    // find the index of the correct word
    var solution = options.indexOf(correctWord);
    return Choice(options, solution);
  }

  Pos advanceByWord(List<List<String>> lines, Pos pos) {
    final i = pos.line, j = pos.word;
    if (j < lines[i].length - 1) return Pos(i, j + 1);
    if (i < lines.length - 1) return Pos(i + 1, 0);
    throw OutOfWordsException();
  }

  Pos advanceByLine(List<List<String>> lines, Pos pos) {
    final i = pos.line, j = pos.word;
    if (j < lines[i].length - 1) return Pos(i, lines[i].length - 1);
    if (i < lines.length - 1) return Pos(i + 1, lines[i + 1].length - 1);
    throw OutOfWordsException();
  }

  Pos lookAhead(List<List<String>> lines, Pos pos) {
    final i = pos.line, j = pos.word;
    if (j < lines[i].length - 1) return Pos(i, lines[i].length - 1);
    if (i < lines.length - 1) return Pos(i + 1, lines[i + 1].length - 1);
    throw OutOfWordsException();
  }

  Pos retractWord(List<List<String>> words, Pos pos) {
    final i = pos.line, j = pos.word;
    if (i == 0 && j <= 0) return const Pos(0, -1);
    if (j > 0) return Pos(i, j - 1);
    if (i > 0) return Pos(i - 1, words[i - 1].length - 1);
    throw OutOfWordsException();
  }

  Pos retractLine(List<List<String>>? words, Pos pos) {
    final i = pos.line;
    if (i == 0) return const Pos(0, -1);
    if (i > 0) return Pos(i - 1, words![i - 1].length - 1);
    throw OutOfWordsException();
  }

  List<String> _wordRepo(List<List<String>> words, Pos pos) {
    final index = words
            .sublist(0, pos.line)
            .fold(0, (dynamic count, line) => count + line.length) +
        pos.word;
    return index < words.length
        ? words
            .expand((items) => items)
            .map((word) => TextService().stripPunctuation(word))
            .toList()
            .sublist(index + 1)
            .toSet()
            .toList()
        : [];
  }
}
