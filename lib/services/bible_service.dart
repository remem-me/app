import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../util/verse_util.dart';
import 'verse_service.dart';

const BIBLE_PROXY_PATH =
    'proxy/?version=%(tag)s&book=%(book)s&chap=%(chap)d&verses=%(start)d-%(end)d';

/// Needs to be instantiated with a language before first use
class BibleService {

  static final BibleService _instance = BibleService._internal();
  final _server = I<Settings>().getOrDefault(Settings.BIBLE_SERVER);
  Locale? _locale;
  late Map<String, BibleSite> sites;
  Map<String, BibleVersion>? versions;
  late Map<String, Map<String, String>> books;

  BibleService._internal();

  factory BibleService() {
    return _instance;
  }

  Future<void> init(Locale locale) async {
    locale = TextUtil.bibleLocale(locale) ?? L10n.current.locale;
    if (!TextUtil.localeScriptEqual(_locale, locale)) {
      _locale = locale;
      final uri = Uri.parse('$_server/patterns/${locale.languageCode}.json');
      final response = await http.get(uri);
      debugPrint('Response status: ${response.statusCode}');
      debugPrint(
          'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
      final body = json.decode(utf8.decode(response.body.codeUnits));
      books = _booksFromJson(body);
      sites = _sitesFromJson(body);
      versions = _versionsFromJson(body);
    }
  }

  BibleQuery? query(
      String versionKey, String reference, Map<String, Book> books) {
    final book = VerseUtil.book(reference, books);
    final chapter = VerseUtil.chapter(reference);
    var first = VerseUtil.first(reference);
    var last = VerseService().last(reference);
    var hasRange = VerseService().hasRange(reference);
    BibleVersion? version = versions?[versionKey];
    if (version != null && book.key != null && chapter > 0) {
      return BibleQuery(versionKey, version, book.key!, chapter,
          first > 0 ? first : 1, last > 0 ? last : hasRange ? 0 : first);
    } else {
      return null;
    }
  }

  Map<String, BibleVersion> _versionsFromJson(json) {
    return {
      for (var entry in json['versions'].entries)
        entry.key: BibleVersion(
          entry.value['name'],
          entry.value['tag'],
          sites[entry.value['site']],
          books[entry.value['books']],
        )
    };
  }

  Map<String, BibleSite> _sitesFromJson(json) {
    return {
      for (var entry in json['sites'].entries)
        entry.key: BibleSite.fromJson(entry.value)
    };
  }

  Map<String, Map<String, String>> _booksFromJson(json) {
    return {
      for (var books in json['books'].entries)
        books.key: Map<String, String>.from(books.value)
    };
  }

}
