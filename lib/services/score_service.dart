import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';

import '../blocs/current_account/current_account.dart';

class ScoreService {
  static final ScoreService _instance = ScoreService._internal();

  factory ScoreService() {
    return _instance;
  }

  ScoreService._internal();

  int numActive(List<Verse> verses) {
    return verses.where((verse) => verse.due == null).toList().length;
  }

  int numDue(List<Verse> verses) {
    return verses.where((verse) => verse.due != null).toList().length;
  }

  int totalScore(List<int> scores) {
    return scores.fold(0, (prev, element) => prev + element);
  }

  int todayScore(List<ScoreEntity> scores) {
    return scores
        .where((score) => score.date == DateService().today)
        .fold<int>(0, (acc, score) => acc + score.change);
  }

  List<int> scoreLevels(List<Verse> verses) {
    final result = <int?>[];
    for (var verse in verses) {
      if (verse.level < 1) continue; // CONTINUE
      final change = TextService().countWordsOfText(verse.passage);
      if (verse.level > result.length) result.length = verse.level;
      if (result[verse.level - 1] == null) {
        result[verse.level - 1] = change;
      } else {
        result[verse.level - 1] = result[verse.level - 1]! + change;
      }
    }
    return result.map((value) => value ?? 0).toList();
  }

  Map<String, DayScore> rangeDays(Map<String, DayScore> scores, int length) {
    var date = DateService().today - length;
    final result = <String, DayScore>{};
    for (int i = 0; i <= length; i++) {
      final key = date.formatIso();
      result[key] = DayScore(date, scores[key]?.change ?? 0);
      date = date + 1;
    }
    return result;
  }

  Future<bool> goalReached(List<ScoreEntity> scores, LocalDate today) async {
    final goal = I<CurrentAccount>().dailyGoal;
    final args = HasReachedGoalArgs(scores, today, goal);
    return await compute(ScoreUtil.hasReachedGoal, args);
  }

  void showGoalReachedMessage() {
    I<MessageService>().show(SnackBar(
        behavior: SnackBarBehavior.floating,
        margin: const EdgeInsets.all(16),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4),
            side: const BorderSide(color: Flat.amber, width: 1)),
        content: Builder(builder: (ctx) {
          return Text(
            L10n.of(ctx).t8('Scores.goal.message'),
            style: const TextStyle(fontSize: 16),
          );
        })));
  }
}
