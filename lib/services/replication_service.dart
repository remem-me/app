import 'package:app_core/app_core.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';

/// Goes through all replication queues,
/// sends all items in the queues,
/// uses repository's updateList() method
class ReplicationService {
  static final _log = Logger('ReplicationService');
  final _replicators = <Type, Replicator>{};
  final _order = [AccountEntity, TagEntity, VerseEntity, ScoreEntity];
  late Settings _settings;

  static final ReplicationService _instance = ReplicationService._internal();

  factory ReplicationService() {
    return _instance;
  }

  ReplicationService._internal() {
    _settings = I<Settings>();
  }

  Future<void> register(Type entityType, Replicator replicator) async {
    _log.fine('ReplicationService.register(entityType=$entityType)');
    _replicators.putIfAbsent(entityType, () => replicator);
  }

  Future<void> replicate({bool all = false}) async {
    await send(all: all);
    await receive(all: all);
  }

  Future<void> send({bool all = false}) async {
    if(!I.isRegistered<L10n>()) await L10n.initialized;
    final accountState = _replicators[AccountEntity]!.bloc.state;
    if (accountState is! EntitiesLoadSuccess<AccountEntity>) {
      throw MessageException({
        'messages': [L10n.current.t8('Account.none')]
      });
    }
    final errors = <String, List<String>>{};
    for (var entityType in _order) {
      if(_replicators[entityType] == null) continue;
      final replicator = _replicators[entityType]!;
      try {
        all
            ? await replicator.sendAll(accountState.items)
            : await replicator.sendQueued(accountState.items);
      } on MessageException catch (e) {
        errors.addAll(e.messages);
      }
    }
    if (errors.isNotEmpty) {
      throw MessageException(errors);
    }
  }

  Future<void> receive({bool all = false}) async {
    _log.fine('ReplicationService.receive(all=$all)');
    final errors = <String, List<String>>{};
    for (var entityType in _order) {
      if(_replicators[entityType] == null) continue;
      final since = all ? null : _getLastReceived(entityType);
      final client = all ? null : _settings.deviceId;
      try {
        await _receiveWhenReady(entityType, since, client);
      } on MessageException catch (e) {
        errors.addAll(e.messages);
      }
    }
    if (errors.isNotEmpty) {
      throw MessageException(errors);
    }
  }

  Future<void> clearLocalStorage() async {
    final accountState = _replicators[AccountEntity]!.bloc.state
        as EntitiesLoadSuccess<AccountEntity>;
    for (var entityType in _order) {
      await _replicators[entityType]!.clearLocalStorage(accountState.items);
      _deleteLastReceived(entityType);
    }
    _settings.delete(Settings.RPL_DEVICE_ID);
  }

  Future<void> clearLastReceived() async {
    for (var entityType in _order) {
      await _deleteLastReceived(entityType);
    }
  }

  Future<void> _receiveWhenReady(
      Type entityType, int? since, int? client) async {
    await _replicators[entityType]!.receiveWhenReady(since, client);
    _setLastReceived(entityType);
  }

  int _getLastReceived(Type entityType) {
    final key = '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}';
    final value = _settings.getInt(
            '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}') ??
        0;
    _log.fine('ReplicationService.getLastReceived $key $value');
    return value;
  }

  Future<void> _setLastReceived(Type entityType) async {
    final key = '${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}';
    final value = IdGenerator().now();
    await _settings.putInt(key, IdGenerator().now());
    _log.fine('ReplicationService.setLastReceived $key $value');
  }

  Future<void> _deleteLastReceived(Type entityType) {
    return _settings
        .delete('${Settings.RPL_LAST_RECEIVED}_${repositoryKey(entityType)}');
  }
}
