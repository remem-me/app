import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';

import '../blocs/current_account/current_account.dart';
import '../util/verse_util.dart';

class VerseService {
  static final VerseService _instance = VerseService._internal();
  late final CurrentAccount _account;
  L10n? _refL10n;

  factory VerseService() {
    return _instance;
  }

  VerseService._internal() {
    _account = I<CurrentAccount>();
  }

  Future<void> _updateRefL10n() async {
    final referenceLocale =
        TextUtil.appLocale(I<CurrentAccount>().langRef) ?? L10n.current.locale;
    if (!TextUtil.localeScriptEqual(_refL10n?.locale, referenceLocale)) {
      _refL10n = L10n(referenceLocale);
      await _refL10n!.load();
    }
  }

  Future<List<Verse>> fromEntities(Iterable<VerseEntity> entities) async {
    final books = _account.books;
    final reviewLimit = _account.reviewLimit;
    final reviewFrequency = _account.reviewFrequency;
    final deviceId = I<Settings>().deviceId;
    final args = FromEntitiesArgs(
        entities, books, reviewLimit, reviewFrequency, deviceId);
    return compute(VerseUtil.fromEntities, args);
  }

  Verse committed(Verse v) {
    final today = DateService().today;
    return v.copyWith(
        commit: today,
        due: VerseUtil.due(
          commit: today,
          reviewLimit: _account.reviewLimit,
          reviewFrequency: _account.reviewFrequency,
        ),
        level: 0,
        color: VerseUtil.color(0));
  }

  Verse forgotten(Verse v) {
    final today = DateService().today;
    return v.copyWith(
        review: today,
        due: VerseUtil.due(
            review: today,
            level: 0,
            reviewFrequency: _account.reviewFrequency,
            reviewLimit: _account.reviewLimit),
        level: 0,
        color: VerseUtil.color(0));
  }

  Verse remembered(Verse v) {
    final today = DateService().today;
    final limit = _account.reviewLimit;
    final offsetBefore = VerseUtil.dueOffset(
      v.level,
      reviewFrequency: _account.reviewFrequency,
    );
    final offset = VerseUtil.dueOffset(
      v.level + 1,
      reviewFrequency: _account.reviewFrequency,
    );
    if (limit == 0 || limit >= offset) {
      return v.copyWith(
          review: today,
          due: today + offset,
          level: v.level + 1,
          color: VerseUtil.color(v.level + 1));
    } else if (limit > offsetBefore) {
      return v.copyWith(
          review: today,
          due: today + limit,
          level: v.level + 1,
          color: VerseUtil.color(v.level + 1));
    } else {
      return v.copyWith(review: today, due: today + limit);
    }
  }

  Verse movedToNew(Verse v) {
    return v
        .copyWith(level: -1, nullValues: ['commit', 'review', 'due', 'color']);
  }

  Verse movedToDue(Verse v) {
    final today = DateService().today;
    if (v.review != null) {
      // revert remembered
      final limit = _account.reviewLimit;
      final offsetBefore = VerseUtil.dueOffset(
        v.level - 1,
        reviewFrequency: _account.reviewFrequency,
      );
      final offset = VerseUtil.dueOffset(
        v.level,
        reviewFrequency: _account.reviewFrequency,
      );
      if (limit == 0 || limit >= offset) {
        return v.copyWith(
            review: today - offsetBefore,
            due: today,
            level: v.level - 1,
            color: VerseUtil.color(v.level - 1));
      } else {
        return v.copyWith(review: today - limit, due: today);
      }
    } else if (v.commit != null) {
      return v.copyWith(
        commit: today,
        due: VerseUtil.due(
          commit: today,
          reviewLimit: _account.reviewLimit,
          reviewFrequency: _account.reviewFrequency,
        ),
      );
    } else {
      return v;
    }
  }

  Verse postponed(Verse v, int days) {
    if (v.review != null) {
      return v.copyWith(review: v.review! + days);
    } else if (v.commit != null) {
      return v.copyWith(commit: v.commit! + days);
    } else {
      return v;
    }
  }

  Verse addTag(Verse v, TagEntity tag) {
    final tags = Map<int, String?>.of(v.tags);
    tags[tag.id] = tag.text;
    return v.copyWith(
      tags: tags,
    );
  }

  Verse removeTag(Verse v, TagEntity tag) {
    final tags = Map<int, String?>.of(v.tags);
    tags.remove(tag.id);
    return v.copyWith(
      tags: tags,
    );
  }

  bool hasTag(Verse v, TagEntity tag) {
    return v.tags.containsKey(tag.id);
  }

  bool hasRange(String reference) {
    return reference.contains(RegExp(r'\d+\s*[:.,。：、．]\d+\s*[-〜一—][\d\s]*$'));
  }

  int last(String reference) {
    final pattern = RegExp(r'(?<=\d+\w*\s*[-〜一—]\s*)\d+(?=\w*\s*$)');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  String? numberPart(String reference) {
    final pattern = RegExp(r'(?<!^)\d+.+$');
    final match = pattern.firstMatch(reference);
    return match?.group(0);
  }

  Future<String> spokenReference(String reference) async {
    await _updateRefL10n();
    final book = VerseUtil.book(reference, _account.books).spokenName;
    final chapter = VerseUtil.chapter(reference);
    final first = VerseUtil.first(reference);
    final last = this.last(reference);
    return book != null && chapter > 0
        ? first > 0
            ? last > 0
                ? '$book $chapter, ${_refL10n!.t8('Verses.range', [first.toString(), last.toString()])}'
                : '$book $chapter, $first'
            : '$book $chapter'
        : reference;
  }

  String formattedReference(String reference) {
    final bookName = VerseUtil.book(reference, _account.books).name;
    final numbers = numberPart(reference);
    return bookName != null ? '$bookName $numbers' : reference;
  }

  @Deprecated("Current text-to-speech engines add breathers on linebreaks")
  String spokenPassage(String passage, Locale locale) {
    if (locale.languageCode == 'zh') {
      return passage.replaceAll(
          RegExp(r'(?<![\u3000-\u303F]\s*)\s*\n+', unicode: true), '。\n');
    } else {
      return passage.replaceAll(
          RegExp(r'(?<!\p{P}\s*)\s*\n+', unicode: true), ';\n');
    }
  }

  List<String> lines(Verse verse, {referenceIncluded = true}) {
    var result = TextService().lines(verse.passage);
    if (referenceIncluded && _account.referenceIncluded) {
      result[result.length - 1] += '\n';
      result.add('#### ${verse.reference}');
    }
    return result;
  }

  List<List<String>> words(Verse verse) {
    var text = verse.passage;
    if (!_account.referenceIncluded) {
      text = TextService().stripVerseNumbers(text);
    }
    text = TextService().stripMarkdown(text);
    final result = TextService().words(text);
    if (_account.referenceIncluded) {
      result[result.length - 1][result[result.length - 1].length - 1] += '\n\n';
      result.add(TextService().splitReference(verse.reference));
    }
    return result;
  }

  String subject(Verse verse) {
    var subject = verse.reference;
    if (verse.topic != null && verse.topic!.isNotEmpty) {
      subject =
          '${VerseService().title(verse)} (${VerseService().subtitle(verse)})';
    }
    return subject;
  }

  String title(Verse verse) {
    return _account.topicPreferred ? verse.topic ?? '' : verse.reference;
  }

  String subtitle(Verse verse) {
    return _account.topicPreferred ? verse.reference : verse.topic ?? '';
  }
}
