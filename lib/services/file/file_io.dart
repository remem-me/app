import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

class FileWriter {
  Future<bool> write(String text, String name) async {
    final path = (await getApplicationDocumentsDirectory()).path;
    await File('$path/$name').writeAsString(text);
    final result = await Share.shareXFiles([XFile('$path/$name')]);
    return result.status == ShareResultStatus.success;
  }
}
