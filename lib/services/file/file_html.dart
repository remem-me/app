import 'dart:convert';
import 'dart:html' as html;

class FileWriter {
  Future<bool> write(String text, String name) {
    // Encode the content
    List<int> bytes = utf8.encode(text);

    // Create a blob
    final blob = html.Blob([bytes]);

    // Generate a URL for the blob
    final url = html.Url.createObjectUrlFromBlob(blob);

    // Create a download link
    final anchor = html.AnchorElement(href: url)
      ..setAttribute("download", name)
      ..style.display = 'none';

    // Add to the DOM and trigger download
    html.document.body!.children.add(anchor);
    anchor.click();

    // Clean up
    html.document.body!.children.remove(anchor);
    html.Url.revokeObjectUrl(url);
    return Future.value(true);
  }
}
