import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../paths/paths.dart';

class HomeFlashcardService {
  static final HomeFlashcardService _instance = HomeFlashcardService._internal();

  factory HomeFlashcardService() {
    return _instance;
  }

  HomeFlashcardService._internal();

  Future<FlashcardLoaded> toFlashcardLoaded(HomeFlashcardPath path) async {
    var (box, verses) = await findBoxOfVerse(path);
    var index = verses.indexWhere((verse) => verse.id == path.id);
    if (box == VerseBox.KNOWN) {
      final verse = verses.removeAt(index);
      index = 0;
      verses.shuffle();
      final batchSize = I<CurrentAccount>().inverseLimit;
      if (batchSize > 0 && batchSize <= verses.length) {
        verses = verses.sublist(0, batchSize - 1);
      }
      verses.insert(index, verse);
    } else if (box == VerseBox.DUE) {
      final batchSize = I<CurrentAccount>().reviewBatch;
      if (batchSize > 0 && batchSize < verses.length) {
        if (index + batchSize > verses.length) {
          index = index + batchSize - verses.length;
          verses = verses.sublist(verses.length - batchSize, verses.length);
        } else {
          verses = verses.sublist(index, index + batchSize);
          index = 0;
        }
      }
    }
    return FlashcardLoaded(
        verses: verses,
        box: box,
        index: index,
        reverse: false,
        inverse: box == VerseBox.KNOWN);
  }

  Future<(VerseBox?, List<Verse>)> findBoxOfVerse(
      HomeFlashcardPath path) async {
    VerseBox? box;
    List<Verse> verses = [];
    if (I<TabBloc>().state.isBoxed) {
      final verseLists = await Future.wait([
        _orderedVerses(I<OrderedVersesNewBloc>()),
        _orderedVerses(I<OrderedVersesDueBloc>()),
        _orderedVerses(I<OrderedVersesKnownBloc>()),
      ]);
      final i =
      verseLists.indexWhere((it) => it.any((verse) => verse.id == path.id));
      if (i > -1) {
        box = VerseBox.values[i];
        verses = verseLists[i];
      }
    } else {
      final it = await _orderedVerses(I<OrderedVersesAllBloc>());
      if (it.any((verse) => verse.id == path.id)) {
        box = VerseBox.ALL;
        verses = it;
      }
    }
    return (box, [...verses]);
  }

  Future<List<Verse>> _orderedVerses(OrderedVersesBloc bloc) {
    return bloc.stream
        .startWith(bloc.state)
        .where((orderedState) =>
        [OrderSuccess, OrderFailure].contains(orderedState.runtimeType))
        .map((orderedState) =>
    orderedState is OrderSuccess ? orderedState.verses : <Verse>[])
        .first;
  }
}
