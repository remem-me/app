import 'dart:async';
import 'dart:convert';

import 'package:app_core/app_core.dart';
import 'package:hive/hive.dart';
import 'package:home_widget/home_widget.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';


import '../blocs/blocs.dart';
import '../models/models.dart';
import '../util/verse_util.dart';

class HomeWidgetService {
  static final _log = Logger('HomeWidgetService');
  static const _storageName = 'homeWidget';
  static final HomeWidgetService _instance = HomeWidgetService._internal();

  factory HomeWidgetService() {
    return _instance;
  }

  HomeWidgetService._internal();

  Future<bool> update(List<Verse> verses) async {
    _log.fine('$runtimeType.update(${verses.length} verses)');
    final account = I<CurrentAccount>();
    final allVerses = await VerseService().fromEntities(verses);
    _storeWidgetData(
      allVerses,
      orderDue: account.orderDue,
      orderKnown: account.orderKnown,
      orderNew: account.orderNew,
      topicPreferred: account.topicPreferred,
      emptyMessage: L10n.current.t8('Verse.passage.default'),
    );
    return _updateStackWidget(
      allVerses,
      orderDue: account.orderDue,
      orderKnown: account.orderKnown,
      orderNew: account.orderNew,
      topicPreferred: account.topicPreferred,
      emptyMessage: L10n.current.t8('Verse.passage.default'),
    );
  }

  Future<bool> updateFromStorage() async {
    _log.fine('$runtimeType.updateFromStorage()');
    final box = await Hive.openBox(_storageName);
    final lastDate = box.get('date') as LocalDate;
    final today = DateService().today;
    _log.fine('lastDate ($lastDate) < today ($today): ${lastDate < today}');
    if (lastDate < today) {
      box.put('date', today);
      return _updateStackWidget(
        (box.get('verses') ?? []).cast<Verse>(),
        orderDue: box.get('orderDue') as VerseOrder,
        orderKnown: box.get('orderKnown') as VerseOrder,
        orderNew: box.get('orderNew') as VerseOrder,
        topicPreferred: box.get('topicPreferred') as bool,
        emptyMessage: box.get('emptyMessage') as String,
      );
    } else {
      return true;
    }
  }

  Future<bool> _updateStackWidget(
    List<Verse> allVerses, {
    required VerseOrder orderDue,
    required VerseOrder orderKnown,
    required VerseOrder orderNew,
    required bool topicPreferred,
    required String emptyMessage,
  }) {
    _log.fine('$runtimeType._updateStackWidget(${allVerses.length} verses)');
    var verses = VerseUtil.mapVersesToBoxDue(allVerses, orderDue);
    var box = VerseBox.DUE;
    if (verses.isEmpty) {
      verses = VerseUtil.mapVersesToBoxKnown(allVerses, orderKnown);
      box = VerseBox.KNOWN;
    }
    if (verses.isEmpty) {
      verses = VerseUtil.mapVersesToBoxNew(allVerses, orderNew);
      box = VerseBox.NEW;
    }
    return Future.wait<bool?>([
      HomeWidget.saveWidgetData('topicPreferred', topicPreferred),
      HomeWidget.saveWidgetData('box', box.name),
      HomeWidget.saveWidgetData('empty', emptyMessage),
      HomeWidget.saveWidgetData(
        'verses',
        jsonEncode(verses.map((entity) => entity.toJson()).toList()),
      ),
      HomeWidget.updateWidget(
        name: 'StackWidgetProvider',
        androidName: 'StackWidgetProvider',
        iOSName: 'StackWidget',
        qualifiedAndroidName: 'me.remem.app.StackWidgetProvider',
      ),
    ]).then((value) {
      return !value.contains(false);
    });
  }

  Future<void> _storeWidgetData(
    List<Verse> allVerses, {
    required VerseOrder orderDue,
    required VerseOrder orderKnown,
    required VerseOrder orderNew,
    required bool topicPreferred,
    required String emptyMessage,
  }) async {
    _log.fine('HomeWidgetService._storeWidgetData(${allVerses.length} verses)');
    final box = await Hive.openBox(_storageName);
    await Future.wait<void>([
      box.put('date', DateService().today),
      box.put('verses', allVerses),
      box.put('orderDue', orderDue),
      box.put('orderKnown', orderKnown),
      box.put('orderNew', orderNew),
      box.put('topicPreferred', topicPreferred),
      box.put('emptyMessage', emptyMessage),
    ]);
  }
}
