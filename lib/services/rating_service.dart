import 'package:app_core/app_core.dart';
import 'package:remem_me/config/app_config.dart';
import 'package:repository/repository.dart';
import 'package:store_checker/store_checker.dart';

class RatingService {
  static RatingService? _instance;
  static const _countdownStart = 40;
  late Future<Source> _installationSource;

  factory RatingService([Future<Source>? source]) {
    _instance = _instance ?? RatingService._internal(source);
    return _instance!;
  }

  RatingService._internal(Future<Source>? source) : _installationSource = source ?? StoreChecker.getSource;

  bool isReadyForRating() {
    return _count == 0;
  }

  bool isRatingDone() {
    var date = I<Settings>().getDate(Settings.RATING_DATE);
    return date != null && date.addMonths(12) > DateService().today;
  }

  Future<void> countDownToRating() async {
    if (await isRatable() && _count > 0) {
      await I<Settings>().putInt(Settings.RATING_COUNT, _count - 1);
    }
  }

  Future<void> postponeRating() async {
    await I<Settings>().putInt(Settings.RATING_COUNT, 20);
  }

  Future<void> markRatingDone() async {
    await I<Settings>().putDate(Settings.RATING_DATE, DateService().today);
  }

  Future<bool> isRatable() async {
    return [
      Source.IS_INSTALLED_FROM_HUAWEI_APP_GALLERY,
      Source.IS_INSTALLED_FROM_AMAZON_APP_STORE,
      Source.IS_INSTALLED_FROM_APP_STORE,
      Source.IS_INSTALLED_FROM_PLAY_STORE,
    ].contains(await _installationSource);
  }

  Future<Uri> uriStore() async {
    Source source = await _installationSource;
    switch (source) {
      case Source.IS_INSTALLED_FROM_HUAWEI_APP_GALLERY:
        return Uri.parse('https://appgallery.huawei.com/app/C107457387');
      case Source.IS_INSTALLED_FROM_AMAZON_APP_STORE:
        return Uri.parse('amzn://apps/android?p=me.remem.app');
      case Source.IS_INSTALLED_FROM_APP_STORE:
        return Uri.parse('https://apps.apple.com/app/id1600128209');
      case Source.IS_INSTALLED_FROM_PLAY_STORE:
        return Uri.parse('market://details?id=${AppConfig.googleId}');
      default:
        throw MessageException({
          'messages': ['No URI to $source available.']
        });
    }
  }

  int get _count {
    return I<Settings>().getInt(Settings.RATING_COUNT) ?? _countdownStart;
  }
}
