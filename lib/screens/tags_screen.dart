import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../models/tip.dart';
import '../paths/paths.dart';
import '../util/build_context.dart';
import '../widgets/widgets.dart';

/// Navigation endpoint for tag management
/// Creates a TagScreen
/// Screen for viewing and managing a list of tags
class TagsScreen extends StatelessWidget {
  const TagsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            titleSpacing: screenSize(context) != ScreenSize.small ? 16.0 : 0.0,
            title: Text(L10n.of(context).t8('Tags.title')),
            actions: [
              if (kIsWeb)
                IconButton(
                    tooltip: L10n.of(context).t8('Tags.restore'),
                    icon: const Icon(Icons.restore_from_trash_rounded),
                    onPressed: () => BlocProvider.of<NavigationCubit>(context)
                        .go(TagBinPath())),
              const HelpButton('labels/'),
            ]),
        body: content(context),
        drawer: screenSize(context) == ScreenSize.small ? NavDrawer() : null,
        floatingActionButton: _btnAddTag(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat);
  }

  Widget content(BuildContext ctx) {
    return BlocBuilder<TagsBloc, EntitiesState>(
      builder: (BuildContext ctx, EntitiesState state) {
        if (state is EntitiesLoadSuccess) {
          return Stack(children: [
            if (state.items.isEmpty) empty(ctx),
            listWidget(ctx, state.items as List<TagEntity>),
            if (state.isUpdating) const LoadingIndicator(),
          ]);
        }
        return const LoadingIndicator();
      },
    );
  }

  Widget listWidget(BuildContext ctx, List<TagEntity> tags) {
    return ValueListenableBuilder<Box>(
        valueListenable:
            I<Settings>().listenable(keys: [Settings.TIP_TAG_PUBLISH]),
        builder: (context, bx, wdg) {
          final tips = _tips(context);
          return ListView.builder(
            padding: const EdgeInsets.only(top: 0, bottom: 128),
            itemCount: tags.length + tips.length,
            itemBuilder: (ctx, i) {
              return i < tips.length
                  ? TipTile(tips[i])
                  : tagItem(ctx, tags[i - tips.length]);
            },
          );
        });
  }

  Widget empty(BuildContext ctx) {
    return const EmptyList(icon: Icons.label_rounded);
  }

  List<Tip> _tips(BuildContext ctx) {
    final s = I<Settings>();
    final tipIconColor = Theme.of(ctx).disabledColor;
    return [
      if ((s.getBool(Settings.TIP_TAG_PUBLISH) ?? true) &&
          I<AuthBloc>().state is AccessRemote)
        Tip(
            settingKey: Settings.TIP_TAG_PUBLISH,
            icon: Icon(Icons.public_rounded, color: tipIconColor),
            l10nKey: 'Decks.empty'),
    ];
  }

  Widget tagItem(BuildContext ctx, TagEntity tag) {
    return Column(
      children: [
        ListTile(
          contentPadding: const EdgeInsets.only(left: 8.0),
          tileColor: Theme.of(ctx).colorScheme.surfaceContainerLowest,
          splashColor: Theme.of(ctx).colorScheme.surfaceContainerHigh,
          horizontalTitleGap: 8.0,
          leading: _btnEditTag(ctx, tag),
          title: _tagTitle(ctx, tag),
          onTap: () async => _showEditDialog(ctx, tag),
          trailing: I<AuthBloc>().state is AccessRemote
              ? IntrinsicWidth(
                  child: Row(
                    children: [
                      _btnPublishTag(ctx, tag),
                      _btnViewCollection(ctx, tag),
                    ],
                  ),
                )
              : null,
        ),
        const VerseDivider(),
      ],
    );
  }

  Tooltip _tagTitle(BuildContext ctx, TagEntity tag) {
    return Tooltip(
        message:
            '${L10n.of(ctx).t8('Button.edit')}/${L10n.of(ctx).t8('Button.delete')}',
        child: Text(tag.text));
  }

  Widget _btnViewCollection(BuildContext ctx, TagEntity tag) {
    return SizedBox(
        width: kMinInteractiveDimension,
        child: tag.published
            ? IconButton(
                tooltip: L10n.of(ctx).t8('Deck.preview'),
                icon: const Icon(Icons.preview_rounded),
                onPressed: () => I<NavigationCubit>()
                    .go(CollectionDetailPath(id: tag.id, parent: TagsPath())))
            : null);
  }

  IconButton _btnPublishTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      tooltip: L10n.of(ctx)
          .t8(tag.published ? 'Deck.published' : 'Deck.unpublished'),
      icon:
          Icon(tag.published ? Icons.public_rounded : Icons.public_off_rounded),
      onPressed: () =>
          tag.published ? _updateDeck(ctx, tag) : _createDeck(ctx, tag),
    );
  }

  IconButton _btnDeleteTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      tooltip: L10n.of(ctx).t8('Button.delete'),
      icon: const Icon(Icons.delete_rounded),
      onPressed: () {
        BlocProvider.of<TagsBloc>(ctx)
            .add(EntitiesUpdated<TagEntity>([tag.copyWith(deleted: true)]));
        BlocProvider.of<VersesBloc>(ctx).add(TagDeleted(tag.id));
      },
    );
  }

  Widget _btnEditTag(BuildContext ctx, TagEntity tag) {
    return SizedBox(
      width: kMinInteractiveDimension,
      child: IconButton(
        tooltip:
            '${L10n.of(ctx).t8('Button.edit')}/${L10n.of(ctx).t8('Button.delete')}',
        icon: const Icon(Icons.edit_rounded),
        onPressed: () async => _showEditDialog(ctx, tag),
      ),
    );
  }

  Future<void> _showEditDialog(BuildContext ctx, TagEntity tag) async {
    await showDialog<TagEntity>(
        context: ctx,
        builder: (BuildContext context) {
          final repository = RepositoryProvider.of<TagsRepository>(context);
          final tagsBloc = BlocProvider.of<TagsBloc>(context);
          final versesBloc = BlocProvider.of<VersesBloc>(ctx);
          return TagEditDialog(
              tag: tag,
              onSave: (ctx, tag) async {
                await repository.update(tag);
                tagsBloc.add(EntitiesUpdated<TagEntity>([tag], persist: false));
              },
              onDelete: (ctx, tag) async {
                tag = await repository.update(tag.copyWith(deleted: true));
                tagsBloc.add(EntitiesUpdated<TagEntity>([tag], persist: false));
                versesBloc.add(TagDeleted(tag.id));
              });
        });
  }

  Widget _btnAddTag(BuildContext ctx) {
    return FloatingActionButton(
      onPressed: () async {
        await showDialog<TagEntity>(
            context: ctx,
            builder: (BuildContext context) => TagEditDialog(
                  onSave: (context, tag) async {
                    await RepositoryProvider.of<TagsRepository>(ctx)
                        .create(tag);
                    BlocProvider.of<TagsBloc>(ctx)
                        .add(EntitiesAdded<TagEntity>([tag], persist: false));
                  },
                ));
      },
      backgroundColor: Theme.of(ctx).colorScheme.primaryFixed,
      child: const Icon(
        Icons.add_rounded,
      ),
    );
  }

  Future<void> _createDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    await showDialog<DeckEntity>(
        //useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              tagText: tag.text,
              deck: DeckEntity(id: tag.id, name: tag.text),
              onSave: (context, deck) async {
                final tagsBloc = BlocProvider.of<TagsBloc>(ctx);
                final navigator = Navigator.of(context);
                await repository.create(deck);
                tagsBloc.add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
                navigator.pop(); // get navigator from dialog's context
              },
              onDelete: (context, deck) async {
                Navigator.pop(context); // navigator from dialog's context
              },
            ));
  }

  Future<void> _updateDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    final deck = await repository.read(tag.id);
    await showDialog<DeckEntity>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              tagText: tag.text,
              deck: deck,
              onSave: (context, deck) async {
                await repository.update(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
                Navigator.of(context).pop(); // navigator from dialog's context
              },
              onDelete: (context, deck) async {
                await repository.delete(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: false)],
                    persist: false));
                Navigator.of(context).pop(); // navigator from dialog's context
              },
            ));
  }

  void _share(BuildContext ctx, TagEntity tag) {
    final subject = '${L10n.of(ctx).t8('Collection.title')}: '
        '${tag.text}';
    Share.share('$subject\n$WEB_URL/collections/${tag.id}', subject: subject);
  }
}
