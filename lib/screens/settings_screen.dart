import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/widgets/settings/general_setting.dart';
import 'package:repository/repository.dart';

import '../blocs/current_account/current_account.dart';
import '../blocs/navigation/navigation_cubit.dart';
import '../models/account.dart';
import '../paths/paths.dart';
import '../util/build_context.dart';
import '../widgets/widgets.dart';

/// Navigation endpoint for settings management
/// Screen for accessing the various settings dialogs
class SettingsScreen extends StatelessWidget {
  const SettingsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          titleSpacing: screenSize(context) != ScreenSize.small ? 16.0 : 0.0,
          title: Text(L10n.of(context).t8('Settings.title'))),
      body: content(context),
      drawer: screenSize(context) == ScreenSize.small ? NavDrawer() : null,
    );
  }

  Widget content(BuildContext ctx) {
    return BlocBuilder<AuthBloc, AuthState>(builder: (context, auth) {
      return BlocBuilder<CurrentAccount, Account?>(
          bloc: I<CurrentAccount>(),
          builder: (context, account) {
            return ListView(padding: EdgeInsets.all(16.0), children: [
              if (auth is Access && account?.id != null && account!.id > -1)
                _accountSetting(ctx, auth),
              const Setting(
                l10nKey: 'Setting.general.title',
                form: GeneralSettingForm(),
              ),
              const Setting(
                l10nKey: 'Setting.speech.title',
                form: SpeechSettingForm(),
                icon: Icons.video_settings,
              ),
              // FlashcardSetting(),
              const Setting(
                l10nKey: 'Setting.study.title',
                form: StudySettingForm(),
                icon: Icons.school_rounded,
              ),
              if (!kIsWeb) const ReminderSetting(),
            ]);
          });
    });
  }

  ListTile _accountSetting(BuildContext ctx, AuthState auth) {
    return ListTile(
      leading: const Icon(Icons.folder_shared_rounded),
      title: Text(L10n.of(ctx).t8('Account.title')),
      subtitle: Text(I<CurrentAccount>().name + (auth is AccessLocal ? ' (${L10n.of(ctx).t8('User.local')})' : '')),
      onTap: () {
        I<NavigationCubit>().go(AccountEditPath());
      },
    );
  }
}
