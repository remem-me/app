import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../theme.dart';

typedef OnImportCallback = Function(List<VerseEntity> verses);

class CollectionDetailScreen extends StatelessWidget {
  const CollectionDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CollectionDetailBloc, CollectionDetailState>(
        builder: (BuildContext ctx, CollectionDetailState state) {
      final auth = BlocProvider.of<AuthBloc>(ctx).state;
      if (state is CollectionDetailLoadSuccess) {
        final hasCurrentAccount = I<CurrentAccount>().id > -1;
        return Scaffold(
            appBar: AppBar(
              title: Text(state.collection.name),
              actions: [
                _btnImport(ctx, auth, hasCurrentAccount, state),
                _btnShare(ctx, state),
              ],
            ),
            body: Stack(children: [
              Scrollbar(
                  thumbVisibility: false,
                  child: SingleChildScrollView(
                    primary: true,
                      child: SafeArea(
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      _collectionInfo(ctx, state),
                      const Divider(thickness: 1.0, height: 0),
                      CollectionVersesList(collection: state.collection),
                    ]),
                  ))),
              if (state.isUpdating) const LoadingIndicator(),
            ]));
      } else {
        return Scaffold(appBar: AppBar(), body: const LoadingIndicator());
      }
    });
  }

  IconButton _btnImport(BuildContext ctx, AuthState auth,
      bool hasCurrentAccount, CollectionDetailLoadSuccess state) {
    return IconButton(
        disabledColor: AppTheme.dark.disabledColor,
        tooltip: L10n.of(ctx).t8('Verses.import') +
            (auth is Access
                ? hasCurrentAccount
                    ? ''
                    : ' (${L10n.of(ctx).t8('Account.missing')})'
                : ' (${L10n.of(ctx).t8('User.login.required')})'),
        icon: const Icon(Icons.download_rounded),
        onPressed: auth is Access &&
                hasCurrentAccount &&
                state.collection.verses.isNotEmpty &&
                I<CurrentAccount>().id != state.collection.publisher.id
            ? () => _import(ctx, state)
            : null);
  }

  IconButton _btnShare(BuildContext ctx, CollectionDetailLoadSuccess state) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Collection.share'),
        icon: const Icon(Icons.share_rounded),
        onPressed: () => _share(ctx, state));
  }

  Container _collectionInfo(
      BuildContext ctx, CollectionDetailLoadSuccess state) {
    return Container(
        color: Theme.of(ctx).colorScheme.surfaceContainerLowest,
        child: Material(
            type: MaterialType.transparency,
            child: CollectionContent(
              state.collection,
              hasFixedHeight: false,
            )));
  }

  void _import(
      BuildContext ctx, CollectionDetailLoadSuccess collectionState) async {
    BlocProvider.of<CollectionDetailBloc>(ctx)
        .add(CollectionImported(collectionState.collection.id));
  }

  void _share(BuildContext ctx, CollectionDetailLoadSuccess collectionState) {
    final subject = '${L10n.of(ctx).t8('Collection.title')}: '
        '${collectionState.collection.name}';
    Share.share(
        '$subject\n$WEB_URL/collections/${collectionState.collection.id}',
        subject: subject);
  }
}
