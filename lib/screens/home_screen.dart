import 'dart:async';
import 'dart:math';

import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/home_bar/undo_button.dart';
import 'package:remem_me/widgets/search/search.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../dialogs/dialogs.dart';
import '../util/build_context.dart';

class HomeScreen extends StatefulWidget {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      GlobalKey<ScaffoldState>();
  final bool isBoxedView;

  const HomeScreen(this.isBoxedView, {super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  static final _log = Logger('HomeScreen');
  late TabController _tabController;
  StreamSubscription? _tabBlocSubscription;

  @override
  void initState() {
    super.initState();
    _log.fine('$runtimeType.initState');
    _tabController = _initTabController();
    _tabBlocSubscription = _subscribeToTabBloc();
  }

  @override
  void dispose() {
    _tabBlocSubscription?.cancel();
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    final size = screenSize(ctx);
    switch (size) {
      case ScreenSize.small:
        return _scaffoldSmall(ctx);
      case ScreenSize.medium:
        return _scaffoldMedium(ctx);
      case ScreenSize.large:
        return _scaffoldLarge(ctx);
    }
  }

  TabController _initTabController() {
    // TabBloc listens to TabController
    final tabBloc = I<TabBloc>();
    return TabController(
        vsync: this, length: 3, initialIndex: tabBloc.state.box.index)
      ..addListener(() {
        if (!_tabController.indexIsChanging) {
          final box = VerseBox.values[_tabController.index];
          //I<NavigationCubit>().updateHomeBox(TabState(true, box));
          tabBloc.add(TabUpdated(box));
        }
      });
  }

  StreamSubscription _subscribeToTabBloc() {
    // TabController listens to TabBloc
    final tabBloc = I<TabBloc>();
    return tabBloc.propagateStream.listen((tab) {
      _log.fine('HomeScreenState._subscribeToTabBloc()');
      _log.fine('  _tabController.index=${_tabController.index}');
      _log.fine('  i=$tab');
      if (tab.isBoxed && tab.box.index != _tabController.index) {
        _tabController.index = tab.box.index;
      }
    });
  }

  Scaffold _scaffoldSmall(BuildContext ctx) {
    return Scaffold(
      key: HomeScreen.scaffoldKey,
      appBar: _appBar(ctx),
      body: _tabView(ctx),
      floatingActionButton: _fabRow(ctx),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: NavDrawer(),
      endDrawer: const NavigationTags(),
    );
  }

  Widget _scaffoldMedium(BuildContext ctx) {
    return Scaffold(
      key: HomeScreen.scaffoldKey,
      appBar: _appBar(ctx),
      body: _tabView(ctx),
      floatingActionButton: _fabRow(ctx),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      endDrawer: const NavigationTags(),
    );
  }

  Widget _scaffoldLarge(BuildContext ctx) {
    return Row(children: [
      Expanded(
          flex: 3,
          child: Scaffold(
            key: HomeScreen.scaffoldKey,
            appBar: _appBar(ctx, hasEndDrawer: false),
            body: _tabView(ctx),
            floatingActionButton: _fabRow(ctx),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
          )),
      Expanded(
          flex: 1,
          child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  left: BorderSide(width: 1.0, color: AppColor.dark),
                ),
              ),
              child: const NavigationTags(poppable: false))),
    ]);
  }

  Widget _tabView(BuildContext ctx) {
    if (!TickerMode.of(context)) {
      // Workaround for a regression error in Flutter
      // https://github.com/flutter/flutter/issues/86222#issuecomment-1875046579
      // https://github.com/tomgilder/routemaster/issues/150#issuecomment-882063618
      return const SizedBox();
    }
    return BlocBuilder<CurrentAccount, Account?>(
        bloc: I<CurrentAccount>(),
        builder: (context, account) {
          _log.fine('$runtimeType._tabView account.id ${account?.id}');
          return widget.isBoxedView
              ? TabBarView(
                  key: const ValueKey('tabBarView'),
                  controller: _tabController,
                  children: [
                      BlocProvider<OrderedVersesBloc>.value(
                        value: I<OrderedVersesNewBloc>(),
                        child: OrderedVerses(
                            // see https://api.flutter.dev/flutter/widgets/PageStorage-class.html
                            key: PageStorageKey<String>(
                                '${account?.id}${VerseBox.NEW}')),
                      ),
                      BlocProvider<OrderedVersesBloc>.value(
                        value: I<OrderedVersesDueBloc>(),
                        child: OrderedVerses(
                            key: PageStorageKey<String>(
                                '${account?.id}${VerseBox.DUE}')),
                      ),
                      BlocProvider<OrderedVersesBloc>.value(
                        value: I<OrderedVersesKnownBloc>(),
                        child: OrderedVerses(
                            key: PageStorageKey<String>(
                                '${account?.id}${VerseBox.KNOWN}')),
                      )
                    ])
              : BlocProvider<OrderedVersesBloc>.value(
                  value: I<OrderedVersesAllBloc>(),
                  child: OrderedVerses(
                      key: PageStorageKey<String>(
                          '${account?.id}${VerseBox.ALL}')));
        });
  }

  Widget _transcriptionDialog(BuildContext ctx) {
    return BlocBuilder<TranscriptionCubit, Transcription?>(
        bloc: I<TranscriptionCubit>(),
        builder: (context, transcription) {
          return transcription != null
              ? TranscriptionDialog(transcription)
              : const SizedBox();
        });
  }

  PreferredSizeWidget _appBar(BuildContext ctx, {bool hasEndDrawer = true}) {
    return StackedBar(
        key: const ValueKey('homeBar'),
        base: _homeBar(ctx, hasEndDrawer),
        overlay: BlocBuilder<SelectionBloc, SelectionState>(
            builder: (context, state) {
          return Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: state.selection.isNotEmpty
                  ? SelectionBar()
                  : state.isSearchActive
                      ? SearchVersesBar(hasEndDrawer: hasEndDrawer)
                      : const SizedBox.shrink());
        }));
  }

  PreferredSizeWidget _homeBar(BuildContext ctx, bool hasEndDrawer) {
    return AppBar(
      titleSpacing: screenSize(ctx) != ScreenSize.small ? 16.0 : 0.0,
      title: const HomeTitle(),
      actions: [
        const UndoButton(),
        const SearchVersesButton(),
        if (hasEndDrawer) const FilterIconButton(),
      ],
      bottom: widget.isBoxedView
          ? BoxTabBar(_tabController)
          : const BoxAllBar() as PreferredSizeWidget,
    );
  }

  Widget _fabRow(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(builder:
        (BuildContext ctx, EntitiesState<AccountEntity> accountsState) {
      return Visibility(
          visible: accountsState is EntitiesLoadSuccess<AccountEntity>,
          child: StreamBuilder<PlaybackState>(
              stream: I<TtsHandler>().playbackState,
              builder: (ctx, snapshot) => snapshot.data?.processingState ==
                      AudioProcessingState.ready
                  ? ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight:
                            max(0, MediaQuery.of(context).size.height - 96.0),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Expanded(child: _transcriptionDialog(ctx)),
                          const TtsFabRow(),
                        ],
                      ),
                    )
                  : const BoxFabRow()));
    });
  }
}
