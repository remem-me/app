import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/widgets.dart';

import '../theme.dart';
import '../util/build_context.dart';

class ScoresScreen extends StatelessWidget {
  const ScoresScreen({super.key});

  @override
  Widget build(BuildContext ctx) {
    final size = screenSize(ctx);
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          titleSpacing: screenSize(ctx) != ScreenSize.small ? 16.0 : 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(L10n.of(ctx).t8('Scores.title')),
              Text(
                L10n.of(ctx).t8('Scores.summary'),
                style: TextStyle(fontSize: 14.0, color: AppTheme.appBar.foregroundColor),
              ),
            ],
          ),
        ),
        body: Column(
          children: [
            TabBar(
              labelColor: Theme.of(ctx).colorScheme.primary,
              indicatorColor: Theme.of(ctx).colorScheme.primary,
              indicatorWeight: 3,
              tabs: [
                Tab(text: L10n.of(ctx).t8('Scores.newest.title')),
                Tab(text: L10n.of(ctx).t8('Scores.highest.title')),
                Tab(text: L10n.of(ctx).t8('Scores.total.title')),
              ],
            ),
            Divider(
              height: 0,
              thickness: 1,
              color: Theme.of(ctx).colorScheme.primary,
            ),
            const Expanded(
              child: SafeArea(
                child: TabBarView(
                  children: [
                    NewScores(),
                    HighScores(),
                    TotalScores(),
                  ],
                ),
              ),
            )
          ],
        ),
        drawer: size == ScreenSize.small ? NavDrawer() : null,
      ),
    );
  }
}
