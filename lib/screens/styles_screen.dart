import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../util/util.dart';
import '../widgets/widgets.dart';

class StylesScreen extends StatelessWidget {
  static RootIsolateToken? rootIsolateToken = RootIsolateToken.instance;

  const StylesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final size = screenSize(context);
    return Scaffold(
      appBar: AppBar(
        titleSpacing: screenSize(context) != ScreenSize.small ? 16.0 : 0.0,
        title: const Text('Styles'),
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FutureBuilder(
                future: PackageInfo.fromPlatform(),
                builder: (BuildContext context,
                    AsyncSnapshot<PackageInfo> snapshot) {
                  return Text(
                      'RememberMe version ${snapshot.data?.version ?? ''}');
                },
              ),
              //Text('displayLarge', style: Theme.of(context).textTheme.displayLarge),
              //Text('displayMedium', style: Theme.of(context).textTheme.displayMedium),
              Text('displaySmall',
                  style: Theme.of(context).textTheme.displaySmall),
              Text('headlineLarge',
                  style: Theme.of(context).textTheme.headlineLarge),
              Text('headlineMedium*',
                  style: Theme.of(context).textTheme.headlineMedium),
              Text('headlineSmall',
                  style: Theme.of(context).textTheme.headlineSmall),
              Text('titleLarge', style: Theme.of(context).textTheme.titleLarge),
              Text('titleMedium',
                  style: Theme.of(context).textTheme.titleMedium),
              Text('titleSmall*',
                  style: Theme.of(context).textTheme.titleSmall),
              Text('bodyLarge', style: Theme.of(context).textTheme.bodyLarge),
              Text('bodyMedium* (default text)',
                  style: Theme.of(context).textTheme.bodyMedium),
              Text('bodySmall', style: Theme.of(context).textTheme.bodySmall),
              Text('labelLarge (button)',
                  style: Theme.of(context).textTheme.labelLarge),
              Text('labelMedium',
                  style: Theme.of(context).textTheme.labelMedium),
              Text('labelSmall*',
                  style: Theme.of(context).textTheme.labelSmall),
              Text('colorScheme.primaryFixed',
                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                      color: Theme.of(context).colorScheme.primaryFixed)),
              Text('colorScheme.secondary',
                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                      color: Theme.of(context).colorScheme.secondary)),
              Text('colorScheme.secondaryContainer',
                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                      color: Theme.of(context).colorScheme.secondaryContainer)),
              TextButton(onPressed: () {}, child: const Text('TextButton')),
              ElevatedButton(
                  onPressed: () {}, child: const Text('ElevatedButton')),
              OutlinedButton(
                  onPressed: () async {},
                  child: const Text('OutlinedButton'))
            ],
          )),
      drawer: size == ScreenSize.small
          ? NavDrawer()
          : null,
    );
  }
}
