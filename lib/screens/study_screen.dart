import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/study/shaker.dart';
import 'package:remem_me/widgets/study/study.dart';

import '../theme.dart';
import '../widgets/app_bar/app_bar.dart';
import '../widgets/settings/settings.dart';
import '../widgets/widgets.dart';

class StudyScreen extends StatelessWidget {
  const StudyScreen({super.key});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return Scaffold(
        appBar: StackedBar(
          base: _appBar(state, ctx),
          overlay: _progressIndicator(ctx),
        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Expanded(child: Shaker(child: TextPane(state))),
          Theme(
            data: AppTheme.twilight,
            child: Material(
              elevation: 4.0,
              shadowColor: Colors.black,
              child: Container(
                  color: AppTheme.dark.colorScheme.outlineVariant,
                  child: const StudyAnimator()),
            ),
          ),
        ]),
        resizeToAvoidBottomInset: true,
      );
    });
  }

  AppBar _appBar(StudyState state, BuildContext ctx) {
    return AppBar(
        leading: ([Obfuscation, Puzzle, LineUp, Typing]
                .contains(state.runtimeType))
            ? IconButton(
                tooltip: L10n.of(ctx).t8('Button.back'),
                icon: const Icon(Icons.arrow_back_rounded),
                onPressed: () =>
                    BlocProvider.of<StudyBloc>(ctx).add(const StudyStarted()),
              )
            : null,
        title: Text(_title(ctx, state)),
        actions: const [
          Setting(
            l10nKey: 'Setting.study.title',
            form: StudySettingForm(),
            isCollapsed: true,
          )
        ]);
  }

  String _title(BuildContext ctx, StudyState state) {
    final CurrentAccount currentAccount = I<CurrentAccount>();
    if (state is Study) {
      if (currentAccount.topicPreferred &&
          state.verse.topic != null &&
          state.verse.topic!.isNotEmpty) {
        return state.verse.topic!;
      }
      if (!currentAccount.referenceIncluded) {
        return state.verse.reference;
      }
    }
    return L10n.of(ctx).t8('Study.title');
  }

  Widget _progressIndicator(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return state is Study
          ? ProgressBar(
              countWrong: state.countWrong,
              countToGo: state.countToGo,
              countRight: state.countRight)
          : SizedBox.shrink();
    });
  }
}
