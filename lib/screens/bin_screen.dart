import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../util/build_context.dart';

/// Navigation endpoint for tag management
/// Creates a TagScreen
/// Screen for viewing and managing a list of tags
class BinScreen<E extends Entity, T extends E> extends StatelessWidget {
  final IconData typeIcon;

  const BinScreen({super.key, required this.typeIcon});

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    return BlocBuilder<BinBloc<E, T>, BinState<E>>(
        builder: (BuildContext ctx, BinState<E> state) {
      final hasSelection =
          state is BinLoadSuccess<E> && state.selection.isNotEmpty;
      final l10n = L10n.of(ctx);
      final bloc = BlocProvider.of<BinBloc<E, T>>(ctx);
      return Scaffold(
        appBar: AppBar(
          titleSpacing: screenSize(ctx) != ScreenSize.small ? 16.0 : 0.0,
          title: Text(l10n.t8('Bin.title')),
          actions: [
            if (hasSelection)
              TextButton(
                style: TextButton.styleFrom(
                    foregroundColor: Colors.white, padding: const EdgeInsets.all(16.0)),
                child: Text(l10n.t8('Bin.restore').toUpperCase()),
                onPressed: () => bloc.add(Restored()),
              ),
            if (hasSelection)
              TextButton(
                style: TextButton.styleFrom(
                    foregroundColor: Colors.white, padding: const EdgeInsets.all(16.0)),
                child: Text(l10n.t8('Bin.clear').toUpperCase()),
                // todo: clear bin
                onPressed: () => bloc.add(Shredded()),
              ),
            // if (hasSelection) IconButton(onPressed: () {}, icon: Icon(Icons.restore_from_trash_rounded)),
            // if (!hasSelection) IconButton(onPressed: () {}, icon: Icon(Icons.delete_forever_rounded)),
          ],
        ),
        body: content(ctx, state),
        drawer:
            screenSize(ctx) == ScreenSize.small && nav.state is! TagBinPath
                ? NavDrawer()
                : null,
      );
    });
  }

  Widget content(BuildContext ctx, BinState<E> state) {
    if (state is BinLoadSuccess<E>) {
      return Stack(children: [
        _listWidget(ctx, state),
        if (state.isUpdating) const LoadingIndicator(),
      ]);
    } else {
      return const LoadingIndicator();
    }
  }

  Widget _listWidget(BuildContext ctx, BinLoadSuccess<E> state) {
    return state.entities.isNotEmpty
        ? ListView.builder(
            itemCount: state.entities.length,
            itemBuilder: (ctx, itemPosition) {
              Entity tag = state.entities[itemPosition];
              return _entityItem(ctx, tag as E, state);
            },
          )
        : const EmptyList(icon: Icons.delete_rounded);
  }

  Widget _entityItem(BuildContext ctx, E entity, BinLoadSuccess<E> state) {
    final l10n = L10n.of(ctx);
    final date = DateFormat.yMMMd(l10n.locale.toString())
        .add_Hms()
        .format(DateTime.fromMillisecondsSinceEpoch(entity.modified));
    final isSelected = state.selection.keys.contains(entity.id);
    return ListTile(
      leading: Icon(typeIcon),
      title: Text(entity.label, overflow: TextOverflow.ellipsis),
      subtitle: Text(l10n.t8('Bin.deletedOn', [date])),
      selected: isSelected,
      trailing: Checkbox(
        value: isSelected,
        onChanged: (bool? value) =>
            BlocProvider.of<BinBloc<E, T>>(ctx).add(WasteSelected<E>(entity)),
      ),
    );
  }
}
