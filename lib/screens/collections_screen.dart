import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/search/search.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../util/build_context.dart';

class CollectionsScreen extends StatelessWidget {
  const CollectionsScreen({super.key});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
        builder: (BuildContext ctx, CollectionsState state) {
      return Scaffold(
        appBar: _appBar(ctx, state),
        body: const CollectionsList(),
        drawer: screenSize(ctx) == ScreenSize.small ? NavDrawer() : null,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }

  PreferredSizeWidget _appBar(BuildContext ctx, CollectionsState state) {
    return StackedBar(
      base: _titleBar(ctx, state),
      overlay: Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: state.query.search != null
              ? const GlobalSearch()
              : const SizedBox.shrink()),
    );
  }

  PreferredSizeWidget _titleBar(BuildContext ctx, CollectionsState state) {
    final isAccountRemote = I<CurrentAccount>().state != null &&
        I<AuthBloc>().state is AccessRemote;
    return AppBar(
      titleSpacing: screenSize(ctx) != ScreenSize.small ? 16.0 : 0.0,
      title: const CollectionsTitle(),
      actions: [
        if (isAccountRemote)
          IconButton(
            tooltip: L10n.of(ctx).t8('Deck.publish'),
            icon: const Icon(Icons.publish_rounded),
            onPressed: () => I<NavigationCubit>().go(TagsPath()),
          ),
        IconButton(
          tooltip: L10n.of(ctx).t8('Button.search'),
          icon: const Icon(Icons.search_rounded),
          onPressed: () {
            BlocProvider.of<CollectionsBloc>(ctx).add(SearchDisplayed(true));
          },
        ),
        const HelpButton('collections/')
      ],
      bottom: const QueryBar(),
    );
  }
}
