import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/widgets/home_bar/undo_button.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../dialogs/dialogs.dart';
import '../util/util.dart';
import '../widgets/app_bar/app_bar.dart';

// This is the type used by the popup menu.
enum FlashcardMenuAction { edit, study, share, tags, help }

/// Navigation endpoint for flashcards
/// Listens for the current verse (which keeps changing during a reviewing process)
/// and creates a new flashcard screen for it
class FlashcardScreen extends StatelessWidget {
  final void Function(Verse)? onEdit;
  final void Function(Verse) onStudy;
  final void Function(Verse) onShare;

  const FlashcardScreen(
      {super.key, this.onEdit, required this.onStudy, required this.onShare});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        builder: (BuildContext ctx, FlashcardState state) => Scaffold(
              appBar: StackedBar(
                  base: AppBar(
                    title: Text(_titleText(ctx, state)),
                    actions: state is FlashcardRun ? _menu(ctx, state) : null,
                  ),
                  overlay: state is FlashcardRun
                      ? ProgressBar(
                          countWrong: state.countForgotten,
                          countToGo: state.verses.length,
                          countRight: state.countRemembered)
                      : const SizedBox.shrink()),
              body: FlashcardController(
                  bloc: BlocProvider.of<FlashcardBloc>(ctx)),
            ));
  }

  _titleText(BuildContext ctx, FlashcardState state) {
    if (state is FlashcardRun) {
      if ((state.box == VerseBox.ALL ||
              state.box == VerseBox.KNOWN && !state.inverse ||
              state.reverse) &&
          (!I<CurrentAccount>().referenceIncluded ||
              [VerseService().lines(state.verse).length, 0]
                  .contains(state.revealLines))) {
        return state.verse.topic != null && state.verse.topic!.isNotEmpty
            ? VerseService().title(state.verse)
            : state.verse.reference;
      } else {
        return L10n.of(ctx).t8(state.inverse
            ? 'Flashcard.sayReference'
            : state.box == VerseBox.NEW
                ? 'Flashcard.learnPassage'
                : 'Flashcard.sayPassage');
      }
    } else {
      return '';
    }
  }

  /// Creates a menu for actions related to the verse
  List<Widget> _menu(BuildContext ctx, FlashcardRun run) {
    var verses = run.verses;
    var index = run.index;
    if (BlocProvider.of<FlashcardBloc>(ctx) is CollectionFlashcardBloc) {
      verses = [verses[index]];
      index = 0;
    }
    return <Widget>[
      if (BlocProvider.of<FlashcardBloc>(ctx) is HomeFlashcardBloc &&
          !run.reverse &&
          run.previous != null)
        _undoButton(ctx),
      if (run.reverse)
        TtsButton(
            verses: verses,
            index: index,
            box: run.box,
            spare: _popupMenuButton(ctx, run)),
      if (!run.reverse && run.previous == null) const HelpButton('study/'),
      if (!run.reverse) _popupMenuButton(ctx, run)
    ];
  }

  UndoButton _undoButton(BuildContext ctx) {
    return UndoButton(
      onUndo: () => BlocProvider.of<FlashcardBloc>(ctx)
          .add(const FlashcardAction(VerseAction.UNDONE)),
    );
  }

  PopupMenuButton<FlashcardMenuAction> _popupMenuButton(
      BuildContext ctx, FlashcardRun run) {
    return PopupMenuButton<FlashcardMenuAction>(
      icon: const Icon(Icons.more_vert_rounded),
      onSelected: (FlashcardMenuAction action) => _do(ctx, run, action),
      itemBuilder: (BuildContext context) =>
          <PopupMenuEntry<FlashcardMenuAction>>[
        PopupMenuItem<FlashcardMenuAction>(
          value: FlashcardMenuAction.study,
          child: Text(L10n.of(ctx).t8('Study.title')),
        ),
        if (onEdit != null)
          PopupMenuItem<FlashcardMenuAction>(
            value: FlashcardMenuAction.edit,
            child: Text(L10n.of(ctx).t8('Button.edit')),
          ),
        if (onEdit != null)
          PopupMenuItem<FlashcardMenuAction>(
            value: FlashcardMenuAction.tags,
            child: Text(L10n.of(ctx).t8('Tags.title')),
          ),
        PopupMenuItem<FlashcardMenuAction>(
          value: FlashcardMenuAction.share,
          child: Text(L10n.of(ctx).t8('Verse.share.title')),
        ),
        PopupMenuItem<FlashcardMenuAction>(
          value: FlashcardMenuAction.help,
          child: Text(L10n.of(ctx).t8('Documentation.title')),
        ),
      ],
    );
  }

  /// Perfoms actions initiated from the menu
  void _do(
      BuildContext ctx, FlashcardRun run, FlashcardMenuAction action) async {
    switch (action) {
      case FlashcardMenuAction.edit:
        return onEdit!(run.verse);
      case FlashcardMenuAction.study:
        return onStudy(run.verse);
      case FlashcardMenuAction.share:
        return onShare(run.verse);
      case FlashcardMenuAction.tags:
        return _tags(ctx, run.verse);
      case FlashcardMenuAction.help:
        return _help(ctx);
    }
  }

  Future _tags(BuildContext ctx, Verse verse) async {
    final flashcardBloc = BlocProvider.of<FlashcardBloc>(ctx);
    final updatedVerses = await showDialog<List<Verse>>(
        context: ctx,
        builder: (BuildContext context) {
          return TagSelectionDialog(
            tags: List.from((BlocProvider.of<TagsBloc>(ctx).state
                    as EntitiesLoadSuccess<TagEntity>)
                .items),
            selection: {verse.id: verse},
          );
        });
    if (updatedVerses != null) {
      flashcardBloc.add(FlashcardVerseUpdated(updatedVerses.first));
      I<VersesBloc>().add(EntitiesUpdated(updatedVerses, persist: false));
      I<UndoBloc>().add(UndoAdded([
        Undo(EntityAction.update, [verse])
      ]));
    }
  }

  void _help(BuildContext ctx) {
    launchDocumentation(ctx, path: 'study/')();
  }
}
