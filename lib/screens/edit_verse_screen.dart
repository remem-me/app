import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/bible_service.dart';
import 'package:remem_me/services/photos_service.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/bible/bible.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../widgets/widgets.dart';

class EditVerseScreen extends EntityForm<VerseEntity> {
  final SharedVerse? shared;

  EditVerseScreen(
      {super.key, required super.onSave, Verse? verse, String? sharedData})
      : shared = (sharedData != null ? SharedVerse(sharedData) : null),
        super(entity: verse?.toEntity(), poppable: true);

  @override
  EditVerseScreenState createState() => EditVerseScreenState();
}

class EditVerseScreenState
    extends EntityFormState<VerseEntity, EditVerseScreen> {
  final _referenceKey = GlobalKey<FormFieldState>();
  final _sourceKey = GlobalKey<FormFieldState>();
  final _passageKey = GlobalKey<FormFieldState>();
  final _imageKey = GlobalKey<FormFieldState>();
  late final CurrentAccount _currentAccount;

  String? _reference;
  String? _passage;
  String? _topic;
  String? _source;
  String? _image;
  int? _level;

  BibleQuery? _query;
  var _includeNumbers = false;

  EditVerseScreenState() {
    _currentAccount = I<CurrentAccount>();
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _includeNumbers = I<Settings>().getBool(Settings.INCLUDE_NUMBERS) ?? true;
      //errors = {'reference': ['not recognized']};
    });
    I<BibleService>()
        .init(_currentAccount.language)
        .then((_) => _createQuery());
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final settings = I<Settings>();
    return Scaffold(
        appBar: AppBar(
            title: _query == null
                ? Text(widget.entity?.reference ??
                    L10n.of(context).t8('Verse.add'))
                : null,
            leading: IconButton(
              tooltip: L10n.of(context).t8('Button.save'),
              icon: const Icon(Icons.save_rounded),
              onPressed: saveForm,
            ),
            actions: [
              if (_query != null) ...[
                Tooltip(
                    message: L10n.of(context).t8('Verse.numbers'),
                    child: const Icon(Icons.format_list_numbered_rounded)),
                Theme(
                  data: theme.copyWith(
                      colorScheme: theme.colorScheme
                          .copyWith(outline: Colors.transparent)),
                  child: Switch(
                      // todo: set colorscheme.outline in ad-hoc theme
                      value: _includeNumbers,
                      activeColor: AppTheme.dark.colorScheme.secondary,
                      inactiveTrackColor: Colors.white30,
                      inactiveThumbColor: Colors.white70,
                      //AppTheme.light.colorScheme.surfaceContainerHighest,
                      onChanged: (value) {
                        Settings().putBool(Settings.INCLUDE_NUMBERS, value);
                        setState(() => _includeNumbers = value);
                      }),
                ),
                const SizedBox(width: 96)
              ] else
                const HelpButton('verses/')
            ]),
        body: Stack(children: [
          Form(
              key: formKey,
              onChanged: _createQuery,
              child: Scrollbar(
                child: SingleChildScrollView(
                    child: Padding(
                  padding: const EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 16.0),
                  // listen to tips getting removed from within the form
                  child: ValueListenableBuilder<Box>(
                      valueListenable: settings.listenable(keys: [
                        Settings.TIP_VERSE_FETCH,
                        Settings.TIP_VERSE_FORMAT,
                        Settings.TIP_VERSE_LANGUAGE
                      ]),
                      builder: (context, bx, wdg) {
                        return SafeArea(
                            child: Column(children: fields(context)));
                      }),
                )),
              )),
          if (isUpdating) const LoadingIndicator(),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: _query != null
            ? FloatingActionButton(
                backgroundColor: AppTheme.dark.colorScheme.secondaryFixed,
                onPressed: () => _retrievePassage(context),
                child: const Icon(Icons.menu_book_rounded))
            : null);
  }

  void _retrievePassage(BuildContext ctx) async {
    final result = await showDialog<String>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) {
          return BibleWeb(_query!);
        });
    if (result != null) _passageKey.currentState!.didChange(result);
  }

  @override
  VerseEntity updatedEntity() {
    return widget.entity!.copyWith(
        reference: _reference,
        passage: _passage!.replaceAll('\r', ''),
        topic: _topic,
        source: _source,
        image: _image,
        level: _level,
        nullValues: [
          if (_topic == null) 'topic',
          if (_source == null) 'source',
          if (_image == null) 'image',
        ]);
  }

  @override
  VerseEntity createdEntity() {
    if (_currentAccount.id < 0) {
      throw MessageException({
        'messages': [L10n.of(context).t8('Account.missing')]
      });
    }
    final tagged =
        BlocProvider.of<TaggedVersesBloc>(context).state as TaggedVersesSuccess;
    final tags = Map.fromEntries(tagged.tags
        .where((tag) => tag.included == true)
        .map((tag) => MapEntry(tag.id, tag.label)));
    return VerseEntity(_reference!, _passage!,
        topic: _topic,
        source: _source,
        image: _image,
        level: _level ?? -1,
        accountId: _currentAccount.id,
        tags: tags);
  }

  @override
  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        ..._referenceRows(),
        ..._sourceAndTopicRows(),
        ..._imageRows(),
        ..._passageRows(),
        if (widget.entity != null && widget.entity!.level >= 0) ..._levelRows(),
      ]);
  }

  List<Widget> _referenceRows() {
    String key = _currentAccount.langRef.languageCode == 'zh'
        ? '${_currentAccount.langRef.languageCode}_${_currentAccount.langRef.scriptCode ?? 'Hant'}'
        : _currentAccount.langRef.languageCode;
    final referenceMap = {
      ...REFERENCE_DEFAULTS,
      'zh_Hans': REFERENCE_DEFAULTS['zh_CN'],
      'zh_Hant': REFERENCE_DEFAULTS['zh_TW'],
    };
    return [
      if ((I<Settings>().getBool(Settings.TIP_VERSE_FETCH) ?? true) &&
          (widget.shared == null && _query == null)) ...[
        TipRow(Tip(
          l10nKey: 'Tip.verse.fetch',
          icon: Icon(Icons.menu_book_rounded,
              color: Theme.of(context).disabledColor),
          settingKey: Settings.TIP_VERSE_FETCH,
        )),
        SizedBox(height: 8.0),
      ] else
        SizedBox(height: spacing),
      StringAutocomplete(
        l10nKey: 'Verse.reference',
        l10nKeySummary: L10n.of(context).t8('Verse.reference.summary',
            [referenceMap[key] ?? referenceMap['en']!]),
        optionsBuilder: _bookOptionBuilder,
        initialValue: widget.shared?.reference ?? widget.entity?.reference,
        fieldKey: _referenceKey,
        textDirection: _currentAccount.langRefDirection,
        autofocus: widget.entity == null,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: Validators.compose([
          Validators.error<String?>(errors['reference'], _reference),
          Validators.required<String?>(context),
          Validators.maxLength(context, 500),
        ]),
        onSaved: (value) => _reference = value,
      )
    ];
  }

  List<Widget> _sourceAndTopicRows() {
    final passageValue = _passageKey.currentState?.value;
    return [
      if ((I<Settings>().getBool(Settings.TIP_VERSE_LANGUAGE) ?? true) &&
          (passageValue == null || passageValue.isEmpty)) ...[
        TipRow(Tip(
          l10nKey: 'Tip.verse.language',
          icon: Icon(Icons.language_rounded,
              color: Theme.of(context).disabledColor),
          settingKey: Settings.TIP_VERSE_LANGUAGE,
        )),
        SizedBox(height: 8.0)
      ] else
        SizedBox(height: spacing),
      Row(
        children: [
          SizedBox(
              width: 128,
              child: StringInput(
                  fieldKey: _sourceKey,
                  l10nKey: 'Verse.source',
                  initialValue: widget.shared?.source ??
                      widget.entity?.source ??
                      (widget.shared?.passage == null
                          ? _currentAccount.defaultSource ??
                              BIBLE_DEFAULTS[
                                  _currentAccount.language.languageCode]
                          : null),
                  optionBuilder: _sourceOptionBuilder,
                  optionTooltip: L10n.of(context).t8('Bible.versions'),
                  validator: Validators.maxLength(context, 250),
                  onSaved: (value) => _source = value?.trim() ?? '')),
          SizedBox(width: spacing),
          Expanded(
            child: StringInput(
                l10nKey: 'Verse.topic',
                initialValue: widget.entity?.topic,
                validator: Validators.maxLength(context, 1000),
                onSaved: (value) => _topic = value?.trim()),
          ),
        ],
      ),
    ];
  }

  List<Widget> _imageRows() {
    return [
      SizedBox(height: spacing),
      StringInput(
        fieldKey: _imageKey,
        l10nKey: 'Verse.image',
        initialValue: widget.entity?.image,
        inputFormatters: [
          FilteringTextInputFormatter.deny(RegExp(' ')),
        ],
        validator: Validators.compose([
          Validators.url(context),
          Validators.maxLength(context, 1500),
        ]),
        onSaved: (value) => _image = value?.trim(),
        actionButton: IconButton(
            onPressed: () => _retrieveImage(context),
            icon: const Icon(Icons.image)),
      ),
    ];
  }

  List<Widget> _passageRows() {
    final currentValue = _passageKey.currentState?.value;
    return [
      if ((I<Settings>().getBool(Settings.TIP_VERSE_FORMAT) ?? true) &&
          (currentValue is String && currentValue.isNotEmpty)) ...[
        TipRow(Tip(
          l10nKey: 'Tip.verse.format',
          icon: Icon(Icons.info_outline_rounded,
              color: Theme.of(context).disabledColor),
          settingKey: Settings.TIP_VERSE_FORMAT,
        )),
        SizedBox(height: 8.0),
      ] else
        SizedBox(height: spacing),
      StringInput(
          fieldKey: _passageKey,
          maxLines: null,
          minLines: 3,
          l10nKey: 'Verse.passage',
          textDirection: _currentAccount.languageDirection,
          initialValue: widget.shared?.passage ?? widget.entity?.passage,
          validator: Validators.compose([
            Validators.required<String?>(context),
            Validators.maxLength(context, 50000),
          ]),
          onSaved: (value) => _passage = value),
    ];
  }

  List<Widget> _levelRows() {
    return [
      SizedBox(height: spacing),
      IntegerInput(
          l10nKey: 'Verse.level',
          initialValue: widget.entity?.level,
          validator: Validators.compose([
            Validators.minimum(context, 0),
            Validators.maximum(context, 99)
          ]),
          onSaved: (value) => _level = value),
    ];
  }

  void _retrieveImage(BuildContext ctx) async {
    final result = await showDialog<String>(
        useRootNavigator: false,
        context: ctx,
        builder: (BuildContext context) {
          return BlocProvider<PhotosBloc>(
              create: (BuildContext ctx) {
                return PhotosBloc(PhotosService())..add(PhotosInitiated());
              },
              child: const ImageDialog());
        });
    if (result != null) _imageKey.currentState!.didChange(result);
  }

  void _createQuery() {
    if (_referenceKey.currentState!.value != null &&
        _sourceKey.currentState!.value != null) {
      setState(() {
        _query = I<BibleService>().query(_sourceKey.currentState!.value,
            _referenceKey.currentState!.value, _currentAccount.books);
      });
    }
  }

  FutureOr<Iterable<String>> _bookOptionBuilder(TextEditingValue value) {
    return _currentAccount.books.entries
        .where((entry) =>
            RegExp(r'^' + entry.key + r'$').hasMatch(value.text.toLowerCase()))
        .map((entry) => '${entry.value.name!} ');
  }
}

OptionBuilder _sourceOptionBuilder = (BuildContext context) {
  if (BibleService().versions != null) {
    final sortedVersions = BibleService().versions!.entries.toList()
      ..sort((a, b) => a.value.name!.compareTo(b.value.name!));
    return sortedVersions
        .map((entry) => PopupMenuItem<String>(
              value: entry.key,
              child: Text(entry.value.name!),
            ))
        .toList();
  } else {
    return [];
  }
};
