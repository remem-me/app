import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/start/basic_info.dart';
import 'package:remem_me/widgets/user/user_form.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../paths/paths.dart';
import '../util/build_context.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({super.key});

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  final StartBloc _startBloc = I<StartBloc>();
  final Map<String, BuildContext> _dialogContexts = {};
  late StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _subscription =
        _startBloc.stream.startWith(_startBloc.state).listen((state) {
      _closeAllDialogs();
      if (state is LogInFeedback || state is ForgotPasswordFeedback) {
        _logIn(state as StartFeedback);
      } else if (state is SignUpFeedback) {
        _signUp(state);
      } else if (state is ResetPasswordFeedback) {
        _resetPassword(state);
      } else if (state is StartInfo) {
        _showInfo(state);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: I<StartBloc>(),
        buildWhen: (prev, next) =>
            (prev is StartInProgress || next is StartInProgress),
        builder: (ctx, state) {
          debugPrint('StartScreen.builder $state');
          return Scaffold(
              appBar: AppBar(
                  title: Text(L10n.of(ctx).t8('App.name')),
                  titleSpacing: 16.0,
                  actions: [
                    IconButton(
                      tooltip: L10n.of(ctx).t8('Documentation.title'),
                      onPressed: launchDocumentation(ctx, path: 'accounts/'),
                      icon: const Icon(Icons.help_outline_rounded),
                    ),
                    if (kDebugMode)
                      const Setting(
                        l10nKey: 'Settings.title',
                        form: ServerSettingForm(),
                        isCollapsed: true,
                      )
                  ]),
              body: Stack(children: [
                Column(children: [
                  Expanded(
                      child: SafeArea(
                        child: BasicInfo(
                            onLogin: () => I<StartBloc>().add(LogInInitiated()),
                            onRegister: () =>
                                I<StartBloc>().add(SignUpInitiated())),
                      )),
                ]),
                if (state is StartInProgress) const LoadingIndicator(),
              ]));
        });
  }

  Future<void> _logIn(StartFeedback state) {
    return _showDialog(
        context: context,
        state: state,
        dialog: Dialog(
            child: UserForm(
                errors: state.messages,
                initialValues: state.credentials,
                hasRepeatPassword: false,
                titleL10nKey: 'User.login.title',
                submitL10nKey: 'User.login.submit',
                onSubmit: (credentials) {
                  I<StartBloc>().add(LoggedIn(
                      email: credentials['email']!,
                      password: credentials['password']!));
                },
                onForgotPassword: (credentials) {
                  I<StartBloc>()
                      .add(PasswordForgotten(email: credentials['email']!));
                })));
  }

  Future<void> _signUp(SignUpFeedback state) {
    return _showDialog(
        context: context,
        state: state,
        dialog: Dialog(
            child: UserForm(
          errors: state.messages,
          initialValues: state.credentials,
          hasRepeatPassword: true,
          titleL10nKey: 'User.register.title',
          submitL10nKey: 'User.register.submit',
          cancelL10nKey: kIsWeb ? 'Start.collections' : 'User.local',
          onSubmit: (credentials) {
            I<StartBloc>().add(SignedUp(
                email: credentials['email']!,
                password: credentials['password']!));
          },
          onCancel: () {
            kIsWeb
                ? I<NavigationCubit>().go(CollectionsPath())
                : I<AuthBloc>().add(Permitted());
            I<StartBloc>().add(DialogDismissed(state.dialogId));
          },
        )));
  }

  Future<void> _resetPassword(ResetPasswordFeedback state) {
    return _showDialog(
        context: context,
        state: state,
        dialog: Dialog(
            child: UserForm(
          errors: state.messages,
          initialValues: state.credentials,
          hasUsername: false,
          hasRepeatPassword: true,
          titleL10nKey: 'User.resetPassword.title',
          submitL10nKey: 'User.resetPassword.submit',
          onSubmit: (credentials) {
            I<StartBloc>()
                .add(PasswordSubmitted(password: credentials['password']!));
          },
        )));
  }

  Future<void> _showInfo(StartInfo state) {
    return _showDialog(
        context: context,
        state: state,
        dialog: AlertDialog(
            title: Text(state.title),
            content: SingleChildScrollView(
                child: ListBody(children: <Widget>[
              Text(state.message),
            ])),
            actions: <Widget>[
              TextButton(
                  child: Text(L10n.of(context).t8('Button.ok').toUpperCase()),
                  onPressed: () {
                    I<StartBloc>().add(DialogDismissed(state.dialogId));
                  })
            ]));
  }

  Future<void> _showDialog({
    required BuildContext context,
    required StartDialogState state,
    required Widget dialog,
  }) {
    return showDialog<void>(
        context: context,
        builder: (BuildContext dialogContext) {
          _addDialogContext(state.dialogId, dialogContext);
          return dialog;
        }).then((_) {
      // the dialog got closed and doesn't need to be tracked anymore
      _dialogContexts.remove(state.dialogId);
      // update the bloc state accordingly
      I<StartBloc>().add(DialogDismissed(state.dialogId));
    });
  }

  void _addDialogContext(String dialogId, BuildContext ctx) {
    _dialogContexts[dialogId] = ctx;
  }

  void _closeAllDialogs() {
    for (var entry in _dialogContexts.entries) {
      if (entry.value.mounted) {
        Navigator.of(entry.value).pop();
      }
    }
    _dialogContexts.clear();
  }

  void _closeOtherDialogs(String dialogId) {
    for (var entry in _dialogContexts.entries) {
      if (entry.key != dialogId && entry.value.mounted) {
        Navigator.of(entry.value).pop();
      }
    }
    _dialogContexts.clear();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }
}
