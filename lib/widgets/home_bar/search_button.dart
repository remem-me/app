import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';

class SearchVersesButton extends StatelessWidget {
  const SearchVersesButton({super.key});


  @override
  Widget build(BuildContext context) {
    return IconButton(
      tooltip: L10n.of(context).t8('Button.search'),
      icon: Builder(builder: (ctx) {
        final filteredState = ctx.watch<FilteredVersesBloc>().state;
        return _hasQuery(filteredState)
            ? _iconHasQuery(ctx) : _iconDefault(ctx);
      }),
      onPressed: () {
        BlocProvider.of<SelectionBloc>(context).add(SearchActivated());
      },
    );
  }

  bool _hasQuery(
      FilteredVersesState filteredState) {
    if (filteredState is FilteredVersesSuccess &&
        (filteredState.queryFilter.query.length ?? 0) > 0) return true;
    return false;
  }

  Icon _iconDefault(BuildContext ctx) {
    return const Icon(Icons.search_rounded);
  }

  Icon _iconHasQuery(BuildContext ctx) {
    return Icon(Icons.search_rounded, color: AppColorScheme.dark.secondary);
  }
}
