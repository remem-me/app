import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';
import '../../screens/screens.dart';

class FilterIconButton extends StatelessWidget {
  const FilterIconButton({super.key});

  @override
  Widget build(BuildContext context) {
    return

      IconButton(
        tooltip: L10n.of(context).t8('Tags.navigation'),
        icon: Builder(builder: (ctx) {
          final filteredState = ctx.watch<FilteredVersesBloc>().state;
          final tagsState = ctx.watch<TaggedVersesBloc>().state;
          return _anyIncluded(filteredState, tagsState)
              ? _iconIncluded(ctx)
              : _anyExcluded(filteredState, tagsState)
              ? _iconExcluded(ctx)
              : _iconDefault(ctx);
        }),
        onPressed: () => HomeScreen.scaffoldKey.currentState!.openEndDrawer(),
      );
  }

  bool _anyIncluded(
      FilteredVersesState filtered, TaggedVersesState tagged) {
    if (filtered is FilteredVersesSuccess) {
        if(filtered.queryFilter.query.isNotEmpty) return false;
        if(filtered.reviewedTodayFilter.reviewedToday == true) return true;
    }
    if (tagged is TaggedVersesSuccess &&
        tagged.tags.any((tag) => tag.included == true && !tag.deleted)) {
      return true;
    }
    return false;
  }

  bool _anyExcluded(
      FilteredVersesState filtered, TaggedVersesState tagged) {
    if (filtered is FilteredVersesSuccess) {
      if (filtered.queryFilter.query.isNotEmpty) return false;
      if (filtered.reviewedTodayFilter.reviewedToday == false) return true;
    }
    if (tagged is TaggedVersesSuccess &&
        tagged.tags.any((tag) => tag.included == false && !tag.deleted)) {
      return true;
    }
    return false;
  }

  Icon _iconDefault(BuildContext ctx) {
    return const Icon(Icons.visibility_outlined);
  }

  Icon _iconExcluded(BuildContext ctx) {
    return Icon(Icons.visibility_off_outlined,
        color: AppColorScheme.dark.secondary);
  }

  Icon _iconIncluded(BuildContext ctx) {
    return Icon(Icons.visibility, color: AppColorScheme.dark.secondary);
  }
}
