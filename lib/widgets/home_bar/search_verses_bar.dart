import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/home_bar/filter_icon_button.dart';

class SearchVersesBar extends StatelessWidget {
  final bool hasEndDrawer;

  const SearchVersesBar({
    super.key,
    required this.hasEndDrawer,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
        elevation: 0,
        title: _searchField(context),
        leading: _clearSearchBtn(context),
        actions: [
          if (hasEndDrawer) const FilterIconButton(),
        ]);
  }

  IconButton _clearSearchBtn(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.cancel'),
        icon: const Icon(Icons.close_rounded),
        onPressed: () {
          BlocProvider.of<SelectionBloc>(ctx).add(SearchDeactivated());
          BlocProvider.of<FilteredVersesBloc>(ctx)
              .add(const SearchFilterUpdated(''));
        });
  }

  Widget _searchField(BuildContext ctx) {
    return TextFormField(
      autofocus: true,
      initialValue: (BlocProvider.of<FilteredVersesBloc>(ctx).state
              as FilteredVersesSuccess)
          .queryFilter
          .query,
      decoration: InputDecoration(
        hintText: L10n.of(ctx).t8('Button.search'),
      ),
      onChanged: (String value) {
        BlocProvider.of<FilteredVersesBloc>(ctx)
            .add(SearchFilterUpdated(value));
      },
    );
  }

  IconButton _confirmSearchBtn(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.apply'),
        icon: const Icon(Icons.check_rounded),
        onPressed: () {
          BlocProvider.of<SelectionBloc>(ctx).add(SearchDeactivated());
        });
  }
}
