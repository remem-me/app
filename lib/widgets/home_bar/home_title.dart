import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../models/models.dart';

class HomeTitle extends StatelessWidget {
  const HomeTitle({super.key});

  @override
  Widget build(BuildContext context) => BlocBuilder<CurrentAccount, Account?>(
      bloc: I<CurrentAccount>(),
      builder: (context, account) {
        return Text(account != null
            ? I<CurrentAccount>().state!.name
            : L10n.of(context).t8('App.name'));
      });
}
