import 'dart:ui_web' as ui_web;
import 'package:web/web.dart' as web;

import 'package:flutter/material.dart';

class WebFrame extends StatelessWidget {
  final String url;

  const WebFrame(this.url, {super.key});

  @override
  Widget build(BuildContext context) {
    ui_web.platformViewRegistry.registerViewFactory(
      url,
          (int viewId, {Object? params}) {
        // Create and return an HTML Element from here
        final web.HTMLIFrameElement iframe = web.HTMLIFrameElement()
          ..style.width = '100%'
          ..style.height = '100%'
          ..src = url
          ..style.border = 'none';
        return iframe;
      },
    );
    return HtmlElementView(viewType: url, key: UniqueKey());
  }

}