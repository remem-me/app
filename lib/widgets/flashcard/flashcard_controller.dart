import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/loading_indicator.dart';

import 'flashcard.dart';

class FlashcardController extends StatefulWidget {
  final FlashcardBloc bloc;

  const FlashcardController({super.key, required this.bloc});

  @override
  State<StatefulWidget> createState() {
    return _FlashcardControllerState();
  }
}

class _FlashcardControllerState extends State<FlashcardController> {
  static final _log = Logger('_FlashcardControllerState');
  final _animatorKey = GlobalKey<FlashcardAnimatorState>();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FlashcardBloc, FlashcardState>(
      bloc: widget.bloc,
      listener: (ctx, state) {
        if (state is FlashcardEnd) {
          Navigator.pop(ctx);
        }
      },
      builder: (ctx, state) {
        _log.fine('FlashcardController.blocBuilder $state');
        if (state is FlashcardRun) {
          if (!state.visible) {
            _animatorKey.currentState?.reset();
            widget.bloc.add(FlipReset());
            return const SizedBox.shrink();
          }
          return BlocProvider<VoiceBloc>(
              create: (_) => VoiceBloc(widget.bloc),
              child: ClipRect(
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                    child: SafeArea(
                      child: Column(
                        verticalDirection: VerticalDirection.up,
                        children: <Widget>[
                          Container(
                              margin:
                                  const EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0),
                              height: 56.0,
                              child: _buildToolbar(state)),
                          Expanded(
                              child: Container(
                                  margin: const EdgeInsets.fromLTRB(
                                      0.0, 4.0, 0.0, 0.0),
                                  constraints:
                                      const BoxConstraints(maxWidth: 800),
                                  child: FlashcardAnimator(
                                      key: _animatorKey, bloc: widget.bloc))),
                        ],
                      ),
                    ),
                  ),
                ),
              ));
        } else {
          return const LoadingIndicator();
        }
      },
    );
  }

  Widget _buildToolbar(FlashcardRun state) {
    if (state.flipping) return const SizedBox.shrink();
    if (!state.reverse) {
      return FlipToolbar(
        animatorKey: _animatorKey,
        bloc: widget.bloc,
      );
    }
    switch (state.box) {
      case VerseBox.NEW:
        return CommitToolbar(animatorKey: _animatorKey, bloc: widget.bloc);
      case VerseBox.ALL:
        return FlipToolbar(animatorKey: _animatorKey, bloc: widget.bloc);
      case VerseBox.KNOWN:
        return state.inverse
            ? ReviewToolbar(
                animatorKey: _animatorKey, bloc: widget.bloc, run: state)
            : FlipToolbar(animatorKey: _animatorKey, bloc: widget.bloc);
      case VerseBox.DUE:
      default:   // running on a collection
        return ReviewToolbar(
            animatorKey: _animatorKey, bloc: widget.bloc, run: state);
    }
  }
}
