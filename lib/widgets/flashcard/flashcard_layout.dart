import 'package:app_core/app_core.dart';
import 'package:blur/blur.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../theme.dart';

abstract class FlashcardLayout extends StatelessWidget {
  const FlashcardLayout({
    super.key,
    required this.run,
    this.onTap,
    this.onLongPress,
  });

  final FlashcardRun run;
  final Function? onTap;
  final void Function()? onLongPress;

  Alignment get gradientBegin;

  Alignment get gradientEnd;

  Widget content(BuildContext ctx);

  @override
  Widget build(BuildContext ctx) {
    final layers = backgroundLayers();
    layers.add(Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.circular(8),
        onTap: onTap as void Function()?,
        onLongPress: onLongPress,
        child: Theme(data: AppTheme.dark.copyWith(canvasColor: Colors.transparent), child: content(ctx)),
      ),
    ));
    return Card(
      elevation: 4,
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
      child: Stack(children: layers),
    );
  }

  List<Widget> backgroundLayers();

  Widget levelBackground() {
    // todo: get default color from account setting
    return Stack(children: [
      Container(
          color: run.verse.color ?? AppColor.base, margin: const EdgeInsets.all(1.0)),
      BackgroundUtil.lightGradientLayer(begin: gradientBegin, end: gradientEnd)
    ]);
  }

  Widget blurredImageBackground() {
    return Blur(
        blur: 2.5,
        blurColor: Colors.black,
        child: imageBackground(),
      );
  }

  Widget imageBackground() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              run.verse.image!,  // todo: if isWeb use CORS proxy, encode URL as query param using Uri.encodeComponent
            )),
      ),
    );
  }

  Widget defaultBackground() {
    return levelBackground();
  }

  List<Shadow> textShadows() {
    return run.verse.image != null
        ? [
            const Shadow(
              blurRadius: 8.0,
              color: Colors.black,
              offset: Offset(0.0, 1.0),
            ),
          ]
        : [];
  }
}
