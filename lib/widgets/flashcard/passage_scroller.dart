import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/verse_service.dart';

import '../../theme.dart';

class PassageScroller extends StatefulWidget {
  final FlashcardRun run;

  const PassageScroller({super.key, required this.run});

  @override
  State<StatefulWidget> createState() {
    return _PassageScrollerState();
  }
}

class _PassageScrollerState extends State<PassageScroller> {
  final ScrollController _scrollController =
      ScrollController(initialScrollOffset: 0);

  @override
  void initState() {
    super.initState();
    final lines =
        VerseService().lines(widget.run.verse, referenceIncluded: false);
    if ((widget.run.revealLines > 0 &&
        widget.run.revealLines <= lines.length)) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      });
    }
  }

  @override
  Widget build(BuildContext ctx) {
    final account = I<CurrentAccount>();
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewport) =>
            RawScrollbar(
                thumbColor: AppTheme.dark.focusColor,
                thickness: 8,
                radius: const Radius.circular(8),
                mainAxisMargin: 8,
                controller: _scrollController,
                thumbVisibility: true,
                child: ScrollConfiguration(
                    behavior: ScrollConfiguration.of(context)
                        .copyWith(scrollbars: false),
                    child: SingleChildScrollView(
                        key: const PageStorageKey<String>('passage_scroller'),
                        controller: _scrollController,
                        child: Container(
                            padding: const EdgeInsets.all(16.0),
                            constraints: BoxConstraints(
                              minHeight: viewport.maxHeight,
                              minWidth: double.infinity,
                            ),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  _body(account),
                                ]))))));
  }

  Directionality _body(CurrentAccount account) {
    return Directionality(
      textDirection: account.languageDirection,
      child: MarkdownBody(
          styleSheet: MarkdownStyleSheet(
            h4: TextStyle(fontSize: 14.0),
            p: AppTheme.dark.textTheme.bodyLarge?.copyWith(
                height: 1.5, fontSize: 16.0, fontFamily: account.font),
            a: AppTheme.dark.textTheme.bodySmall
                ?.copyWith(fontSize: 12.0, fontFamily: account.font),
            blockSpacing: 16.0,
            blockquoteDecoration: BoxDecoration(
              color: Colors.grey[500]?.withValues(alpha: 0.38),
              borderRadius: BorderRadius.circular(8),
            ), // Works as expected, hides the box.
          ),
          data: TextService().verseNumbersToMarkdown(_text(widget.run))),
    );
  }

  String _text(FlashcardRun run) {
    final lines =
        VerseService().lines(run.verse, referenceIncluded: !run.inverse);
    return TextService().lineBreaksToMarkdown(
        run.revealLines == 0 || run.revealLines == lines.length
            ? lines.join()
            : '${lines.getRange(0, run.revealLines).join()}...');
  }
}
