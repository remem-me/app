import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/services/text_service.dart';

import '../../models/models.dart';
import '../../services/verse_service.dart';
import '../../theme.dart';
import '../audio/audio.dart';
import 'flashcard.dart';

class FlashcardReference extends FlashcardLayout {
  final GlobalKey<VoiceRecorderState>? voiceRecorderKey;
  final void Function()? onHint;

  const FlashcardReference({
    super.key,
    this.voiceRecorderKey,
    required run,
    onTap,
    onLongPress,
    this.onHint,
  }) : super(run: run, onTap: onTap, onLongPress: onLongPress);

  @override
  final gradientBegin = Alignment.topLeft;
  @override
  final gradientEnd = Alignment.bottomRight;

  @override
  Widget content(BuildContext ctx) {
    final account = I<CurrentAccount>();
    final textDirection = account.langRefDirection;
    final withSubtitle =
        run.hinting == null || run.hinting!.contains(FlashcardHint.subtitle);
    final withTags =
        run.hinting == null || run.hinting!.contains(FlashcardHint.tags);
    final withBeginning =
        run.hinting != null && run.hinting!.contains(FlashcardHint.beginning);
    final hintsEnabled = !run.inverse && !run.reverse;
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: Stack(alignment: AlignmentDirectional.topCenter, children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                if (run.verse.source != null) ...[
                  _source(),
                  const SizedBox(height: 8.0, width: double.infinity),
                ],
                _title(ctx, textDirection),
                const SizedBox(height: 8.0, width: double.infinity),
                _subtitle(ctx, textDirection, withSubtitle),
                const SizedBox(height: 16.0, width: double.infinity),
                _tags(ctx, withTags),
                const SizedBox(height: 16.0, width: double.infinity),
                _beginning(account, withBeginning),
              ]),
          if (hintsEnabled) Positioned(top: 0, child: _hintButton(ctx)),
          if (voiceRecorderKey != null)
            Positioned(bottom: 0, child: VoiceRecorder(key: voiceRecorderKey))
        ]));
  }

  Text _source() {
    return Text(run.verse.source!,
        textAlign: TextAlign.center,
        style: AppTheme.dark.textTheme.titleMedium!
            .copyWith(color: Colors.white70, shadows: textShadows()));
  }

  Text _title(BuildContext ctx, TextDirection textDirection) {
    return Text(VerseService().title(run.verse),
        textDirection: textDirection,
        textAlign: TextAlign.center,
        style: Theme.of(ctx).textTheme.titleLarge!.copyWith(
              fontSize: 20.0,
              color: Colors.white,
              shadows: textShadows(),
            ));
  }

  Text _subtitle(
      BuildContext ctx, TextDirection textDirection, bool withSubtitle) {
    return Text(VerseService().subtitle(run.verse),
        textDirection: textDirection,
        textAlign: TextAlign.center,
        style: Theme.of(ctx).textTheme.titleMedium!.copyWith(
            color: withSubtitle ? Colors.white : Colors.transparent,
            shadows: withSubtitle ? textShadows() : null));
  }

  Wrap _tags(BuildContext ctx, bool withTags) {
    return Wrap(
        spacing: 8.0,
        alignment: WrapAlignment.center,
        children: run.verse.tags.values
            .map(
              (tag) => Chip(
                side: BorderSide.none,
                label: Text(
                  tag ?? '',
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
                labelStyle: TextStyle(
                    color: withTags ? Colors.white : Colors.transparent),
                backgroundColor:
                    withTags ? Colors.grey[500]?.withValues(alpha: 0.38) : null,
              ),
            )
            .toList());
  }

  Widget _beginning(CurrentAccount account, bool withBeginning) {
    return Directionality(
      textDirection: account.languageDirection,
      child: MarkdownBody(
        styleSheet: MarkdownStyleSheet(
          textAlign: WrapAlignment.center,
          h1: TextStyle(
              color: withBeginning ? Colors.white : Colors.transparent,
              shadows: withBeginning ? textShadows() : null),
          h2: TextStyle(
              color: withBeginning ? Colors.white : Colors.transparent,
              shadows: withBeginning ? textShadows() : null),
          h3: TextStyle(
              color: withBeginning ? Colors.white : Colors.transparent,
              shadows: withBeginning ? textShadows() : null),
          p: AppTheme.dark.textTheme.bodyLarge!.copyWith(
              fontFamily: account.font,
              fontSize: 16.0,
              color: withBeginning ? Colors.white : Colors.transparent,
              shadows: withBeginning ? textShadows() : null),
          a: AppTheme.dark.textTheme.bodySmall?.copyWith(
              color: withBeginning ? null : Colors.transparent,
              fontFamily: account.font,
              fontSize: 12.0),
        ),
        data: TextService().verseNumbersToMarkdown(
            TextService().beginning(run.verse.passage)),
      ),
    );
  }

  @override
  List<Widget> backgroundLayers() {
    final layers = <Widget>[];
    layers.add(levelBackground());
    if (run.verse.image != null) {
      layers.add(imageBackground());
    }
    return layers;
  }

  Center _hintButton(BuildContext ctx) {
    return Center(
        child: Ink(
            decoration: ShapeDecoration(
                color: AppTheme.dark.colorScheme.surface.withOpacity(0.12),
                shape: const CircleBorder()),
            child: IconButton(
                iconSize: 28,
                tooltip:
                    '${L10n.of(ctx).t8('Flashcard.hint')} ${kIsWeb ? '⏏️' : '␣'}',
                icon: Icon(
                  Icons.tips_and_updates_rounded,
                  color: Colors.white.withOpacity(0.64),
                ),
                onPressed: onHint)));
  }
}
