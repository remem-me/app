import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:remem_me/util/build_context.dart';
import 'package:remem_me/widgets/settings/settings.dart';
import 'package:repository/repository.dart';

import '../svg.dart';

class BasicInfo extends StatefulWidget {
  final void Function()? onRegister;
  final void Function()? onLogin;

  const BasicInfo({super.key, this.onRegister, this.onLogin});

  @override
  State<BasicInfo> createState() => _BasicInfoState();
}

class _BasicInfoState extends State<BasicInfo> {
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      return ValueListenableBuilder<Box>(
          valueListenable:
              I<Settings>().listenable(keys: [Settings.TIP_VERSES_RECOVER]),
          builder: (context, bx, wdg) {
            return Scrollbar(
              controller: _scrollController,
              child: CustomScrollView(controller: _scrollController, slivers: [
                SliverToBoxAdapter(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(16.0, 0.0, 4.0, 0.0),
                    child: !kIsWeb &&
                            (I<Settings>()
                                    .getBool(Settings.TIP_VERSES_RECOVER) ??
                                true)
                        ? TipRow.recover(context)
                        : const SizedBox(height: 16.0),
                  ),
                ),
                SliverFillRemaining(
                    hasScrollBody: false,
                    child: Center(
                        child: orientation == Orientation.landscape
                            ? _landscapeLayout(context)
                            : _portraitLayout(context)))
              ]),
            );
          });
    });
  }

  Widget _portraitLayout(BuildContext ctx) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 8),
          _icon(),
          const SizedBox(height: 16),
          _text(ctx),
        ]);
  }

  Widget _landscapeLayout(BuildContext ctx) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(width: 16),
          _icon(),
          Flexible(child: _text(ctx)),
        ]);
  }

  Widget _icon() {
    return Container(
      color: Colors.transparent,
      child: const SvgAsset('appicon', width: 160, height: 160),
    );
  }

  Widget _text(BuildContext ctx) {
    return Container(
      constraints: const BoxConstraints(maxWidth: 336),
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          _appSlogan(ctx),
          _appName(ctx),
          const SizedBox(height: 8),
          _appSummary(ctx),
          const SizedBox(height: 8),
          _btnRegister(ctx),
          const SizedBox(height: 8),
          _btnLogin(ctx),
          _btnPrivacy(ctx),
        ],
      ),
    );
  }

  Text _appSlogan(BuildContext ctx) {
    return Text(
      L10n.of(ctx).t8('App.slogan'),
      style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
            color: Theme.of(ctx).colorScheme.primary,
            letterSpacing: -1.5,
            fontSize: 28,
          ),
      textAlign: TextAlign.center,
    );
  }

  Text _appName(BuildContext ctx) {
    return Text(
      L10n.of(ctx).t8('App.name'),
      style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
            fontSize: 36,
            color: Theme.of(ctx).colorScheme.primary,
          ),
      textAlign: TextAlign.center,
    );
  }

  Text _appSummary(BuildContext ctx) {
    return Text(L10n.of(ctx).t8('Start.summary'),
        style: Theme.of(ctx).textTheme.titleMedium,
        textAlign: TextAlign.center);
  }

  ElevatedButton _btnRegister(BuildContext ctx) {
    return ElevatedButton(
        key: const ValueKey('registerButton'),
        style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white,
            backgroundColor: Theme.of(ctx).colorScheme.secondaryFixed),
        onPressed: widget.onRegister,
        child: Text(L10n.of(ctx).t8('Start.register').toUpperCase()));
  }

  ElevatedButton _btnLogin(BuildContext ctx) {
    return ElevatedButton(
      key: const ValueKey('loginButton'),
      style: ElevatedButton.styleFrom(
          foregroundColor: Colors.white,
          backgroundColor: Theme.of(context).colorScheme.primaryFixed),
      onPressed: widget.onLogin,
      child: Text(L10n.of(ctx).t8('Start.login').toUpperCase()),
    );
  }

  TextButton _btnPrivacy(BuildContext ctx) {
    return TextButton(
        onPressed: launchDocumentation(ctx, path: 'privacy/'),
        child: Text(L10n.of(ctx).t8('Documentation.privacy'),
            style: Theme.of(ctx).textTheme.bodySmall));
  }
}
