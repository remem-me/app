import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

class ZoomCubit extends Cubit<double> {
  late double _scaleFactor;

  ZoomCubit() : super(1.0) {
    reset();
  }
  
  void reset() {
    _scaleFactor = Settings().getDouble(Settings.ZOOM_SCALE_FACTOR) ?? 1.0;
    emit(_scaleFactor);
  }

  void start() {
    emit(_scaleFactor);
  }

  Future<void> update(double scale) async {
    final next = _scaleFactor * scale;
    if (next >= 0.6 && next <= 2.0 && (next - state).abs() >= 0.01) emit(next);
  }

  void end() {
    _scaleFactor = state;
    Settings().putDouble(Settings.ZOOM_SCALE_FACTOR, _scaleFactor);
  }
}
