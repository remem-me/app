import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/widgets/zoom/zoom_cubit.dart';

class ZoomDetector extends StatelessWidget {
  final zoom = I<ZoomCubit>();
  final Widget child;

  ZoomDetector({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    final MediaQueryData data = MediaQuery.of(context);
    return GestureDetector(
        onScaleStart: (details) {
          zoom.start();
        },
        onScaleUpdate: (details) {
          zoom.update(details.scale);
        },
        onScaleEnd: (details) {
          zoom.end();
        },
        child: BlocBuilder<ZoomCubit, double>(
            bloc: I<ZoomCubit>(),
            builder: (BuildContext ctx, zoom) {
              return MediaQuery(
                data: data.copyWith(textScaler: TextScaler.linear(data.textScaleFactor * zoom)),
                child: child,
              );
            }));
  }
}
