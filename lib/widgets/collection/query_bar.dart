import 'package:flutter/material.dart';

import 'collection.dart';

class QueryBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize = const Size.fromHeight(48.0);

  const QueryBar({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(children: [
          LanguageSelector(),
          OrderingSelector(),
        ]));
  }
}
