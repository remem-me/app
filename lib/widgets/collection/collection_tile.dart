import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/widgets/collection/collection.dart';

import '../../paths/paths.dart';

/// Layout for displaying a collection in a list or grid
class CollectionTile extends StatelessWidget {
  const CollectionTile(this.collection, {super.key});

  final Collection collection;

  @override
  Widget build(BuildContext ctx) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<NavigationCubit>(ctx)
            .go(CollectionDetailPath(id: collection.id), propagates: true);
      },
      onLongPress: () {},
      child: _card(ctx),
    );
  }

  Widget _card(BuildContext ctx) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: CollectionContent(collection, hasFixedHeight: true),
    );
  }
}
