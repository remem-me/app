import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../theme.dart';
import '../../util/util.dart';

class LanguageSelector extends StatelessWidget {
  const LanguageSelector({super.key});


  @override
  Widget build(BuildContext ctx) {
    return Expanded(
        child: Row(children: [
      const Icon(
        Icons.language_rounded,
      ),
      const SizedBox(width: 8.0),
      Expanded(child: BlocBuilder<CollectionsBloc, CollectionsState>(
          builder: (BuildContext ctx, CollectionsState state) {
        return DropdownButton<String>(
            isExpanded: true,
            value: state.query.language,
            underline: const SizedBox.shrink(),
            dropdownColor: AppTheme.dark.colorScheme.surfaceBright,
            onChanged: (String? value) {
              BlocProvider.of<CollectionsBloc>(ctx).add(
                  CollectionsInitiated(state.query.copyWith(language: value)));
            },
            items: languageMap(ctx).entries.map<DropdownMenuItem<String>>((entry) {
              return DropdownMenuItem<String>(
                value: entry.key,
                child: Text(
                  entry.value,
                  overflow: TextOverflow.ellipsis,
                ),
              );
            }).toList());
      }))
    ]));
  }
}
