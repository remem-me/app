import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:repository/repository.dart';

import '../../blocs/navigation/navigation_cubit.dart';
import '../../models/collections/collection.dart';
import '../../models/models.dart';
import '../../paths/paths.dart';
import '../box/default_verse_tile.dart';
import '../settings/tip_tile.dart';

class CollectionVersesList extends StatelessWidget {
  const CollectionVersesList({
    super.key,
    required this.collection,
  });

  final Collection collection;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box>(
        valueListenable:
            I<Settings>().listenable(keys: Settings.VERSES_LIST_TIPS),
        builder: (context, bx, wdg) {
          final verses = collection.verses;
          final tips = _tips(context, collection.verses);
          return ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              padding: const EdgeInsets.only(top: 0, bottom: 16.0),
              itemCount: verses.length + tips.length,
              itemBuilder: (BuildContext ctx, int i) {
                return i < tips.length
                    ? TipTile(tips[i])
                    : SimpleVerseTile(
                        verses[i - tips.length],
                        onTap: () => _goToFlashcard(
                            ctx, collection.id, verses[i - tips.length]),
                      );
              },
            addAutomaticKeepAlives: false,
          );
        });
  }

  List<Tip> _tips(BuildContext ctx, List<Verse> verses) {
    final s = I<Settings>();
    final tipIconColor = Theme.of(ctx).disabledColor;
    return [
      if ((s.getBool(Settings.TIP_COLLECTION_VERSE) ?? true) &&
          verses.isNotEmpty)
        Tip(
            settingKey: Settings.TIP_COLLECTION_VERSE,
            icon: Icon(Icons.school_rounded, color: tipIconColor),
            l10nKey: 'Tip.collection.verse'),
    ];
  }

  void _goToFlashcard(BuildContext ctx, int collectionId, Verse verse) {
    BlocProvider.of<NavigationCubit>(ctx).go(
        CollectionFlashcardPath(id: collectionId, verseId: verse.id),
        propagates: true);
  }
}
