
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';

class CollectionsTitle extends StatelessWidget {
  const CollectionsTitle({super.key});

  @override
  Widget build(BuildContext ctx) => Text(L10n.of(ctx).t8('Collections.title'));
}