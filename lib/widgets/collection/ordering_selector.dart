import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';

import '../../theme.dart';

class OrderingSelector extends StatelessWidget {
  const OrderingSelector({super.key});

  @override
  Widget build(BuildContext ctx) {
    return Expanded(
        child: Row(children: [
      const Icon(
        Icons.sort_rounded,
      ),
      const SizedBox(width: 8.0),
      Expanded(child: BlocBuilder<CollectionsBloc, CollectionsState>(
          builder: (BuildContext ctx, CollectionsState state) {
        return DropdownButton<CollectionOrder>(
          isExpanded: true,
          value: state.query.order,
          underline: const SizedBox.shrink(),
          dropdownColor: AppTheme.dark.colorScheme.surfaceBright,
          onChanged: (CollectionOrder? value) {
            BlocProvider.of<CollectionsBloc>(ctx)
                .add(CollectionsInitiated(state.query.copyWith(order: value)));
          },
          items: CollectionOrder.values
              .map<DropdownMenuItem<CollectionOrder>>((CollectionOrder value) {
            return DropdownMenuItem<CollectionOrder>(
              value: value,
              child: Text(L10n.of(ctx).t8(value.toString()),
                overflow: TextOverflow.ellipsis,
              ),
            );
          }).toList(),
        );
      }))
    ]));
  }
}
