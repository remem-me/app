import 'dart:math';

import 'package:flutter/material.dart';

class Flipper extends StatefulWidget {
  final Widget front;
  final Widget back;
  final void Function()? onTap;
  final void Function()? onLongPress;
  final AnimationController? animationController;

  const Flipper(
      {super.key,
      required this.animationController,
      required this.front,
      required this.back,
      this.onTap, this.onLongPress});

  @override
  State<StatefulWidget> createState() => _FlipperState();
}

class _FlipperState extends State<Flipper> {
  late Animation _animation;
  late Function _animationListener;

  @override
  void initState() {
    super.initState();
    _animationListener = () {
      setState(() {
        /* Trigger a rebuild */
      });
    };
    _animation = Tween(end: 1.0, begin: 0.0).animate(CurvedAnimation(
        parent: widget.animationController!, curve: Curves.easeInOut))
      ..addListener(_animationListener as void Function());
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Transform(
          alignment: FractionalOffset.center,
          transform: Matrix4.identity()
            ..setEntry(3, 2, 0.001)
            ..rotateY(pi * _animation.value),
          child: GestureDetector(
            onTap: widget.onTap,
            onLongPress: widget.onLongPress,
            child: _animation.value <= 0.5
                ? widget.front
                : Transform(
                    alignment: Alignment.center,
                    transform: Matrix4.rotationY(pi),
                    child: widget.back,
                  ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animation.removeListener(_animationListener as void Function());
    super.dispose();
  }

}
