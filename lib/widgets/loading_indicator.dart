import 'package:flutter/material.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({super.key});

  @override
  Widget build(BuildContext ctx) {
    return Container(
        color: Theme.of(ctx).colorScheme.surface.withOpacity(0.5),
        child: const Center(
          child: CircularProgressIndicator(),
        ));
  }
}
