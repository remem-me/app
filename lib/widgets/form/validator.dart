import 'package:flutter/cupertino.dart';
import 'package:app_core/app_core.dart';

typedef Validator<T> = String? Function(T item);

abstract class Validators {
  static final _emailPattern = RegExp(
      r'^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})');

  static final _urlPattern = RegExp(
    r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)');

  static Validator<T> required<T>(BuildContext ctx) =>
      (T val) => val == null || val is String && val.trim().isEmpty
          ? L10n.of(ctx).t8('Validator.required')
          : null;

  static Validator<String?> email(BuildContext ctx) => (String? val) =>
      val!.isEmpty || _emailPattern.hasMatch(val) ? null : L10n.of(ctx).t8('Validator.email');

  static Validator<String?> url(BuildContext ctx) => (String? val) =>
      val!.isEmpty || _urlPattern.hasMatch(val) ? null : L10n.of(ctx).t8('Validator.url');

  static Validator<String?> minLength(BuildContext ctx, int? min) =>
      (String? val) => min != null && val!.length < min
          ? L10n.of(ctx).t8('Validator.minLength', [min.toString()])
          : null;

  static Validator<String?> maxLength(BuildContext ctx, int max) =>
      (String? val) => val!.length > max
          ? L10n.of(ctx).t8('Validator.maxLength', [max.toString()])
          : null;

  static Validator<int?> minimum(BuildContext ctx, int min) =>
      (int? val) => val != null && val < min
          ? L10n.of(ctx).t8('Validator.minimum', [min.toString()])
          : null;

  static Validator<int?> maximum(BuildContext ctx, int max) =>
      (int? val) => val != null && val > max
          ? L10n.of(ctx).t8('Validator.maximum', [max.toString()])
          : null;

  static Validator<T> equal<T>(
          BuildContext ctx, GlobalKey<FormFieldState> field, l10nKey) =>
      (T val) => val != field.currentState!.value
          ? L10n.of(ctx).t8('Validator.equal', [L10n.of(ctx).t8(l10nKey)])
          : null;

  static Validator<T> error<T>(List<String?>? errors, T wrongValue) => (T val) {
        return val == wrongValue && errors != null ? errors.join('\n') : null;
      };

  static Validator<T> compose<T>(List<Validator<T>> validators) {
    return (valueCandidate) {
      List<String> allResults = [];
      for (var validator in validators) {
        final validatorResult = validator.call(valueCandidate);
        if (validatorResult != null) {
          allResults.add(validatorResult);
        }
      }
      return allResults.isEmpty ? null : allResults.join('\n');
    };
  }
}
