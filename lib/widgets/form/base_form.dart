import 'package:flutter/material.dart';


// Parent class for all forms

abstract class BaseForm extends StatefulWidget {
  final bool poppable;

  const BaseForm({
    super.key,
    this.poppable = false,
  });
}

abstract class BaseFormState<T extends BaseForm>
    extends State<T> {
  final formKey = GlobalKey<FormState>();
  final spacing = 16.0;
  bool isUpdating = false;

  Map<String, List<String>> errors = {};

  List<Widget> errorMessages(BuildContext ctx) {
    return errors['messages'] != null
        ? [
            Text(errors['messages']!.join('\n'),
                style: Theme.of(ctx)
                    .textTheme
                    .bodyLarge!
                    .copyWith(color: Theme.of(ctx).colorScheme.error)),
            SizedBox(height: spacing),
          ]
        : [];
  }

  List<Widget> fields(BuildContext ctx) {
    return errorMessages(ctx);
  }
}
