import 'package:community_charts_flutter/community_charts_flutter.dart';
import 'package:flutter/material.dart';

class ScoreChart extends StatelessWidget {
  final List<Series<dynamic, String>> seriesList;
  final int labelRotation;
  final int goal;

  const ScoreChart(this.seriesList,
      {super.key, this.labelRotation = 0, this.goal = 0});

  @override
  Widget build(BuildContext context) {
    return BarChart(
      seriesList,
      behaviors: [
        if (goal > 0)
          RangeAnnotation([
            LineAnnotationSegment(goal, RangeAnnotationAxisType.measure,
                color: _goalColor(context))
          ]),
      ],
      domainAxis: OrdinalAxisSpec(
        renderSpec: SmallTickRendererSpec(
          labelRotation: labelRotation,
          labelStyle: TextStyleSpec(color: _legendColor(context)),
          lineStyle: LineStyleSpec(color: _lineColor(context)),
        ),
      ),
      primaryMeasureAxis: NumericAxisSpec(
          renderSpec: GridlineRendererSpec(
              labelStyle: TextStyleSpec(color: _legendColor(context)),
              lineStyle: LineStyleSpec(color: _lineColor(context)))),
      animate: false,
      barRendererDecorator: BarLabelDecorator<String>(
          insideLabelStyleSpec: TextStyleSpec(
              color: ColorUtil.fromDartColor(
                  Theme.of(context).colorScheme.surface)),
          outsideLabelStyleSpec: TextStyleSpec(
              color: ColorUtil.fromDartColor(
                  Theme.of(context).colorScheme.onSurface))),
    );
  }

  Color _legendColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.onSurface.withOpacity(0.64));

  Color _lineColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.onSurface.withOpacity(0.12));

  Color _goalColor(BuildContext ctx) => ColorUtil.fromDartColor(
      Theme.of(ctx).colorScheme.secondaryContainer);
}
