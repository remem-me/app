import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';
import '../widgets.dart';

class HighScores extends StatelessWidget {
  const HighScores({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScoresBloc, EntitiesState<ScoreEntity>>(
      builder: (context, state) {
        if (state is EntitiesLoadInProgress<ScoreEntity>) {
          return const LoadingIndicator();
        } else if (state is EntitiesLoadSuccess<ScoreEntity>) {
          var dayScores = ScoreUtil.dayScores(state.items);
          return ListView.separated(
            padding: const EdgeInsets.all(16),
            itemCount: dayScores.length,
            itemBuilder: (ctx, i) => Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  width: 48,
                  child: Text(
                    (i + 1).toString(),
                    style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                        color: Theme.of(ctx).textTheme.headlineLarge!.color),
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  width: 128,
                  child: Text(
                    dayScores[i].change.toString(),
                    style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                        fontWeight: FontWeight.w700,
                        color: Theme.of(ctx).colorScheme.secondary),
                  ),
                ),
                const SizedBox(width: 16),
                Text(
                  FromToday(L10n.of(ctx)).format2Lines(dayScores[i].date),
                  style: Theme.of(ctx).textTheme.bodyMedium,
                ),
              ],
            ),
            separatorBuilder: (ctx, i) => const Divider(thickness: 1),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }
}
