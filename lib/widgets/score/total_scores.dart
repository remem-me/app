import 'package:community_charts_flutter/community_charts_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widget_keys.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../../services/score_service.dart';
import '../../util/verse_util.dart';

class TotalScores extends StatelessWidget {
  const TotalScores({super.key});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
      builder: (ctx, state) {
        if (state is TaggedVersesInProgress) {
          return const LoadingIndicator(
            key: RememMeKeys.scoresLoadInProgressIndicator,
          );
        } else if (state is TaggedVersesSuccess) {
          final knownVerses = VerseUtil
              .mapVersesToBoxKnown(state.verses, VerseOrder.ALPHABET);
          final levelScores = ScoreService().scoreLevels(knownVerses);
          final totalScore = ScoreService().totalScore(levelScores);
          return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            const SizedBox(
              height: 8,
            ),
            Text(
              totalScore.toString(),
              style: Theme.of(ctx).textTheme.headlineMedium!.copyWith(
                  fontWeight: FontWeight.w700,
                  color: Theme.of(ctx).colorScheme.secondary),
            ),
            Text(
              L10n.of(ctx).t8('Scores.total.summary'),
              style: Theme.of(ctx).textTheme.bodyMedium,
            ),
            Expanded(
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(4, 4, 4, 32),
                    child: ScoreChart(
                      _createSeries(ctx, levelScores),
                    ))),
          ]);
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  List<Series<int, String>> _createSeries(BuildContext ctx, List<int> data) {
    return [
      Series<int, String>(
        id: 'Scores',
        colorFn: (_, i) =>
            ColorUtil.fromDartColor(VerseUtil.color(i! + 1)),
        domainFn: (score, i) => (i! + 1).toString(),
        measureFn: (score, _) => score,
        labelAccessorFn: (score, _) => score > 0 ? score.toString() : '',
        data: data,
      )
    ];
  }
}
