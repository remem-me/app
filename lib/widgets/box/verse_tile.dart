import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/box/box.dart';

import '../../services/verse_service.dart';

/// Layout for displaying a verse in a list
class VerseTile extends StatelessWidget {
  final Verse verse;
  final void Function()? onTap;
  final void Function()? onLongPress;
  final Color? tileColor;
  final Widget? verseBadge;
  final bool selected;

  const VerseTile(
      {super.key,
      required this.verse,
      this.onTap,
      this.onLongPress,
      this.tileColor,
      this.verseBadge,
      this.selected = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          minVerticalPadding: 0.0,
          horizontalTitleGap: 12.0,
          contentPadding: const EdgeInsets.symmetric(horizontal: 8.0),
          tileColor: Theme.of(context).colorScheme.surfaceContainerLowest,
          splashColor: Theme.of(context).colorScheme.surfaceContainerHigh,
          leading: SizedBox.square(dimension: 48.0, child: verseBadge),
          titleAlignment: ListTileTitleAlignment.titleHeight,
          title: ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 56.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(height: 4.0),
                  _description(context, verse),
                  _tags(context, verse),
                ]),
          ),
          onTap: () => onTap!(),
          onLongPress: () => onLongPress!(),
          selected: selected,
        ),
        const VerseDivider(),
      ],
    );
  }

  Widget _description(BuildContext ctx, Verse verse) {
    FromToday fromToday = FromToday(L10n.of(ctx));
    String subtitle = VerseService().subtitle(verse);
    String source = verse.source ?? '';
    String due = fromToday.format(verse.due);

    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    VerseService().title(verse),
                    style: Theme.of(ctx)
                        .textTheme
                        .titleMedium
                        ?.copyWith(height: 0.0),
                    // for fixed list item height
                    // overflow: TextOverflow.ellipsis,
                    // maxLines: 1,
                    // softWrap: false,
                  ),
                  if (subtitle.isNotEmpty) ...[
                    const SizedBox(height: 2.0),
                    Text(
                      VerseService().subtitle(verse),
                      style: Theme.of(ctx)
                          .textTheme
                          .bodyMedium
                          ?.copyWith(height: 0.0),
                      overflow: TextOverflow.fade,
                      // for fixed list item height:
                      // overflow: TextOverflow.ellipsis,
                      // maxLines: 1,
                      // softWrap: false,
                    )
                  ]
                ]),
          ),
          ConstrainedBox(
            constraints:
                BoxConstraints(maxWidth: MediaQuery.of(ctx).size.width / 4),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  if (source.isNotEmpty)
                    Text(source,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(ctx).textTheme.bodyMedium),
                  if (source.isNotEmpty && due.isNotEmpty)
                    const SizedBox(height: 4.0),
                  if (due.isNotEmpty)
                    Text(fromToday.format(verse.due),
                        style: Theme.of(ctx).textTheme.bodySmall?.copyWith(
                              letterSpacing: -0.5,
                            ))
                ]),
          ),
        ]);
  }

  Widget _tags(BuildContext ctx, Verse verse) {
    return verse.tags.isEmpty
        ? const SizedBox(height: 8.0)
        : Padding(
            padding: const EdgeInsets.only(top: 2.0, bottom: 1.0),
            child: Wrap(
              alignment: WrapAlignment.end,
              verticalDirection: VerticalDirection.up,
              children: verse.tags.values
                  .map((tag) => Container(
                      decoration: BoxDecoration(
                        color: Theme.of(ctx).brightness == Brightness.light
                            ? Theme.of(ctx)
                                .colorScheme
                                .outlineVariant
                                .withOpacity(0.5)
                            : Theme.of(ctx).colorScheme.outlineVariant,
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(4.0),
                            topRight: Radius.circular(4.0)),
                      ),
                      padding: const EdgeInsets.fromLTRB(6.0, 0.0, 6.0, 0.0),
                      margin: const EdgeInsets.only(left: 6.0),
                      child: Text(tag ?? '',
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: Theme.of(ctx).textTheme.bodySmall!.copyWith(
                                letterSpacing: -0.5,
                                height: 0.0,
                              ))))
/*                              color: Theme.of(ctx)
                                  .textTheme
                                  .labelSmall!
                                  .color!
                                  .withOpacity(0.64)))))*/
                  .toList(),
            ),
          );
  }
}
