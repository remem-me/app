import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';

import '../../paths/paths.dart';
import '../widgets.dart';

/// Layout for displaying a verse in a list
class SelectableVerseTile extends StatelessWidget {
  final Verse verse;

  const SelectableVerseTile(this.verse, {super.key});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<SelectionBloc, SelectionState>(
        builder: (ctx, state) => VerseTile(
              verse: verse,
              onTap: () =>
                  _launchOrToggle(ctx, state.selection, inSecondaryMode: false),
              onLongPress: () =>
                  _launchOrToggle(ctx, state.selection, inSecondaryMode: true),
              verseBadge: _verseBadge(ctx, state.selection),
              selected: state.selection.keys.contains(verse.id),
            ));
  }

  void _launchOrToggle(BuildContext ctx, Map<int, Verse> selection,
      {required bool inSecondaryMode}) {
    if (selection.isNotEmpty) {
      _toggleSelected(ctx, selection);
    } else {
      final bloc = BlocProvider.of<OrderedVersesBloc>(ctx);
      final state = bloc.state;
      if (state is OrderSuccess && state.verses.isNotEmpty) {
        final verses = [...state.verses];
        BlocProvider.of<FlashcardBloc>(ctx).add(FlashcardLoaded(
            verses: verses,
            box: bloc.box,
            inverse: bloc.box == VerseBox.KNOWN && !inSecondaryMode,
            reverse: bloc.box != VerseBox.KNOWN && inSecondaryMode,
            discrete: true,
            index: verses.indexWhere((it) => it.id == verse.id)));
        I<NavigationCubit>()
            .go(HomeFlashcardPath(id: verse.id), propagates: false);
      }
    }
  }

  Widget _verseBadge(BuildContext ctx, Map<int, Verse> selection) {
    return VerseBadge(
      key: UniqueKey(),
      verse: verse,
      isSelected: selection.keys.contains(verse.id),
      onSelect: () => _toggleSelected(ctx, selection),
      onLongPress: () => _toggleSelected(ctx, selection),
    );
  }

  _toggleSelected(BuildContext ctx, Map<int, Verse> selection) {
    // todo: just use bool isSelected, move rest to bloc,
    final updatedSelection = Map<int, Verse>.of(selection);
    if (selection.keys.contains(verse.id)) {
      updatedSelection.remove(verse.id);
      BlocProvider.of<SelectionBloc>(ctx)
          .add(SelectionUpdated(updatedSelection));
    } else {
      updatedSelection[verse.id] = verse;
      BlocProvider.of<SelectionBloc>(ctx)
          .add(SelectionUpdated(updatedSelection));
    }
  }
}
