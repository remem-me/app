import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';
import 'package:cached_network_image_platform_interface/cached_network_image_platform_interface.dart';

import '../widgets.dart';

/// Layout for displaying a verse in a list
class SimpleVerseTile extends StatelessWidget {
  final Verse verse;
  final void Function()? onTap;

  const SimpleVerseTile(this.verse, {super.key,  this.onTap });

  @override
  Widget build(BuildContext ctx) {
    return VerseTile(
          verse: verse,
          onTap: onTap ?? () {},
          onLongPress: () {},
          verseBadge: _verseBadge(ctx),
        );
  }

  Widget _verseBadge(BuildContext ctx) {
    final layers = <Widget>[];
    layers.add(_placeholder());
    if (verse.image != null) {
      layers.add(_image());
    }
    return ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(4.0)),
        child: Stack(
          fit: StackFit.expand,
          children: layers,
        ));
  }

  Widget _image() {
    return FittedBox(
        fit: BoxFit.cover,
        child: CachedNetworkImage(
          errorWidget: (ctx, url, err) => const SizedBox.shrink() ,
          imageUrl: verse.image!,
          imageRenderMethodForWeb: ImageRenderMethodForWeb.HttpGet,
        ));
  }

  SvgAsset _placeholder() => const SvgAsset('app_logo');
}
