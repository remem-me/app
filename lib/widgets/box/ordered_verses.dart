import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/widgets.dart';

class OrderedVerses extends StatefulWidget {
  static final _log = Logger('OrderedVerses');

  const OrderedVerses({super.key});

  @override
  State<OrderedVerses> createState() => _OrderedVersesState();
}

class _OrderedVersesState extends State<OrderedVerses>
    with AutomaticKeepAliveClientMixin<OrderedVerses> {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<OrderedVersesBloc, OrderedVersesState>(
        builder: (BuildContext ctx, OrderedVersesState state) {
      if (state is OrderInProgress) {
        return const LoadingIndicator();
      } else if (state is OrderSuccess) {
        return Stack(children: [
          if (state.verses.isEmpty) EmptyList(icon: _icon(ctx)),
          VersesList(verses: state.verses),
          if (state.isUpdating) const LoadingIndicator(),
        ]);
      } else {
        return EmptyList(icon: _icon(ctx));
      }
    });
  }

  IconData _icon(BuildContext ctx) {
    switch (BlocProvider.of<OrderedVersesBloc>(ctx).box) {
      case VerseBox.NEW:
        return Icons.inbox_rounded;
      case VerseBox.DUE:
        return Icons.check_box_outline_blank_rounded;
      case VerseBox.KNOWN:
      default:
        return Icons.check_box_rounded;
    }
  }

  @override
  bool get wantKeepAlive => true;
}
