import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class BoxTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabController _controller;

  const BoxTabBar(this._controller, {super.key});

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, TabState>(builder: (context, state) {
      return TabBar(
        controller: _controller,
        labelStyle: Theme.of(ctx).textTheme.bodySmall,
        labelColor: Colors.white,
        labelPadding: const EdgeInsets.symmetric(horizontal: 2.0),
        tabs: [
          BlocProvider<OrderedVersesBloc>.value(
            value: I<OrderedVersesNewBloc>(),
            child: BoxTab(
              iconData: Icons.inbox_rounded,
              l10nKey: 'Box.new',
              tab: VerseBox.NEW,
              hasOrderMenu: state.box == VerseBox.NEW,
            ),
          ),
          BlocProvider<OrderedVersesBloc>.value(
            value: I<OrderedVersesDueBloc>(),
            child: BoxTab(
                iconData: Icons.check_box_outline_blank_rounded,
                l10nKey: 'Box.due',
                tab: VerseBox.DUE,
                hasOrderMenu: state.box == VerseBox.DUE),
          ),
          BlocProvider<OrderedVersesBloc>.value(
            value: I<OrderedVersesKnownBloc>(),
            child: BoxTab(
                iconData: Icons.check_box_rounded,
                l10nKey: 'Box.known',
                tab: VerseBox.KNOWN,
                hasOrderMenu: state.box == VerseBox.KNOWN),
          )
        ],
        onTap: (i) async {
          if (!_controller.indexIsChanging) {
            await _editOrder(ctx, i);
          }
        },
      );
    });
  }

  Future<void> _editOrder(BuildContext ctx, int i) async {
    final bloc = (OrderedVersesBloc.from(TabState(true, VerseBox.values[i])));
    final prevOrder = (bloc.state as OrderSuccess).order;
    final nextOrder = await showDialog<VerseOrder>(
        context: ctx,
        builder: (BuildContext context) {
          return BoxOrderDialog(
            tab: VerseBox.values[i],
            prevOrder: prevOrder,
          );
        });
    if (nextOrder != null) bloc.add(OrderUpdated(nextOrder));
  }

  @override
  Size get preferredSize => const Size.fromHeight(56.0);
}
