import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';
import '../widgets.dart';

class VersesList extends StatefulWidget {
  final List<Verse> verses;

  const VersesList({super.key, required this.verses});

  @override
  State<VersesList> createState() => _VersesListState();
}

class _VersesListState extends State<VersesList> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext ctx) {
    return Scrollbar(
        controller: _scrollController,
        thumbVisibility: false,
        interactive: true,
        thickness: 8,
        radius: const Radius.circular(4),
        child: StreamBuilder<LocalDate>(
            stream: I<ScoresBloc>().goalReachedStream,
            // todo: listen to date change (like FilteredVersesBloc._mapAppResumedToState)
            builder: (ctx, snapshot) {
            return ValueListenableBuilder<Box>(
                valueListenable:
                    I<Settings>().listenable(keys: Settings.VERSES_LIST_TIPS),
                builder: (context, bx, wdg) {
                  final tips = _tips(ctx, widget.verses, snapshot.data);
                  return SafeArea(
                    bottom: false,
                    child: ListView.builder(
                      padding: const EdgeInsets.only(top: 0, bottom: 128),
                      controller: _scrollController,
                      itemCount: widget.verses.length + tips.length,
                      itemBuilder: (BuildContext ctx, int i) {
                        return i < tips.length
                            ? TipTile(tips[i])
                            : SelectableVerseTile(
                                widget.verses[i - tips.length]);
                      },
                    ),
                  );
                });
          }
        ));
  }

  List<Tip> _tips(
      BuildContext ctx, List<Verse> verses, LocalDate? goalReached) {
    final s = I<Settings>();
    final tipIconColor = Theme.of(ctx).disabledColor;
    final generalTips = [
      if ((s.getBool(Settings.TIP_ACCOUNT_LOCAL) ?? true) &&
          I<AuthBloc>().state is AccessLocal)
        Tip(
            settingKey: Settings.TIP_ACCOUNT_LOCAL,
            icon: Icon(Icons.cloud_off_rounded, color: tipIconColor),
            l10nKey: 'Tip.user.local'),
      if ((s.getBool(Settings.TIP_VERSE_SELECT) ?? true) && verses.isNotEmpty)
        Tip(
            settingKey: Settings.TIP_VERSE_SELECT,
            icon: Icon(Icons.arrow_downward_rounded, color: tipIconColor),
            l10nKey: 'Tip.verse.select'),
    ];
    switch (BlocProvider.of<OrderedVersesBloc>(ctx).box) {
      case VerseBox.NEW:
        return [
          if ((s.getBool(Settings.TIP_NEW_EMPTY) ?? true) && verses.isEmpty)
            Tip(
                settingKey: Settings.TIP_NEW_EMPTY,
                icon: Icon(Icons.add_rounded, color: tipIconColor),
                l10nKey: 'Tip.new.empty'),
          if ((s.getBool(Settings.TIP_NEW_ACTION) ?? true) && verses.isNotEmpty)
            Tip(
                settingKey: Settings.TIP_NEW_ACTION,
                icon: Icon(Icons.school_rounded, color: tipIconColor),
                l10nKey: 'Tip.new.action'),
          ...generalTips
        ];
      case VerseBox.DUE:
        return [
          if ((s.getBool(Settings.TIP_DUE_DONE) ?? true) &&
              goalReached == DateService().today)
            Tip(
                settingKey: Settings.TIP_DUE_DONE,
                icon: Icon(Icons.check_box_rounded, color: tipIconColor),
                l10nKey: 'Tip.due.done'),
          if ((s.getBool(Settings.TIP_DUE_EMPTY) ?? true) &&
              verses.isEmpty &&
              goalReached != DateService().today)
            Tip(
                settingKey: Settings.TIP_DUE_EMPTY,
                icon: Icon(Icons.inbox_rounded, color: tipIconColor),
                l10nKey: 'Tip.due.empty'),
          if ((s.getBool(Settings.TIP_DUE_ACTION) ?? true) &&
              verses.isNotEmpty &&
              I<CurrentAccount>().dailyGoal > 0 &&
              goalReached != DateService().today)
            Tip(
                settingKey: Settings.TIP_DUE_ACTION,
                icon: SvgAsset('cards_outline',
                    height: 20.0, color: tipIconColor),
                l10nKey: 'Tip.due.action'),
          ...generalTips
        ];
      case VerseBox.KNOWN:
        return [
          if ((s.getBool(Settings.TIP_KNOWN_EMPTY) ?? true) && verses.isEmpty)
            Tip(
                settingKey: Settings.TIP_KNOWN_EMPTY,
                icon: Icon(Icons.arrow_back_rounded, color: tipIconColor),
                l10nKey: 'Tip.known.empty'),
          if ((s.getBool(Settings.TIP_KNOWN_ACTION) ?? true) &&
              verses.isNotEmpty)
            Tip(
                settingKey: Settings.TIP_KNOWN_ACTION,
                icon:
                    SvgAsset('cards_filled', height: 20.0, color: tipIconColor),
                l10nKey: 'Tip.known.action'),
          ...generalTips
        ];
      default:
        return generalTips;
    }
  }
}
