
import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';

import '../widgets.dart';

class BoxTab extends StatelessWidget {
  final VerseBox tab;
  final String l10nKey;
  final IconData iconData;
  final bool hasOrderMenu;

  const BoxTab(
      {super.key,
      required this.tab,
      required this.l10nKey,
      required this.iconData,
      required this.hasOrderMenu});

  @override
  Widget build(BuildContext ctx) {
    final List<Widget> stackItems = [
      Tab(
          height: 54.0,
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Icon(iconData, size: 28.0),
                LayoutBuilder(
                    builder: (BuildContext context, BoxConstraints viewport) =>
                        SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            constraints: BoxConstraints(minWidth: viewport.maxWidth),
                            child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(L10n.of(ctx).t8(l10nKey).toUpperCase()),
                                  const SizedBox(width: 4.0),
                                  const BoxCount(),
                                ]),
                          ),
                        )),
              ]))
    ];
    if (hasOrderMenu) {
      stackItems.add(Positioned(
        bottom: 2,
        right: 0,
        child: SvgAsset('triangle',
            height: 10, color: Theme.of(ctx).tabBarTheme.unselectedLabelColor),
      ));
    }

    return Stack(children: stackItems);
  }
}
