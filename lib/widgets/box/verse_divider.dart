import 'package:flutter/material.dart';

class VerseDivider extends StatelessWidget {
  const VerseDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Divider(
      thickness: 1.0,
      height: 0.0,
      indent: 64.0,
      endIndent: 8.0,
    );
  }
}
