import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../blocs/blocs.dart';

class BoxedToggle extends StatelessWidget {
  const BoxedToggle({
    super.key,
  });

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, TabState>(
        buildWhen: (prev, next) => prev.isBoxed != next.isBoxed,
        builder: (context, tabState) {
          return ListTile(
            leading: Icon(tabState.isBoxed
                ? Icons.layers_outlined
                : Icons.layers_clear_outlined),
            title: Text('${L10n.of(ctx).t8('Boxes.title')}: ${L10n.of(ctx).t8(
                    tabState.isBoxed ? 'Settings.on' : 'Settings.off')}'),
            onTap: () {
              (I<TabBloc>()).add(BoxedToggled());
            },
          );
        });
  }
}