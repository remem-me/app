import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';

import '../widgets.dart';

class TtsFabRow extends StatelessWidget {
  const TtsFabRow({super.key});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlaybackState>(
        stream: I<TtsHandler>().playbackState,
        builder: (ctx, snapshot) {
          var buttons = <Widget>[
            _dim(ctx, Setting(
              l10nKey: 'Setting.speech.title',
              form: const SpeechSettingForm(),
              icon: Icons.video_settings,
              iconColor: Theme.of(context).floatingActionButtonTheme.foregroundColor,
              isCollapsed: true,
            )),
            const SizedBox(width: 4),
            const TtsFabSkipPrevious(),
            const SizedBox(width: 4),
            const TtsFabStop(),
            const SizedBox(width: 4),
            if (snapshot.data?.playing ?? false)
              const TtsFabPause()
            else
              const TtsFabResume(),
            const SizedBox(width: 4),
            const TtsFabSkipNext(),
            const HelpButton('audio/'),
          ];
          return Row(mainAxisSize: MainAxisSize.min, children: buttons);
        });
  }

  Opacity _dim(BuildContext ctx, Widget child) {
    return Opacity(
      opacity: 0.7,
      child: Material(
        color: Colors.transparent,
        elevation: 0,
        child: Ink(
            decoration: ShapeDecoration(
                color: Theme.of(ctx).floatingActionButtonTheme.backgroundColor, shape: const CircleBorder()),
            child: child),
      ),
    );
  }
}

class TtsFabPause extends StatelessWidget {
  const TtsFabPause({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'pauseBtn',
      onPressed: () {
        TtsHandler().pause();
      },
      child: const Icon(
        Icons.pause_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabResume extends StatelessWidget {
  const TtsFabResume({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'resumeBtn',
      onPressed: () {
        TtsHandler().play();
      },
      child: const Icon(
        Icons.play_arrow_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabStop extends StatelessWidget {
  const TtsFabStop({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'stopBtn',
      onPressed: () {
        TtsHandler().stop();
      },
      child: const Icon(
        Icons.stop_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabSkipNext extends StatelessWidget {
  const TtsFabSkipNext({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'skipNextBtn',
      onPressed: () {
        TtsHandler().skipToNext();
      },
      child: const Icon(
        Icons.skip_next_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabSkipPrevious extends StatelessWidget {
  const TtsFabSkipPrevious({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'skipPreviousBtn',
      onPressed: () {
        TtsHandler().skipToPrevious();
      },
      child: const Icon(
        Icons.skip_previous_rounded,
        color: Colors.white,
      ),
    );
  }
}
