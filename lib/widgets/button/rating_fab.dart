import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../services/rating_service.dart';
import '../widgets.dart';

class RatingFab extends StatelessWidget {
  const RatingFab({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'ratingBtn',
      onPressed: () async {
        var isOk = await showDialog<bool>(
          context: context,
          builder: (BuildContext context) => buildDialog(context),
        );
        if (isOk ?? false) {
          RatingService().markRatingDone();
          final uri = await RatingService().uriStore();
          if (await canLaunchUrl(uri)) await launchUrl(uri);
        }
      },
      backgroundColor:Theme.of(context).colorScheme.secondaryFixed,
      child: const Icon(
        Icons.star,
        color: Colors.white,
      ),
    );
  }

  AlertDialog buildDialog(BuildContext ctx) {
    return AlertDialog(
      // title: Center(child: Text(L10n.of(ctx).t8('Rating.title'))),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            color: Colors.transparent,
            child: const SvgAsset('appicon', width: 128, height: 128),
          ),
          const SizedBox(height: 16.0),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List<Icon>.generate(5,
                  (_) => const Icon(Icons.star_rounded, color: FlatDark.amber),
                  growable: false)),
          const SizedBox(height: 16.0),
          Text(L10n.of(ctx).t8('Rating.message')),
        ],
      ),
      actions: <Widget>[
        TextButton(
          style: TextButton.styleFrom(
              foregroundColor: Theme.of(ctx).textTheme.bodySmall!.color),
          onPressed: () => Navigator.pop(ctx, false),
          child: Text(L10n.of(ctx).t8('Button.cancel').toUpperCase()),
        ),
        TextButton(
          onPressed: () => Navigator.pop(ctx, true),
          child: Text(L10n.of(ctx).t8('Button.ok').toUpperCase()),
        ),
      ],
    );
  }
}
