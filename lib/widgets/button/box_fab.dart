import 'package:app_core/app_core.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/home_paths.dart';
import 'package:remem_me/services/rating_service.dart';
import 'package:remem_me/widgets/button/rating_fab.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';

import '../../blocs/text_to_speech/tts_handler.dart';

class BoxFabRow extends StatelessWidget {
  const BoxFabRow({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, TabState>(
        builder: (context, tabState) =>
            BlocBuilder<OrderedVersesBloc, OrderedVersesState>(
                bloc: OrderedVersesBloc.from(tabState),
                builder: (context, versesState) {
                  final isNotEmpty = versesState is OrderSuccess &&
                      versesState.verses.isNotEmpty;
                  var box = tabState.box;
                  if (!tabState.isBoxed) {
                    box = VerseBox.ALL;
                  }
                  var buttons = <Widget>[];
                  final primary = Theme.of(context).colorScheme.primaryFixed;
                  final hasCurrentAccount = I<CurrentAccount>().id > -1;
                  if (!hasCurrentAccount) {
                    buttons = [const HelpButton('accounts/')];
                  } else {
                    switch (box) {
                      case VerseBox.DUE:
                        buttons = isNotEmpty
                            ? [
                                const SizedBox(width: 48),
                                const BoxFabTts(),
                                if (_hasRatingFab) ...[
                                  const SizedBox(width: 8),
                                  const RatingFab(),
                                ],
                                const SizedBox(width: 8),
                                BoxFabReview(backgroundColor: primary),
                                const SizedBox(width: 8),
                                const HelpButton('study/'),
                              ]
                            : [
                                if (_hasRatingFab) ...[
                                  const SizedBox(width: 48),
                                  const RatingFab(),
                                  const SizedBox(width: 8),
                                ],
                                const HelpButton('study/'),
                              ];
                        break;
                      case VerseBox.KNOWN:
                        buttons = isNotEmpty
                            ? [
                                const SizedBox(width: 48),
                                const BoxFabTts(),
                                if (_hasRatingFab) ...[
                                  const SizedBox(width: 8),
                                  const RatingFab(),
                                ],
                                const SizedBox(width: 8),
                                BoxFabInverse(backgroundColor: primary),
                                const SizedBox(width: 8),
                                const HelpButton('study/'),
                              ]
                            : [
                                if (_hasRatingFab) ...[
                                  const SizedBox(width: 48),
                                  const RatingFab(),
                                  const SizedBox(width: 8),
                                ],
                                const HelpButton('study/'),
                              ];
                        break;
                      case VerseBox.NEW:
                        buttons = [
                          const SizedBox(width: 48),
                          if (isNotEmpty) ...[
                            const BoxFabTts(),
                            const SizedBox(width: 8),
                          ],
                          if (_hasRatingFab) ...[
                            const RatingFab(),
                            const SizedBox(width: 8),
                          ],
                          BoxFabAdd(backgroundColor: primary),
                          const SizedBox(width: 8),
                          const HelpButton('verses/'),
                        ];
                        break;
                      case VerseBox.ALL:
                        buttons = isNotEmpty
                            ? [
                                const BoxFabTts(),
                                const SizedBox(width: 8),
                                const BoxFabAdd(),
                                const SizedBox(width: 8),
                                const BoxFabReview(),
                              ]
                            : [
                                const HelpButton('verses/'),
                              ];
                        break;
                    }
                  }
                  return Row(mainAxisSize: MainAxisSize.min, children: buttons);
                }));
  }

  get _hasRatingFab {
    return !kIsWeb &&
        RatingService().isReadyForRating() &&
        !RatingService().isRatingDone();
  }
}

/// FloatingActionButton that is aware of which box it is used on
abstract class BoxFab extends StatelessWidget {
  final Color? backgroundColor;

  const BoxFab({super.key, this.backgroundColor});

  @override
  Widget build(BuildContext context) => BlocBuilder<TabBloc, TabState>(
          builder: (BuildContext ctx, TabState state) {
        return buildWithBloc(ctx, OrderedVersesBloc.from(state));
      });

  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc);

  CircleBorder border(BuildContext ctx) {
    return const CircleBorder(
        side: BorderSide(width: 4.0, color: Colors.white38));
  }
}

class BoxFabAdd extends BoxFab {
  const BoxFabAdd({super.key, super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return FloatingActionButton(
          key: const Key('addBtn'),
          heroTag: 'addBtn',
          onPressed: () {
            BlocProvider.of<NavigationCubit>(ctx).go(HomeCreatePath());
          },
          backgroundColor: backgroundColor,
          tooltip: L10n.of(ctx).t8('Verse.add'),
          child: const Icon(
            Icons.add_rounded,
          ),
        );
      },
    );
  }
}

class BoxFabReview extends BoxFab {
  const BoxFabReview({super.key, super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState) {
      return FloatingActionButton(
        key: const Key('reviewBtn'),
        heroTag: 'reviewBtn',
        onPressed: () {
          final state = bloc.state;
          if (state is OrderSuccess && state.verses.isNotEmpty) {
            final selection = selectionState.selection;
            final batchSize = I<CurrentAccount>().reviewBatch;
            List<Verse> verses;
            if (selection.isNotEmpty) {
              verses = selection.values.toList();
              BlocProvider.of<SelectionBloc>(ctx).add(SelectionCleared());
            } else if (batchSize > 0 && batchSize < state.verses.length) {
              verses = state.verses.sublist(0, batchSize);
            } else {
              verses = [...state.verses];
            }
            if (state.order == VerseOrder.RANDOM) verses.shuffle();
            BlocProvider.of<FlashcardBloc>(ctx).add(
                FlashcardLoaded(verses: verses, box: bloc.box, inverse: false));
            I<NavigationCubit>()
                .go(HomeFlashcardPath(id: verses.first.id), propagates: false);
          }
        },
        backgroundColor: backgroundColor,
        shape: selectionState.selection.isEmpty ? null : border(context),
        child: const SvgAsset('cards_outline', height: 20),
      );
    });
  }

  /// Returns the first id of the current selection (or null)
  int? _idFromSelection(
      BuildContext ctx, List<Verse> verses, Iterable<int> selectedIds) {
    final id = selectedIds.isNotEmpty
        ? verses
            .where((verse) => selectedIds.contains(verse.id))
            .map((verse) => verse.id)
            .firstOrNull
        : null;
    if (selectedIds.isNotEmpty) {
      BlocProvider.of<SelectionBloc>(ctx).add(SelectionCleared());
    }
    return id;
  }
}

class BoxFabInverse extends BoxFab {
  const BoxFabInverse({super.key, super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState) {
      return FloatingActionButton(
        key: const Key('inverseBtn'),
        heroTag: 'inverseBtn',
        onPressed: () {
          final state = bloc.state;
          if (state is OrderSuccess && state.verses.isNotEmpty) {
            final selection = selectionState.selection;
            final batchSize = I<CurrentAccount>().inverseLimit;
            List<Verse> verses;
            if (selection.isNotEmpty) {
              verses = [...(selection.values)]..shuffle();
              BlocProvider.of<SelectionBloc>(ctx).add(SelectionCleared());
            } else {
              verses = [...state.verses]..shuffle();
              if (batchSize > 0 && batchSize < state.verses.length) {
                verses = verses.sublist(0, batchSize);
              }
            }
            BlocProvider.of<FlashcardBloc>(ctx).add(
                FlashcardLoaded(verses: verses, box: bloc.box, inverse: true));
            I<NavigationCubit>().go(HomeFlashcardPath(id: verses.first.id));
          }
        },
        backgroundColor: backgroundColor,
        shape: selectionState.selection.isEmpty ? null : border(context),
        child: const SvgAsset('cards_filled', height: 20),
      );
    });
  }
}

class BoxFabTts extends BoxFab {
  const BoxFabTts({super.key, super.backgroundColor});

  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return BlocBuilder<SelectionBloc, SelectionState>(
        builder: (context, selectionState) {
      return FloatingActionButton(
        key: const Key('ttsBtn'),
        heroTag: 'ttsBtn',
        onPressed: () {
          final verses = [...(bloc.state as OrderSuccess).verses];
          int index =
              _indexFromSelection(ctx, verses, selectionState.selection.keys);
          I<TtsHandler>()
            ..setUp(verses, index: index > -1 ? index : 0, box: bloc.box)
            ..play();
        },
        backgroundColor: backgroundColor,
        shape: selectionState.selection.isEmpty ? null : border(context),
        child: const Icon(
          Icons.play_arrow_rounded,
        ),
      );
    });
  }

  /// Returns the index of the current selection (or -1)
  int _indexFromSelection(
      BuildContext ctx, List<Verse> verses, Iterable<int> selectedIds) {
    final i = selectedIds.isNotEmpty
        ? verses.indexWhere((verse) => selectedIds.contains(verse.id))
        : -1;
    if (selectedIds.isNotEmpty) {
      BlocProvider.of<SelectionBloc>(ctx).add(SelectionCleared());
    }
    return i;
  }
}
