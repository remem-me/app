import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';

import '../../blocs/text_to_speech/tts_handler.dart';
import '../../models/models.dart';
import '../widgets.dart';

class TtsButton extends StatelessWidget {
  const TtsButton({
    super.key,
    required this.verses,
    required this.index,
    required this.box,
    this.spare,
  });

  final List<Verse> verses;
  final int index;
  final VerseBox? box;
  final Widget? spare;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlaybackState>(
        stream: TtsHandler().playbackState,
        builder: (ctx, snapshot) {
          return Row(
            children: [
              if (snapshot.data?.playing ?? false) ...[
                IconButton(
                    icon: const Icon(Icons.stop_rounded),
                    onPressed: () {
                      TtsHandler().stop();
                    }),
                const Setting(
                  l10nKey: 'Setting.speech.title',
                  form: SpeechSettingForm(),
                  icon: Icons.video_settings,
                  isCollapsed: true,
                ),
              ] else ...[
                IconButton(
                    icon: const Icon(Icons.play_arrow_rounded),
                    onPressed: () {
                      TtsHandler()
                        ..setUp(verses, index: index)
                        ..play(single: true);
                    }),
                if (spare != null) spare!
              ]
            ],
          );
        });
  }
}
