import 'package:flutter/material.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/web_frame/web_frame.dart';

import 'bible.dart';

class BibleWeb extends StatelessWidget {
  final BibleQuery query;

  const BibleWeb(this.query, {super.key});

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
        child: Stack(
      children: [
        WebFrame(query.uri.toString()),
        Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
                height: 48.0,
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                alignment: Alignment.centerLeft,
                color: Colors.black87,
                child: Text(query.version.site!.name!,
                    style: AppTheme.dark.textTheme.titleLarge!.copyWith(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w600,
                      color: AppTheme.dark.colorScheme.secondary,
                    )))),
        Positioned(
          right: 16.0,
          bottom: 20.0,
          child: PasteFab(query: query),
        ),
      ],
    ));
  }
}
