import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';
import 'package:remem_me/models/bibles/bibles.dart';

class PasteFab extends StatefulWidget {
  final BibleQuery query;

  const PasteFab({super.key,
    required this.query,
  });

  @override
  State<StatefulWidget> createState() {
    return _PasteFabState();
  }
}

class _PasteFabState extends State<PasteFab> {
  String? clipboardText;

  @override
  void initState() {
    super.initState();
    // query = BibleQuery('ESV', 66, 1, 1, 2);
    widget.query.get().then((value) async {
      await Clipboard.setData(ClipboardData(text: value));
      setState(() {
        clipboardText = value;
      });
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return PointerInterceptor(
        child: FloatingActionButton(
      backgroundColor: Theme.of(ctx).colorScheme.secondaryFixed,
      onPressed: () async {
        debugPrint('Paste pressed');
        final data = await Clipboard.getData(Clipboard.kTextPlain);
        Navigator.of(ctx).pop(data != null ? data.text : clipboardText);
      },
      child: clipboardText == null
          ? const Padding(
              padding: EdgeInsets.all(16.0),
              child: CircularProgressIndicator(color: Colors.white))
          : const Icon(Icons.paste_rounded),
    ));
  }
}
