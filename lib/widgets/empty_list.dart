import 'package:flutter/material.dart';

class EmptyList extends StatelessWidget {
  final IconData icon;

  const EmptyList({super.key, required this.icon});

  @override
  Widget build(BuildContext ctx) {
    return Center(
      child: Icon(
        icon,
        color: Theme.of(ctx).textTheme.bodyLarge!.color!.withOpacity(0.1),
        size: 256.0,
      ),
    );
  }
}
