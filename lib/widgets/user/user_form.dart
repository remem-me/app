import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/widgets/form/form.dart';

typedef OnSubmitCallback = Function(Map<String, String> credentials);

class UserForm extends StatefulWidget {
  final Map<String, String?> initialValues;
  final OnSubmitCallback onSubmit;
  final OnSubmitCallback? onForgotPassword;
  final OnSubmitCallback? onDelete;
  final void Function()? onCancel;
  final String titleL10nKey;
  final String submitL10nKey;
  final String? cancelL10nKey;
  final bool hasRepeatPassword;
  final bool hasUsername;
  final bool hasEmailAsUsername;
  final Map<String, List<String?>> errors;

  const UserForm(
      {super.key,
      this.initialValues = const {},
      required this.onSubmit,
      this.onForgotPassword,
      this.onDelete,
      this.onCancel,
      this.hasRepeatPassword = false,
      this.hasUsername = true,
      this.hasEmailAsUsername = true,
      required this.titleL10nKey,
      required this.submitL10nKey,
      this.cancelL10nKey,
      this.errors = const {}});

  @override
  State<StatefulWidget> createState() {
    return _UserFormState();
  }
}

/// Form for credentials
class _UserFormState extends State<UserForm> {
  static const _spacing = 16.0;
  final _formKey = GlobalKey<FormState>();
  final _fieldKeyEmail = GlobalKey<FormFieldState>();
  final _fieldKeyPassword = GlobalKey<FormFieldState>();
  final _credentials = <String, String>{};
  bool _isPasswordVisible = false;

  @override
  Widget build(BuildContext ctx) {
    // final fields = [fieldUser(ctx), fieldPassword(ctx)];

    final fields = <Widget>[
      Row(
        children: [
          Expanded(
              child: Text(L10n.of(ctx).t8(widget.titleL10nKey),
                  style: Theme.of(ctx).textTheme.titleLarge)),
          if (widget.onDelete != null) _deleteButton(ctx),
        ],
      ),
    ];

    if (widget.errors['messages'] != null) {
      fields.addAll([const SizedBox(height: _spacing), _errorMessages(ctx)]);
    }

    if (widget.hasUsername) {
      fields.addAll([
        const SizedBox(height: _spacing),
        widget.hasEmailAsUsername ? _emailField(ctx) : _nameField(ctx),
      ]);
    }

    fields.addAll([
      const SizedBox(height: _spacing),
      _passwordField(ctx),
    ]);

    if (widget.hasRepeatPassword) {
      fields.addAll([
        const SizedBox(height: _spacing),
        _repeatPasswordField(ctx),
      ]); // todo: validation
    }

    fields.addAll([
      Wrap(
        alignment: WrapAlignment.spaceBetween,
        children: [
          if (widget.onCancel != null) _cancelButton(ctx),
          if (widget.onForgotPassword != null) _forgotPasswordButton(ctx),
          const SizedBox(width: _spacing),
          _submitButton(ctx),
        ],
      ),
    ]);

    return _form(ctx, fields);
  }

  _form(BuildContext ctx, List<Widget> fields) {
    return Scrollbar(
      child: Container(
        constraints: const BoxConstraints(maxWidth: 512),
        child: SingleChildScrollView(
            primary: true,  // uses its scroll controller in Scrollbar
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                  key: _formKey,
                  child: AutofillGroup(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: fields,
                  ))),
            )),
      ),
    );
  }

  Text _errorMessages(BuildContext ctx) {
    debugPrint('Errors:\n${widget.errors['messages']!.join('\n')}');
    return Text(widget.errors['messages']!.join('\n'),
        key: const ValueKey('errorText'),
        style: Theme.of(ctx)
            .textTheme
            .bodyLarge!
            .copyWith(color: Theme.of(ctx).colorScheme.error));
  }

  StringInput _emailField(BuildContext ctx) {
    return StringInput(
      fieldKey: _fieldKeyEmail,
      l10nKey: 'User.email',
      iconData: Icons.email_rounded,
      initialValue: widget.initialValues['email'],
      autofillHints: const [AutofillHints.email],
      validator: Validators.compose([
        Validators.error<String?>(
            widget.errors['email'], widget.initialValues['email']),
        // errors! null on deleting user
        Validators.email(ctx),
        Validators.required(ctx),
      ]),
      autovalidateMode: widget.initialValues['email'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['email'] = value!.trim(),
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _nameField(BuildContext ctx) {
    return StringInput(
      l10nKey: 'User.name',
      iconData: Icons.folder_shared_rounded,
      initialValue: widget.initialValues['name'],
      autofillHints: const [AutofillHints.username],
      validator: Validators.error<String?>(
          widget.errors['name'], widget.initialValues['name']),
      autovalidateMode: widget.initialValues['name'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['name'] = value!.trim(),
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _repeatPasswordField(BuildContext ctx) {
    return StringInput(
      l10nKey: 'User.passwordRepeat',
      iconData: Icons.replay_rounded,
      obscureText: !_isPasswordVisible,
      validator: Validators.equal(
        ctx,
        _fieldKeyPassword,
        'User.password',
      ),
      onSaved: (_) {},
      onFieldSubmitted: (_) => _submit(),
    );
  }

  StringInput _passwordField(BuildContext ctx) {
    return StringInput(
      fieldKey: _fieldKeyPassword,
      l10nKey: 'User.password',
      iconData: Icons.security_rounded,
      initialValue: widget.initialValues['password'],
      autofillHints: const [AutofillHints.password],
      obscureText: !_isPasswordVisible,
      actionButton: IconButton(
        icon: Icon(
          _isPasswordVisible ? Icons.visibility : Icons.visibility_off,
        ),
        onPressed: () {
          setState(() {
            _isPasswordVisible = !_isPasswordVisible;
          });
        },
      ),
      validator: Validators.compose([
        Validators.error<String?>(
            widget.errors['password'], widget.initialValues['password']),
        Validators.required(ctx),
      ]),
      autovalidateMode: widget.initialValues['password'] != null
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      onSaved: (value) => _credentials['password'] = value?.trim() ?? '',
      onFieldSubmitted: (_) => _submit(),
    );
  }

  TextButton _submitButton(BuildContext ctx) {
    return TextButton(
        key: const ValueKey('submitButton'),
        style: TextButton.styleFrom(padding: const EdgeInsets.all(16.0)),
        onPressed: _submit,
        child: Text(L10n.of(ctx).t8(widget.submitL10nKey).toUpperCase()));
  }

  void _submit() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      widget.onSubmit(_credentials);
    }
  }

  IconButton _deleteButton(BuildContext ctx) {
    return IconButton(
        tooltip: L10n.of(ctx).t8('Button.delete'),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onDelete!(_credentials);
          }
        },
        icon: const Icon(Icons.delete_rounded));
  }

  TextButton _forgotPasswordButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(
            foregroundColor: Theme.of(ctx).textTheme.bodySmall!.color,
            padding: const EdgeInsets.all(16.0)),
        onPressed: () {
          if (_fieldKeyEmail.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onForgotPassword!(_credentials);
          }
        },
        child: Text(L10n.of(ctx).t8('User.forgotPassword').toUpperCase()));
  }

  TextButton _cancelButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(
            foregroundColor: Theme.of(ctx).textTheme.bodySmall!.color,
            padding: const EdgeInsets.all(16.0)),
        onPressed: widget.onCancel,
        child: Text(L10n.of(ctx)
            .t8(widget.cancelL10nKey ?? 'Button.cancel')
            .toUpperCase()));
  }
}
