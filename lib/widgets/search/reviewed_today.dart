import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../models/verse.dart';

class ReviewedToday extends StatelessWidget {
  final Iterable<Verse> verses;
  const ReviewedToday(this.verses, {super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
        builder: (ctx, filteredState) {
      final reviewedToday =
          (filteredState as FilteredVersesSuccess).reviewedTodayFilter.reviewedToday;
      return ListTile(
        enabled: verses.isNotEmpty,
        leading: _leadingIcon(reviewedToday),
        trailing:Text(verses.length.toString()),
        title: Text(L10n.of(ctx).t8('Filter.reviewedToday')),
        onTap: () {
          (BlocProvider.of<FilteredVersesBloc>(ctx))
              .add(ReviewedTodayFilterUpdated(_toggleTap(reviewedToday)));
        },
        onLongPress: () {
          (BlocProvider.of<FilteredVersesBloc>(ctx))
              .add(ReviewedTodayFilterUpdated(_toggleLongPress(reviewedToday)));
        },
      );
    });
  }

  Widget _leadingIcon(bool? reviewedToday) {
    return Icon(reviewedToday != null
        ? reviewedToday
            ? Icons.visibility_rounded
            : Icons.visibility_off_outlined
        : Icons.visibility_outlined);
  }

  bool? _toggleLongPress(bool? reviewedToday) {
    return false;
  }

  bool? _toggleTap(bool? reviewedToday) {
    return reviewedToday == null ? true : null;
  }
}
