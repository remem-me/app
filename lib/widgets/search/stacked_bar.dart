
import 'package:flutter/material.dart';

import '../../theme.dart';

/// Displays an overlay on top of an AppBar.
/// The overlay needs to be positioned,
/// e.g. Positioned(top: 0, right: 0, left: 0, child: overlay)
class StackedBar extends StatelessWidget implements PreferredSizeWidget {
  final PreferredSizeWidget base;
  final Widget overlay;

  const StackedBar({super.key, required this.base, required this.overlay});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: AppTheme.twilight,
      child: Stack(children: [
        base,
        overlay,
      ],),
    );
  }

  @override
  Size get preferredSize => base.preferredSize;
}