import 'dart:math';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../../blocs/blocs.dart';

class Shaker extends StatefulWidget {
  const Shaker({
    super.key,
    required this.child,
    this.shakeOffset = 8,
    this.shakeCount = 2,
    this.shakeDuration = const Duration(milliseconds: 300),
  });

  final Widget child;
  final double shakeOffset;
  final int shakeCount;
  final Duration shakeDuration;

  @override
  ShakerState createState() => ShakerState();
}

class ShakerState extends State<Shaker> with SingleTickerProviderStateMixin {
  late final animationController =
      AnimationController(vsync: this, duration: widget.shakeDuration);

  @override
  void initState() {
    super.initState();
    animationController.addStatusListener(_updateStatus);
    BlocProvider.of<StudyBloc>(context).feedback.listen((isCorrect) {
      if (!isCorrect &&
          (I<Settings>().getBool(Settings.STUDY_VIBRATION_ON) ?? true)) {
        shake();
        HapticFeedback.heavyImpact();
      }
    });
  }

  @override
  void dispose() {
    animationController.removeStatusListener(_updateStatus);
    animationController.dispose();
    super.dispose();
  }

  void _updateStatus(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      animationController.reset();
    }
  }

  void shake() {
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      child: widget.child,
      builder: (context, child) {
        final sineValue =
            sin(widget.shakeCount * 2 * pi * animationController.value);
        return Transform.translate(
          offset: Offset(sineValue * widget.shakeOffset, 0),
          child: child,
        );
      },
    );
  }
}
