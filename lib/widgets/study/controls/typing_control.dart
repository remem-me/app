import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/widgets/study/study.dart';

class TypingControl extends StudyControl<Typing> {
  const TypingControl({super.key, required super.onIn, required super.onOut})
      : super(height: 48, mainAxisAlignment: MainAxisAlignment.spaceBetween);

  @override
  KeyEventResult keyPressed(
      KeyDownEvent event, BuildContext ctx, Typing state) {
    if (event.logicalKey == LogicalKeyboardKey.backspace) {
      BlocProvider.of<StudyBloc>(ctx).add(Retracted());
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space &&
        !state.isComplete) {
      nextHint(ctx, state);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space &&
        state.isComplete) {
      menu(ctx);
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }

  @override
  List<Widget> buttons(BuildContext ctx, LineUp state) {
    return [
      ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 48.0),
          child: HintButton(onPressed: () => nextHint(ctx, state))),
      state.isComplete
          ? MenuButton(onPressed: () => menu(ctx))
          : const TypingBox(),
      ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 48.0),
          child: const RetractButton(small: true)),
    ];
  }

  void nextHint(BuildContext ctx, LineUp state) {
    final p = state.placeholders != null, i = state.initials != null;
    BlocProvider.of<StudyBloc>(ctx)
        .add(StudyHinted(hasPlaceholders: !i, hasInitials: !i && p));
  }
}
