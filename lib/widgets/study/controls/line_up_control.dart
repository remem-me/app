import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';

import '../../../blocs/blocs.dart';
import '../study.dart';

class LineUpControl extends StudyControl<LineUp> {
  const LineUpControl(
      {super.key, required super.onIn, required super.onOut});


  @override
  KeyEventResult keyPressed(KeyDownEvent event, BuildContext ctx, LineUp state) {
    if (event.logicalKey == LogicalKeyboardKey.backspace) {
      BlocProvider.of<StudyBloc>(ctx).add(Retracted());
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space && !state.isComplete) {
      nextHint(ctx, state);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.space && state.isComplete) {
      menu(ctx);
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.arrowRight) {
      BlocProvider.of<StudyBloc>(ctx).add(const LinedUp(byLine: false));
      return KeyEventResult.handled;
    } else if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
      BlocProvider.of<StudyBloc>(ctx).add(const LinedUp(byLine: true));
      return KeyEventResult.handled;
    }
    return KeyEventResult.ignored;
  }


  @override
  List<Widget> buttons(BuildContext ctx, LineUp state) {
    return [
      Expanded(child: HintButton(onPressed: () => nextHint(ctx, state))),
      Expanded(
        flex: 2,
          child: state.isComplete
              ? MenuButton(onPressed: () => menu(ctx))
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(child: _btnNextWord(ctx)),
                    Expanded(child: _btnNextLine(ctx)),
                  ],
                )),
      const Expanded(child: RetractButton()),
    ];
  }

  IconButton _btnNextWord(BuildContext ctx) {
    return IconButton(
        tooltip: '${L10n.of(ctx).t8('Study.nextWord')} ${kIsWeb ? '➡️' : '⇨'}',
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(const LinedUp(byLine: false));
        },
        icon: const Icon(Icons.plus_one_rounded));
  }

  IconButton _btnNextLine(BuildContext ctx) {
    return IconButton(
        tooltip: '${L10n.of(ctx).t8('Study.nextLine')} ${kIsWeb ? '⬇️️' : '⇩'}',
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(const LinedUp(byLine: true));
        },
        icon: const Icon(Icons.playlist_add_rounded));
  }

  void nextHint(BuildContext ctx, LineUp state) {
        final p = state.placeholders != null, i = state.initials != null;
        BlocProvider.of<StudyBloc>(ctx).add(StudyHinted(
            hasPlaceholders: state.placeholders == null,
            hasInitials: p == i &&
                !['zh', 'ja'].contains(I<CurrentAccount>().language.languageCode)));
      }

}
