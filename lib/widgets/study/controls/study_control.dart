import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class StudyControl<T extends Study> extends StatelessWidget {
  final AnimationCallback onIn;
  final AnimationCallback onOut;
  final double height;
  final MainAxisAlignment mainAxisAlignment;

  const StudyControl(
      {super.key,
      required this.onOut,
      required this.onIn,
      this.height = 56,
      this.mainAxisAlignment = MainAxisAlignment.spaceAround});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<StudyBloc, StudyState>(
        listener: (BuildContext ctx, StudyState state) => onIn((_) {}),
        builder: (BuildContext ctx, StudyState state) {
          return Container(
              height: height,
              alignment: Alignment.center,
              child: state is Study ? bar(ctx, state as T) : const SizedBox.shrink());
        });
  }

  Widget bar(BuildContext ctx, T state) {
    return Focus(
      autofocus: true,
      onKeyEvent: (node, event) {
        if (event is KeyDownEvent) {
          return keyPressed(event, ctx, state);
        }
        return KeyEventResult.ignored;
      },
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: mainAxisAlignment,
        mainAxisSize: MainAxisSize.max,
        children: buttons(ctx, state),
      ),
    );
  }

  KeyEventResult keyPressed(KeyDownEvent event, BuildContext ctx, T state) {
    return KeyEventResult.ignored;
  }

  List<Widget> buttons(BuildContext ctx, T state) {
    return [
      Expanded(
        child: IconButton(
            tooltip: L10n.of(ctx).t8('Study.obfuscate'),
            onPressed: () =>
                onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(const Obfuscated())),
            icon: const Icon(Icons.wb_cloudy_rounded)),
      ),
      Expanded(
        child: IconButton(
            tooltip: L10n.of(ctx).t8('Study.puzzle'),
            onPressed: () =>
                onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(const Puzzled())),
            icon: const Icon(Icons.extension_rounded)),
      ),
      Expanded(
        child: IconButton(
            tooltip: L10n.of(ctx).t8('Study.lineUp'),
            onPressed: () =>
                onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(const LinedUp())),
            icon: const Icon(Icons.subject_rounded)),
      ),
      Expanded(
        child: IconButton(
            tooltip: L10n.of(ctx).t8('Study.typing'),
            onPressed: () =>
                onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(const Typed())),
            icon: const Icon(Icons.keyboard_rounded)),
      ),
    ];
  }

  void menu(BuildContext ctx) {
    onOut((_) => BlocProvider.of<StudyBloc>(ctx).add(const StudyStarted()));
  }
}
