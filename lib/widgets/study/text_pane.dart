import 'dart:math';

import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';

import 'study.dart';

class TextPane extends StatefulWidget {
  final StudyState state;

  const TextPane(this.state, {super.key});

  @override
  State<StatefulWidget> createState() {
    return _TextPaneState();
  }
}

class _TextPaneState extends State<TextPane> {
  final ScrollController _scrollController = ScrollController();

  final GlobalKey scrollTo = GlobalKey();

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget as TextPane);
    if ([LineUp, Puzzle, Typing].contains(widget.state.runtimeType)) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final scrollToContext = scrollTo.currentContext;
        if (scrollToContext != null) {
          Scrollable.ensureVisible(scrollToContext,
              duration: const Duration(milliseconds: 400));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final study = widget.state;
    if (study is Study) {
      final framer = Framer(context, study);
      final CurrentAccount account = I<CurrentAccount>();
      return Scrollbar(
          controller: _scrollController,
          child: DefaultTextStyle(
            style: framer.stylePlain(),
            child: SafeArea(
              child: ListView.builder(
                key: const PageStorageKey<String>('study_scroller'),
                padding: const EdgeInsets.all(16.0),
                controller: _scrollController,
                itemCount: min(study.lines.length, study.pos.line + 1),
                itemBuilder: (ctx, i) {
                  return TextWrap(i,
                      study: study,
                      framer: framer,
                      scrollTo: scrollTo,
                      textDirection:
                          account.referenceIncluded && i == study.lines.length - 1
                              ? account.langRefDirection
                              : account.languageDirection);
                },
              ),
            ),
          ));
    } else {
      return const SizedBox.shrink();
    }
  }
}
