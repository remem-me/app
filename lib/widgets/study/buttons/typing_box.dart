import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../../theme.dart';

class TypingBox extends StatefulWidget {
  const TypingBox({super.key});

  @override
  State<TypingBox> createState() => _TypingBoxState();
}

class _TypingBoxState extends State<TypingBox> with WidgetsBindingObserver {
  static final TextInputType _KEYBOARD_TEXT = TextInputType.text;
  static final TextInputType _KEYBOARD_NUMBER =
      TextInputType.numberWithOptions(decimal: true);
  late FocusNode _focusNode;
  late TextEditingController _typingController;
  late StreamSubscription _clearSubscription;
  late StreamSubscription _keyboardTypeSubscription;
  late VoidCallback _settingsListener;
  final ValueListenable _settingsListenable =
      I<Settings>().listenable(keys: [Settings.STUDY_DICTATION_ON]);
  TextInputType _keyboardType = TextInputType.text;

  @override
  void initState() {
    super.initState();
    _typingController = TextEditingController();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
    _clearSubscription = _subscribeClear();
    _keyboardTypeSubscription = _subscribeKeyboardTypeByInput();
    _settingsListener = _listenKeyboardTypeBySetting();
    _settingsListenable.addListener(_settingsListener);
  }

  StreamSubscription<StudyState> _subscribeClear() {
    return BlocProvider.of<StudyBloc>(context).stream.distinct((prev, next) {
      return prev is! Typing ||
          next is! Typing ||
          prev.pos.line == next.pos.line;
    }).listen((_) {
      if (_typingController.value.text.length > 500) _typingController.clear();
    });
  }

  StreamSubscription<TextInputType> _subscribeKeyboardTypeByInput() {
    return BlocProvider.of<StudyBloc>(context)
        .stream
        .startWith(BlocProvider.of<StudyBloc>(context).state)
        .map(_keyboardTypeFromStudyState)
        .where((keyboardType) => _keyboardType != keyboardType)
        .listen((keyboardType) {
      if (!(I<Settings>().getBool(Settings.STUDY_DICTATION_ON) ?? true)) {
        _updateKeyboardType(keyboardType);
      }
    });
  }

  Function() _listenKeyboardTypeBySetting() {
    return () {
      if ((I<Settings>().getBool(Settings.STUDY_DICTATION_ON) ?? true)) {
        if (_keyboardType != _KEYBOARD_TEXT) {
          _updateKeyboardType(_KEYBOARD_TEXT);
        }
      } else {
        final state = BlocProvider.of<StudyBloc>(context).state;
        if (_keyboardType != _KEYBOARD_NUMBER &&
            _keyboardTypeFromStudyState(state) == _KEYBOARD_NUMBER) {
          _updateKeyboardType(_KEYBOARD_NUMBER);
        }
      }
    };
  }

  TextInputType _keyboardTypeFromStudyState(state) => state is Typing &&
          double.tryParse(
                  TextService().stripPunctuation(state.nextWord ?? '')) !=
              null
      ? _KEYBOARD_NUMBER
      : _KEYBOARD_TEXT;

  void _updateKeyboardType(TextInputType keyboardType) {
    setState(() {
      _keyboardType = keyboardType;
    });
    _focusNode.unfocus();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _focusNode.requestFocus();
    });
  }

  @override
  void dispose() {
    _focusNode.ancestors.first.requestFocus();
    _focusNode.dispose();
    _clearSubscription.cancel();
    _keyboardTypeSubscription.cancel();
    _settingsListenable.removeListener(_settingsListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) {
    return Stack(
      children: [
        _display(ctx),
        Positioned.fill(child: _listener(ctx)),
      ],
    );
  }

  Widget _display(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      final typing = state as Typing;
      return Container(
          padding: const EdgeInsets.only(left: 8, right: 8),
          constraints: const BoxConstraints(minWidth: 48),
          decoration: BoxDecoration(
            color: typing.isCorrect
                ? AppTheme.dark.colorScheme.secondaryFixed
                : AppTheme.dark.colorScheme.tertiary,
            borderRadius: const BorderRadius.all(Radius.circular(8)),
          ),
          height: 48,
          child: Center(
              child: Text(
            typing.letters ?? '   ',
            style: AppTheme.dark.textTheme.labelLarge!.copyWith(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w400,
                fontFamily: I<CurrentAccount>().font),
          )));
    });
  }

  Widget _listener(BuildContext ctx) {
    bool isDictationOn =
        Settings().getBool(Settings.STUDY_DICTATION_ON) ?? true;
    return Opacity(
        opacity: 0.0,
        child: TextField(
            controller: _typingController,
            focusNode: _focusNode,
            showCursor: false,
            enableSuggestions: isDictationOn,
            enableInteractiveSelection: false,
            enableIMEPersonalizedLearning: isDictationOn,
            autocorrect: false,
            obscureText: !isDictationOn,
            keyboardType: _keyboardType,
            obscuringCharacter: ' ',
            inputFormatters: [
              TextInputFormatter.withFunction(
                  (TextEditingValue oldValue, TextEditingValue newValue) {
                List<String> newWords =
                    TextService().newText(oldValue.text, newValue.text);
                for (var word in newWords) {
                  if (word.isNotEmpty) {
                    BlocProvider.of<StudyBloc>(ctx).add(Typed(letters: word));
                  }
                }
                return isDictationOn ? newValue : const TextEditingValue();
              }),
            ]));
  }
}
