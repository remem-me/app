import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/study/study.dart';

class MenuButton extends StudyButton {
  final void Function() onPressed;

  const MenuButton({super.key, required this.onPressed, bool super.small = false});

  @override
  Widget build(BuildContext ctx) {
    return IconButton(
        tooltip: '${L10n.of(ctx).t8('Button.menu')} ${kIsWeb ? '🈁' : '␣'}',
        padding: EdgeInsets.all(small! ? 4.0 : 8.0),
        onPressed: onPressed,
        icon: const Icon(Icons.more_horiz_rounded));
  }
}
