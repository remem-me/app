import 'dart:math';

import 'package:flutter/material.dart';

import '../../blocs/blocs.dart';
import '../../models/models.dart';
import 'study.dart';

class TextWrap extends StatelessWidget {
  final int i;
  final Study study;
  final Framer framer;
  final GlobalKey scrollTo;
  final TextDirection textDirection;

  const TextWrap(this.i,
      {super.key,
      required this.study,
      required this.framer,
      required this.scrollTo,
      required this.textDirection});

  @override
  Widget build(BuildContext context) {
    List<Widget> items = _wrapItems();
    return Padding(
      padding: EdgeInsets.only(bottom: _paddingBottom()),
      child: Wrap(
        textDirection: textDirection,
        children: items,
      ),
    );
  }

  List<Widget> _wrapItems() {
    final study = this.study;
    var items = <Widget>[];
    for (var j = 0;
        i < study.pos.line && j < study.lines[i].length ||
            i == study.pos.line && j <= study.pos.word;
        j++) {
      items.add(framer.format(Pos(i, j), textDirection));
      if (study is LineUp && study.revealed == Pos(i, j) ||
          study is Puzzle && study.pos == Pos(i, j)) {
        items.add(SizedBox.shrink(key: scrollTo));
      }
    }
    return items;
  }

  double _paddingBottom() {
    final lineBreaks = '\n'.allMatches(study.lines[i].last).length;
    return max(lineBreaks - 1, 0) * 16.0;
  }
}
