import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SvgAsset extends StatelessWidget {
  final String assetName;
  final double? width;
  final double? height;
  final BoxFit fit;
  final Color? color;
  final AlignmentGeometry alignment;
  final String? semanticLabel;

  const SvgAsset(this.assetName,
      {super.key,
      this.width,
      this.height,
      this.fit = BoxFit.contain,
      this.color,
      this.alignment = Alignment.center,
      this.semanticLabel});

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset('assets/svg/$assetName.svg',
        width: width,
        height: height,
        fit: fit,
        colorFilter:
            color != null ? ColorFilter.mode(color!, BlendMode.srcIn) : null,
        alignment: alignment,
        semanticsLabel: semanticLabel);
  }
}
