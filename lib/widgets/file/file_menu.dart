import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/file_import_service.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';
import '../../services/file_export_service.dart';

enum FileMenuAction { export, import }

class FileMenuButton extends StatelessWidget {
  final void Function()? onDo;

  const FileMenuButton({super.key, this.onDo});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<FileMenuAction>(
      tooltip: L10n.of(context).t8('File.title'),
      icon: const Icon(Icons.file_open_rounded),
      onSelected: (FileMenuAction action) => _do(context, action),
      itemBuilder: (BuildContext context) => <PopupMenuEntry<FileMenuAction>>[
        PopupMenuItem<FileMenuAction>(
          value: FileMenuAction.export,
          child: Text(L10n.of(context).t8('File.export')),
        ),
        PopupMenuItem<FileMenuAction>(
          value: FileMenuAction.import,
          child: Text(L10n.of(context).t8('File.import')),
        ),
      ],
    );
  }

  void _do(BuildContext ctx, FileMenuAction action) async {
    if (onDo != null) onDo!();
    switch (action) {
      case FileMenuAction.export:
        _export(ctx);
        break;
      case FileMenuAction.import:
        _import(action, ctx);
        break;
    }
  }

  void _export(BuildContext ctx) async {
    final tabState = I<TabBloc>().state;
    final bloc = OrderedVersesBloc.from(tabState);
    final verses = (bloc.state as OrderSuccess).verses;
    final isExported = await FileExportService().writeCsv(verses);
    MessageService().info(L10n.current.t8(
        'File.export.success', [(isExported ? verses.length : 0).toString()]));
  }

  Future<void> _import(FileMenuAction action, BuildContext ctx) async {
    final tagsRepository = RepositoryProvider.of<TagsRepository>(ctx);
    final tagsBloc = BlocProvider.of<TagsBloc>(ctx);
    final versesBloc = BlocProvider.of<VersesBloc>(ctx);
    final undoBloc = BlocProvider.of<UndoBloc>(ctx);
    final newTags = <TagEntity>[];

    final List<VerseEntity> verses = await FileImportService().read(newTags);
    if (verses.isNotEmpty) {
      if (newTags.isNotEmpty) {
        await tagsRepository.createList(newTags);
        tagsBloc.add(EntitiesAdded<TagEntity>(newTags, persist: false));
      }
      versesBloc.add(EntitiesAdded<VerseEntity>(verses));
      undoBloc.add(UndoAdded([
        Undo(EntityAction.create, verses),
        if (newTags.isNotEmpty) Undo(EntityAction.create, newTags),
      ]));
      // undoBloc.add(UndoCleared());
      MessageService().info(
          L10n.current.t8('File.import.success', [verses.length.toString()]));
    } else {
      MessageService().info(
          L10n.current.t8('File.import.empty', [verses.length.toString()]));
    }
  }
}
