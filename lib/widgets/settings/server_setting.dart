import 'package:flutter/material.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:repository/repository.dart';

class ServerSettingForm extends StatefulWidget {
  const ServerSettingForm({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ServerSettingState();
  }
}

class _ServerSettingState extends State<ServerSettingForm> {
  var _rpl = Settings().getOrDefault(Settings.RPL_SERVER);
  var _bible = Settings().getOrDefault(Settings.BIBLE_SERVER);
  var _image = Settings().getOrDefault(Settings.IMAGE_SERVER);
  var _legacy = Settings().getOrDefault(Settings.LEGACY_SERVER);
  var _doc = Settings().getOrDefault(Settings.DOC_SERVER);

  @override
  Widget build(BuildContext ctx) {
    return Padding(
        padding: const EdgeInsets.all(16),
        child: Column(mainAxisSize: MainAxisSize.min, children: [
          StringInput(
              l10nKey: 'Replication server',
              initialValue: _rpl,
              onChanged: (value) async {
                Settings().put(Settings.RPL_SERVER, value);
                setState(() {
                  _rpl = value;
                });
              }),
          StringInput(
              l10nKey: 'Bible server',
              initialValue: _bible,
              onChanged: (value) async {
                Settings().put(Settings.BIBLE_SERVER, value);
                setState(() {
                  _bible = value;
                });
              }),
          StringInput(
              l10nKey: 'Image server',
              initialValue: _image,
              onChanged: (value) async {
                Settings().put(Settings.IMAGE_SERVER, value);
                setState(() {
                  _image = value;
                });
              }),
          StringInput(
              l10nKey: 'Legacy server',
              initialValue: _legacy,
              onChanged: (value) async {
                Settings().put(Settings.LEGACY_SERVER, value);
                setState(() {
                  _legacy = value;
                });
              }),
          StringInput(
              l10nKey: 'Documentation server',
              initialValue: _doc,
              onChanged: (value) async {
                Settings().put(Settings.DOC_SERVER, value);
                setState(() {
                  _doc = value;
                });
              }),
        ]));
  }
}
