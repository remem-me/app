import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SettingNumber extends StatelessWidget {
  final double value;
  final void Function(double) onChanged;
  final IconData icon;
  final String l10nKey;
  final Widget? suffix;
  final double? min;
  final double? max;

  const SettingNumber(
      {super.key,
      required this.value,
      required this.onChanged,
      required this.icon,
      required this.l10nKey,
      this.suffix,
      this.min,
      this.max});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Tooltip(message: L10n.of(context).t8(l10nKey), child: Icon(icon)),
      const SizedBox(width: 24.0),
      Expanded(
        child: TextFormField(
          initialValue: value == 0
              ? ''
              : value % 1 == 0
                  ? value.toInt().toString()
                  : value.toString(),
          keyboardType: const TextInputType.numberWithOptions(decimal: true),
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*$'))
          ],
          textAlign: TextAlign.end,
          onChanged: (value) {
            if (value.isNotEmpty) {
              var num = double.parse(value);
              if (min != null && num < min!) num = min!;
              if (max != null && num > max!) num = max!;
              onChanged(num);
            } else {
              onChanged(0.0);
            }
          },
        ),
      ),
      if (suffix != null) suffix!,
      const SizedBox(width: 24.0),
    ]);
  }
}
