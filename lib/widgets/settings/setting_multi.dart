import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/settings/setting_button.dart';

class SettingOption<T> {
  final T value;
  final IconData icon;
  final String? l10nKey;

  const SettingOption(
    this.value, {
    required this.icon,
    this.l10nKey,
  });
}

class SettingMulti<T> extends StatelessWidget {
  final IconData icon;
  final String l10nKey;
  final List<SettingOption<T>> options;
  final T selected;
  final void Function(T) onChanged;

  const SettingMulti({
    super.key,
    required this.icon,
    required this.l10nKey,
    required this.options,
    required this.selected,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Tooltip(message: L10n.of(context).t8(l10nKey), child: Icon(icon)),
        ...options.map((option) {
          return buildButton(context, option);
        }),
      ],
    );
  }

  Widget buildButton(
    BuildContext ctx,
    SettingOption option,
  ) {
    return SettingButton(
        iconData: option.icon,
        isSelected: selected == option.value,
        onPressed: () => onChanged(option.value),
        l10nKey: option.l10nKey);
  }
}
