import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:repository/repository.dart';

import '../../models/tip.dart';

class TipRow extends StatelessWidget {
  final Tip tip;

  const TipRow(this.tip, {super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Tooltip(message: L10n.of(context).t8('Tip.title'), child: tip.icon),
        const SizedBox(width: 20),
        Expanded(
            child: Text(
          L10n.of(context).t8(tip.l10nKey),
          style: Theme.of(context).textTheme.bodyMedium!.copyWith(
              color: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .color!
                  .withOpacity(0.64)),
        )),
        Tooltip(
          message: L10n.of(context).t8('Tip.hide'),
          child: IconButton(
              icon: const Icon(Icons.close_rounded),
              onPressed: () => I<Settings>().putBool(tip.settingKey, false)),
        ),
      ],
    );
  }

  static TipRow tooltip(BuildContext ctx) {
    final tipIconColor =  Theme.of(ctx).disabledColor;
    return TipRow(Tip(
      l10nKey: kIsWeb ? 'Tip.tooltip.hover' : 'Tip.tooltip.press',
      icon: Icon(Icons.arrow_downward_rounded, color: tipIconColor),
      settingKey: Settings.TIP_TOOLTIP,
    ));
  }

  static TipRow recover(BuildContext ctx) {
    final tipIconColor =  Theme.of(ctx).disabledColor;
    return TipRow(Tip(
      l10nKey: 'Tip.verses.recover',
      icon: Icon(Icons.info_outline_rounded, color: tipIconColor),
      settingKey: Settings.TIP_VERSES_RECOVER,
    ));
  }
}
