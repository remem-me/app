import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';

class SettingSlider extends StatelessWidget {
  final IconData icon;
  final String l10nKey;
  final double value;
  final void Function(double) onChanged;
  final double min;
  final double max;
  final int? divisions;

  const SettingSlider(
      {super.key,
      required this.value,
      required this.onChanged,
      required this.icon,
      required this.l10nKey,
      required this.min,
      required this.max,
      this.divisions});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Tooltip(message: L10n.of(context).t8(l10nKey), child: Icon(icon)),
      Expanded(
          child: Slider(
        value: value,
        min: min,
        max: max,
        divisions: divisions,
        label: ((value * 100.0).roundToDouble()/100).toString(),
        onChanged: onChanged,
      )),
    ]);
  }
}
