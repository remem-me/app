import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';
import '../zoom/zoom.dart';

@visibleForTesting
class GeneralSettingForm extends StatefulWidget {
  const GeneralSettingForm({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GeneralSettingFormState();
  }
}

class _GeneralSettingFormState extends State<GeneralSettingForm> {
  final settings = I<Settings>();

  @override
  Widget build(BuildContext ctx) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 480),
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: ValueListenableBuilder<Box>(
              valueListenable:
                  I<Settings>().listenable(keys: Settings.ALL_TIPS),
              builder: (context, bx, wdg) {
                final isTipsOn = Settings.ALL_TIPS
                    .every((key) => settings.getBool(key) ?? true);
                return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(L10n.of(context).t8('Setting.general.title'),
                          style: Theme.of(ctx).textTheme.titleSmall),
                      if (settings.getBool(Settings.TIP_TOOLTIP) ?? true)
                        TipRow.tooltip(ctx),
                      const SizedBox(height: 8),
                      SettingSwitch(
                          value: isTipsOn,
                          onChanged: (result) {
                            for (String key in Settings.ALL_TIPS) {
                              settings.putBool(key, result);
                            }
                          },
                          icon: Icons.info_rounded,
                          l10nKey: 'Setting.general.tips'),
                      const SizedBox(height: 8),
                      SettingMulti<ThemeMode>(
                          icon: Icons.brightness_medium_rounded,
                          l10nKey: 'Setting.theme.title',
                          options: [
                            SettingOption<ThemeMode>(
                              ThemeMode.light,
                              icon: Icons.light_mode_rounded,
                              l10nKey: 'Setting.theme.light',
                            ),
                            SettingOption<ThemeMode>(
                              ThemeMode.dark,
                              icon: Icons.dark_mode_rounded,
                              l10nKey: 'Setting.theme.dark',
                            ),
                            SettingOption<ThemeMode>(
                              ThemeMode.system,
                              icon: Icons.brightness_auto_rounded,
                              l10nKey: 'Setting.theme.system',
                            ),
                          ],
                          selected: settings.getEnum(
                                  Settings.THEME_MODE, ThemeMode.values) ??
                              ThemeMode.system,
                          onChanged: (result) async {
                            await settings.putEnum(Settings.THEME_MODE, result);
                            setState(() {});
                          }),
                      const SizedBox(height: 8),
                      SettingSlider(
                          value:
                              settings.getDouble(Settings.ZOOM_SCALE_FACTOR) ??
                                  1.0,
                          onChanged: (result) async {
                            await settings.putDouble(
                                Settings.ZOOM_SCALE_FACTOR, result);
                            I<ZoomCubit>().reset();
                            setState(() {});
                          },
                          icon: Icons.format_size_rounded,
                          l10nKey: 'Setting.general.zoom',
                          min: 0.6,
                          max: 2.0,
                          divisions: 14),
                    ]);
              })),
    );
  }
}
