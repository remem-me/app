import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/box/box.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';

class TipTile extends StatelessWidget {
  final Tip tip;

  const TipTile(this.tip, {super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          minVerticalPadding: 0.0,
          contentPadding: const EdgeInsets.only(left: 8.0),
          horizontalTitleGap: 12.0,
          leading: Tooltip(
            message: L10n.of(context).t8('Tip.title'),
            child: SizedBox.square(
              dimension: 48.0,
              child: Center(child: tip.icon),
              //child: Icon(Icons.arrow_downward_rounded),
            ),
          ),
          title: ConstrainedBox(
            constraints: const BoxConstraints(minHeight: 56.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(L10n.of(context).t8(tip.l10nKey),
                    style: Theme.of(context).textTheme.bodyMedium!.copyWith(color: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .color!
                        .withOpacity(0.64))),
              ],
            ),
          ),
          trailing: SizedBox.square(
                  dimension: 48.0,
                  child: Tooltip(
                    message: L10n.of(context).t8('Tip.hide'),
                    child: IconButton(
                        icon: const Icon(Icons.close_rounded),
                        onPressed: () => I<Settings>()
                            .putBool(tip.settingKey, false)),
                  )),
        ),
        const VerseDivider(),
      ],
    );
  }
}
