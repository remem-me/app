import 'package:flutter/material.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class FlashcardSettingForm extends StatefulWidget {
  const FlashcardSettingForm({super.key});

  @override
  State<StatefulWidget> createState() {
    return _FlashcardSettingFormState();
  }
}

class _FlashcardSettingFormState extends State<FlashcardSettingForm> {
  @override
  Widget build(BuildContext ctx) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 480),
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            SettingSlider(
              // todo: replace with flashcard settings
              value: Settings().getDouble(Settings.SPEECH_SPEED) ?? 0.5,
              onChanged: (result) async {
                await Settings().putDouble(Settings.SPEECH_SPEED, result);
                setState(() {});
              },
              icon: Icons.speed_rounded,
              l10nKey: 'Setting.speech.rate',
              min: 0.0,
              max: 1.0,
              divisions: 10,
            ),
          ])),
    );
  }
}
