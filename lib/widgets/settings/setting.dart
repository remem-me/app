import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';

import '../../theme.dart';

class Setting extends StatelessWidget {
  final String l10nKey;
  final Widget form;
  final bool isCollapsed;
  final IconData icon;
  final Color? iconColor;

  const Setting({
    super.key,
    required this.l10nKey,
    required this.form,
    this.isCollapsed = false,
    this.icon = Icons.settings_rounded,
    this.iconColor,
  });

  @override
  Widget build(BuildContext context) {
    final title = L10n.of(context).t8(l10nKey);
    action() => edit(context);
    return isCollapsed
        ? IconButton(
            color: iconColor,
            tooltip: title,
            icon: Icon(icon),
            onPressed: action,
          )
        : ListTile(
            leading: Icon(icon, color: iconColor),
            title: Text(title),
            onTap: action,
          );
  }

  Future<String?> edit(BuildContext ctx) {
    return showDialog<String>(
        context: ctx,
        builder: (BuildContext context) {
          return Theme(
              data:
                  MediaQuery.of(context).platformBrightness == Brightness.light
                      ? AppTheme.light
                      : AppTheme.dark,
              child: Dialog(child: form));
        });
  }
}
