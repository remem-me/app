import 'package:flutter/material.dart';
import 'package:app_core/app_core.dart';

class SettingButton extends StatelessWidget {
  final IconData iconData;
  final bool isSelected;
  final void Function() onPressed;
  final String? l10nKey;

  const SettingButton(
      {super.key,
      required this.iconData,
      this.isSelected = false,
      required this.onPressed,
      this.l10nKey});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 24),
        child: isSelected
            ? Container(
            decoration: BoxDecoration(
              color: AppColorScheme.dark.primaryFixed,
              border: Border.all(
                width: 2,
                color: AppColorScheme.dark.primaryFixed,
              ),
              borderRadius: BorderRadius.circular(8.0),
            ),
                child: _tooltip(
                    context,
                    _iconButton(context, 0,
                        color: AppColorScheme.dark.onSurface)))
            : Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                    color: IconTheme.of(context).color!,
                  ),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: _tooltip(context, _iconButton(context, 0))));
  }

  Widget _tooltip(BuildContext ctx, Widget child) {
    if (l10nKey == null) return child; // RETURN
    final l10n = L10n.of(ctx);
    return Tooltip(
        message:
            '${l10n.t8(l10nKey!)}: ${l10n.t8(isSelected ? 'Settings.on' : 'Settings.off')}',
        child: child);
  }

  IconButton _iconButton(BuildContext ctx, double padding, {Color? color}) {
    return IconButton(
      splashColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.24),
      highlightColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.24),
      hoverColor: Theme.of(ctx).colorScheme.primary.withOpacity(0.12),
      padding: EdgeInsets.all(padding),
      //constraints: const BoxConstraints(),
      icon: Icon(iconData, color: color),
      onPressed: onPressed,
    );
  }
}
