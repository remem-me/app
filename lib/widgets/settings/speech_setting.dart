import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/util/build_context.dart';
import 'package:remem_me/widgets/settings/setting_number.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';
import '../widgets.dart';

class SpeechSettingForm extends StatefulWidget {
  const SpeechSettingForm({super.key});

  @override
  State<SpeechSettingForm> createState() => _SpeechSettingFormState();
}

class _SpeechSettingFormState extends State<SpeechSettingForm> {
  final Settings _settings = I<Settings>();

  bool get _playlist => _settings.getBool(Settings.SPEECH_PLAYLIST) ?? true;

  bool get _repeat => _settings.getBool(Settings.SPEECH_REPEAT) ?? false;

  bool get _musicOff => _settings.getBool(Settings.SPEECH_MUSIC_OFF) ?? true;

  bool get _captions => _settings.getBool(Settings.SPEECH_CAPTIONS) ?? false;

  bool get _script => _settings.getBool(Settings.SPEECH_SCRIPT) ?? true;

  @override
  Widget build(BuildContext ctx) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 480),
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: ValueListenableBuilder<Box>(
              valueListenable: I<Settings>().listenable(keys: [
                Settings.TIP_TOOLTIP,
              ]),
              builder: (ctx, bx, wdg) {
                return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(L10n.of(ctx).t8('Setting.speech.title'),
                          style: Theme.of(ctx).textTheme.titleMedium),
                      if (_settings.getBool(Settings.TIP_TOOLTIP) ?? true) ...[
                        const SizedBox(height: 8),
                        TipRow.tooltip(ctx)
                      ],
                      if (I<TtsService>().isVoiceSelectable) _buildVoices(ctx),
                      _buildSpeed(),
                      _buildVolume(),
                      const SizedBox(height: 8.0),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(child: _buildPause(ctx)),
                          Expanded(child: _buildTimer(ctx))
                        ],
                      ),
                      const SizedBox(height: 16.0),
                      _buildControl(ctx),
                      const SizedBox(height: 16.0),
                      _buildTranscription(ctx),
                    ]);
              })),
    );
  }

  Widget _buildVoices(BuildContext ctx) {
    return BlocBuilder<CurrentAccount, Account?>(
        bloc: I<CurrentAccount>(),
        builder: (context, account) {
          if (account == null) return SizedBox.shrink(); // RETURN
          return Column(
            children: account.language == account.langRef
                ? [
                    _buildVoice(ctx, account.language,
                        iconData: Icons.record_voice_over_rounded)
                  ]
                : [
                    _buildVoice(ctx, account.language,
                        l10nKey: 'Verse.passage',
                        iconData: Icons.record_voice_over_outlined),
                    _buildVoice(ctx, account.langRef,
                        l10nKey: 'Verse.reference',
                        iconData: Icons.record_voice_over_rounded)
                  ],
          );
        });
  }

  Widget _buildVoice(BuildContext ctx, Locale locale,
      {String? l10nKey, required IconData iconData}) {
    final List<DropdownMenuEntry<Voice>> entries = I<TtsService>()
        .voices
        .map(
            (voice) => DropdownMenuEntry(value: voice, label: voice.toString()))
        .toList();
    final t8 = L10n.of(context).t8;
    final sectionName = l10nKey != null ? '(${t8(l10nKey)}) ' : '';
    final localeName = localeNameOf(ctx, locale.toString());
    return Row(children: [
      Tooltip(
          message: "${t8('Setting.speech.voice')} $sectionName- $localeName",
          child: Icon(iconData)),
      SizedBox(width: 20.0),
      Expanded(
          child: FutureBuilder(
        future: I<TtsService>().defaultVoice(locale),
        builder: (context, snapshot) {
          return DropdownMenu<Voice>(
            hintText: t8('Setting.speech.voice.empty'),
            expandedInsets: EdgeInsets.zero,
            textStyle: TextStyle(overflow: TextOverflow.ellipsis),
            inputDecorationTheme: InputDecorationTheme(
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
            initialSelection: _settings.getVoice(locale) ?? snapshot.data,
            dropdownMenuEntries: entries,
            onSelected: (result) async {
              if (result != null) {
                await _settings.putVoice(locale, result);
                setState(() {});
              }
            },
          );
        },
      )),
    ]);
  }

  Widget _buildSpeed() {
    return SettingSlider(
        value: _settings.getDouble(Settings.SPEECH_SPEED) ?? 0.5,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_SPEED, result);
          setState(() {});
        },
        icon: Icons.speed_rounded,
        l10nKey: 'Setting.speech.rate',
        min: 0.0,
        max: 1.0,
        divisions: 10);
  }

  Widget _buildPause(BuildContext context) {
    return SettingNumber(
        value: _settings.getDouble(Settings.SPEECH_PAUSE) ?? 2.0,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_PAUSE, result);
          setState(() {});
        },
        icon: Icons.pause_rounded,
        l10nKey: 'Setting.speech.pause',
        suffix: Text(L10n.of(context).t8('Setting.speech.seconds')),
        min: 0.0,
        max: 3600.0);
  }

  Widget _buildTimer(BuildContext context) {
    return SettingNumber(
        value: _settings.getDouble(Settings.SPEECH_TIMER) ?? 0.0,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_TIMER, result);
          setState(() {});
        },
        icon: Icons.av_timer_rounded,
        l10nKey: 'Setting.speech.timer',
        suffix: Text(L10n.of(context).t8('Setting.speech.minutes')),
        min: 0.0,
        max: 1000.0);
  }

  Widget _buildVolume() {
    return SettingSlider(
        value: _settings.getDouble(Settings.SPEECH_VOLUME) ?? 1.0,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_VOLUME, result);
          setState(() {});
        },
        icon: Icons.volume_up_rounded,
        l10nKey: 'Setting.speech.volume',
        min: 0.0,
        max: 1.0,
        divisions: 10);
  }

  Widget _buildControl(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.speech.control'),
          child: const Icon(Icons.play_arrow_rounded)),
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        SettingButton(
            isSelected: _playlist,
            iconData: Icons.playlist_play_rounded,
            l10nKey: 'Speech.playlist',
            onPressed: () async {
              await _settings.putBool(Settings.SPEECH_PLAYLIST, !_playlist);
              setState(() {});
            }),
        SettingButton(
            isSelected: _repeat,
            iconData: Icons.repeat_rounded,
            l10nKey: 'Speech.repeat',
            onPressed: () async {
              await _settings.putBool(Settings.SPEECH_REPEAT, !_repeat);
              setState(() {});
            }),
        if (!kIsWeb && Platform.isIOS)
          SettingButton(
              isSelected: _musicOff,
              iconData: Icons.music_off_rounded,
              l10nKey: 'Speech.musicOff',
              onPressed: () async {
                await _settings.putBool(Settings.SPEECH_MUSIC_OFF, !_musicOff);
                setState(() {});
              }),
      ]))
    ]);
  }

  Widget _buildTranscription(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.speech.transcription'),
          child: const Icon(Icons.subtitles_outlined)),
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        SettingButton(
            isSelected: _captions,
            iconData: Icons.short_text_rounded,
            l10nKey: 'Speech.captions',
            onPressed: () async {
              if (!_captions && _script) {
                await _settings.putBool(Settings.SPEECH_SCRIPT, false);
              }
              await _settings.putBool(Settings.SPEECH_CAPTIONS, !_captions);
              setState(() {});
            }),
        SettingButton(
            isSelected: _script,
            iconData: Icons.view_headline_rounded,
            l10nKey: 'Speech.script',
            onPressed: () async {
              if (!_script && _captions) {
                await _settings.putBool(Settings.SPEECH_CAPTIONS, false);
              }
              await _settings.putBool(Settings.SPEECH_SCRIPT, !_script);
              setState(() {});
            }),
      ]))
    ]);
  }
}
