import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/services/notification_service.dart';
import 'package:repository/repository.dart';

class ReminderSetting extends StatefulWidget {
  final bool isCollapsed;

  const ReminderSetting({super.key, this.isCollapsed = false});

  @override
  State<StatefulWidget> createState() {
    return _ReminderSettingState();
  }
}

class _ReminderSettingState extends State<ReminderSetting> {
  bool _isOn = false;

  @override
  void initState() {
    I<NotificationService>()
        .pendingNotificationRequests()
        .then((value) => setState(() {
              _isOn = value.isNotEmpty;
            }));
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    debugPrint('ReminderSetting.build: isOn = $_isOn');
    final icon = _isOn ? Icons.alarm_on_rounded : Icons.alarm_off_rounded;
    final title =
        '${L10n.of(ctx).t8('Reminder.title')}: ${_isOn ? _currentValue?.format(ctx) ?? L10n.of(ctx).t8('Settings.on') : L10n.of(ctx).t8('Settings.off')}';
    action() => _isOn ? disable() : enable(ctx);
    return widget.isCollapsed
        ? IconButton(
            tooltip: title,
            icon: Icon(icon),
            onPressed: action,
          )
        : ListTile(
            leading: Icon(icon),
            title: Text(title),
            onTap: action,
          );
  }

  Future<String?> enable(BuildContext ctx) async {
    await I<NotificationService>().requestPermission();
    final initialTime = _currentValue ?? TimeOfDay.now();
    var time = await showTimePicker(context: ctx, initialTime: initialTime);
    if (time != null) {
      //NotificationService().scheduleInstantReminder();  // for debugging
      I<NotificationService>().scheduleDailyReminder(time.hour, time.minute);
      Settings().putInt(Settings.REMINDER_TIME_H, time.hour);
      Settings().putInt(Settings.REMINDER_TIME_M, time.minute);
      setState(() {
        _isOn = true;
      });
    }
    return null;
  }

  TimeOfDay? get _currentValue {
    int? h = Settings().getInt(Settings.REMINDER_TIME_H);
    int? m = Settings().getInt(Settings.REMINDER_TIME_M);
    return h != null && m != null ? TimeOfDay(hour: h, minute: m) : null;
  }

  void disable() {
    I<NotificationService>().cancelDailyReminder();
    setState(() {
      _isOn = false;
    });
  }
}
