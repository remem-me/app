import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';

import '../widgets.dart';

class StudySettingForm extends StatefulWidget {
  const StudySettingForm({super.key});

  @override
  State<StatefulWidget> createState() {
    return _StudySettingFormState();
  }
}

class _StudySettingFormState extends State<StudySettingForm> {
  final Settings _settings = I<Settings>();

  bool get _initialsDefaultOn =>
      _settings.getBool(Settings.STUDY_INITIALS_DEFAULT) ?? true;

  bool get _placeholdersDefaultOn =>
      _settings.getBool(Settings.STUDY_PLACEHOLDERS_DEFAULT) ?? true;

  bool get _speechOn => _settings.getBool(Settings.STUDY_SPEECH_ON) ?? true;

  bool get _dictationOn =>
      _settings.getBool(Settings.STUDY_DICTATION_ON) ?? true;

  bool get _vibrationOn =>
      _settings.getBool(Settings.STUDY_VIBRATION_ON) ?? true;

  int get _puzzleSize => _settings.getInt(Settings.STUDY_PUZZLE_SIZE) ?? 5;

  double get _speed => _settings.getDouble(Settings.SPEECH_SPEED) ?? 0.5;

  double get _volume => _settings.getDouble(Settings.SPEECH_VOLUME) ?? 1.0;

  @override
  Widget build(BuildContext ctx) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 480),
      child: Padding(
          padding: const EdgeInsets.all(16),
          child: ValueListenableBuilder<Box>(
              valueListenable: I<Settings>().listenable(keys: [
                Settings.TIP_TOOLTIP,
              ]),
              builder: (context, bx, wdg) {
                return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(L10n.of(context).t8('Setting.study.title'),
                          style: Theme.of(ctx).textTheme.titleSmall),
                      const SizedBox(height: 8),
                      if (_settings.getBool(Settings.TIP_TOOLTIP) ?? true) ...[
                        TipRow.tooltip(ctx)
                      ],
                      const SizedBox(height: 8),
                      _buildDefaultHint(ctx),
                      const SizedBox(height: 8),
                      _buildPuzzleSize(ctx),
                      const SizedBox(height: 8),
                      _buildTyping(ctx),
                      const SizedBox(height: 8),
                      _buildSpeechControl(ctx),
                      const SizedBox(height: 8),
                      _buildSpeechSpeed(),
                      _buildSpeechVolume(),
                    ]);
              })),
    );
  }

  Widget _buildDefaultHint(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.study.defaultHint'),
          child: const Icon(Icons.tips_and_updates)),
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        if (!['zh', 'ja'].contains(I<CurrentAccount>().language.languageCode))
          SettingButton(
              isSelected: _initialsDefaultOn,
              iconData: Icons.title_rounded,
              l10nKey: 'Study.hint.initials',
              onPressed: () async {
                await _settings.putBool(
                    Settings.STUDY_INITIALS_DEFAULT, !_initialsDefaultOn);
                setState(() {});
              }),
        SettingButton(
            isSelected: _placeholdersDefaultOn,
            iconData: Icons.minimize_rounded,
            l10nKey: 'Study.hint.placeholders',
            onPressed: () async {
              await _settings.putBool(
                  Settings.STUDY_PLACEHOLDERS_DEFAULT, !_placeholdersDefaultOn);
              setState(() {});
            }),
      ]))
    ]);
  }

  Widget _buildPuzzleSize(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.study.puzzleSize'),
          child: const Icon(Icons.extension_rounded)),
      Expanded(
          child: Slider(
        value: _puzzleSize.toDouble(),
        min: 3,
        max: 12,
        divisions: 9,
        label: _puzzleSize.toString(),
        onChanged: (double value) async {
          await _settings.putInt(Settings.STUDY_PUZZLE_SIZE, value.round());
          setState(() {});
        },
      )),
    ]);
  }

  Widget _buildTyping(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.study.keyboard'),
          child: const Icon(Icons.keyboard_rounded)),
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            SettingButton(
                isSelected: !_dictationOn,
                iconData: Icons.onetwothree_rounded,
                l10nKey: 'Setting.study.keyboard.numberPad',
                onPressed: () async {
                  await _settings.putBool(Settings.STUDY_DICTATION_ON, false);
                  setState(() {});
                }),
            SettingButton(
                isSelected: _dictationOn,
                iconData: Icons.keyboard_voice_rounded,
                l10nKey: 'Setting.study.keyboard.dictation',
                onPressed: () async {
                  await _settings.putBool(Settings.STUDY_DICTATION_ON, true);
                  setState(() {});
                }),
          ]))
    ]);
  }

  Widget _buildSpeechControl(BuildContext ctx) {
    return Row(children: [
      Tooltip(
          message: L10n.of(ctx).t8('Setting.speech.control'),
          child: const Icon(Icons.audiotrack_rounded)),
      Expanded(
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        SettingButton(
            isSelected: _speechOn,
            iconData: Icons.volume_up_rounded,
            l10nKey: 'Study.speech',
            onPressed: () async {
              await _settings.putBool(Settings.STUDY_SPEECH_ON, !_speechOn);
              setState(() {});
            }),
        SettingButton(
            isSelected: _vibrationOn,
            iconData: Icons.vibration_rounded,
            l10nKey: 'Study.vibration',
            onPressed: () async {
              await _settings.putBool(
                  Settings.STUDY_VIBRATION_ON, !_vibrationOn);
              setState(() {});
            }),
      ]))
    ]);
  }

  Widget _buildSpeechSpeed() {
    return SettingSlider(
        value: _speed,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_SPEED, result);
          setState(() {});
        },
        icon: Icons.speed_rounded,
        l10nKey: 'Setting.speech.rate',
        min: 0.0,
        max: 1.0,
        divisions: 10);
  }

  Widget _buildSpeechVolume() {
    return SettingSlider(
        value: _volume,
        onChanged: (result) async {
          await _settings.putDouble(Settings.SPEECH_VOLUME, result);
          setState(() {});
        },
        icon: Icons.volume_up_rounded,
        l10nKey: 'Setting.speech.volume',
        min: 0.0,
        max: 1.0,
        divisions: 10);
  }
}
