import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:repository/repository.dart';

class SettingSwitch extends StatelessWidget {
  final IconData icon;
  final String l10nKey;
  final String? settingKey;
  final bool value;
  final void Function(bool) onChanged;

  const SettingSwitch(
      {super.key,
      required this.value,
      required this.onChanged,
      required this.icon,
      required this.l10nKey,
      this.settingKey});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Tooltip(message: L10n.of(context).t8(l10nKey), child: Icon(icon)),
      const SizedBox(width: 16.0),
      Switch(
        value: value,
        onChanged: (bool result) async {
          if (settingKey != null) Settings().putBool(settingKey!, result);
          onChanged(result);
        },
      ),
    ]);
  }
}
