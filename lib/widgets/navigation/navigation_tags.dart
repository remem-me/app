import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app_core/app_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/file/file_menu.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';

import '../../util/util.dart';
import '../box/box.dart';
import '../search/search.dart';

class NavigationTags extends StatelessWidget {
  final bool poppable;

  const NavigationTags({super.key, this.poppable = true});

  @override
  Widget build(BuildContext context) {
    return Drawer(
        elevation: 0,
        child: BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
            builder: (ctx, taggedState) {
          if (taggedState is TaggedVersesSuccess) {
            return BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
                builder: (ctx, filteredState) {
              return SafeArea(
                child: Stack(
                  children: [
                    ListView(children: [
                      _topRow(ctx),
                      ..._defaultItems(ctx, taggedState, filteredState),
                      ..._tagItems(taggedState, ctx),
                    ]),
                    if (taggedState.isUpdating) const LoadingIndicator(),
                  ],
                ),
              );
            });
          } else {
            return const SizedBox.shrink();
          }
        }));
  }

  Row _topRow(BuildContext ctx) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FileMenuButton(onDo: () => Navigator.pop(ctx)),
        IconButton(
          tooltip: L10n.of(ctx).t8('Documentation.title'),
          onPressed: launchDocumentation(ctx, path: 'labels/'),
          icon: const Icon(Icons.help_outline_rounded),
        ),
      ],
    );
  }

  List<Widget> _defaultItems(BuildContext ctx, TaggedVersesSuccess taggedState,
      FilteredVersesState filteredState) {
    final Iterable<Verse> filteredVerses =
        filteredState is FilteredVersesSuccess
            ? filteredState.filteredVerses
            : const [];
    final versesReviewedToday = taggedState.verses
        .where((verse) => verse.review == DateService().today);
    return [
      const BoxedToggle(),
      _displayAll(ctx, taggedState, filteredVerses),
      _displayUntagged(ctx, taggedState, filteredVerses),
      ReviewedToday(versesReviewedToday),
    ];
  }

  Iterable<Widget> _tagItems(
      TaggedVersesSuccess taggedState, BuildContext ctx) {
    return taggedState.tags.map((tag) => ListTile(
          leading: _leadingIcon(tag, taggedState),
          title: Text(tag.text),
          trailing: Text(tag.size.toString()),
          onTap: () {
            (BlocProvider.of<TagsBloc>(ctx)).add(
                EntitiesUpdated<TagEntity>([_toggleTap(tag)], wait: false));
          },
          onLongPress: () {
            (BlocProvider.of<TagsBloc>(ctx)).add(EntitiesUpdated<TagEntity>(
                [_toggleLongPress(tag)],
                wait: false));
          },
        ));
  }

  ListTile _displayUntagged(
      BuildContext ctx, TaggedVersesSuccess state, Iterable<Verse> filtered) {
    final untagged = state.verses.where((verse) => verse.tags.isEmpty);
    return ListTile(
      enabled: untagged.isNotEmpty && !ListUtil.equal(untagged, filtered),
      leading: const Icon(Icons.label_off_outlined),
      title: Text(L10n.of(ctx).t8('Verses.untagged')),
      trailing: Text(untagged.length.toString()),
      onTap: () {
        (BlocProvider.of<FilteredVersesBloc>(ctx))
            .add(const ReviewedTodayFilterUpdated(null));
        (BlocProvider.of<TagsBloc>(ctx)).add(EntitiesUpdated<TagEntity>(
            _tagsToExclude(state.tags),
            wait: false));
      },
    );
  }

  ListTile _displayAll(
      BuildContext ctx, TaggedVersesSuccess state, Iterable<Verse> filtered) {
    return ListTile(
      enabled: filtered.length != state.verses.length,
      leading: const Icon(Icons.apps_outlined),
      title: Text(L10n.of(ctx).t8('Verses.all')),
      trailing: Text(state.verses.length.toString()),
      onTap: () {
        (BlocProvider.of<FilteredVersesBloc>(ctx))
            .add(const ReviewedTodayFilterUpdated(null));
        (BlocProvider.of<TagsBloc>(ctx)).add(
            EntitiesUpdated<TagEntity>(_tagsToClear(state.tags), wait: false));
      },
    );
  }

  Widget _leadingIcon(Tag tag, TaggedVersesSuccess state) {
    return Icon(tag.included != null
        ? tag.included!
            ? Icons.visibility_rounded
            : Icons.visibility_off_outlined
        : Icons.visibility_outlined);
  }

  Tag _toggleLongPress(Tag tag) {
    return tag.copyWith(included: false);
  }

  Tag _toggleTap(Tag tag) {
    if (tag.included == null) {
      return tag.copyWith(included: true);
    } else {
      return tag.copyWith(nullValues: ['included']);
    }
  }

  List<Tag> _tagsToClear(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != null)
        .map((tag) => tag.copyWith(nullValues: ['included']))
        .toList();
  }

  List<Tag> _tagsToExclude(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != false)
        .map((tag) => tag.copyWith(included: false))
        .toList();
  }
}
