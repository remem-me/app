import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';

import '../../config/config.dart';
import '../../models/account.dart';
import '../../theme.dart';
import '../widgets.dart';

class NavDrawer extends StatefulWidget implements Navigation {
  @override
  final bool poppable;
  late final NavigationCubit nav;

  NavDrawer({super.key, this.poppable = true}) {
    nav = I<NavigationCubit>();
  }

  @override
  State<StatefulWidget> createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  bool isInAccountMode = false;

  @override
  Widget build(BuildContext ctx) {
    final items = <Widget>[];
    items.add(_header(ctx));
    if (isInAccountMode) {
      items.add(NavigationAccounts(
        poppable: widget.poppable,
      ));
    } else {
      items.add(NavigationMenu(poppable: widget.poppable));
    }
    return Drawer(
      elevation: 0,
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: Column(children: [
        Expanded(
            child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: items)),
        SafeArea(
          left: false,
          top: false,
          right: false,
          child: _appVersion(ctx),
        ),
      ]),
    );
  }

  Widget _header(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
        builder: (context, state) => UserAccountsDrawerHeader(
              accountName: Row(children: [
                Expanded(
                    child: BlocBuilder<CurrentAccount, Account?>(
                        builder: (context, account) => Text(account != null
                            ? account.name
                            : L10n.of(ctx).t8('Account.none')))),
                if (!kIsWeb && I<AuthBloc>().state is AccessRemote)
                  ReplicationButton(onDone: () {
                    //if (widget.poppable) Navigator.pop(ctx);
                  }),
                if (!kIsWeb && I<AuthBloc>().state is AccessLocal)
                  Tooltip(
                    message: L10n.of(ctx).t8('Tip.user.local'),
                    child: Icon(Icons.cloud_off_rounded,
                        color: AppTheme.appBar.iconTheme!.color),
                  )
              ]),
              accountEmail: null,
              onDetailsPressed: () =>
                  setState(() => isInAccountMode = !isInAccountMode),
              //arrowColor: AppTheme.appBar.iconTheme!.color!,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/bg_title.png'),
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter),
              ),
            ));
  }

  Widget _appVersion(BuildContext ctx) {
    return FutureBuilder(
      future: PackageInfo.fromPlatform(),
      builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
        final info = snapshot.data;
        return info != null
            ? Text(
                style: Theme.of(ctx).textTheme.bodySmall,
                '${L10n.of(ctx).t8('App.name')} ${info.version} (${info.buildNumber}) ${AppConfig.googleId == 'org.bible.remember_me' ? '' : 'β'}')
            : const SizedBox.shrink();
      },
    );
  }
}
