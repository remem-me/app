import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/legacy_service.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:remem_me/widgets/navigation/navigation.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:repository/repository.dart';

import '../../paths/paths.dart';
import '../../services/start_service.dart';

class NavigationAccounts extends StatelessWidget implements Navigation {
  @override
  final bool poppable;

  const NavigationAccounts({super.key, this.poppable = true});

  @override
  Widget build(BuildContext ctx) {
    final nav = BlocProvider.of<NavigationCubit>(ctx);
    final accountsState = ctx.watch<AccountsBloc>().state;
    final auth = ctx.watch<AuthBloc>().state;
    final currentAccount = ctx.watch<CurrentAccount>().state;

    final tiles = [
      if (accountsState is EntitiesLoadSuccess<AccountEntity>) ...[
        ...accountsState.items.map((AccountEntity entity) => ListTile(
              leading: const Icon(Icons.folder_shared_rounded),
              title: Text(entity.name),
              selected: currentAccount?.id == entity.id,
              onTap: () async {
                await (BlocProvider.of<CurrentAccount>(ctx)).setEntity(entity);
                nav.goHome();
                if (poppable) Navigator.pop(ctx);
              },
            )),
        if (auth is AccessRemote) ListTile(
          leading: const Icon(Icons.drive_folder_upload),
          title: Text(L10n.of(ctx).t8('Account.attach')),
          onTap: () {
            _attachAccount(ctx);
          },
        ),
        ListTile(
          leading: const Icon(Icons.add),
          title: Text(L10n.of(ctx).t8('Account.create')),
          selected: nav.state is AccountCreatePath,
          onTap: () async {
            nav.go(AccountCreatePath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (kIsWeb && auth is AccessRemote)
          ListTile(
            leading: const Icon(Icons.restore_from_trash_rounded),
            title: Text(L10n.of(ctx).t8('Account.restore')),
            selected: nav.state is AccountBinPath,
            onTap: () async {
              nav.go(AccountBinPath());
              if (poppable) Navigator.pop(ctx);
            },
          )
      ],
      if (auth is AccessRemote)
        ListTile(
            leading: const Icon(Icons.account_circle_rounded),
            title: Text(L10n.of(ctx).t8('User.title')),
            onTap: () {
              _editUser(ctx);
            }),
      if (auth is Access)
        ListTile(
            leading: const Icon(Icons.logout),
            title: Text(L10n.of(ctx).t8('Auth.logout')),
            onTap: () {
              _logOut(ctx);
              if (poppable) Navigator.pop(ctx);
            })
      else
        ListTile(
            leading: const Icon(Icons.login_rounded),
            title: Text(L10n.of(ctx).t8('Auth.login')),
            onTap: () {
              nav.goHome();
              if (poppable) Navigator.pop(ctx);
            }),
    ];
    return Column(children: tiles);
  }

  void _logOut(BuildContext ctx) {
    BlocProvider.of<NavigationCubit>(ctx).go(const StartBasePath());
  }

  Future<void> _attachAccount(BuildContext ctx) async {
    Map<String, String> initialCredentials = const {};
    Map<String, List<String>>? errors = const {};
    while (errors != null) {
      errors = await (showDialog<Map<String, List<String>>>(
          barrierDismissible: false,
          context: ctx,
          builder: (BuildContext context) => Dialog(
                  child: UserForm(
                initialValues: initialCredentials,
                errors: errors ?? const {},
                hasRepeatPassword: false,
                hasEmailAsUsername: false,
                titleL10nKey: 'User.login.title',
                submitL10nKey: 'User.login.submit',
                onCancel: () => Navigator.of(ctx, rootNavigator: true).pop(),
                onSubmit: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    final account = await LegacyService().attachAccount(
                        name: credentials['name'],
                        password: credentials['password']);
                    await ReplicationService().clearLastReceived();
                    await I<CurrentAccount>().setEntity(account);
                    BlocProvider.of<AccountsBloc>(ctx).add(
                        // todo: Looking up a deactivated widget's ancestor is unsafe.
                        EntitiesAdded<AccountEntity>([account],
                            persist: false));
                    Navigator.of(ctx, rootNavigator: true).pop();
                    if (poppable) Navigator.pop(ctx);
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
              ))));
    }
  }

  Future<void> _editUser(BuildContext ctx) async {
    Map<String, String> initialCredentials = {
      'email': I<Settings>().get(Settings.USER_EMAIL)!
    };
    Map<String, List<String>>? errors = const {};
    while (errors != null) {
      errors = await (showDialog<Map<String, List<String>>>(
          barrierDismissible: false,
          context: ctx,
          builder: (BuildContext context) => Dialog(
                  child: UserForm(
                initialValues: initialCredentials,
                errors: errors ?? const {},
                titleL10nKey: 'User.title',
                submitL10nKey: 'Button.save',
                onCancel: () => Navigator.of(ctx, rootNavigator: true).pop(),
                onSubmit: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    final stored = await I<StartService>().changeEmail(
                        email: credentials['email']!,
                        password: credentials['password']!);
                    Settings().put(Settings.USER_EMAIL, stored['email']!);
                    Navigator.of(ctx, rootNavigator: true).pop();
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
                onDelete: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    await I<StartService>()
                        .deleteUser(password: credentials['password']!);
                    Navigator.of(ctx, rootNavigator: true).pop();
                    _logOut(ctx);
                    MessageService().info(L10n.of(ctx).t8('User.deleted'));
                  } on MessageException catch (e) {
                    Navigator.of(ctx, rootNavigator: true).pop(e.messages);
                  }
                },
              ))));
    }
  }
}
