import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../config/config.dart';
import '../../paths/paths.dart';
import '../../services/rating_service.dart';
import '../../util/util.dart';

class NavigationMenu extends StatelessWidget {
  final bool poppable;

  const NavigationMenu({super.key, this.poppable = false});

  @override
  Widget build(BuildContext context) {
    final nav = BlocProvider.of<NavigationCubit>(context);
    final hasCurrentAccount = I<CurrentAccount>().id > -1;
    return BlocBuilder<NavigationCubit, AppRoutePath>(
        builder: (BuildContext ctx, state) {
      final auth = BlocProvider.of<AuthBloc>(ctx).state;
      return Column(children: [
        if (_isInstallable)
          ListTile(
            leading: const Icon(Icons.install_mobile_rounded),
            title: Text(L10n.of(ctx).t8('Store.title')),
            onTap: () => _install(ctx),
          ),
        ListTile(
          leading: const Icon(Icons.home_rounded),
          title: Text(L10n.of(ctx).t8('Verses.title')),
          selected: state is HomePath,
          onTap: () async {
            nav.goHome();
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (auth is Access && hasCurrentAccount)
          ListTile(
            leading: const Icon(Icons.new_label_rounded),
            title: Text(L10n.of(ctx).t8('Tags.edit')),
            selected: state is TagsPath,
            onTap: () async {
              nav.go(TagsPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        ListTile(
          leading: const Icon(Icons.collections_bookmark_rounded),
          title: Text(L10n.of(ctx).t8('Collections.title')),
          selected: state is CollectionsPath,
          onTap: () async {
            nav.go(CollectionsPath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (auth is Access && hasCurrentAccount)
          ListTile(
            leading: const Icon(Icons.trending_up_rounded),
            title: Text(L10n.of(ctx).t8('Scores.title')),
            selected: state is ScoresPath,
            onTap: () async {
              nav.go(ScoresPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        if (auth is AccessRemote && hasCurrentAccount && kIsWeb)
          ListTile(
            leading: const Icon(Icons.restore_from_trash_rounded),
            title: Text(L10n.of(ctx).t8('Verses.restore')),
            selected: state is VerseBinPath,
            onTap: () async {
              nav.go(VerseBinPath());
              if (poppable) Navigator.pop(ctx);
            },
          ),
        ListTile(
          leading: const Icon(Icons.settings_rounded),
          title: Text(L10n.of(ctx).t8('Settings.title')),
          selected: state is SettingsPath,
          onTap: () async {
            nav.go(SettingsPath());
            if (poppable) Navigator.pop(ctx);
          },
        ),
        ListTile(
          leading: const Icon(Icons.help_outline_rounded),
          title: Text(L10n.of(ctx).t8('Documentation.title')),
          onTap: () async {
            await launchDocumentation(ctx, doc: 'docs/')();
            if (poppable) Navigator.pop(ctx);
          },
        ),
        if (!kIsWeb && RatingService().isReadyForRating())
          ListTile(
              leading: const Icon(Icons.star_rounded),
              title: Text(L10n.of(ctx).t8('Rating.title')),
              onTap: () => _rate(ctx)),
        if (kDebugMode)
          ListTile(
              leading: const Icon(Icons.style_rounded),
              title: const Text('Styles'),
              onTap: () async {
                nav.go(StylesPath());
                if (poppable) Navigator.pop(ctx);
              }),
      ]);
    });
  }

  Future<void> _rate(BuildContext ctx) async {
    RatingService().markRatingDone();
    final uri = await RatingService().uriStore();
    if (await canLaunchUrl(uri)) await launchUrl(uri);
    if (!ctx.mounted) return; // RETURN
    if (poppable) Navigator.pop(ctx);
  }

  Future<void> _install(BuildContext ctx) async {
    final uri = defaultTargetPlatform == TargetPlatform.iOS
        ? Uri.parse('https://apps.apple.com/app/id1600128209')
        : Uri.parse(
            'https://play.google.com/store/apps/details?id=${AppConfig.googleId}');
    if (await canLaunchUrl(uri)) await launchUrl(uri);
    if (!ctx.mounted) return; // RETURN
    if (poppable) Navigator.pop(ctx);
  }

  bool get _isInstallable {
    return kIsWeb &&
        [TargetPlatform.iOS, TargetPlatform.android]
            .contains(defaultTargetPlatform);
  }
}
