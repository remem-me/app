
export 'navigation_accounts.dart';
export 'navigation_drawer.dart';
export 'navigation_menu.dart';
export 'navigation_tags.dart';

abstract class Navigation {
  bool get poppable;
}