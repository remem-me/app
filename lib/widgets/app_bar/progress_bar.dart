import 'package:flutter/material.dart';

import '../../theme.dart';

class ProgressBar extends StatelessWidget {
  const ProgressBar({
    super.key, required this.countWrong, required this.countToGo, required this.countRight,
  });

  final int countWrong;
  final int countToGo;
  final int countRight;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Row(children: [
        Expanded(
            flex: countWrong,
            child: Container(
                color: AppTheme.dark.colorScheme.tertiary,
                height: 4)),
        Expanded(
            flex: countToGo,
            child: Container(color: Colors.transparent, height: 4)),
        Expanded(
            flex: countRight,
            child: Container(
                color: AppTheme.dark.colorScheme.secondary, height: 4)),
      ]),
    );
  }
}