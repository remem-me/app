import 'app_route_path.dart';

class HomePath extends AppRoutePath {
  static final String name = 'verses';

  const HomePath();

  factory HomePath.fromUri(Uri uri) {
    switch (uri.pathSegments.length) {
      case 1:
        return const HomePath();
      case 2:
        if (HomeCreatePath.matches(uri)) {
          return HomeCreatePath();
        } else if (HomeFlashcardPath.matches(uri)) {
          return HomeFlashcardPath.fromUri(uri);
        }
        break;
      case 3:
        if (HomeEditPath.matches(uri)) {
          return HomeEditPath.fromUri(uri);
        } else if (HomeStudyPath.matches(uri)) {
          return HomeStudyPath.fromUri(uri);
        }
    }
    throw InvalidPathException('Invalid home path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return uri.pathSegments.isNotEmpty && uri.pathSegments.first == name;
  }

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}


class HomeCreatePath extends HomePath implements ChildPath {
  static final String name = 'add';
  final String? data;

  const HomeCreatePath({this.data});

  const HomeCreatePath.fromUri(Uri uri, {this.data});

  @override
  List<Object?> get props => [data];

  static bool matches(Uri uri) {
    return HomePath.matches(uri) && uri.pathSegments[1] == name;
  }

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => HomePath();

  @override
  String toString() => '${parent.toString()}/$segment';
}


class HomeFlashcardPath extends HomePath implements ChildPath {
  final int id;

  const HomeFlashcardPath({required this.id});

  HomeFlashcardPath.fromUri(Uri uri) : id = int.parse(uri.pathSegments[1]);

  static bool matches(Uri uri) {
    return HomePath.matches(uri) && int.tryParse(uri.pathSegments[1]) != null;
  }

  @override
  List<Object?> get props => [id];

  @override
  String get segment => id.toString();

  @override
  AppRoutePath get parent => HomePath();

  AppRoutePath get study => HomeStudyPath(id: id);

  AppRoutePath get edit => HomeEditPath(id: id);

  HomeFlashcardPath copyWith({
    int? id,
    int? verseId,
  }) {
    return HomeFlashcardPath(
      id: id ?? this.id,
    );
  }

  @override
  String toString() => '${parent.toString()}/$segment';
}


class HomeStudyPath extends HomeFlashcardPath {
  static final String name = 'study';
  const HomeStudyPath({required super.id});

  HomeStudyPath.fromUri(super.uri) : super.fromUri();

  static bool matches(Uri uri) {
    return HomeFlashcardPath.matches(uri) && uri.pathSegments[2] == name;
  }

  @override
  List<Object?> get props => [id];

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => HomeFlashcardPath(id: id);

  @override
  String toString() => '${parent.toString()}/$segment';
}


class HomeEditPath extends HomeFlashcardPath {
  static final String name = 'edit';
  const HomeEditPath({required super.id});

  HomeEditPath.fromUri(super.uri) : super.fromUri();

  static bool matches(Uri uri) {
    return HomeFlashcardPath.matches(uri) && uri.pathSegments[2] == name;
  }

  @override
  List<Object?> get props => [id];

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => HomeFlashcardPath(id: id);

  @override
  String toString() => '${parent.toString()}/$segment';
}
