import 'paths.dart';

class CollectionsPath extends AppRoutePath {
  static final String name = 'collections';

  const CollectionsPath();

  factory CollectionsPath.fromUri(Uri uri) {
    switch (uri.pathSegments.length) {
      case 1:
        return CollectionsPath();
      case 2:
        if (CollectionDetailPath.matches(uri)) {
          return CollectionDetailPath.fromUri(uri);
        }
        break;
      case 3:
        if (CollectionFlashcardPath.matches(uri)) {
          return CollectionFlashcardPath.fromUri(uri);
        }
        break;
      case 4:
        if (CollectionStudyPath.matches(uri)) {
          return CollectionStudyPath.fromUri(uri);
        }
    }
    throw InvalidPathException('Invalid collections path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return uri.pathSegments.isNotEmpty && uri.pathSegments.first == name;
  }

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}


class CollectionDetailPath extends CollectionsPath implements ChildPath {
  final int id;
  @override
  final AppRoutePath parent;

  const CollectionDetailPath({required this.id, this.parent = const CollectionsPath()});

  CollectionDetailPath.fromUri(Uri uri, {this.parent = const CollectionsPath()})
      : id = int.parse(uri.pathSegments[1]);

  static bool matches(Uri uri) {
    return CollectionsPath.matches(uri) && int.tryParse(uri.pathSegments[1]) != null;
  }

  @override
  List<Object?> get props => [id];

  @override
  String get segment => id.toString();

  @override
  String toString() => '${parent.toString()}/$segment';
}


class CollectionFlashcardPath extends CollectionDetailPath {
  final int verseId;

  const CollectionFlashcardPath({required super.id, required this.verseId});

  CollectionFlashcardPath.fromUri(super.uri)
      : verseId = int.parse(uri.pathSegments[2]),
        super.fromUri();

  static bool matches(Uri uri) {
    return CollectionsPath.matches(uri) && int.tryParse(uri.pathSegments[2]) != null;
  }

  @override
  List<Object?> get props => [id, verseId];

  @override
  String get segment => verseId.toString();

  @override
  AppRoutePath get parent => CollectionDetailPath(id: id);

  AppRoutePath get study => CollectionStudyPath(id: id, verseId: verseId);

  CollectionFlashcardPath copyWith({
    int? id,
    int? verseId,
  }) {
    return CollectionFlashcardPath(
      id: id ?? this.id,
      verseId: verseId ?? this.verseId,
    );
  }

  @override
  String toString() => '${parent.toString()}/$segment';
}


class CollectionStudyPath extends CollectionFlashcardPath {
  static final String name = 'study';

  const CollectionStudyPath({required super.id, required super.verseId});

  CollectionStudyPath.fromUri(super.uri) : super.fromUri();

  static bool matches(Uri uri) {
    return CollectionsPath.matches(uri) && uri.pathSegments[3] == name;
  }

  @override
  List<Object?> get props => [id, verseId];

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => CollectionFlashcardPath(id: id, verseId: verseId);

  @override
  String toString() => '${parent.toString()}/$segment';
}
