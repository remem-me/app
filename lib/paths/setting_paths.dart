import 'app_route_path.dart';

class SettingsPath extends AppRoutePath {
  static const String name = 'settings';

  const SettingsPath();

  factory SettingsPath.fromUri(Uri uri) {
    if (uri.pathSegments.length == 1 && uri.pathSegments[0] == name) {
      return const SettingsPath();
    } else if (AccountCreatePath.matches(uri)) {
      return AccountCreatePath();
    } else if (AccountEditPath.matches(uri)) {
      return AccountCreatePath();
    }
    throw InvalidPathException('Invalid settings path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return uri.pathSegments.isNotEmpty && uri.pathSegments.first == name;
  }

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}


class AccountEditPath extends SettingsPath implements ChildPath {
  static const String name = 'account';

  const AccountEditPath();

  static bool matches(Uri uri) {
    return SettingsPath.matches(uri) && uri.pathSegments[2] == name;
  }

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => SettingsPath();

  @override
  String toString() => '${parent.toString()}/$segment';
}


class AccountCreatePath extends SettingsPath implements ChildPath {
  static const String name = 'add';

  const AccountCreatePath();

  static bool matches(Uri uri) {
    return SettingsPath.matches(uri) && uri.pathSegments[2] == name;
  }

  @override
  String get segment => name;

  @override
  AppRoutePath get parent => SettingsPath();

  @override
  String toString() => '${parent.toString()}/$segment';
}
