import 'app_route_path.dart';

abstract class SimplePath extends AppRoutePath {
  static const String name = '';

  const SimplePath();

  factory SimplePath.fromUri(Uri uri) {
    if (ScoresPath.matches(uri)) {
      return ScoresPath.fromUri(uri);
    } else if (StylesPath.matches(uri)) {
      return StylesPath.fromUri(uri);
    } else if (VerseBinPath.matches(uri)) {
      return VerseBinPath.fromUri(uri);
    } else if (AccountBinPath.matches(uri)) {
      return AccountBinPath.fromUri(uri);
    }
    throw InvalidPathException('Invalid simple path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return ScoresPath.matches(uri) ||
        StylesPath.matches(uri) ||
        VerseBinPath.matches(uri) ||
        AccountBinPath.matches(uri);
  }

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}

class ScoresPath extends SimplePath {
  static const String name = 'scores';

  const ScoresPath() : super();

  const ScoresPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class StylesPath extends SimplePath {
  static const String name = 'styles';

  const StylesPath() : super();

  const StylesPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class VerseBinPath extends SimplePath {
  static const String name = 'bin';

  const VerseBinPath() : super();

  const VerseBinPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class AccountBinPath extends SimplePath {
  static const String name = 'account-bin';

  const AccountBinPath() : super();

  const AccountBinPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}



