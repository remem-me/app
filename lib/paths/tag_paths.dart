import 'app_route_path.dart';

class TagsPath extends AppRoutePath {
  static const String name = 'labels';

  const TagsPath();

  factory TagsPath.fromUri(Uri uri) {
    if (uri.pathSegments.length == 1 && uri.pathSegments[0] == name) {
      return const TagsPath();
    } else if (TagBinPath.matches(uri)) {
      return TagBinPath();
    }
    throw InvalidPathException('Invalid tags path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return uri.pathSegments.isNotEmpty && uri.pathSegments.first == name;
  }

  @override
  List<Object?> get props => [];

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}


class TagBinPath extends TagsPath implements ChildPath {
  static const String name = 'bin';

  const TagBinPath() : super();

  static bool matches(Uri uri) {
    return TagsPath.matches(uri) && uri.pathSegments[1] == name;
  }

  @override
  AppRoutePath get parent => TagsPath();

  @override
  String get segment => name;

  @override
  String toString() => '${parent.toString()}/$segment';
}