import 'package:app_core/app_core.dart';
import 'package:equatable/equatable.dart';

import '../blocs/blocs.dart';
import 'paths.dart';


class InvalidPathException implements Exception {
  final String path;
  InvalidPathException(this.path);

  @override
  String toString() => 'InvalidPathException: $path';
}

abstract class AppRoutePath extends Equatable {
  static final String name = '';

  const AppRoutePath();

  factory AppRoutePath.fromUri(Uri uri) {
    if (StartBasePath.matches(uri)) return I<NavigationCubit>().state;
    if (StartPath.matches(uri)) return StartPath.fromUri(uri);
    if (HomePath.matches(uri)) return HomePath.fromUri(uri);
    if (CollectionsPath.matches(uri)) return CollectionsPath.fromUri(uri);
    if (TagsPath.matches(uri)) return TagsPath.fromUri(uri);
    if (SimplePath.matches(uri)) return SimplePath.fromUri(uri);
    if (SettingsPath.matches(uri)) return SettingsPath.fromUri(uri);
    return InvalidPath(uri.path);
  }

  @override
  List<Object?> get props => [];

  String get segment => '';

  String get path => '';

  @override
  String toString() => 'AppRoutePath';
}

abstract class ChildPath {
  AppRoutePath get parent;
}

class InvalidPath extends AppRoutePath {
  final String _path;

  const InvalidPath(String path) : _path = path;

  @override
  List<Object?> get props => [_path];

  @override
  String get path => _path;

  @override
  String toString() => _path;
}
