import 'package:app_core/app_core.dart';
import 'package:logging/logging.dart';

import '../blocs/blocs.dart';
import 'paths.dart';

abstract class StartPath extends AppRoutePath {
  static final _log = Logger('AppRouteInformationParser');
  static const String name = '';

  const StartPath();

  factory StartPath.fromUri(Uri uri) {
    if (StartBasePath.matches(uri)) {
      return StartBasePath.fromUri(uri);
    } else if (LoginPath.matches(uri)) {
      I<StartBloc>().add(const LogInInitiated());
      return LoginPath.fromUri(uri);
    } else if (RegisterSuccessPath.matches(uri)) {
      I<StartBloc>().add(const Activated());
      return RegisterSuccessPath.fromUri(uri);
    } else if (RegisterErrorPath.matches(uri)) {
      I<StartBloc>().add(const ActivateFailed());
      return RegisterErrorPath.fromUri(uri);
    } else if (ResetPasswordPath.matches(uri)) {
      final uidb64 = uri.queryParameters['uidb64']!;
      final token = uri.queryParameters['token']!;
      I<StartBloc>().add(ResetPasswordInitiated(uidb64, token));
      return ResetPasswordPath.fromUri(uri);
    }
    throw InvalidPathException('Invalid start path: ${uri.path}');
  }

  static bool matches(Uri uri) {
    return StartBasePath.matches(uri) ||
        LoginPath.matches(uri) ||
        RegisterSuccessPath.matches(uri) ||
        RegisterErrorPath.matches(uri) ||
        ResetPasswordPath.matches(uri);
  }

  @override
  String get segment => name;

  @override
  String toString() => '/$segment';
}

class StartBasePath extends StartPath {
  static const String name = '';

  const StartBasePath() : super();

  const StartBasePath.fromUri(Uri uri);

  static bool matches(Uri uri) => uri.pathSegments.isEmpty;

  @override
  String get segment => name;
}

class RegisterSuccessPath extends StartPath {
  static const String name = 'register-success';

  const RegisterSuccessPath() : super();

  const RegisterSuccessPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class RegisterErrorPath extends StartPath {
  static const String name = 'register-error';

  const RegisterErrorPath() : super();

  const RegisterErrorPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class LoginPath extends StartPath {
  static const String name = 'login';
  final AppRoutePath redirectTo;

  const LoginPath({this.redirectTo = const HomePath()}) : super();

  const LoginPath.fromUri(Uri uri, {this.redirectTo = const HomePath()});

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 && uri.pathSegments[0] == name;

  @override
  String get segment => name;
}

class ResetPasswordPath extends StartPath {
  static const String name = 'reset-password';

  const ResetPasswordPath() : super();

  const ResetPasswordPath.fromUri(Uri uri);

  static bool matches(Uri uri) =>
      uri.pathSegments.length == 1 &&
      uri.pathSegments[0] == name &&
      uri.queryParameters['uidb64'] != null &&
      uri.queryParameters['token'] != null;
}
