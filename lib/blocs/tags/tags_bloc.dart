import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

class TagsBloc extends EntitiesBloc<TagEntity, TagEntity> {
  TagsBloc()
      : super(initialState: const InitialEntitiesState<TagEntity>()) {
    subscribeToAccountSelected();
  }

  @override
  EntitiesRepository<TagEntity> get repository =>
      I<EntitiesRepository<TagEntity>>();

  @override
  Future<List<TagEntity>> toSorted(Iterable<TagEntity> items) async {
    final String Function(TagEntity element)? sortedBy = (it) => it.text;
    return compute(ListUtil.toSorted<TagEntity, String>,
        (items: items, sortedBy: sortedBy));
  }

  @override
  Future<List<TagEntity>> toModels(Iterable<TagEntity> entities) async {
    return entities.toList();
  }
}
