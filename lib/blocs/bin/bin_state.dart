import 'package:equatable/equatable.dart';
import 'package:repository/repository.dart';

abstract class BinState<T extends Entity> extends Equatable {
  const BinState();

  @override
  List<Object?> get props => [];
}

class BinLoadInProgress<T extends Entity> extends BinState<T> {}

class BinLoadSuccess<T extends Entity> extends BinState<T> {
  final List<T> entities;
  final Map<int, T> selection;
  final bool isUpdating;

  const BinLoadSuccess(
      [this.entities = const [],
      this.selection = const {},
      this.isUpdating = false]);

  @override
  List<Object?> get props => [entities, selection, isUpdating];

  @override
  String toString() => 'BinLoadSuccess { entities: ${entities.runtimeType}(${entities.length}), '
      'selection: $selection, isUpdating: $isUpdating }';

  BinLoadSuccess<T> copyWith({Map<int, T>? selection, bool? isUpdating}) {
    return BinLoadSuccess<T>(
        this.entities, selection ?? this.selection, isUpdating ?? this.isUpdating);
  }
}

class BinLoadFailure<T extends Entity> extends BinState<T> {
  final String errorMessage;

  const BinLoadFailure(this.errorMessage);

  @override
  List<Object?> get props => [errorMessage];

  @override
  String toString() {
    return 'BinLoadFailure { errorMessage: $errorMessage }';
  }
}
