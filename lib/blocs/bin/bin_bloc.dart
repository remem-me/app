import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs.dart';

class BinBloc<E extends Entity, T extends E>
    extends Bloc<BinEvent<E>, BinState<E>> {
  final EntitiesRepository<E> repository;
  final EntitiesBloc<E, T> entitiesBloc;

  BinBloc(
      {required this.repository,
      required this.entitiesBloc,
      required BinState<E> initialState})
      : super(initialState) {
    on<BinEvent<E>>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (BinState<E> result) => result),
        transformer: sequential()); // process events sequentially
  }

  Stream<BinState<E>> mapEventToState(BinEvent<E> event) async* {
    if (event is BinLoaded<E>) {
      yield* mapBinLoadedToState(event);
    } else if (event is Shredded) {
      yield* mapShreddedToState();
    } else if (event is Restored) {
      yield* mapRestoredToState();
    } else if (event is WasteSelected) {
      yield* mapWasteSelectedToState(event as WasteSelected<E>);
    } else if (event is AllWasteSelected) {
      yield* mapAllWasteSelectedToState();
    } else if (event is NoWasteSelected) {
      yield* mapNoWasteSelectedToState();
    }
  }

  Stream<BinState<E>> mapBinLoadedToState(BinLoaded<E> event) async* {
    yield BinLoadInProgress();
    try {
      var account = await I<CurrentAccount>()
          .stream
          .startWith(I<CurrentAccount>().state)
          .firstWhere((account) => account != null);
      var entities =
          await repository.readList(accountId: account!.id, deleted: true);
      yield BinLoadSuccess<E>(entities);
    } on MessageException {
      yield BinLoadFailure<E>('RemoteException');
    }
  }

  Stream<BinState<E>> mapShreddedToState() async* {
    final BinState<E> state = this.state;
    if (state is BinLoadSuccess<E>) {
      yield state.copyWith(isUpdating: true);
      await Future.forEach<E>(state.selection.values, (entity) async {
        await repository.delete(entity);
      });
      yield BinLoadSuccess<E>(_reducedList(state.selection.values));
    }
  }

  Stream<BinState<E>> mapRestoredToState() async* {
    final BinState<E> state = this.state;
    if (state is BinLoadSuccess<E>) {
      yield state.copyWith(isUpdating: true);
      final List<E> restoredEntities = state.selection.values
          .map((entity) => entity.copyWith(deleted: false) as E)
          .toList();
      await repository.updateList(restoredEntities);
      entitiesBloc.add(EntitiesAdded<E>(restoredEntities, persist: false));
      yield BinLoadSuccess(_reducedList(state.selection.values));
    }
  }

  Stream<BinState<E>> mapWasteSelectedToState(WasteSelected<E> event) async* {
    final BinState<E> state = this.state;
    if (state is BinLoadSuccess<E>) {
      final updatedSelection = Map.of(state.selection);
      if (state.selection.keys.contains(event.entity.id)) {
        updatedSelection.remove(event.entity.id);
      } else {
        updatedSelection[event.entity.id] = event.entity;
      }
      yield state.copyWith(selection: updatedSelection);
    }
  }

  Stream<BinState<E>> mapAllWasteSelectedToState() async* {
    final BinState<E> state = this.state;
    if (state is BinLoadSuccess<E>) {
      yield state.copyWith(
          selection: Map.fromEntries(
              state.entities.map((entity) => MapEntry(entity.id, entity))));
    }
  }

  Stream<BinState<E>> mapNoWasteSelectedToState() async* {
    final BinState<E> state = this.state;
    if (state is BinLoadSuccess<E>) {
      yield state.copyWith(selection: const {});
    }
  }

  List<E> _reducedList(Iterable<E> entities) {
    return (state as BinLoadSuccess<E>)
        .entities
        .where((entity) => !entities.contains(entity))
        .toList();
  }
}
