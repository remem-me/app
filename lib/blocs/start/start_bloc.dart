import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/start_service.dart';

part 'start_event.dart';

part 'start_state.dart';

class StartBloc extends Bloc<StartEvent, StartState> {
  static final _log = Logger('StartBloc');

  StartBloc() : super(const StartInitialState()) {
    _log.fine('StartBloc constructor started');
    on<StartEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (StartState result) => result),
        transformer: sequential());
    I<AuthBloc>().stream.where((auth) => auth is Access).listen((auth) {
      final nav = I<NavigationCubit>();
      final path = nav.state;
      if (path is LoginPath) {
        nav.go(path.redirectTo);
      } else if (path is StartPath) {
        nav.go(HomePath());
      }
    });
  }

  Stream<StartState> mapEventToState(StartEvent event) async* {
    if (event is DialogDismissed) {
      yield* mapDialogDismissedToState(event);
    } else {
      await _loggedOut;
      if (event is PasswordForgotten) {
        yield* mapPasswordForgottenToState(event);
      } else if (event is LogInInitiated) {
        yield* mapLogInInitiatedToState(event);
      } else if (event is LoggedIn) {
        yield* mapLoggedInToState(event);
      } else if (event is SignUpInitiated) {
        yield* mapSignUpInitiatedToState(event);
      } else if (event is SignedUp) {
        yield* mapRegisteredToState(event);
      } else if (event is Activated) {
        yield* mapActivatedToState(event);
      } else if (event is ActivateFailed) {
        yield* mapActivateFailedToState(event);
      } else if (event is ResetPasswordInitiated) {
        yield* mapResetPasswordInitiatedToState(event);
      } else if (event is PasswordSubmitted) {
        yield* mapPasswordSubmittedToState(event);
      }
    }
  }

  Stream<StartState> mapPasswordForgottenToState(
      PasswordForgotten event) async* {
    yield const StartInProgress();
    yield await I<StartService>().forgotPassword(email: event.email);
  }

  Stream<StartState> mapLogInInitiatedToState(LogInInitiated event) async* {
    yield LogInFeedback(state.credentials, messages: {});
  }

  Stream<StartState> mapSignUpInitiatedToState(SignUpInitiated event) async* {
    yield SignUpFeedback(state.credentials, messages: {});
  }

  Stream<StartState> mapLoggedInToState(LoggedIn event) async* {
    yield const StartInProgress();
    StartState result = await I<StartService>()
        .login(email: event.email, password: event.password);
    if (result is LogInSuccess) {
      I<AuthBloc>().add(result.granted);
    }
    yield result;
  }

  Stream<StartState> mapRegisteredToState(SignedUp event) async* {
    yield const StartInProgress();
    yield await I<StartService>()
        .register(email: event.email, password: event.password);
  }

  Stream<StartState> mapActivatedToState(Activated event) async* {
    final state = this.state;
    if(!I.isRegistered<L10n>()) await L10n.initialized;
    yield StartInfo(state.credentials,
        dialogId: 'register-success',
        title: L10n.current.t8('User.register.success.title'),
        message: L10n.current.t8('User.register.success.summary'));
  }

  Stream<StartState> mapActivateFailedToState(ActivateFailed event) async* {
    final state = this.state;
    if(!I.isRegistered<L10n>()) await L10n.initialized;
    yield StartInfo(state.credentials,
        dialogId: 'register-error',
        title: L10n.current.t8('User.register.error.title'),
        message: L10n.current.t8('User.register.error.summary'));
  }

  Stream<StartState> mapResetPasswordInitiatedToState(
      ResetPasswordInitiated event) async* {
    yield ResetPasswordFeedback(state.credentials,
        messages: {}, uidb64: event.uidb64, token: event.token);
  }

  Stream<StartState> mapPasswordSubmittedToState(
      PasswordSubmitted event) async* {
    final state = this.state as ResetPasswordFeedback;
    yield const StartInProgress();
    yield await I<StartService>().submitPassword(
        password: event.password, uidb64: state.uidb64, token: state.token);
  }

  Stream<StartState> mapDialogDismissedToState(DialogDismissed event) async* {
    final state = this.state;
    yield state is StartDialogState && event.dialogId == state.dialogId
        ? StartInitialState(state.credentials)
        : state;
  }

  Future<void> get _loggedOut {
    return I<AuthBloc>()
        .stream // RplLogged out might still be in process
        .startWith(I<AuthBloc>().state)
        .firstWhere((auth) => auth is NoAuth);
  }

/*Stream<UserState> mapAccountAttachedToState(Authenticated event) async* {
    yield const UserInProgress();
    yield await _service.register(email: event.email, password: event.password);
  }*/
}
