part of 'start_bloc.dart';

abstract class StartState extends Equatable {
  final Map<String, String> credentials;

  const StartState([this.credentials = const {}]);

  @override
  List<Object?> get props => [credentials];
}

class StartInitialState extends StartState {
  const StartInitialState([super.credentials]);
}

class StartInProgress extends StartState {
  const StartInProgress();
}

abstract class StartDialogState extends StartState {
  final String dialogId;

  const StartDialogState(super.credentials, {required this.dialogId});

  @override
  List<Object?> get props => super.props + [dialogId];
}

abstract class StartFeedback extends StartDialogState {
  final Map<String, List<String?>> messages;

  const StartFeedback(super.credentials,
      {required super.dialogId, required this.messages});

  @override
  List<Object?> get props => super.props + [messages];

  @override
  String toString() {
    return 'StartFeedback { dialogId: $dialogId, messages: $messages, credentials: $credentials }';
  }
}

class StartInfo extends StartDialogState {
  final String title;
  final String message;

  const StartInfo(super.credentials,
      {required super.dialogId, required this.title, required this.message});

  @override
  List<Object?> get props => super.props + [title, message];

  @override
  String toString() {
    return 'StartInfo { dialogId: $dialogId, title: $title, message: $message, credentials: $credentials }';
  }
}

class ForgotPasswordFeedback extends StartFeedback {
  const ForgotPasswordFeedback(super.credentials,
      {super.dialogId = 'forgot-password', required super.messages});
}

class SignUpFeedback extends StartFeedback {
  const SignUpFeedback(super.credentials,
      {super.dialogId = 'sign-up', required super.messages});
}

class LogInFeedback extends StartFeedback {
  const LogInFeedback(super.credentials,
      {super.dialogId = 'log-in', required super.messages});
}

class ResetPasswordFeedback extends StartFeedback {
  final String uidb64;
  final String token;

  const ResetPasswordFeedback(super.credentials,
      {super.dialogId = 'reset-password',
      required super.messages,
      required this.uidb64,
      required this.token});

  @override
  List<Object?> get props => super.props + [uidb64, token];

  @override
  String toString() {
    return 'ResetPasswordFeedback { dialogId: $dialogId, '
        'credentials: $credentials, messages: $messages, '
        'uidb64: $uidb64, token: $token }';
  }
}

class LogInSuccess extends StartState {
  final Granted granted;

  const LogInSuccess(this.granted);

  @override
  List<Object?> get props => super.props + [granted];

  @override
  String toString() {
    return 'LogInSuccess { granted: $granted, credentials: $credentials }';
  }
}


class ResetPasswordSuccess extends StartState {
  final Map<String, List<String>> messages;

  const ResetPasswordSuccess(
      {required this.messages})
      : super();

  @override
  List<Object?> get props => [messages];

  @override
  String toString() {
    return 'ResetPasswordSuccess { messages: $messages }';
  }
}
