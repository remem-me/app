part of 'start_bloc.dart';

abstract class StartEvent extends Equatable {
  const StartEvent();

  @override
  List<Object?> get props => [];
}

class PasswordForgotten extends StartEvent {
  final String email;

  const PasswordForgotten({required this.email});

  @override
  List<Object?> get props => [email];

  @override
  String toString() => 'PasswordForgotten { email: $email }';
}

abstract class Authenticated extends StartEvent {
  final String email;
  final String password;

  const Authenticated({required this.email, required this.password});

  @override
  List<Object?> get props => [email, password];

  @override
  String toString() => 'Authenticated { email: $email, password: $password }';
}

class SignedUp extends Authenticated {
  const SignedUp({required super.email, required super.password});

  @override
  String toString() => 'SignedUp { email: $email, password: $password }';
}

class Activated extends StartEvent {
  const Activated();
}

class ActivateFailed extends StartEvent {
  const ActivateFailed();
}

class LogInInitiated extends StartEvent {
  const LogInInitiated();
}

class SignUpInitiated extends StartEvent {
  const SignUpInitiated();
}

class DialogDismissed extends StartEvent {
  final String dialogId;

  const DialogDismissed(this.dialogId);
}

class LoggedIn extends Authenticated {
  const LoggedIn({required super.email, required super.password});

  @override
  String toString() => 'LoggedIn { email: $email, password: $password }';
}

class LoggedOut extends StartEvent {}

class ResetPasswordInitiated extends StartEvent {
  final String uidb64;
  final String token;

  const ResetPasswordInitiated(this.uidb64, this.token);

  @override
  String toString() =>
      'ResetPasswordInitiated { uidb64: $uidb64, token: $token }';
}

class PasswordSubmitted extends StartEvent {
  final String password;

  const PasswordSubmitted({required this.password});

  @override
  List<Object?> get props => [password];

  @override
  String toString() => 'PasswordSubmitted { password: $password }';
}
