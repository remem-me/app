import 'package:equatable/equatable.dart';
import '../../models/box.dart';

abstract class TabEvent extends Equatable {
  const TabEvent();

  @override
  List<Object?> get props => [];
}

class TabUpdated extends TabEvent {
  final VerseBox box;
  final bool? isBoxed;
  final bool isLaunched;
  final bool propagates;

  const TabUpdated(this.box, {this.isBoxed, this.isLaunched = false, this.propagates = false});

  @override
  List<Object?> get props => [box, isBoxed];

  @override
  String toString() => 'TabUpdated { tab: $box, isBoxed: $isBoxed }';
}

class BoxedToggled extends TabEvent {}

