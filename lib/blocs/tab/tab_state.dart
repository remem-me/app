import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

class TabState extends Equatable {
  final bool isBoxed;
  final VerseBox box;
  final int launches;  // recreates the HomeScreen after launch from HomeWidget

  const TabState(this.isBoxed, this.box, [this.launches = 0]);

  @override
  List<Object?> get props => [isBoxed, box, launches];

  TabState copyWith(
      {bool? isBoxed, VerseBox? box, int? launches}) {
    assert(box != VerseBox.ALL);
    return TabState(
      isBoxed ?? this.isBoxed,
      box ?? this.box,
      launches ?? this.launches,
    );
  }

  @override
  String toString() {
    return 'TabState { isBoxed: $isBoxed, box: $box, restarts: $launches }';
  }
}
