import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../models/box.dart';

class TabBloc extends Bloc<TabEvent, TabState> {
  // propagates to inner widget state
  late final _propagateController = StreamController<TabState>.broadcast();

  TabBloc() : super(const TabState(true, VerseBox.DUE)) {

    on<TabUpdated>((event, emit) {
      final next = state.copyWith(
          box: event.box != VerseBox.ALL ? event.box : state.box,
          isBoxed: event.isBoxed ?? state.isBoxed,
          launches: event.isLaunched ? state.launches + 1 : state.launches);
      emit(next);
      if(event.propagates) _propagateController.add(next);
    });
    on<BoxedToggled>(
        (event, emit) {
          final next = state.copyWith(isBoxed: !state.isBoxed);
          emit(next);
          _propagateController.add(next);
        });
  }

  Stream<TabState> get propagateStream => _propagateController.stream;
}
