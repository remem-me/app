import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:share_handler/share_handler.dart';

import '../../paths/paths.dart';

class NavigationCubit extends Cubit<AppRoutePath> {
  static final _log = Logger('NavigationCubit');
  static final urlRegex = RegExp(r'^https:\/\/web\.remem\.me(\/\S*)?$');

  StreamSubscription? _sharingIntentSubscription;

  // propagates to inner widget state
  final _propagateController = StreamController<AppRoutePath>.broadcast();

  NavigationCubit() : super(StartBasePath()) {
    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    // intent format: ShareMedia://dataUrl=ShareKey#text
    if (!kIsWeb) {
      final handler = ShareHandlerPlatform.instance;
      _sharingIntentSubscription =
          handler.sharedMediaStream.listen((SharedMedia media) {
        _log.fine('Shared text received when app in memory: ${media.content}');
        if (media.content != null && !urlRegex.hasMatch(media.content!)) {
          go(HomeCreatePath(data: media.content));
        }
      }, onError: (err) {
        debugPrint("$err");
      });

      // For sharing or opening urls/text coming from outside the app while the app is closed
      handler.getInitialSharedMedia().then((SharedMedia? media) {
        if (media != null) {
          _log.fine('Shared text received when app closed: ${media.content}');
          if (media.content != null && !urlRegex.hasMatch(media.content!)) {
            go(HomeCreatePath(data: media.content));
          }
        }
      });
    }
  }

  Stream<AppRoutePath> get propagateStream => _propagateController.stream;

  void go(AppRoutePath path, {bool propagates = false}) {
    if (propagates) _propagateController.add(path);
    emit(path);
  }

  void goHome() {
    go(HomePath());
  }

  @override
  Future<void> close() {
    _sharingIntentSubscription?.cancel();
    return super.close();
  }
}
