import 'package:equatable/equatable.dart';
import 'package:repository/repository.dart';

abstract class EntitiesState<E extends Entity> extends Equatable {
  const EntitiesState();

  @override
  List<Object?> get props => [];
}

class InitialEntitiesState<E extends Entity> extends EntitiesState<E> {
  const InitialEntitiesState();
}

class EntitiesLoadInProgress<E extends Entity> extends EntitiesState<E> {}

class EntitiesLoadSuccess<T extends Entity> extends EntitiesState<T> {
  final List<T> items;
  final bool isUpdating;

  const EntitiesLoadSuccess(
      [this.items = const [], this.isUpdating = false]);

  @override
  List<Object?> get props => [items, isUpdating];

  @override
  String toString() =>
      'EntitiesLoadSuccess { items: ${items.runtimeType}(${items.length}), '
          'isUpdating: $isUpdating }';

  EntitiesLoadSuccess<T> copyWith({List<T>? items
    , bool? isUpdating}) {
    return EntitiesLoadSuccess(
        items ?? this.items, isUpdating ?? this.isUpdating);
  }
}

// todo: mixin MessageClass

class EntitiesLoadFailure<E extends Entity> extends EntitiesState<E> {
  final String errorMessage;

  const EntitiesLoadFailure(this.errorMessage);

  @override
  List<Object?> get props => [errorMessage];

  @override
  String toString() {
    return 'EntitiesLoadFailure { errorMessage: $errorMessage }';
  }
}
