import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

/// Manages loading, adding, and updating of entities of type E.
/// Its success state can contain extended models T of the entities.
abstract class EntitiesBloc<E extends Entity, T extends E>
    extends Bloc<EntitiesEvent<E>, EntitiesState<E>> {
  static final _log = Logger('EntitiesBloc');
  StreamSubscription? accountSubscription;
  StreamSubscription? authSubscription;

  EntitiesBloc({required EntitiesState? initialState})
      : super(initialState as EntitiesState<E>) {
    on<EntitiesEvent<E>>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (EntitiesState<E> result) => result),
        transformer: sequential()); // process events sequentially
    subscribeToAuthChange();
  }

  EntitiesRepository<E> get repository;

  Stream<EntitiesState<E>> mapEventToState(EntitiesEvent<E> event) async* {
    if (event is EntitiesLoaded<E>) {
      yield* mapEntitiesLoadedToState(event);
    } else if (event is EntitiesAdded<E>) {
      yield* mapEntitiesAddedToState(event);
    } else if (event is EntitiesUpdated<E>) {
      yield* mapEntitiesUpdatedToState(event);
    } else if (event is CurrentAccountUnset<E>) {
      yield EntitiesLoadFailure<E>('No current account');
    }
  }

  Stream<EntitiesState<E>> mapEntitiesLoadedToState(
      EntitiesLoaded<E> event) async* {
    yield EntitiesLoadInProgress<E>();
    try {
      final entities = await repository.readList(accountId: event.account?.id);
      final items = await toModels(entities);
      if (event.account == null ||
          event.account!.id == I<CurrentAccount>().id) {
        // use loaded entities only if account has not changed in the meantime
        yield EntitiesLoadSuccess<T>(await toSorted(items));
      }
    } on MessageException catch (e) {
      MessageService().warn(e.messages['messages']!.join('\n'));
      yield EntitiesLoadFailure<E>('EntitiesLoadedException');
    } on Exception catch (e) {
      MessageService().warn(e.toString());
      yield EntitiesLoadFailure<E>('EntitiesLoadedException');
    }
  }

  Stream<EntitiesState<E>> mapEntitiesAddedToState(
      EntitiesAdded<E> event) async* {
    if (event.entities.isEmpty) return; // RETURN
    final state = this.state;
    if (state is EntitiesLoadSuccess<T>) {
      try {
        if (event.persist) {
          yield state.copyWith(isUpdating: true);
          await repository.createList(event.entities);
        }
        final Iterable<T> addedItems = await toModels(event.entities);
        final List<T> allItems = [...state.items, ...addedItems];
        yield state.copyWith(items: await toSorted(allItems), isUpdating: false);
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield state.copyWith(isUpdating: false);
      }
    } else if (state is! EntitiesLoadInProgress<E>) {
      final Iterable<T> addedItems = await toModels(event.entities);
      yield EntitiesLoadSuccess<T>(await toSorted(addedItems));
    }
  }

  Stream<EntitiesState<E>> mapEntitiesUpdatedToState(
      EntitiesUpdated<E> event) async* {
    if (event.entities.isEmpty) return; // RETURN
    final state = this.state;
    if (state is EntitiesLoadSuccess<T> && event.entities.isNotEmpty) {
      try {
        _log.fine('$runtimeType.mapEntitiesUpdatedToState started');
        if (event.persist) {
          if (event.wait) {
            yield state.copyWith(isUpdating: true);
            await repository.updateList(event.entities);
          } else {
            repository.updateList(event.entities).then((_) {},
                onError: (e) =>
                    MessageService().warn(e.messages['messages']!.join('\n')));
          }
        }
        final updatedItems = await toModels(event.entities);
        _log.fine('$runtimeType.mapEntitiesUpdatedToState toModels completed');
        final List<T> allItems = await compute(
            ListUtil.update<T>, (original: state.items, updated: updatedItems));
        _log.fine('$runtimeType.mapEntitiesUpdatedToState replacing completed');
        final resultState = state.copyWith(items: await toSorted(allItems));
        _log.fine('$runtimeType.mapEntitiesUpdatedToState ended');
        yield resultState;
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield state.copyWith(isUpdating: false);
      }
    }
  }

  void subscribeToAccountSelected() {
    final currentAccount = I<CurrentAccount>();
    accountSubscription = currentAccount.selected().listen((account) => add(
        account == null
            ? CurrentAccountUnset<E>()
            : EntitiesLoaded<E>(account: account)));
  }

  void subscribeToAuthChange() {
    final currentAccount = I<CurrentAccount>();
    final auth = I<AuthBloc>();
    authSubscription = auth.stream.listen((authState) {
      if (authState is Access && currentAccount.state != null) {
        if (state is! EntitiesLoadInProgress<E>) {
          add(EntitiesLoaded<E>(account: currentAccount.state));
        }
      }
    });
  }

  Future<List<T>> toModels(Iterable<E> entities);

  Future<List<T>> toSorted(Iterable<T> items);

  @override
  Future<void> close() {
    accountSubscription?.cancel();
    authSubscription?.cancel();
    return super.close();
  }
}
