import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/status.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:rxdart/rxdart.dart';

import '../current_account/current_account.dart';
import '../blocs.dart';

part 'collections_event.dart';

part 'collections_state.dart';

class CollectionsBloc extends Bloc<CollectionsEvent, CollectionsState> {
  late CollectionService service;

  EventTransformer<CollectionsEvent> debounce<CollectionsEvent>() {
    return (events, mapper) {
      final debouncedEvents = events
          .where((event) => event is! SearchDisplayed)
          .debounceTime(const Duration(milliseconds: 500));
      final notDebouncedEvents =
      events.where((event) => event is SearchDisplayed);
      return MergeStream([debouncedEvents, notDebouncedEvents])
          .flatMap(mapper);
    };
  }

  CollectionsBloc({CollectionsState initialState = const CollectionsState()})
      : super(initialState) {
    service = I<CollectionService>();
    on<CollectionsEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (CollectionsState result) => result),
        transformer: debounce());
  }

  Stream<CollectionsState> mapEventToState(CollectionsEvent event) async* {
    if (event is SearchDisplayed) {
      yield* _mapSearchDisplayedToState(event);
    } else if (event is CollectionsInitiated) {
      yield* _mapCollectionsInitiatedToState(event);
    } else if (event is CollectionsFetched) {
      yield* _mapCollectionsFetchedToState();
    }
  }

  Future<List<Collection>> fetchCollections(Query query) {
    return service.fetchCollections(query);
  }

  Stream<CollectionsState> _mapSearchDisplayedToState(
      SearchDisplayed event) async* {
    final query = event.visible
        ? state.query.copyWith(search: '')
        : state.query.copyWith(nullValues: ['search']);
    if(!event.visible && state.query.search!.isNotEmpty) {
      add(CollectionsInitiated(query));
    } else {
      yield state.copyWith(query: query);
    }
  }

  Stream<CollectionsState> _mapCollectionsInitiatedToState(
      CollectionsInitiated event) async* {
    yield CollectionsState(query: event.query);
    try {
      final collections = await fetchCollections(event.query);
      yield state.copyWith(
        query: event.query,
        fetchStatus: Status.done,
        collections: collections,
        hasReachedMax: service.hasReachedMax(collections.length),
      );
    } on Exception {
      yield state.copyWith(
        fetchStatus: Status.broken,
      );
    }
  }

  Stream<CollectionsState> _mapCollectionsFetchedToState() async* {
    debugPrint('_mapCollectionsFetchedToState: $state');
    if (state.hasReachedMax) {
      yield state;
    } else {
      try {
        final collections = await service.fetchCollections(state.query,
            offset: state.collections.length);
        yield collections.isEmpty
            ? state.copyWith(hasReachedMax: true)
            : state.copyWith(
                fetchStatus: Status.done,
                collections: List.of(state.collections)..addAll(collections),
                hasReachedMax: service.hasReachedMax(collections.length),
              );
      } on Exception {
        yield state.copyWith(fetchStatus: Status.broken);
      }
    }
  }
}

class ImportedBloc extends CollectionsBloc {
  ImportedBloc();

  @override
  Future<List<Collection>> fetchCollections(Query query) {
    final account = I<CurrentAccount>().state;
    if (account != null) {
      return service.fetchImported(account);
    } else {
      throw Exception('Cannot fetch imported collections without account');
    }
  }
}
