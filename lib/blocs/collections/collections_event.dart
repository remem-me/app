part of 'collections_bloc.dart';

abstract class CollectionsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class SearchDisplayed extends CollectionsEvent {
  final bool visible;

  SearchDisplayed(this.visible);

  @override
  List<Object?> get props => [visible];

  @override
  String toString() => 'SearchDisplayed { visible: $visible }';
}

class CollectionsInitiated extends CollectionsEvent {
  final Query query;

  CollectionsInitiated(this.query);

  @override
  List<Object?> get props => [query];

  @override
  String toString() => 'CollectionsInitiated { query: $query }';
}

class CollectionsFetched extends CollectionsEvent {}

