part of 'collections_bloc.dart';

class CollectionsState extends Equatable {
  final Status fetchStatus;
  final List<Collection> collections;
  final bool hasReachedMax;
  final Query query;

  const CollectionsState({
    this.collections = const <Collection>[],
    this.fetchStatus = Status.none,
    this.hasReachedMax = false,
    this.query = const Query(),  // todo: use account language
  });

  @override
  List<Object?> get props => [collections, fetchStatus, hasReachedMax, query];

  @override
  String toString() => 'CollectionsState { collections: ${collections.length}, '
      'fetchStatus: $fetchStatus, hasReachedMax: $hasReachedMax, query: $query }';

  CollectionsState copyWith({
    List<Collection>? collections,
    Status? fetchStatus,
    bool? hasReachedMax,
    Query? query,
  }) {
    return CollectionsState(
        collections: collections ?? this.collections,
        fetchStatus: fetchStatus ?? this.fetchStatus,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        query: query ?? this.query,
    );
  }
}
