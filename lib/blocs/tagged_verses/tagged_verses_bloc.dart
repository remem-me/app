import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs.dart';

class TaggedVersesBloc extends Bloc<TaggedVersesEvent, TaggedVersesState> {
  static final _log = Logger('TaggedVersesBloc');
  late VersesBloc versesBloc;
  late TagsBloc tagsBloc;
  late StreamSubscription versesSubscription;
  late StreamSubscription tagsSubscription;

  TaggedVersesBloc() : super(InitialTaggedVersesState()) {
    versesBloc = I<VersesBloc>();
    tagsBloc = I<TagsBloc>();
    on<TaggedVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (TaggedVersesState result) => result),
        transformer: sequential()); // process events sequentially
    versesSubscription = versesBloc.stream
        .startWith(versesBloc.state)
        .listen(_versesBlocUpdated);
    tagsSubscription =
        tagsBloc.stream.startWith(tagsBloc.state).listen(_tagsBlocUpdated);
  }

  void _tagsBlocUpdated(EntitiesState<TagEntity>? state) {
    if (state is EntitiesLoadSuccess<TagEntity>) {
      if (state.isUpdating) {
        add(TagsUpdated());
      } else {
        add(TagsLoaded(state.items));
      }
    } else if (state is EntitiesLoadFailure<TagEntity>) {
      add(const TagsLoaded([])); // fail silently
    }
  }

  void _versesBlocUpdated(EntitiesState<VerseEntity>? state) {
    if (state is EntitiesLoadSuccess<Verse>) {
      if (state.isUpdating) {
        add(VersesUpdated());
      } else {
        add(VersesLoaded(state.items));
      }
    } else if (state is EntitiesLoadInProgress<VerseEntity>) {
      add(TaggedVersesReloaded());
    } else if (state is EntitiesLoadFailure<VerseEntity>) {
      add(VersesFailed());
    }
  }

  Stream<TaggedVersesState> mapEventToState(TaggedVersesEvent event) async* {
    if (event is VersesUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is TagsUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is VersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TagsLoaded) {
      yield* _mapTagsLoadedToState(event);
    } else if (event is TaggedVersesReloaded) {
      yield* _mapTaggedVersesReloadedToState();
    } else if (event is VersesFailed) {
      yield TaggedVersesFailure();
    }
  }

  Stream<TaggedVersesState> _mapTaggedVersesReloadedToState() async* {
    yield TaggedVersesInProgress();
  }

  Stream<TaggedVersesState> _mapUpdatedToState() async* {
    final state = this.state;
    if (state is TaggedVersesSuccess) {
      yield state.copyWith(isUpdating: true);
    }
  }

  Stream<TaggedVersesState> _mapVersesLoadedToState(
    VersesLoaded event,
  ) async* {
    if (tagsBloc.state is EntitiesLoadSuccess<TagEntity>) {
      yield TaggedVersesInProgress();
      _log.fine(
          '$runtimeType._mapVersesLoadedToState for ${event.runtimeType}');
      final resultState = await _taggedVersesLoadSuccess(
          (tagsBloc.state as EntitiesLoadSuccess<TagEntity>).items,
          event.verses);
      _log.fine(
          '$runtimeType._mapVersesLoadedToState ended');
      yield resultState;
    } else if (tagsBloc.state is EntitiesLoadFailure<TagEntity>) {
      yield TaggedVersesInProgress();
      yield await _taggedVersesLoadSuccess([], event.verses); // fail silently
    }
  }

  Stream<TaggedVersesState> _mapTagsLoadedToState(
    TagsLoaded event,
  ) async* {
    if (versesBloc.state is EntitiesLoadSuccess<Verse>) {
      yield TaggedVersesInProgress();
      yield await _taggedVersesLoadSuccess(event.tags,
          (versesBloc.state as EntitiesLoadSuccess<Verse>).items);
    }
  }

  Future<TaggedVersesSuccess> _taggedVersesLoadSuccess(
      List<TagEntity> tags, List<Verse> verses) async {
    final nonEmptyTags =
        _mapVersesToTags(verses, tags).where((tag) => tag.size! > 0).toList();
    final taggedVerses = _mapTagsToVerses(nonEmptyTags, verses);
    return TaggedVersesSuccess(verses: taggedVerses, tags: nonEmptyTags);
  }

  List<Verse> _mapTagsToVerses(List<TagEntity> tags, List<Verse> verses) {
    if (verses.isNotEmpty && tags.isNotEmpty) {
      return verses
          .map((verse) => verse.copyWith(tags: _tagsOfVerse(verse, tags)))
          .toList();
    } else {
      return verses;
    }
  }

  Map<int, String?> _tagsOfVerse(Verse verse, List<TagEntity> tags) {
    final Map<int, TagEntity?> tagsMap = {for (var tag in tags) tag.id: tag};
    final entries = verse.tags.entries
        .where((entry) => tagsMap[entry.key] != null)
        .map((entry) =>
            MapEntry<int, String?>(entry.key, tagsMap[entry.key]!.text));
    return Map.fromEntries(entries);
  }

  List<Tag> _mapVersesToTags(List<Verse> verses, List<TagEntity> tags) {
    return tags.map((tag) {
      return Tag.fromEntity(tag,
          size:
              verses.where((verse) => verse.tags.keys.contains(tag.id)).length);
    }).toList();
  }

  @override
  Future<void> close() {
    versesSubscription.cancel();
    tagsSubscription.cancel();
    return super.close();
  }
}
