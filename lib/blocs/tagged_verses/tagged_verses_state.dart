import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class TaggedVersesState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialTaggedVersesState extends TaggedVersesState {}

class TaggedVersesInProgress extends TaggedVersesState {}

class TaggedVersesFailure extends TaggedVersesState {}

class TaggedVersesSuccess extends TaggedVersesState {
  final List<Verse> verses;
  final List<Tag> tags;
  final bool isUpdating;

  TaggedVersesSuccess({
    required this.verses,
    required this.tags,
    this.isUpdating = false,
  });

  @override
  List<Object?> get props => [verses, tags, isUpdating];
  // list comparation: https://github.com/felangel/equatable/issues/85

  TaggedVersesSuccess copyWith({List<Verse>? verses, bool? isUpdating}) {
    return TaggedVersesSuccess(
      verses: verses ?? this.verses,
      tags: tags,
      isUpdating: isUpdating ?? this.isUpdating,
    );
  }

  @override
  String toString() {
    return 'TaggedVersesSuccess { verses: ${verses.length}, tags: ${tags.length}, '
        'isUpdating: $isUpdating }';
  }
}
