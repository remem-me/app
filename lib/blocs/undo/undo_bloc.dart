import 'package:app_core/app_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';
import '../blocs.dart';

class UndoBloc extends Bloc<UndoEvent, List<Undo>?> {
  UndoBloc({List<Undo>? initialState}) : super(initialState) {
    on<UndoAdded>((event, emit) => emit(event.undos));
    on<UndoApplied>(_onUndoApplied);
    on<UndoCleared>((event, emit) => emit(null));
  }

  void _onUndoApplied(UndoEvent event, Emitter<List<Undo>?> emit) async {
    for (Undo undo in (state ?? [])) {
      if (undo.entities.first is VerseEntity) {
        await _undo<VerseEntity>(I<VersesBloc>(), undo.action,
            undo.entities as List<VerseEntity>);
      } else if (undo.entities.first is ScoreEntity) {
        await _undo<ScoreEntity>(I<ScoresBloc>(), undo.action,
            undo.entities as List<ScoreEntity>);
      } else if (undo.entities.first is TagEntity) {
        await _undo<TagEntity>(I<TagsBloc>(), undo.action,
            undo.entities as List<TagEntity>);
      } else if (undo.entities.first is AccountEntity) {
        await _undo<AccountEntity>(I<AccountsBloc>(), undo.action,
            undo.entities as List<AccountEntity>);
      } else {
        throw UnimplementedError(
            "Undo for ${undo.entities.first.runtimeType} not implemented");
      }
    }
    emit(null);
  }

  Future<void> _undo<E extends Entity>(
      EntitiesBloc bloc, EntityAction action, List<E> entities) async {
    switch (action) {
      case EntityAction.update:
        bloc.add(EntitiesUpdated<E>(entities));
        break;
      case EntityAction.delete:
        await bloc.repository.updateList(entities);
        bloc.add(EntitiesAdded<E>(entities, persist: false));
        break;
      case EntityAction.create:
        bloc.add(EntitiesUpdated<E>(entities
            .map((entity) => entity.copyWith(deleted: true) as E)
            .toList()));
        break;
    }
  }
}
