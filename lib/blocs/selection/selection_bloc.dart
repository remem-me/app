import 'dart:async';

import 'package:bloc/bloc.dart';

import '../blocs.dart';

class SelectionBloc extends Bloc<SelectionEvent, SelectionState> {
  final TabBloc tabBloc;
  final VersesBloc versesBloc;
  late StreamSubscription tabSubscription;
  late StreamSubscription versesSubscription;

  SelectionBloc({
    required this.tabBloc,
    required this.versesBloc,
  }) : super(const SelectionState({})) {
    on<SelectionUpdated>((event, emit) =>
        emit(state.copyWith(selection: event.selection)));
    on<SelectionCleared>((event, emit) =>
        emit(state.copyWith(selection: {})));
    on<SearchActivated>((event, emit) =>
        emit(state.copyWith(isSearchActive: true)));
    on<SearchDeactivated>((event, emit) =>
        emit(state.copyWith(isSearchActive: false)));
    tabSubscription = tabBloc.stream.listen((state) {
      add(SelectionCleared());
    });
    versesSubscription = versesBloc.stream.listen((state) {
      add(SelectionCleared());
    });
  }

  @override
  Future<void> close() {
    tabSubscription.cancel();
    versesSubscription.cancel();
    return super.close();
  }
}
