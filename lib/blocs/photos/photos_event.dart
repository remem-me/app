part of 'photos_bloc.dart';

abstract class PhotosEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class PhotosInitiated extends PhotosEvent {
  final String? search;

  PhotosInitiated([this.search]);

  @override
  List<Object?> get props => [search];

  @override
  String toString() => 'PhotosInitiated { query: $search }';
}

class PhotosFetched extends PhotosEvent {}
