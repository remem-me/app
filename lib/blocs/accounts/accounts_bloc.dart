import 'dart:async';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:app_core/app_core.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';
import '../blocs.dart';

class AccountsBloc extends EntitiesBloc<AccountEntity, AccountEntity> {
  static final _log = Logger('AccountsBloc');
  late final AuthBloc _auth;
  late final CurrentAccount _currentAccount;
  late final StreamSubscription? _connectionSubscription;
  late final StreamSubscription _accountSelectedSubscription;

  AccountsBloc(
      [EntitiesState<AccountEntity>? initialState =
          const InitialEntitiesState<AccountEntity>()])
      : super(initialState: initialState) {
    _auth = I<AuthBloc>();
    _currentAccount = I<CurrentAccount>();
    if (I.isRegistered<ConnectivityCubit>()) {
      _connectionSubscription =
          I.get<ConnectivityCubit>().stream.distinct((prev, next) {
        _log.fine('prev: $prev | next: $next');
        return ![prev, next].contains(ConnectivityResult.none) || prev == next;
      }).listen((ConnectivityResult result) {
        // This event is irrelevant for AccessLocal
        if (result != ConnectivityResult.none && _auth.state is AccessRemote) {
          if (_currentAccount.state != null) {
            if (!kIsWeb) {
              _log.fine('RplStarted from ConnectivityChange');
              I<ReplicationBloc>().add(const RplStarted());
            }
          } else if (state is! EntitiesLoadInProgress<AccountEntity>) {
            add(const EntitiesLoaded<AccountEntity>());
          }
        }
      });
    }
    _accountSelectedSubscription =
        _currentAccount.selected().listen((state) async {
      TtsHandler().stop();
      I<UndoBloc>().add(UndoCleared());
      if (!kIsWeb &&
          state != null &&
          I<AuthBloc>().state is AccessRemote &&
          I<ReplicationBloc>().state is! ReplicationInProgress) {
        _log.fine('RplStarted from Account selected');
        I<ReplicationBloc>().add(const RplStarted());
      }
      final dueBloc = I<OrderedVersesDueBloc>();
      var orderState = await dueBloc.stream.firstWhere(dueBloc.completed);
      I.get<TabBloc>().add(TabUpdated(
          orderState is OrderSuccess && orderState.verses.isNotEmpty
              ? VerseBox.DUE
              : VerseBox.NEW,
          isBoxed: true,
          propagates: true));
    });
  }

  @override
  EntitiesRepository<AccountEntity> get repository =>
      I<EntitiesRepository<AccountEntity>>();

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesLoadedToState(
      EntitiesLoaded<AccountEntity> event) async* {
    yield EntitiesLoadInProgress();
    try {
      var entities = await toSorted(await repository.readList());
      if (entities.isEmpty && _auth.state is AccessLocal) {
        if (!I.isRegistered<L10n>()) await L10n.initialized;
        final l10n = L10n.current;
        entities = [
          await repository.create(AccountEntity(l10n.t8('User.local'),
              language: l10n.locale, langRef: l10n.locale))
        ];
      }
      if (entities.isNotEmpty) {
        AccountEntity? currentEntity;
        final currentId = _currentAccount.id;
        if (currentId > -1) {
          currentEntity = entities.firstWhereOrNull((a) => a.id == currentId);
        }
        currentEntity ??= entities.first;
        await _currentAccount.setEntity(currentEntity);
      }
      yield EntitiesLoadSuccess<AccountEntity>(entities);
    } on MessageException catch (e) {
      MessageService().warn(e.messages['messages']!.join('\n'));
      yield const EntitiesLoadFailure<AccountEntity>('AccountsLoadedException');
    } on Exception catch (e) {
      MessageService().warn(e.toString());
      yield const EntitiesLoadFailure<AccountEntity>('AccountsLoadedException');
    }
    if (!kIsWeb && I<AuthBloc>().state is AccessRemote) {
      _log.fine('RplStarted from AccountEntitiesLoaded');
      I<ReplicationBloc>().add(const RplStarted());
    }
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesAddedToState(
      EntitiesAdded<AccountEntity> event) {
    return super
        .mapEntitiesAddedToState(event)
        .asyncMap(_determineCurrentAccount());
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesUpdatedToState(
      EntitiesUpdated<AccountEntity> event) {
    return super
        .mapEntitiesUpdatedToState(event)
        .asyncMap(_determineCurrentAccount());
  }

  // todo: test to make sure a current account gets set
  _determineCurrentAccount() => (EntitiesState<AccountEntity> nextState) async {
        if (nextState is EntitiesLoadSuccess<AccountEntity>) {
          var active =
              nextState.items.where((entity) => !entity.deleted).toList();
          if (active.isNotEmpty) {
            try {
              final updatedCurrentAccount = active
                  .firstWhere((account) => account.id == _currentAccount.id);
              await _currentAccount.setEntity(updatedCurrentAccount);
            } on StateError {
              await _currentAccount.setEntity(active[0]);
            }
          } else {
            _log.fine('No active account. Setting currentAccount to null.');
            _currentAccount.setAccount(null);
          }
        }
        return nextState;
      };

  @override
  void subscribeToAuthChange() {
    _log.fine('$runtimeType.subscribeToAuthChange');
    final auth = I<AuthBloc>();
    authSubscription = auth.stream.listen((authState) {
      if (authState is Access) {
        if (state is! EntitiesLoadInProgress<AccountEntity>) {
          add(const EntitiesLoaded<AccountEntity>());
        }
      } else if (authState is NoAuth) {
        _log.fine('authStat is NoAuth. Setting currentAccount to null.');
        _currentAccount.setAccount(null);
        emit(const InitialEntitiesState<AccountEntity>());
      }
    });
  }

  @override
  Future<void> close() async {
    _connectionSubscription?.cancel();
    _accountSelectedSubscription.cancel();
    return super.close();
  }

  @override
  Future<List<AccountEntity>> toSorted(Iterable<AccountEntity> entities) async {
    return entities.toSet().sortedBy((it) => it.name);
  }

  @override
  Future<List<AccountEntity>> toModels(Iterable<AccountEntity> entities) {
    return Future.value(entities.toList());
  }
}
