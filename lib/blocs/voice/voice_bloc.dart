import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:just_audio/just_audio.dart';
import 'package:logging/logging.dart';
import 'package:record/record.dart';
import 'package:remem_me/models/status.dart';

import '../blocs.dart';

part 'voice_event.dart';

part 'voice_state.dart';

class VoiceBloc extends Bloc<VoiceEvent, VoiceState> {
  static final _log = Logger('VoiceBloc');
  final FlashcardBloc flashcardBloc;
  @visibleForTesting
  late Record recorder;
  @visibleForTesting
  late AudioPlayer player;
  late StreamSubscription _flashcardSubscription;
  late StreamSubscription _completedSubscription;
  @visibleForTesting
  AudioSource? source;

  VoiceBloc(this.flashcardBloc, {Record? recorder, AudioPlayer? player})
      : super(const VoiceState(Status.none, Status.none)) {
    this.recorder = recorder ?? Record();
    this.player = player ?? AudioPlayer();
    on<VoiceEvent>(
      (event, emit) => emit.forEach(mapEventToState(event),
          onData: (VoiceState result) => result),
      transformer: sequential(),
    );

    _flashcardSubscription = flashcardBloc.stream
        .distinct((prev, next) =>
            prev is FlashcardRun &&
            next is FlashcardRun &&
            prev.flipping == next.flipping)
        .where((state) =>
            state is FlashcardRun &&
            !state.flipping &&
            !state.inverse &&
            state.reverse)
        .listen((_) => add(PlayPrepared()));

    _completedSubscription = this.player.playerStateStream
        .where((state) => state.processingState == ProcessingState.completed)
        .listen((_) => add(PlayCompleted()));
  }

  Stream<VoiceState> mapEventToState(VoiceEvent event) async* {
    if (event is RecordPrepared) {
      yield* _onRecordPrepared(event);
    } else if (event is RecordStarted) {
      yield* _onRecordStarted(event);
    } else if (event is RecordStopped) {
      yield* _onRecordStopped(event);
    } else if (event is PlayPrepared) {
      yield* _onPlayPrepared(event);
    } else if (event is PlayStarted) {
      yield* _onPlayStarted(event);
    } else if (event is PlayCompleted) {
      yield* _onPlayCompleted(event);
    } else if (event is PlayStopped) {
      yield* _onPlayStopped(event);
    }
  }

  Stream<VoiceState> _onRecordPrepared(RecordPrepared event) async* {
    final isAllowed = await recorder.hasPermission();
    yield state.copyWith(record: isAllowed ? Status.ready : Status.forbidden);
  }

  Stream<VoiceState> _onRecordStarted(RecordStarted event) async* {
    var status = state.record;
    if (status == Status.none) {
      if (await recorder.hasPermission()) {
        status = Status.ready;
      } else {
        yield state.copyWith(record: Status.forbidden);
        return;
      }
    }
    if (status == Status.ready) {
      try {
        await recorder.start();
        bool isActive = await recorder.isRecording();
        if (!isActive) throw Exception('No valid recording session');
        yield state.copyWith(record: Status.active);
      } catch (e) {
        yield state.copyWith(record: Status.broken);
        _log.severe(e.toString());
      }
    }
  }

  Stream<VoiceState> _onRecordStopped(RecordStopped event) async* {
    final path = await recorder.stop();
    if (path == null) {
      yield state.copyWith(record: Status.broken);
      _log.info('Recording not saved.');
    } else {
      source = AudioSource.uri(Uri.parse(path));
      yield state.copyWith(record: Status.ready);
      _log.info('Recording saved as: $path');
    }
  }

  Stream<VoiceState> _onPlayPrepared(PlayPrepared event) async* {
    if (source != null) {
      await player.setAudioSource(source!);
      yield state.copyWith(play: Status.ready);
      add(PlayStarted());
    } else {
      _log.info('Audio source is not set. Cannot prepare to play.');
    }
  }

  Stream<VoiceState> _onPlayStarted(PlayStarted event) async* {
    if (state.play == Status.ready) {
      yield state.copyWith(play: Status.active);
      player.play();
    }
  }

  Stream<VoiceState> _onPlayCompleted(PlayCompleted event) async* {
    if (state.play == Status.active) {
      yield state.copyWith(play: Status.done);
    }
  }

  Stream<VoiceState> _onPlayStopped(PlayStopped event) async* {
    player.stop();
  }

  @override
  Future<void> close() async {
    await _flashcardSubscription.cancel();
    await _completedSubscription.cancel();
    await recorder.dispose();
    await player.dispose();
    return super.close();
  }
}
