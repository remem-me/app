import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../../models/verse.dart';
import '../blocs.dart';

class VersesBloc extends EntitiesBloc<VerseEntity, Verse> {
  static final _log = Logger('VersesBloc');
  late StreamSubscription accountModifiedSubscription;

  VersesBloc(
      {EntitiesState<VerseEntity> initialState =
          const InitialEntitiesState<VerseEntity>()})
      : super(initialState: initialState) {
    subscribeToAccountSelected();
    accountModifiedSubscription = I
        .get<CurrentAccount>()
        .stream
        .distinct((prev, next) {
          final isEqual =
              (prev?.id != next?.id || // account selected is handled elsewhere
                  prev?.canon == next?.canon &&
                      prev?.langRef == next?.langRef &&
                      prev?.reviewFrequency == next?.reviewFrequency &&
                      prev?.reviewLimit == next?.reviewLimit);
          if (!isEqual) _log.fine('Account modified. prev: $prev, next: $next');
          return isEqual;
        })
        .skip(1) // distinct always emits first event
        .listen((_) => add(AccountModified()));
  }

  @override
  EntitiesRepository<VerseEntity> get repository =>
      I<EntitiesRepository<VerseEntity>>();

  @override
  Stream<EntitiesState<VerseEntity>> mapEventToState(
      EntitiesEvent<VerseEntity> event) async* {
    yield* super.mapEventToState(event);
    if (event is TagDeleted) {
      yield* _mapTagDeletedToState(event);
    } else if (event is AccountModified) {
      yield* _mapAccountModifiedToState();
    }
  }

  Stream<EntitiesState<VerseEntity>> _mapTagDeletedToState(
    TagDeleted event,
  ) async* {
    if (state is EntitiesLoadSuccess<Verse>) {
      final updatedVerses = (state as EntitiesLoadSuccess<Verse>)
          .items
          .where((verse) => verse.tags.containsKey(event.tagId))
          .map((verse) {
        final tags = Map.of(verse.tags);
        tags.remove(event.tagId);
        return verse.copyWith(tags: tags); // todo: batch update
      }).toList();
      yield* mapEntitiesUpdatedToState(EntitiesUpdated<Verse>(updatedVerses));
    }
  }

  Stream<EntitiesState<VerseEntity>> _mapAccountModifiedToState() async* {
    final state = this.state;
    if (state is EntitiesLoadSuccess<Verse>) {
      yield state.copyWith(isUpdating: true);
      final verses = await toModels(state.items);
      yield EntitiesLoadSuccess<Verse>(verses);
    }
  }

  @override
  Future<List<Verse>> toModels(Iterable<VerseEntity> entities) {
    return VerseService().fromEntities(entities);
  }

  @override
  Future<List<Verse>> toSorted(Iterable<Verse> items) {
    return compute(
        ListUtil.toSorted<Verse, String>, (items: items, sortedBy: null));
  }

  @override
  Future<void> close() {
    accountModifiedSubscription.cancel();
    return super.close();
  }
}
