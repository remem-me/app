import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/models/models.dart';

class TagDeleted extends EntitiesEvent<Verse> {
  final int tagId;

  const TagDeleted(this.tagId);

  @override
  List<Object?> get props => [tagId];

  @override
  String toString() => 'TagDeleted { tagId: $tagId }';
}

class AccountModified extends EntitiesEvent<Verse> {}

