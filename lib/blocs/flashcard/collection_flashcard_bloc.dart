import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/paths/collection_paths.dart';
import 'package:rxdart/rxdart.dart';

import '../../models/models.dart';
import '../../routers/routers.dart';

class CollectionFlashcardBloc extends FlashcardBloc {
  static final _log = Logger('CollectionFlashcardBloc');
  final CollectionDetailBloc collectionBloc;

  @override
  get defaultHints => const <FlashcardHint>{FlashcardHint.subtitle};

  late StreamSubscription _launchedSubscription;

  CollectionFlashcardBloc({required this.collectionBloc})
      : super(const FlashcardLoadInProgress()) {
    _launchedSubscription = nav.propagateStream
        .startWith(nav.state)
        .where((path) => path is CollectionFlashcardPath)
        .listen((path) {
      add(FlashcardInitiated((path as CollectionFlashcardPath).verseId));
    });
  }

  @override
  Stream<FlashcardState> mapInitiatedToState(FlashcardInitiated event) async* {
    final collectionState = await collectionBloc.stream
        .startWith(collectionBloc.state)
        .firstWhere((state) =>
            state is CollectionDetailLoadSuccess ||
            state is CollectionDetailLoadFailure);
    if (collectionState is CollectionDetailLoadSuccess) {
      final index = collectionState.collection.verses
          .indexWhere((verse) => verse.id == event.id);
      if (index < 0) {
        I<NavigationCubit>()
            .go(CollectionDetailPath(id: collectionState.collection.id));
        yield FlashcardLoadInProgress();
      } else {
        yield* mapLoadedToState(FlashcardLoaded(
            box: null,
            verses: collectionState.collection.verses,
            index: index,
            inverse: false,
            discrete: true));
      }
    }
  }

  @override
  Stream<FlashcardState> mapActionToState(FlashcardAction event) async* {
    if (state is! FlashcardRun) yield state;
    yield* nextStateFromAction(event);
  }

  @override
  Stream<FlashcardState> mapStudiedToState() async* {
    final path = nav.state;
    if (path is CollectionFlashcardPath) {
      nav.go(path.study);
    }
  }

  @override
  void updateNavigation(int verseId) {
    final path = nav.state;
    if (path is CollectionFlashcardPath) {
      nav.go(path.copyWith(verseId: verseId));
    }
  }

  @override
  bool isFlashcardPath(AppRoutePath path) {
    return path is! CollectionsPath || path is CollectionFlashcardPath;
  }

  @override
  Future<void> close() {
    _launchedSubscription.cancel();
    return super.close();
  }
}
