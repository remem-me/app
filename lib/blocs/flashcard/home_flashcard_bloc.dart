import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/home_flashcard_service.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

import '../../paths/paths.dart';
import '../blocs.dart';

class HomeFlashcardBloc extends FlashcardBloc {
  static final _log = Logger('HomeFlashcardBloc');

  @override
  get defaultHints => I<CurrentAccount>().referenceIncluded
      ? <FlashcardHint>{}
      : {FlashcardHint.subtitle, FlashcardHint.tags};

  late VersesBloc versesBloc;
  late ScoresBloc scoresBloc;
  late StreamSubscription _launchedSubscription;

  HomeFlashcardBloc([super.initialState = const FlashcardLoadInProgress()]) {
    versesBloc = I<VersesBloc>();
    scoresBloc = I<ScoresBloc>();
    _launchedSubscription = nav.propagateStream
        .where((path) => path is HomeFlashcardPath)
        .map((path) => path as HomeFlashcardPath)
        .listen((path) async {
      final event = await HomeFlashcardService().toFlashcardLoaded(path);
      if (event.box == null) {
        nav.goHome();
        return; // RETURN
      }
      add(event);
      I<TabBloc>().add(TabUpdated(event.box!, propagates: true));
    });
  }

  @override
  Stream<FlashcardState> mapActionToState(FlashcardAction event) async* {
    if (state is! FlashcardRun) yield state;
    _log.fine(
        '$runtimeType._updateEntitiesFromAction for ${event.runtimeType}');
    _updateEntitiesFromAction(event);
    _log.fine('$runtimeType.nextStateFromAction for ${event.runtimeType}');
    yield* nextStateFromAction(event);
  }

  @override
  Stream<FlashcardState> mapCancelledToState(FlashcardCancelled event) {
    nav.goHome();
    return super.mapCancelledToState(event);
  }

  void _updateEntitiesFromAction(FlashcardAction event) async {
    final run = state as FlashcardRun?;
    final undoBloc = I<UndoBloc>();
    switch (event.action) {
      case VerseAction.COMMITTED:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().committed(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      case VerseAction.FORGOTTEN:
        if (!run!.inverse) {
          final updatedVerse = VerseService().forgotten(run.verse);
          add(FlashcardVerseUpdated(updatedVerse));
          versesBloc.add(
            EntitiesUpdated<Verse>([updatedVerse], wait: false),
          );
          undoBloc.add(UndoAdded([
            Undo(EntityAction.update, <VerseEntity>[run.verse])
          ]));
        }
        break;
      case VerseAction.REMEMBERED:
        if (!run!.inverse) {
          versesBloc.add(
            EntitiesUpdated<Verse>([VerseService().remembered(run.verse)],
                wait: false),
          );
          final score = ScoreEntity(
            vkey: run.verse.id.toString(),
            date: DateService().today,
            change: TextService().countWordsOfText(run.verse.passage),
            accountId: run.verse.accountId!,
          );
          scoresBloc.add(EntitiesAdded([score]));
          undoBloc.add(UndoAdded([
            Undo(EntityAction.update, <VerseEntity>[run.verse]),
            Undo(EntityAction.create, <ScoreEntity>[score]),
          ]));
        }
        break;
      case VerseAction.MOVED_TO_DUE:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToDue(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      case VerseAction.MOVED_TO_NEW:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToNew(run!.verse)],
              wait: false),
        );
        undoBloc.add(UndoAdded([
          Undo(EntityAction.update, <VerseEntity>[run.verse])
        ]));
        break;
      default:
        undoBloc.add(UndoCleared());
        break;
    }
  }

  @override
  Stream<FlashcardState> mapInitiatedToState(FlashcardInitiated event) {
    throw UnimplementedError();
  }

  @override
  Stream<FlashcardState> mapStudiedToState() async* {
    final path = nav.state;
    if (path is HomeFlashcardPath) {
      nav.go(path.study);
    }
  }

  @override
  void updateNavigation(int verseId) {
    final path = nav.state;
    if (path is HomeFlashcardPath) {
      nav.go(path.copyWith(id: verseId));
    }
  }

  @override
  bool isFlashcardPath(AppRoutePath path) {
    return path is! HomePath || path is HomeFlashcardPath;
  }

  @override
  Future<void> close() {
    _launchedSubscription.cancel();
    return super.close();
  }
}
