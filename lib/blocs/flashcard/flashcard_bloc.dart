import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/app_route_path.dart';
import 'package:remem_me/services/verse_service.dart';

import '../blocs.dart';

abstract class FlashcardBloc extends Bloc<FlashcardEvent, FlashcardState> {
  static final _log = Logger('FlashcardBloc');
  final defaultHints = const <FlashcardHint>{};
  final singleHints = const {FlashcardHint.subtitle, FlashcardHint.tags};

  late NavigationCubit nav;
  late StreamSubscription _cancelledSubscription;

  FlashcardBloc(super.initialState) {
    on<FlashcardEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (FlashcardState result) => result),
        transformer: sequential()); // process events sequentially
    nav = I<NavigationCubit>();

    _cancelledSubscription = nav.propagateStream.where((path) {
      return !isFlashcardPath(path) && state is FlashcardRun;
    }).listen((_) => add(FlashcardCancelled()));
  }

  bool isFlashcardPath(AppRoutePath path);

  Stream<FlashcardState> mapEventToState(FlashcardEvent event) async* {
    if (event is FlashcardInitiated) {
      yield* mapInitiatedToState(event);
    } else if (event is FlashcardLoaded) {
      yield* mapLoadedToState(event);
    } else if (event is FlashcardCancelled) {
      yield* mapCancelledToState(event);
    } else if (event is FlashcardAction) {
      yield* mapActionToState(event);
    } else if (event is FlipStarted) {
      yield* mapFlipStartedToState(event);
    } else if (event is FlipEnded) {
      yield* mapFlipEndedToState(event);
    } else if (event is FlipReset) {
      yield* mapFlipResetToState(event);
    } else if (event is FlashcardVerseUpdated) {
      yield* mapFlashcardVerseUpdatedToState(event);
    } else if (event is LineRevealed) {
      yield* mapLineRevealedToState(event);
    } else if (event is FlashcardHinted) {
      yield* mapHintedToState();
    } else if (event is FlashcardStudied) {
      yield* mapStudiedToState();
    }
  }

  Stream<FlashcardState> mapInitiatedToState(FlashcardInitiated event);

  Stream<FlashcardState> mapLoadedToState(FlashcardLoaded event) async* {
    yield FlashcardRun(
        verses: [...event.verses],
        index: event.index,
        box: event.box,
        inverse: event.inverse,
        hinting: event.inverse
            ? null
            : event.box == VerseBox.NEW
                ? singleHints
                : defaultHints,
        reverse: event.reverse,
        visible: event.reverse,
        discrete: event.discrete,
        flipping: false);
  }

  Stream<FlashcardState> mapCancelledToState(FlashcardCancelled event) async* {
    yield const FlashcardLoadInProgress();
  }

  Stream<FlashcardState> mapActionToState(FlashcardAction event);

  Stream<FlashcardState> mapFlipStartedToState(FlipStarted event) async* {
    if (state is FlashcardRun) {
      yield (state as FlashcardRun).copyWith(flipping: true);
    }
  }

  Stream<FlashcardState> mapFlipEndedToState(FlipEnded event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(reverse: !run.reverse, flipping: false);
    }
  }

  Stream<FlashcardState> mapFlipResetToState(FlipReset event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(reverse: false, visible: true);
    }
  }

  Stream<FlashcardState> mapFlashcardVerseUpdatedToState(
      FlashcardVerseUpdated event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(
          verses: run.verses
              .map((verse) => verse.id == event.verse.id ? event.verse : verse)
              .toList());
    }
  }

  Stream<FlashcardState> mapHintedToState() async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      assert(run.hinting != null); // runs that can hint need to start with []
      var hints = {...run.hinting!};
      final hasSubtitle = VerseService().subtitle(run.verse).isNotEmpty;
      final hasTags = run.verse.tags.isNotEmpty;
      if (hasSubtitle && !hints.contains(FlashcardHint.subtitle)) {
        hints.add(FlashcardHint.subtitle);
      } else if (hasTags && !hints.contains(FlashcardHint.tags)) {
        hints.add(FlashcardHint.tags);
      } else if (!hints.contains(FlashcardHint.beginning)) {
        hints.add(FlashcardHint.beginning);
      } else {
        hints = defaultHints;
      }
      yield run.copyWith(hinting: hints);
    }
  }

  Stream<FlashcardState> mapLineRevealedToState(LineRevealed event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(revealLines: event.lines ?? (run.revealLines + 1));
    }
  }

  Stream<FlashcardState> mapStudiedToState();

  Stream<FlashcardState> nextStateFromAction(FlashcardAction event) async* {
    final run = state;
    if (run is! FlashcardRun ||
        [VerseAction.REMEMBERED, VerseAction.COMMITTED]
                .contains(event.action) &&
            run.verses.length == 1) {
      yield FlashcardEnd();
    } else {
      yield _nextMultiFlashcardRun(run, event);
    }
  }

  FlashcardRun _nextMultiFlashcardRun(FlashcardRun run, FlashcardAction event) {
    _log.fine('FlashcardBloc._nextMultiFlashcardRun run: $run, event: $event');
    if (event.action == VerseAction.UNDONE && run.previous != null) {
      return run.previous!; // RETURN
    }
    final countRemembered = event.action == VerseAction.REMEMBERED
        ? run.countRemembered + 1
        : run.countRemembered;
    final countForgotten = event.action == VerseAction.FORGOTTEN
        ? run.countForgotten + 1
        : run.countForgotten;
    late FlashcardRun nextState;
    if ([VerseAction.REMEMBERED, VerseAction.COMMITTED]
        .contains(event.action)) {
      nextState = run.copyWith(
          index: run.index % (run.verses.length - 1),
          verses: [
            ...run.verses.sublist(0, run.index),
            ...run.verses.sublist(run.index + 1)
          ],
          countRemembered: countRemembered,
          countForgotten: countForgotten,
          reverse: false,
          visible: false,
          revealLines: 0,
          hinting: run.hinting != null ? defaultHints : null,
          previous: run.copyWith(reverse: false));
    } else {
      nextState = run.copyWith(
          index: (event.action == VerseAction.RETRACTED
                  ? run.index - 1
                  : event.action == VerseAction.FORGOTTEN && run.discrete
                      ? run.index
                      : run.index + 1) %
              run.verses.length,
          verses: [...run.verses],
          countForgotten: countForgotten,
          reverse: false,
          visible: false,
          revealLines: 0,
          hinting: run.hinting != null ? defaultHints : null,
          previous: run.copyWith(reverse: false));
    }
    _log.fine('FlashcardBloc._nextMultiFlashcardRun nextState: $nextState');
    updateNavigation(nextState.verse.id);
    return nextState;
  }

  void updateNavigation(int verseId) {}

  @override
  Future<void> close() {
    _cancelledSubscription.cancel();
    return super.close();
  }
}
