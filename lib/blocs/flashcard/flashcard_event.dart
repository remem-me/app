import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class FlashcardEvent extends Equatable {
  const FlashcardEvent();

  @override
  List<Object?> get props => [];
}

class FlashcardCancelled extends FlashcardEvent {}

class FlipStarted extends FlashcardEvent {}

class FlipEnded extends FlashcardEvent {}

class FlipReset extends FlashcardEvent {}

class FlashcardHinted extends FlashcardEvent {}

class FlashcardStudied extends FlashcardEvent {}

class LineRevealed extends FlashcardEvent {
  final int? lines;

  const LineRevealed({this.lines});
}

class FlashcardVerseUpdated extends FlashcardEvent {
  final Verse verse;

  const FlashcardVerseUpdated(this.verse);

  @override
  List<Object?> get props => [verse];

  @override
  String toString() => 'FlashcardVerseUpdated { verse: $verse }';
}

class FlashcardInitiated extends FlashcardEvent {
  final int id;

  const FlashcardInitiated(this.id);

  @override
  List<Object?> get props => [id];

  @override
  String toString() => 'SingleFlashcardInitiated { id: $id }';
}

class FlashcardLoaded extends FlashcardEvent {
  final List<Verse> verses;
  final VerseBox? box;
  final bool inverse;
  final bool reverse;
  final bool discrete;
  final int index;

  const FlashcardLoaded(
      {required this.verses,
      this.box = VerseBox.ALL,
      required this.inverse,
      this.reverse = false,
      this.discrete = false,
      this.index = 0});

  @override
  List<Object?> get props => [verses, box, inverse, reverse, discrete, index];

  @override
  String toString() => 'FlashcardLoaded { verses: ${verses.length}, box: $box, '
      'inverse: $inverse, reverse: $reverse, discrete: $discrete index: $index }';
}

class FlashcardAction extends FlashcardEvent {
  final VerseAction action;

  const FlashcardAction(this.action);

  @override
  List<Object?> get props => [action];
}
