import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class FlashcardState extends Equatable {
  const FlashcardState();

  @override
  List<Object?> get props => [];
}

class FlashcardLoadInProgress extends FlashcardState {
  const FlashcardLoadInProgress();
}

class FlashcardEnd extends FlashcardState {}

class FlashcardRun extends FlashcardState {
  final VerseBox? box;  // null if running on a Collection
  final List<Verse> verses;
  final int index;
  final bool inverse; // true -> passage on front, reference on back
  final bool flipping; // true -> flip animation is running
  final bool reverse; // true -> back is visible
  final bool visible; // false -> hide card (e.g. during app navigation)
  final bool discrete; // true -> refresh each forgotten card directly
  final Set<FlashcardHint>? hinting;
  final int revealLines; // 2 -> display 2 lines of the passage
  final int countRemembered;
  final int countForgotten;
  final FlashcardRun? previous;

  const FlashcardRun({
    this.box = VerseBox.ALL,
    required this.verses,
    this.index = 0,
    required this.inverse,
    this.reverse = false,
    this.flipping = false,
    this.visible = true,
    this.discrete = false,
    this.hinting,
    this.revealLines = 0,
    this.countRemembered = 0,
    this.countForgotten = 0,
    this.previous,
  });

  @override
  List<Object?> get props => [
        verses,
        box,
        index,
        inverse,
        reverse,
        flipping,
        visible,
        discrete,
        hinting,
        revealLines,
        countRemembered,
        countForgotten,
        previous,
      ];

  FlashcardRun copyWith({
    bool? reverse,
    bool? flipping,
    bool? visible,
    bool? discrete,
    Set<FlashcardHint>? hinting,
    int? revealLines,
    int? index,
    List<Verse>? verses,
    int? countRemembered,
    int? countForgotten,
    FlashcardRun? previous,
  }) {
    return FlashcardRun(
      verses: verses ?? this.verses,
      box: box,
      inverse: inverse,
      reverse: reverse ?? this.reverse,
      flipping: flipping ?? this.flipping,
      visible: visible ?? this.visible,
      discrete: discrete ?? this.discrete,
      hinting: hinting ?? this.hinting,
      revealLines: revealLines ?? this.revealLines,
      index: index ?? this.index,
      countRemembered: countRemembered ?? this.countRemembered,
      countForgotten: countForgotten ?? this.countForgotten,
      previous: previous ?? this.previous,
    );
  }

  Verse get verse {
    return verses[index];
  }

  @override
  String toString() {
    return 'FlashcardRun { verses: ${verses.length}, box: $box, '
        'inverse: $inverse, reverse: $reverse, flipping: $flipping, '
        'visible: $visible, discrete: $discrete, '
        'hinting: $hinting, revealLines: $revealLines, '
        'index: $index, countRemembered: $countRemembered, '
        'countForgotten: $countForgotten, previous: ${previous != null} }';
  }
}
