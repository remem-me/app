import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../../util/verse_util.dart';
import '../blocs.dart';

class OrderedVersesDueBloc extends OrderedVersesBloc {
  OrderedVersesDueBloc() : super(box: VerseBox.DUE);

  @override
  List<Verse> mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseUtil.mapVersesToBoxDue(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return I<CurrentAccount>().state!.orderDue;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return I<CurrentAccount>().state!.copyWith(orderDue: order);
  }
}
