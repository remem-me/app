import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../blocs.dart';

abstract class OrderedVersesBloc
    extends Bloc<OrderedVersesEvent, OrderedVersesState> {
  final VerseBox box;
  late FilteredVersesBloc filterBloc;
  late StreamSubscription versesSubscription;

  OrderedVersesBloc({required VerseBox box})
      : box = box,
        super(initialState(box)) {
    filterBloc = I<FilteredVersesBloc>();
    on<OrderedVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (OrderedVersesState result) => result),
        transformer: sequential()); // process events sequentially
    versesSubscription = filterBloc.stream.listen(_filteredBlocUpdated);
    _filteredBlocUpdated(filterBloc.state);
  }

  static OrderedVersesState initialState(VerseBox box) {
    switch (I<FilteredVersesBloc>().state.runtimeType) {
      case FilterInProgress:
        return OrderInProgress();
      default:
        return InitialOrderedVersesState();
    }
  }

  void _filteredBlocUpdated(FilteredVersesState? state) {
    if (state is FilteredVersesSuccess) {
      if (state.isUpdating) {
        add(FilteredVersesUpdated());
      } else {
        add(FilteredVersesLoaded(state.filteredVerses));
      }
    } else if (state is FilterInProgress) {
      add(OrderedVersesReloaded());
    } else if (state is FilterFailure) {
      add(FilterFailed());
    }
  }

  static OrderedVersesBloc from(TabState tabState) {
    final box = tabState.isBoxed ? tabState.box : VerseBox.ALL;
    switch (box) {
      case VerseBox.NEW:
        return I<OrderedVersesNewBloc>();
      case VerseBox.DUE:
        return I<OrderedVersesDueBloc>();
      case VerseBox.KNOWN:
        return I<OrderedVersesKnownBloc>();
      case VerseBox.ALL:
        return I<OrderedVersesAllBloc>();
    }
  }

  Stream<OrderedVersesState> mapEventToState(OrderedVersesEvent event) async* {
    if (event is OrderUpdated) {
      yield* mapOrderUpdatedToState(event);
    } else if (event is FilteredVersesLoaded) {
      yield* mapVersesLoadedToState(event);
    } else if (event is FilteredVersesUpdated) {
      yield* mapVersesUpdatedToState();
    } else if (event is OrderedVersesReloaded) {
      yield* _mapOrderedVersesReloadedToState();
    } else if (event is FilterFailed) {
      yield OrderFailure();
    }
  }

  Stream<OrderedVersesState> _mapOrderedVersesReloadedToState() async* {
    yield OrderInProgress();
  }

  Stream<OrderedVersesState> mapOrderUpdatedToState(
    OrderUpdated event,
  ) async* {
    if (filterBloc.state is FilteredVersesSuccess) {
      final verses = (filterBloc.state as FilteredVersesSuccess).filteredVerses;
      final Account updatedAccount = currentAccountWithOrder(event.order);
      I<CurrentAccount>().setAccount(updatedAccount);
      I<AccountsBloc>().add(EntitiesUpdated([updatedAccount]));
      yield OrderSuccess(event.order, mapVersesToBox(verses, event.order));
    }
  }

  Stream<OrderedVersesState> mapVersesLoadedToState(
    FilteredVersesLoaded event,
  ) async* {
    final state = this.state;
    final order = (state is OrderSuccess) ? state.order : currentOrder;
    yield OrderSuccess(order, mapVersesToBox(event.verses, order));
  }

  Stream<OrderedVersesState> mapVersesUpdatedToState() async* {
    final state = this.state;
    if (state is OrderSuccess) {
      yield state.copyWith(isUpdating: true, order: currentOrder);
    }
  }

  bool Function(OrderedVersesState) get completed {
    return (state) =>
        state is OrderSuccess && !state.isUpdating || state is OrderFailure;
  }

  Account currentAccountWithOrder(VerseOrder order);

  VerseOrder get currentOrder;

  List<Verse> mapVersesToBox(List<Verse>? verses, VerseOrder order);

  @override
  Future<void> close() {
    versesSubscription.cancel();
    return super.close();
  }
}
