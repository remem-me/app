import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../../util/util.dart';
import '../blocs.dart';

class OrderedVersesKnownBloc extends OrderedVersesBloc {

  OrderedVersesKnownBloc()
      : super(box: VerseBox.KNOWN);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseUtil.mapVersesToBoxKnown(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return I<CurrentAccount>().state!.orderKnown;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return I<CurrentAccount>().state!.copyWith(orderKnown: order);
  }

}
