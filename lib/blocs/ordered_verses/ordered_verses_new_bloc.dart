import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../../util/util.dart';
import '../blocs.dart';

class OrderedVersesNewBloc extends OrderedVersesBloc {

  OrderedVersesNewBloc()
      : super(box: VerseBox.NEW);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseUtil.mapVersesToBoxNew(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return I<CurrentAccount>().state!.orderNew;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return I<CurrentAccount>().state!.copyWith(orderNew: order);
  }

}
