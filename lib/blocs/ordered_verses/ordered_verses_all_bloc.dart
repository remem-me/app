import 'package:app_core/app_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

import '../../util/verse_util.dart';
import '../blocs.dart';

class OrderedVersesAllBloc extends OrderedVersesBloc {
  OrderedVersesAllBloc()
      : super(box: VerseBox.ALL);

  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseUtil.mapVersesToBoxAll(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return I<CurrentAccount>().state!.orderAll;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return I<CurrentAccount>().state!.copyWith(orderAll: order);
  }
}
