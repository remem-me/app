import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/services/score_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';


import '../blocs.dart';

class ScoresBloc extends EntitiesBloc<ScoreEntity, ScoreEntity> {
  static final LocalDate _initialDate = LocalDate(2010, 3, 24);
  static final _log = Logger('ScoresBloc');
  final _goalReachedController =
      BehaviorSubject<LocalDate>.seeded(_initialDate);
  late StreamSubscription _todaySubscription;

  ScoresBloc()
      : super(initialState: const InitialEntitiesState<ScoreEntity>()) {
    subscribeToAccountSelected();
    _todaySubscription = DateService().todayChangedStream.listen((today) async {
      final state = this.state;
      if (state is EntitiesLoadSuccess<ScoreEntity>) {
        _updateGoalReached(scores: state.items, date: today);
      }
    });
  }

  @override
  EntitiesRepository<ScoreEntity> get repository =>
      I<EntitiesRepository<ScoreEntity>>();

  @override
  Stream<EntitiesState<ScoreEntity>> mapEntitiesLoadedToState(
      EntitiesLoaded<ScoreEntity> event) {
    return super.mapEntitiesLoadedToState(event).doOnData(_doOnData());
  }

  @override
  Stream<EntitiesState<ScoreEntity>> mapEntitiesAddedToState(
      EntitiesAdded<ScoreEntity> event) {
    _log.fine('$runtimeType.mapEntitiesAddedToState for ${event.runtimeType}');
    /* prevent multiple scoring for the same verse */
    final newScores = event.entities
        .where((newScore) => !(state as EntitiesLoadSuccess<ScoreEntity>)
            .items
            .any((score) =>
                score.date == newScore.date && score.vkey == newScore.vkey))
        .toList();
    event = EntitiesAdded<ScoreEntity>(newScores, persist: event.persist);
    return super
        .mapEntitiesAddedToState(event)
        .doOnData(_doOnData(hasScored: true));
  }

  @override
  Stream<EntitiesState<ScoreEntity>> mapEntitiesUpdatedToState(
      EntitiesUpdated<ScoreEntity> event) {
    return super.mapEntitiesUpdatedToState(event).doOnData(_doOnData());
  }

  void Function(EntitiesState<ScoreEntity>) _doOnData(
          {bool hasScored = false}) =>
      (state) {
        if (state is EntitiesLoadSuccess<ScoreEntity>) {
          _updateGoalReached(
            scores: state.items,
            date: DateService().today,
            hasScored: hasScored,
          );
        }
      };

  Future<void> _updateGoalReached({
    required List<ScoreEntity> scores,
    required LocalDate date,
    bool hasScored = false,
  }) async {
    final hasGoalReached = await ScoreService().goalReached(scores, date);
    if (hasGoalReached && _goalReachedController.value != date) {
      if (hasScored) ScoreService().showGoalReachedMessage();
      _goalReachedController.add(date);
    } else if (!hasGoalReached && _goalReachedController.value == date) {
      _goalReachedController.add(_initialDate);
    }
  }

  Stream<LocalDate> get goalReachedStream => _goalReachedController.stream;

  @override
  Future<List<ScoreEntity>> toSorted(Iterable<ScoreEntity> items) async {
    final LocalDate Function(ScoreEntity element)? sortedBy = (it) => it.date;
    return compute(ListUtil.toSorted<ScoreEntity, LocalDate>,
        (items: items, sortedBy: sortedBy));
  }

  @override
  Future<List<ScoreEntity>> toModels(Iterable<ScoreEntity> entities) {
    return Future.value(entities.toList());
  }

  @override
  Future<void> close() {
    _todaySubscription.cancel();
    return super.close();
  }
}
