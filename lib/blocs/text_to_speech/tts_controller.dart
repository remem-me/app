import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/blocs/transcription/transcription.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

enum Speaking { started, paused, cancelled, completed, failed }

enum Stage { reference, topic, passage, breather, rest }

class TtsController extends Cubit<Speaking> {
  static final _log = Logger('TtsController');
  final _ttsKey = 'home';
  @visibleForTesting
  var progress = -1;
  @visibleForTesting
  Verse? verse;
  @visibleForTesting
  Timer? silence;
  Speaking _cancelState = Speaking.cancelled;

  TtsController() : super(Speaking.completed) {
    I<TtsService>().registerCompleteHandler(_ttsKey, () async {
      _proceed();
    });
    I<TtsService>().registerCancelHandler(_ttsKey, () async {
      emit(_cancelState);
    });
    I<TtsService>().registerErrorHandler(_ttsKey, () async {
      emit(Speaking.failed);
    });
  }

  @override
  void emit(Speaking state) {
    if ((I<Settings>().getBool(Settings.SPEECH_SCRIPT) ?? true) &&
        state != Speaking.started) {
      I<TranscriptionCubit>().clear(TranscriptionMode.script);
    }
    super.emit(state);
  }

  void _proceed() {
    _log.fine('proceed $progress of ${stages.length}');
    if (progress + 1 == stages.length) {
      emit(Speaking.completed);
    } else {
      progress++;
      speakCurrentStage(false);
    }
  }

  @visibleForTesting
  List<Stage> get stages {
    return [
      if (I<CurrentAccount>().topicPreferred) ...[
        Stage.topic,
        Stage.breather,
        Stage.reference,
      ] else ...[
        Stage.reference,
      ],
      Stage.breather,
      Stage.passage,
      if (I<CurrentAccount>().referenceIncluded) Stage.breather,
      if (I<CurrentAccount>().referenceIncluded) Stage.reference,
      Stage.rest,
    ];
  }

  void speakVerse(Verse v) {
    final isResumed = state == Speaking.paused && verse?.id == v.id;
    if (!isResumed) {
      verse = v;
      progress = 0;
    }
    speakCurrentStage(isResumed);
    emit(Speaking.started);
  }

  Future<void> stop() async {
    if (silence != null) {
      silence!.cancel();
      silence = null;
      _log.fine('Silence cancelled');
      emit(Speaking.cancelled);
    } else {
      _cancelState = Speaking.cancelled;
      await I<TtsService>().stop();
    }
  }

  Future<void> pause() async {
    if (silence != null) {
      silence!.cancel();
      silence = null;
      _log.fine('Silence cancelled');
      emit(Speaking.paused);
    } else {
      _cancelState = Speaking.paused;
      await I<TtsService>().stop();
    }
  }

  @visibleForTesting
  void speakCurrentStage(bool isResumed) {
    switch (stages[progress]) {
      case Stage.reference:
        return speakReference(isResumed);
      case Stage.passage:
        return speakPassage(isResumed);
      case Stage.topic:
        return speakTopic(isResumed);
      case Stage.breather:
        return speakShortSilence(isResumed);
      case Stage.rest:
        return speakCustomSilence(isResumed);
    }
  }

  @visibleForTesting
  void speakReference(bool isResumed) {
    _log.fine('Speak reference ${verse!.reference}');
    I<VerseService>()
        .spokenReference(verse!.reference)
        .then((reference) => I<TtsService>().speakText(
              _ttsKey,
              reference,
              I<CurrentAccount>().langRef,
              isResumed: isResumed,
            ));
  }

  @visibleForTesting
  void speakPassage(bool isResumed) {
    if (I<Settings>().getBool(Settings.SPEECH_SCRIPT) ?? true) {
      I<TranscriptionCubit>().flash(Transcription(
          TextService().stripMarkdown(verse!.passage),
          mode: TranscriptionMode.script));
    }
    I<TtsService>().speakText(
      _ttsKey,
      TextService().stripMarkdown(I<CurrentAccount>().referenceIncluded
          ? TextService().spokenVerseNumbers(verse!.passage)
          : TextService().stripVerseNumbers(verse!.passage)),
      I<CurrentAccount>().language,
      isResumed: isResumed,
    );
    _log.fine('Speak passage ${verse!.reference}');
  }

  @visibleForTesting
  void speakTopic(bool isResumed) {
    if (verse!.topic?.isEmpty ?? true) {
      _proceed();
    } else {
      I<TtsService>().speakText(
        _ttsKey,
        verse!.topic!,
        I<CurrentAccount>().langRef,
        isResumed: isResumed,
      );
      _log.fine('Speak top ${verse!.topic}');
    }
  }

  @visibleForTesting
  void speakShortSilence(bool isResumed) {
    speakSilence(isResumed, 1.0);
  }

  @visibleForTesting
  void speakCustomSilence(bool isResumed) {
    speakSilence(
        isResumed, I<Settings>().getDouble(Settings.SPEECH_PAUSE) ?? 1.0);
  }

  @visibleForTesting
  void speakSilence(bool isResumed, double seconds) {
    if (isResumed) {
      _proceed();
    } else {
      _log.fine('Silence of $seconds s started');
      silence = Timer(Duration(seconds: seconds.toInt()), () {
        silence = null;
        _log.fine('Silence completed');
        _proceed();
      });
    }
  }
}
