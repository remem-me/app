import 'dart:async';
import 'dart:io';

import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/text_to_speech/tts_controller.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/default_art.dart';
import 'package:remem_me/services/playback_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../blocs.dart';

class Pending {
  final Speaking trigger;
  final Future<void> Function() callback;

  Pending(this.trigger, this.callback);
}

class MediaProcessingState {
  final PlaybackState playbackState;
  final MediaItem? mediaItem;

  MediaProcessingState(this.playbackState, [this.mediaItem]);
}

class TtsHandler extends BaseAudioHandler with QueueHandler {
  static final _log = Logger('TtsHandler');
  static TtsHandler? _instance;
  late TtsController _controller;
  late Future<void> init;
  Pending? _pending;
  int _index = 0;
  List<Verse> _verses = [];
  VerseBox? _box;
  StreamSubscription<OrderedVersesState>? _boxSubscription;
  final Debouncer _debouncer = Debouncer(400);
  Timer? _playTimer;

  factory TtsHandler({TtsController? controller, DefaultArt? defaultArt}) {
    _instance ??= TtsHandler._internal(controller, defaultArt);
    return _instance!;
  }

  TtsHandler._internal(TtsController? controller, DefaultArt? defaultArt) {
    _controller = controller ?? TtsController();
    init = _init();
  }

  Future<void> _init() async {
    await I<PlaybackService>().init;
    _controller.stream.listen(_onSpeakingChanged);
    I<Settings>().listenable(keys: [
      Settings.SPEECH_TIMER,
    ]).addListener(_onTimerSettingChanged);
  }

  void _onSpeakingChanged(engineState) {
    if (_pending != null && _pending!.trigger == engineState) {
      _pending!.callback();
      _pending = null;
    } else if (engineState == Speaking.failed) {
      pause();
    }
  }

  _onTimerSettingChanged() {
    if(_controller.state == Speaking.started) {
      final duration = I<Settings>().getDouble(Settings.SPEECH_TIMER) ?? 0.0;
      _playTimer?.cancel();
      if (duration > 0.0) {
        _playTimer = _createTimer(duration);
      } else {
        _playTimer = null;
      }
    }
  }

  void setUp(List<Verse> verses, {int index = 0, VerseBox? box}) {
    _verses = verses;
    _index = index;
    _box = box;
    if (_box == null) queue.add(I<PlaybackService>().createMediaItems(_verses));
    _updateBoxSubscription();
  }

  void refresh() {
    assert(_box != null);
    var blocState = OrderedVersesBloc.from(TabState(true, _box!)).state;
    if (blocState is OrderSuccess) {
      final currentVerse = _verses[_index];
      _verses = blocState.verses;
      queue.add(I<PlaybackService>().createMediaItems(_verses));
      _index = _verses.indexWhere((verse) => verse.id == currentVerse.id);
      _updateBoxSubscription();
    }
  }

  void _updateBoxSubscription() {
    if (_boxSubscription != null) {
      _boxSubscription!.cancel();
      _boxSubscription = null;
    }
    if (_box != null) {
      OrderedVersesBloc bloc = OrderedVersesBloc.from(TabState(true, _box!));
      _boxSubscription = bloc.stream.listen((state) {
        if (state is OrderInProgress ||
            state is OrderSuccess &&
                state.verses.any((verse) => verse.id == _verses[_index].id)) {
          pause();
        } else {
          _boxSubscription!.cancel();
          _boxSubscription = null;
          stop();
        }
      });
    }
  }

  @override
  Future<void> play({bool single = false}) async {
    // Cancel the previous timer if it exists
    _playTimer?.cancel();

    final timeOut = Settings().getDouble(Settings.SPEECH_TIMER) ?? 0.0;
    if (timeOut > 0.0) {
      _playTimer = _createTimer(timeOut);
    }

    _play(single: single);
  }

  Timer _createTimer(double timeOut) {
    return Timer(Duration(seconds: (timeOut * 60.0).toInt()), () {
      MessageService().info(
          L10n.current.t8('Speech.timer.expired'));
      stop();
    });
  }

  Future<void> _play({required bool single}) async {
    // flutter_tts doesn't activate the session, so we do it here. This
    // allows the app to stop other apps from playing audio while we are
    // playing audio.
    if (await I<PlaybackService>().activateSession()) {
      // If we successfully activated the session, set the state to playing
      // and resume playback.
      if (_box != null) refresh();
      final playlist =
          single ? false : Settings().getBool(Settings.SPEECH_PLAYLIST) ?? true;
      final repeat = Settings().getBool(Settings.SPEECH_REPEAT) ?? false;
      final willPause = !playlist && !repeat;

      mediaItem.add(queue.value[_index]);
      playbackState.add(I<PlaybackService>()
          .played(playbackState.value, willPause, repeat, playlist));
      var next = _nextIndex(queue.value.length, _index, playlist, repeat);
      await _speak(_index, next, willPause, single);
    }
  }

  @override
  Future<void> stop() async {
    _playTimer?.cancel();
    playbackState.add(I<PlaybackService>().stopped(playbackState.value));
    await _controller.stop();
    setUp([]);
  }

  @override
  Future<void> pause() async {
    _playTimer?.cancel();
    if (!playbackState.value.playing) return; // RETURN
    mediaItem.add(queue.value[_index]);
    playbackState.add(I<PlaybackService>().paused(playbackState.value));
    _controller.pause();
  }

  @override
  Future<void> skipToNext() async {
    if (_box != null) refresh();
    await skipToQueueItem(_index + 1);
  }

  @override
  Future<void> skipToPrevious() async {
    if (_box != null) refresh();
    await skipToQueueItem(_index - 1);
  }

  @override
  Future<void> skipToQueueItem(int index) async {
    if (index >= 0 && index < queue.value.length) {
      _index = index;
      mediaItem.add(queue.value[_index]);
      debugPrint(
          'Index: $index, playbackState.value.playing: ${playbackState.value.playing}');
      if (playbackState.value.playing) {
        _pending = Pending(Speaking.cancelled, () async {
          _debouncer.run(play);
        });
        _controller.stop();
      }
    }
  }

  int get index {
    return _index;
  }

  Future<void> _speak(int index, int next, bool willPause, bool single) async {
    if (willPause) next = index;
    if (next >= 0) {
      _pending = Pending(Speaking.completed, () async {
        _index = next;
        willPause ? pause() : _play(single: single);
      });
    } else {
      _pending = Pending(Speaking.completed, stop);
    }
    if (!kIsWeb && Platform.isAndroid) {
      AudioService.androidForceEnableMediaButtons();
    }
    _controller.speakVerse(_verses[index]);
  }

  int _nextIndex(int length, int index, bool playlist, bool repeat) {
    if (!playlist && repeat) return index;
    if (repeat) return (index + 1) % length;
    if (index + 1 < length) return index + 1;
    return -1;
  }

  /// A stream reporting the combined state of the current playback state
  /// and the current media item.
  Stream<MediaProcessingState> get mediaProcessing =>
      Rx.combineLatest2<PlaybackState, MediaItem?, MediaProcessingState>(
          playbackState,
          mediaItem,
          (playbackState, mediaItem) =>
              MediaProcessingState(playbackState, mediaItem));
}
