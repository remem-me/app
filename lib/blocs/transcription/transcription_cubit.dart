import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../../models/models.dart';

class TranscriptionCubit extends Cubit<Transcription?> {
  final Settings _settings = I<Settings>();

  late final VoidCallback _listener;
  late final ValueListenable _settingsListenable;

  TranscriptionCubit() : super(null) {
    _listener = () {
      if (_settings.getBool(Settings.SPEECH_CAPTIONS) == false &&
          state?.mode == TranscriptionMode.caption) {
        clear(TranscriptionMode.caption);
      }
      if (_settings.getBool(Settings.SPEECH_SCRIPT) == false &&
          state?.mode == TranscriptionMode.script) {
        clear(TranscriptionMode.script);
      }
    };
    _settingsListenable = _settings
        .listenable(keys: [Settings.SPEECH_CAPTIONS, Settings.SPEECH_SCRIPT]);
    _settingsListenable.addListener(_listener);
  }

  void flash(Transcription transcription) {
    if (transcription.mode == TranscriptionMode.caption &&
            (_settings.getBool(Settings.SPEECH_CAPTIONS) ?? false) ||
        transcription.mode == TranscriptionMode.script &&
            (_settings.getBool(Settings.SPEECH_SCRIPT) ?? true)) {
      emit(transcription);
    }
  }

  void clear(TranscriptionMode? mode) {
    if (state != null && (mode == null || mode == state!.mode)) {
      emit(null);
    }
  }

  @override
  Future<void> close() {
    _settingsListenable.removeListener(_listener);
    return super.close();
  }
}
