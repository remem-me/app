import 'package:app_core/app_core.dart';
import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';


abstract class FilteredVersesState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitialFilterVersesState extends FilteredVersesState {

}
class FilterInProgress extends FilteredVersesState {}
class FilterFailure extends FilteredVersesState {}

class FilteredVersesSuccess extends FilteredVersesState {
  final List<Verse> filteredVerses;
  final LocalDate date;
  final QueryFilter queryFilter;
  final ReviewedTodayFilter reviewedTodayFilter;
  final bool isUpdating;

  FilteredVersesSuccess(this.filteredVerses, {
    required this.date,
    this.queryFilter = const QueryFilter(),
    this.reviewedTodayFilter = const ReviewedTodayFilter(),
    this.isUpdating = false,
  });

  @override
  List<Object?> get props => [filteredVerses, date, queryFilter, reviewedTodayFilter, isUpdating];
  // list comparison: https://github.com/felangel/equatable/issues/85 and
  // https://stackoverflow.com/questions/69404517/how-to-compare-two-lists-of-objects-by-property-value#answer-69404715

  FilteredVersesSuccess copyWith({
    List<Verse>? filteredVerses,
    LocalDate? date,
    QueryFilter? queryFilter,
    ReviewedTodayFilter? reviewedTodayFilter,
    bool? isUpdating,
  }) =>
      FilteredVersesSuccess(
        this.filteredVerses,
        date: date ?? this.date,
        queryFilter: queryFilter ?? this.queryFilter,
        reviewedTodayFilter: reviewedTodayFilter ?? this.reviewedTodayFilter,
        isUpdating: isUpdating ?? this.isUpdating,
      );

  @override
  String toString() {
    return 'FilteredVersesLoadSuccess { filteredVerses: '
        '${filteredVerses.length}, date: $date, queryFilter: $queryFilter, '
        'reviewedTodayFilter: $reviewedTodayFilter, isUpdating: $isUpdating }';
  }
}
