import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/models/models.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/home_widget_service.dart';
import '../blocs.dart';

class FilteredVersesBloc
    extends Bloc<FilteredVersesEvent, FilteredVersesState> {
  static final _log = Logger('FilteredVersesBloc');
  late TaggedVersesBloc _versesBloc;
  late StreamSubscription _versesSubscription;
  late StreamSubscription _todaySubscription;
  late StreamSubscription _homeWidgetSubscription;

  FilteredVersesBloc() : super(_initialState()) {
    _versesBloc = I<TaggedVersesBloc>();
    on<FilteredVersesEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (FilteredVersesState result) => result),
        transformer: sequential()); // process events sequentially
    _versesSubscription = _versesBloc.stream
        .startWith(_versesBloc.state)
        .listen(_versesBlocUpdated);
    _todaySubscription = DateService().todayChangedStream.listen((today) async {
      add(DateChanged(today));
    });
    if (!kIsWeb) {
      _homeWidgetSubscription = stream
          .distinct((prev, next) =>
              kIsWeb ||
              next is! FilteredVersesSuccess ||
              prev is FilteredVersesSuccess &&
                  listEquals(prev.filteredVerses, next.filteredVerses) &&
                  prev.date == next.date)
          .debounceTime(const Duration(seconds: 2))
          .listen((state) {
        if (state is FilteredVersesSuccess) {
          I<HomeWidgetService>().update(state.filteredVerses);
        }
      });
    }
  }

  static FilteredVersesState _initialState() {
    final versesBloc = I<TaggedVersesBloc>();
    switch (versesBloc.state.runtimeType) {
      case TaggedVersesSuccess:
        return FilteredVersesSuccess(
          (versesBloc.state as TaggedVersesSuccess).verses,
          date: DateService().today,
        );
      case TaggedVersesInProgress:
        return FilterInProgress();
      case InitialTaggedVersesState:
      default:
        return InitialFilterVersesState();
    }
  }

  void _versesBlocUpdated(TaggedVersesState? state) {
    if (state is TaggedVersesSuccess) {
      if (state.isUpdating) {
        add(TaggedVersesUpdated());
      } else {
        add(TaggedVersesLoaded(state.verses));
      }
    } else if (state is TaggedVersesInProgress) {
      add(FilteredVersesReloaded());
    } else if (state is TaggedVersesFailure) {
      add(TaggingFailed());
    }
  }

  Stream<FilteredVersesState> mapEventToState(
      FilteredVersesEvent event) async* {
    if (event is SearchFilterUpdated) {
      yield* _mapSearchFilterUpdatedToState(event);
    } else if (event is ReviewedTodayFilterUpdated) {
      yield* _mapReviewedTodayFilterUpdatedToState(event);
    } else if (event is TaggedVersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TaggedVersesUpdated) {
      yield* _mapVersesUpdatedToState();
    } else if (event is FilteredVersesReloaded) {
      yield* _mapFilteredVersesReloadedToState();
    } else if (event is DateChanged) {
      yield* _mapDateChangedToState(event);
    } else if (event is TaggingFailed) {
      yield FilterFailure();
    }
  }

  Stream<FilteredVersesState> _mapFilteredVersesReloadedToState() async* {
    final state = this.state;
    yield state is FilteredVersesSuccess
        ? state.copyWith(isUpdating: true)
        : FilterInProgress();
  }

  Stream<FilteredVersesState> _mapSearchFilterUpdatedToState(
    SearchFilterUpdated event,
  ) async* {
    final state = this.state;
    final tagged = _versesBloc.state;
    if (state is FilteredVersesSuccess && tagged is TaggedVersesSuccess) {
      yield await compute(_filteredVersesLoadSuccess, (
        tagged: tagged,
        queryFilter: QueryFilter(event.query),
        reviewedTodayFilter: state.reviewedTodayFilter
      ));
    }
  }

  Stream<FilteredVersesState> _mapReviewedTodayFilterUpdatedToState(
    ReviewedTodayFilterUpdated event,
  ) async* {
    final state = this.state;
    final tagged = _versesBloc.state;
    if (state is FilteredVersesSuccess && tagged is TaggedVersesSuccess) {
      yield await compute(_filteredVersesLoadSuccess, (
        tagged: tagged,
        queryFilter: state.queryFilter,
        reviewedTodayFilter: ReviewedTodayFilter(event.value)
      ));
    }
  }

  Stream<FilteredVersesState> _mapVersesLoadedToState(
      TaggedVersesLoaded event) async* {
    final state = this.state;
    final tagged = _versesBloc.state;
    if (tagged is TaggedVersesSuccess) {
      _log.fine('$runtimeType._mapVersesLoadedToState started');
      final resultState = await compute(_filteredVersesLoadSuccess, (
        tagged: tagged,
        queryFilter: state is FilteredVersesSuccess
            ? state.queryFilter
            : const QueryFilter(),
        reviewedTodayFilter: state is FilteredVersesSuccess
            ? state.reviewedTodayFilter
            : const ReviewedTodayFilter()
      ));
      _log.fine('$runtimeType._mapVersesLoadedToState ended');
      yield resultState;
    }
  }

  Stream<FilteredVersesState> _mapVersesUpdatedToState() async* {
    final state = this.state;
    if (state is FilteredVersesSuccess) {
      yield state.copyWith(isUpdating: true);
    }
  }

  Stream<FilteredVersesState> _mapDateChangedToState(DateChanged event) async* {
    final state = this.state;
    final tagged = _versesBloc.state;
    if (state is FilteredVersesSuccess && tagged is TaggedVersesSuccess) {
      yield await compute(_filteredVersesLoadSuccess, (
      tagged: tagged,
      queryFilter: state.queryFilter,
      reviewedTodayFilter: ReviewedTodayFilter(null)
      ));
    }
  }

  static FilteredVersesSuccess _filteredVersesLoadSuccess(
      ({
        TaggedVersesSuccess tagged,
        QueryFilter queryFilter,
        ReviewedTodayFilter reviewedTodayFilter
      }) args) {
    // remove reviewedTodayFilter if there are no verses reviewed today
    final reviewedTodayFilter =
        args.tagged.verses.any((verse) => verse.review == DateService().today)
            ? args.reviewedTodayFilter
            : ReviewedTodayFilter(null);
    var verses = _applyQuery(args.tagged.verses, args.queryFilter);
    if (args.queryFilter.query.isEmpty) {
      verses = _applyTags(verses, args.tagged.tags, reviewedTodayFilter);
    }
    return FilteredVersesSuccess(
      verses,
      date: DateService().today,
      queryFilter: args.queryFilter,
      reviewedTodayFilter: reviewedTodayFilter,
    );
  }

  static List<Verse> _applyQuery(List<Verse> verses, QueryFilter? filter) {
    return verses.where((verse) {
      return filter == null || filter.appliesTo(verse);
    }).toList();
  }

  static List<Verse> _applyTags(
      List<Verse> verses, List<Tag> tags, ReviewedTodayFilter filter) {
    final isAnySelected = _isAnySelected(tags, filter.reviewedToday);
    final Map<int, Tag> tagsMap = {for (var tag in tags) tag.id: tag};
    return verses
        .where((verse) =>
            (!isAnySelected || _isIncluded(verse, tagsMap, filter)) &&
            !_isExcluded(verse, tagsMap, filter))
        .toList();
  }

  static bool _isAnySelected(List<Tag> tags, bool? reviewedToday) {
    return reviewedToday != null && reviewedToday ||
        tags.any((tag) => tag.included == true);
  }

  static bool _isIncluded(
      Verse verse, Map<int, Tag> tagsMap, ReviewedTodayFilter filter) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == true) ||
        filter.isIncluded(verse);
  }

  static bool _isExcluded(
      Verse verse, Map<int, Tag> tagsMap, ReviewedTodayFilter filter) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == false) ||
        filter.isExcluded(verse);
  }

  @override
  Future<void> close() {
    _versesSubscription.cancel();
    _todaySubscription.cancel();
    _homeWidgetSubscription.cancel();
    return super.close();
  }
}
