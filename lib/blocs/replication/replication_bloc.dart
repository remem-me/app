
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:repository/repository.dart';

part 'replication_event.dart';
part 'replication_state.dart';

class ReplicationBloc extends Bloc<ReplicationEvent, ReplicationState> {

  ReplicationBloc() : super(ReplicationInitial()) {
    on<ReplicationEvent>(
            (event, emit) => emit.forEach(mapEventToState(event),
            onData: (ReplicationState result) => result),
        transformer: sequential());
  }

  Stream<ReplicationState> mapEventToState(ReplicationEvent event) async* {
    if (event is RplQueued) {
      yield* mapQueuedToState(event);
    } else if (event is RplPolled) {
      yield* mapPolledToState(event);
    } else if (event is RplStarted) {
      yield* mapStartedToState(event);
    } else if (event is RplLoggedOut) {
      yield* mapLoggedOutToState();
    }
  }

  Stream<ReplicationState> mapQueuedToState(RplQueued event) async* {
    try {
      yield ReplicationInProgress();
      await ReplicationService().send();
      yield ReplicationSuccess();
    } on MessageException catch (e) {
      debugPrint(e.messages['messages']!.join('/n'));
      yield ReplicationDirty();
    }
  }

  Stream<ReplicationState> mapPolledToState(RplPolled event) async* {
    try {
      yield ReplicationInProgress();
      await ReplicationService().receive(all: event.all);
      yield ReplicationSuccess();
    } on MessageException catch (e) {
      debugPrint(e.messages['messages']!.join('/n'));
      yield ReplicationFailure(e.messages);
    }
  }

  Stream<ReplicationState> mapStartedToState(RplStarted event) async* {
      if(I<Settings>().hasNewDbVersion) {
        yield* mapLoggedOutToState();
      }
    try {
      yield ReplicationInProgress();
      await ReplicationService().replicate(all: event.all);
      yield ReplicationSuccess();
    } on MessageException catch (e) {
      debugPrint(e.messages['messages']!.join('/n'));
      yield ReplicationFailure(e.messages);
    }
  }

  Stream<ReplicationState> mapLoggedOutToState() async* {
    try {
      yield ReplicationInProgress();
      await ReplicationService().send();
      await ReplicationService().clearLocalStorage();
      yield ReplicationSuccess();
    } on MessageException catch (e) {
      debugPrint(e.messages['messages']!.join('/n'));
      yield ReplicationFailure(e.messages);
    } finally {
      I<AuthBloc>().add(Revoked());
    }
  }
}
