part of 'replication_bloc.dart';

abstract class ReplicationEvent extends Equatable {
  const ReplicationEvent();

  @override
  List<Object?> get props => [];
}

class RplQueued extends ReplicationEvent {
  const RplQueued();
}

class RplStarted extends ReplicationEvent {
  final bool all;

  const RplStarted({this.all = false});

  @override
  List<Object?> get props => [all];

  @override
  String toString() => 'RplStarted { all: $all }';
}

class RplPolled extends ReplicationEvent {
  final bool all;

  const RplPolled({this.all = false});

  @override
  List<Object?> get props => [all];

  @override
  String toString() => 'RplPolled { all: $all }';}

class RplLoggedOut extends ReplicationEvent {
  const RplLoggedOut();
}
