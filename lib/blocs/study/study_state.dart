import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';

abstract class StudyState extends Equatable {
  @override
  List<Object?> get props => [];
}

class NoStudy extends StudyState {}

class Study extends StudyState {
  final Verse verse;
  final List<List<String>> lines;
  final Pos pos;
  final bool isComplete;
  final int countWrong;
  final int countRight;

  Study(this.verse, this.lines,
      {Pos? pos, bool? isComplete, this.countWrong = 0, this.countRight = 0})
      : isComplete = isComplete ?? false,
        pos = pos ?? TextService().lastPos(lines);

  @override
  List<Object?> get props => [lines, pos, isComplete, countWrong, countRight];

  Study copyWith() {
    return Study(
      verse,
      lines,
      pos: pos,
      isComplete: isComplete,
    );
  }

  get posForCount => pos;

  int get countToGo {
    int count = 0;
    for (int i = posForCount.line; i < lines.length; i++) {
      for (int j = (i == posForCount.line ? posForCount.word + 1 : 0);
          j < lines[i].length;
          j++) {
        count++;
      }
    }
    return count;
  }

  @override
  String toString() => 'StudyState { verse: $verse, lines: [${lines.length}], '
      'isComplete: $isComplete, pos: $pos,  countWrong: $countWrong, countRight: $countRight }';
}

class Obfuscation extends Study {
  final List<List<bool>> obfuscated;
  final List<List<bool>> revealed;

  Obfuscation(super.verse, super.lines,
      {required this.obfuscated, required this.revealed, super.isComplete});

  @override
  List<Object?> get props => super.props + [obfuscated, revealed, isComplete];

  @override
  Obfuscation copyWith({
    bool? isComplete,
    List<List<bool>>? obfuscated,
    List<List<bool>>? revealed,
  }) {
    return Obfuscation(
      verse,
      lines,
      isComplete: isComplete ?? this.isComplete,
      obfuscated: obfuscated ?? this.obfuscated,
      revealed: revealed ?? this.revealed,
    );
  }

  @override
  String toString() => 'Obfuscation { lines: [${lines.length}], pos: $pos, '
      'obfuscated: [${obfuscated.length}], revealed: [${revealed.length}], isComplete: $isComplete }';
}

class Puzzle extends Study {
  final Choice? choice;

  Puzzle(
    super.verse,
    super.lines, {
    super.pos,
    super.isComplete,
    super.countWrong,
    this.choice,
  });

  @override
  List<Object?> get props => super.props + [choice];

  @override
  Puzzle copyWith({
    Pos? pos,
    bool? isComplete,
    int? countWrong,
    Choice? choice,
  }) {
    return Puzzle(
      verse,
      lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      countWrong: countWrong ?? this.countWrong,
      choice: choice ?? this.choice,
    );
  }

  @override
  int get countRight {
    int count = 0;
    for (int i = 0; i <= posForCount.line; i++) {
      for (int j = 0;
          j < (i == posForCount.line ? posForCount.word + 1 : lines[i].length);
          j++) {
        count++;
      }
    }
    return count;
  }

  @override
  String toString() =>
      'Puzzle { lines: [${lines.length}], pos: $pos, choice: $choice, '
      'isComplete: $isComplete, countWrong: $countWrong, countToGo: $countToGo, '
      'countRight: $countRight }';
}

class LineUp extends Study {
  final Pos revealed;
  final Pos? initials;
  final Pos? placeholders;

  LineUp(
    super.verse,
    super.lines, {
    super.pos,
    super.isComplete,
    super.countWrong,
    super.countRight,
    Pos? revealed,
    this.initials,
    this.placeholders,
  }) : revealed = revealed ?? const Pos(0, -1);

  @override
  List<Object?> get props => super.props + [revealed, initials, placeholders];

  @override
  LineUp copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    Pos? initials,
    Pos? placeholders,
    List<String> nullValues = const [],
  }) {
    return LineUp(
      verse,
      lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      revealed: revealed ?? this.revealed,
      initials:
          nullValues.contains('initials') ? null : (initials ?? this.initials),
      placeholders: nullValues.contains('placeholders')
          ? null
          : (placeholders ?? this.placeholders),
    );
  }

  @override
  String toString() => 'LineUp { lines: [${lines.length}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'initials: $initials, placeholders: $placeholders }';
}

class Typing extends LineUp {
  final String? letters;
  final String? nextWord;
  final bool isCorrect;

  Typing(
    super.verse,
    super.lines, {
    super.pos,
    super.isComplete,
    super.countWrong,
    super.countRight,
    super.revealed,
    super.initials,
    super.placeholders,
    this.letters,
    this.nextWord,
    this.isCorrect = false,
  });

  @override
  List<Object?> get props =>
      super.props + [letters, nextWord, isCorrect, countRight, countWrong];

  @override
  Typing copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    Pos? initials,
    Pos? placeholders,
    String? letters,
    String? nextWord,
    bool? isCorrect,
    int? countRight,
    int? countWrong,
    List<String> nullValues = const [],
  }) {
    return Typing(
      verse,
      lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      letters:
          nullValues.contains('letters') ? null : (letters ?? this.letters),
      nextWord: nextWord ?? this.nextWord,
      isCorrect: isCorrect ?? this.isCorrect,
      revealed: revealed ?? this.revealed,
      initials:
          nullValues.contains('initials') ? null : (initials ?? this.initials),
      placeholders: nullValues.contains('placeholders')
          ? null
          : (placeholders ?? this.placeholders),
      countRight: countRight ?? this.countRight,
      countWrong: countWrong ?? this.countWrong,
    );
  }

  @override
  get posForCount => revealed;

  @override
  String toString() => 'Typing { lines: [${lines.length}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'initials: $initials, placeholders: $placeholders, '
      'letters: $letters, nextWord: $nextWord, isCorrect: $isCorrect, '
      'countWrong: $countWrong, countRight: $countRight }';
}
