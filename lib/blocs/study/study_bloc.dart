import 'dart:async';
import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../services/study_service.dart';

class Hints {
  bool placeholders;
  bool initials;

  Hints({required this.placeholders, required this.initials});
}

class Pending {
  final String text;
  final Locale language;

  Pending(this.text, this.language);
}

class StudyBloc extends Bloc<StudyEvent, StudyState> {
  static final _log = Logger('StudyBloc');
  final FlashcardBloc flashcardBloc;
  final _feedbackController = StreamController<bool>();
  final _service = StudyService();
  final _hints = <String, Hints>{};
  final _ttsKey = 'study';
  Pending? _ttsPending;
  String? _ttsSpeaking;
  bool _ttsIsStopping = false;
  late TtsService _ttsService;
  late CurrentAccount _account;

  bool _isReference(List<List<String>> lines, Pos pos) {
    return _account.referenceIncluded && pos.line == lines.length - 1;
  }

  StudyBloc({required this.flashcardBloc}) : super(NoStudy()) {
    _account = I<CurrentAccount>();
    _ttsService = I<TtsService>();
    on<StudyEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (StudyState result) => result),
        transformer: sequential()); // process events sequentially

    _ttsService.registerCancelHandler(_ttsKey, () => _ttsDoneHandler());
    _ttsService.registerCompleteHandler(_ttsKey, () => _ttsDoneHandler());
    _ttsService.registerErrorHandler(_ttsKey, () => _ttsDoneHandler());
    flashcardBloc.stream
        .startWith(flashcardBloc.state)
        .firstWhere((state) => state is FlashcardRun)
        .then((state) => add(StudyStarted((state as FlashcardRun).verse)));
  }

  void _ttsDoneHandler() async {
    _ttsIsStopping = false;
    if (_ttsPending != null) {
      _ttsSpeaking = _ttsPending!.text;
      final language = _ttsPending!.language;
      _log.fine('Cancelled. StudBloc._ttsSpeaking: ${_ttsSpeaking!}');
      _ttsPending = null;
      await _ttsService.speakText(_ttsKey, _ttsSpeaking!, language);
    } else {
      _ttsSpeaking = null;
    }
  }

  Stream<StudyState> mapEventToState(StudyEvent event) async* {
    if (event is StudyStarted) {
      yield* mapStudyStartedToState(event);
    } else if (event is Obfuscated) {
      yield* mapObfuscatedToState(event);
    } else if (event is Revealed) {
      yield* mapRevealedToState(event);
    } else if (event is Puzzled) {
      yield* mapPuzzledToState(event);
    } else if (event is LinedUp) {
      yield* mapLinedUpToState(event);
    } else if (event is Typed) {
      yield* mapTypedToState(event);
    } else if (event is Retracted) {
      yield* mapRetractedToState(event);
    } else if (event is StudyHinted) {
      yield* mapHintedToState(event);
    }
  }

  Stream<StudyState> mapStudyStartedToState(StudyStarted event) async* {
    if (event.verse != null) {
      _resetHints();
      final lines = VerseService().words(event.verse!);
      yield Study(event.verse!, lines, pos: TextService().lastPos(lines));
    } else {
      final study = state as Study;
      if (study is LineUp) {
        if (study is Typing) {
          _hints['typing']!.initials = study.initials != null;
          _hints['typing']!.placeholders = study.placeholders != null;
        } else {
          _hints['line_up']!.initials = study.initials != null;
          _hints['line_up']!.placeholders = study.placeholders != null;
        }
      }
      yield Study(study.verse, study.lines);
    }
  }

  void _resetHints() {
    final initialDefault =
        I<Settings>().getBool(Settings.STUDY_INITIALS_DEFAULT) ?? true;
    final placeholderDefault =
        I<Settings>().getBool(Settings.STUDY_PLACEHOLDERS_DEFAULT) ?? true;
    _hints['typing'] =
        Hints(placeholders: placeholderDefault, initials: initialDefault);
    _hints['line_up'] = Hints(
        placeholders: placeholderDefault,
        initials: initialDefault &&
            !['zh', 'ja'].contains(_account.language.languageCode));
  }

  Stream<StudyState> mapObfuscatedToState(Obfuscated event) async* {
    final study = state as Study;
    if (study is Obfuscation) {
      if (TextService().countMarks(study.revealed) > 0) {
        yield study.copyWith(
            revealed: TextService().wordsToMarks(study.lines),
            isComplete: TextService().countWordsOfRows(study.lines) ==
                TextService().countMarks(study.obfuscated));
      } else {
        final obfuscated = _service.obfuscate(study.lines, study.obfuscated);
        yield study.copyWith(
            obfuscated: _service.obfuscate(study.lines, study.obfuscated),
            isComplete: TextService().countWordsOfRows(study.lines) ==
                TextService().countMarks(obfuscated));
      }
    } else {
      yield Obfuscation(study.verse, study.lines,
          obfuscated: _service.obfuscate(
              study.lines, TextService().wordsToMarks(study.lines)),
          revealed: TextService().wordsToMarks(study.lines));
    }
  }

  Stream<StudyState> mapRevealedToState(Revealed event) async* {
    final study = state as Study;
    _speak(
      TextService().wordsToText(study.lines, event.pos, false),
      isReference: _isReference(study.lines, event.pos),
    );
    if (study is Obfuscation) {
      final revealed = _service.reveal(study.revealed, event.pos);
      yield study.copyWith(revealed: revealed, isComplete: false);
    }
  }

  Stream<StudyState> mapPuzzledToState(Puzzled event) async* {
    final study = state as Study;
    final puzzleSize = I<Settings>().getInt(Settings.STUDY_PUZZLE_SIZE) ?? 5;
    if (study is Puzzle) {
      if (event.isCorrect!) {
        _feedbackController.add(true);
        try {
          final pos = _service.advanceByWord(study.lines, study.pos);
          _speak(TextService().wordsToText(study.lines, pos, false),
              isReference: _isReference(study.lines, pos));
          final nextPos = _service.advanceByWord(study.lines, pos);
          final choice = _service.puzzle(study.lines, nextPos, puzzleSize);
          yield study.copyWith(pos: pos, choice: choice);
        } on OutOfWordsException {
          yield Study(study.verse, study.lines,
              countWrong: study.countWrong, countRight: study.countRight);
        }
      } else {
        _feedbackController.add(false);
        yield study.copyWith(countWrong: study.countWrong + 1);
      }
    } else {
      yield Puzzle(study.verse, study.lines,
          pos: const Pos(0, -1),
          choice: _service.puzzle(study.lines, const Pos(0, 0), puzzleSize));
    }
  }

  Stream<StudyState> mapLinedUpToState(LinedUp event) async* {
    final study = state as Study;
    if (study is LineUp) {
      try {
        final account = I<CurrentAccount>();
        var revealed = _service.advanceByWord(study.lines, study.revealed);
        _speak(TextService().wordsToText(study.lines, revealed, event.byLine!),
            isReference: account.referenceIncluded &&
                revealed.line == study.lines.length - 1);
        if (event.byLine!) {
          revealed = _service.advanceByLine(study.lines, study.revealed);
        }
        var pos = _service.lookAhead(study.lines, revealed);
        yield study.copyWith(
            revealed: revealed,
            pos: pos,
            initials: pos,
            placeholders: pos,
            nullValues: [
              if (study.initials == null) 'initials',
              if (study.placeholders == null) 'placeholders',
            ]);
      } on OutOfWordsException {
        yield study.copyWith(revealed: study.pos, isComplete: true);
      }
    } else {
      const revealed = Pos(0, -1);
      final pos = Pos(0, study.lines[0].length - 1);
      yield LineUp(
        study.verse,
        study.lines,
        revealed: revealed,
        pos: pos,
        placeholders: _hints['line_up']!.placeholders ? pos : null,
        initials: _hints['line_up']!.initials ? pos : null,
      );
    }
  }

  Stream<StudyState> mapRetractedToState(Retracted event) async* {
    final study = state as Study;
    if (study is LineUp) {
      final revealed = _service.retractLine(study.lines, study.revealed);
      if (study is Typing) {
        var rendered = _service.lookAhead(study.lines, revealed);
        final next = _service.advanceByWord(study.lines, revealed);
        yield study.copyWith(
          pos: rendered,
          revealed: revealed,
          nextWord: study.lines[next.line][next.word],
          isComplete: false,
          nullValues: [
            'isCorrect',
            'letters',
            if (['zh', 'ja'].contains(_account.language.languageCode))
              'initials'
          ],
        );
      } else {
        var pos = _service.lookAhead(study.lines, revealed);
        yield study.copyWith(revealed: revealed, pos: pos, isComplete: false);
      }
    }
  }

  Stream<StudyState> mapHintedToState(StudyHinted event) async* {
    final study = state as Study;
    if (study is LineUp) {
      yield study.copyWith(
          initials: study is Typing
              ? _service.advanceByWord(study.lines, study.revealed)
              : study.pos,
          placeholders: study.pos,
          nullValues: [
            if (!event.hasInitials) 'initials',
            if (!event.hasPlaceholders) 'placeholders'
          ]);
    }
  }

  Stream<StudyState> mapTypedToState(Typed event) async* {
    final study = state as Study;
    if (study is Typing) {
      if (TextService().startsWith(study.nextWord!, event.letters!)) {
        _feedbackController.add(true);
        try {
          final revealed = _service.advanceByWord(study.lines, study.revealed);
          final next = _service.advanceByWord(study.lines, revealed);
          final rendered = _service.lookAhead(study.lines, revealed);
          _speak(study.nextWord!,
              isReference: _isReference(study.lines, revealed));
          yield study.copyWith(
            isCorrect: true,
            pos: rendered,
            revealed: revealed,
            letters: event.letters,
            nextWord: study.lines[next.line][next.word],
            placeholders: study.placeholders != null ? rendered : null,
            countRight: study.countRight + 1,
            nullValues: ['initials'],
          );
        } on OutOfWordsException {
          _speak(study.nextWord!, isReference: _account.referenceIncluded);
          yield study.copyWith(
            revealed: study.pos,
            isComplete: true,
            placeholders: study.placeholders != null ? study.pos : null,
            countRight: study.countRight + 1,
            nullValues: ['initials'],
          );
        }
      } else {
        _feedbackController.add(false);
        yield study.copyWith(
            letters: event.letters,
            isCorrect: false,
            countWrong: study.countWrong + 1);
      }
    } else {
      const revealed = Pos(0, -1);
      const next = Pos(0, 0);
      final rendered = _service.lookAhead(study.lines, revealed);
      yield Typing(
        study.verse,
        study.lines,
        revealed: revealed,
        pos: rendered,
        nextWord: study.lines[next.line][next.word],
        initials: null,
        placeholders: _hints['typing']!.placeholders ? rendered : null,
      );
    }
  }

  _speak(String text, {bool isReference = false}) async {
    final isSpeechOn = I<Settings>().getBool(Settings.STUDY_SPEECH_ON) ?? true;
    if (isSpeechOn) {
      final language = isReference ? _account.langRef : _account.language;
      final spoken =
          isReference ? await VerseService().spokenReference(text) : text;
      if (_ttsSpeaking == null) {
        _ttsSpeaking = text;
        await _ttsService.speakText(_ttsKey, spoken, language);
      } else {
        _ttsPending = Pending(spoken, language);
        if (_ttsIsStopping) {
          _ttsIsStopping = true;
          await _ttsService.stop();
        }
      }
    }
  }

  Stream<bool> get feedback {
    return _feedbackController.stream;
  }

  @override
  Future<void> close() {
    _feedbackController.close();
    return super.close();
  }
}
