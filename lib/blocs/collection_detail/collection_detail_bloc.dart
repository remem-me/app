import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import '../../paths/paths.dart';
import '../../util/verse_util.dart';

class CollectionDetailBloc
    extends Bloc<CollectionDetailEvent, CollectionDetailState> {
  late CollectionService service;
  late TagsBloc tagsBloc;
  late VersesBloc versesBloc;
  late AccountsBloc accountsBloc;
  late NavigationCubit nav;
  late CurrentAccount currentAccount;
  late StreamSubscription _navSubscription;

  CollectionDetailBloc() : super(CollectionDetailInitialState()) {
    nav = I<NavigationCubit>();
    currentAccount = I<CurrentAccount>();
    service = I<CollectionService>();
    tagsBloc = I<TagsBloc>();
    versesBloc = I<VersesBloc>();
    accountsBloc = I<AccountsBloc>();
    _navSubscription =
        nav.stream.startWith(nav.state).distinct((prev, next) {
      return prev is! CollectionDetailPath && next is! CollectionDetailPath ||
          prev == next;
    }).listen((path) {
      final state = this.state;
      if (path is! CollectionDetailPath) {
        add(CollectionDetailDismissed());
      } else if (state is! CollectionDetailLoadSuccess ||
          state.collection.id != path.id) {
        add(CollectionDetailLoaded(path.id));
      }
    });
    on<CollectionDetailEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (CollectionDetailState result) => result),
        transformer: sequential()); // process events sequentially
  }

  Stream<CollectionDetailState> mapEventToState(
      CollectionDetailEvent event) async* {
    if (event is CollectionDetailLoaded) {
      yield* mapCollectionDetailLoadedToState(event);
    } else if (event is CollectionDetailDismissed) {
      yield* mapCollectionDetailDismissedToState();
    } else if (event is CollectionImported) {
      yield* mapCollectionImportedToState(event);
    }
  }

  Stream<CollectionDetailState> mapCollectionDetailLoadedToState(
      CollectionDetailLoaded event) async* {
    try {
      debugPrint('mapCollectionDetailLoadedToState');
      final collection = await service.fetchCollection(event.id);
      debugPrint('collection: ${collection.id}');
      await currentAccount.setPublisher(collection.publisher);
      final rankedVerses = collection.verses
          .map((verse) => verse.copyWith(
              rank: VerseUtil.rank(verse.reference, currentAccount.books)))
          .toList();
      rankedVerses.sort(VerseUtil.compare(_detectedSortOrder(rankedVerses)));
      debugPrint('rankedVerses: ${rankedVerses.length}');
      yield CollectionDetailLoadSuccess(
          collection.copyWith(verses: rankedVerses));
    } on FetchException {
      I<NavigationCubit>().go(CollectionsPath());
      yield CollectionDetailLoadInProgress(event.id);
    }
  }

  VerseOrder _detectedSortOrder(List<Verse> verses) {
    final pattern = RegExp(r'\d+');
    return verses.every(
            (verse) => verse.topic != null && pattern.hasMatch(verse.topic!))
        ? VerseOrder.TOPIC
        : VerseOrder.CANON;
  }

  Stream<CollectionDetailState> mapCollectionDetailDismissedToState() async* {
    await currentAccount.setPublisher(null);
    yield CollectionDetailInitialState();
  }

  Stream<CollectionDetailState> mapCollectionImportedToState(
      CollectionImported event) async* {
    final state = this.state;
    if (state is CollectionDetailLoadSuccess) {
      yield state.copyWith(isUpdating: true);
      final account = currentAccount.state!;
      final tagsState = tagsBloc.state;
      TagEntity? tag;
      bool isReimport =
          account.importedDecks.any((id) => id == state.collection.id);
      if (isReimport && tagsState is EntitiesLoadSuccess<TagEntity>) {
        // reuse existing tag if collection is re-imported
        tag = tagsState.items.firstWhereOrNull(
            (tag) => tag.label == state.collection.label && !tag.deleted);
      }
      if (tag == null) {
        tag = TagEntity(state.collection.label, accountId: account.id);
        await I<EntitiesRepository<TagEntity>>().create(tag);
        tagsBloc.add(EntitiesAdded<TagEntity>([tag], persist: false));
      }
      if (!isReimport) {
        final updatedAccount = account.copyWith(importedDecks: <int>[
          ...account.importedDecks,
          state.collection.id
        ]);
        await I<EntitiesRepository<AccountEntity>>().update(updatedAccount);
        accountsBloc.add(
            EntitiesUpdated<AccountEntity>([updatedAccount], persist: false));
      }
      final verses = state.collection.verses
          .where((remoteVerse) {
            // exclude duplicates if collection is re-imported
            final localState = versesBloc.state;
            return !isReimport ||
                localState is! EntitiesLoadSuccess<Verse> ||
                !localState.items.any((localVerse) =>
                    localVerse.tags.keys.contains(tag!.id) &&
                    localVerse.reference == remoteVerse.reference &&
                    localVerse.source == remoteVerse.source &&
                    !localVerse.deleted);
          })
          .map((verse) => verse.copyWith(
                id: IdGenerator().next(),
                accountId: account.id,
                tags: {tag!.id: tag.text},
              ))
          .toList();
      if (verses.isNotEmpty) versesBloc.add(EntitiesAdded<VerseEntity>(verses));
      I<UndoBloc>().add(UndoAdded([
        Undo(EntityAction.create, <TagEntity>[tag]),
        Undo(EntityAction.update, <AccountEntity>[account]),
        if (verses.isNotEmpty) Undo(EntityAction.create, verses),
      ]));
      yield state.copyWith(isUpdating: false);
      nav.goHome();
    }
  }

  @override
  Future<void> close() {
    _navSubscription.cancel();
    return super.close();
  }
}
