import 'package:equatable/equatable.dart';
import 'package:remem_me/models/collections/collections.dart';

abstract class CollectionDetailState extends Equatable {
  const CollectionDetailState();

  @override
  List<Object?> get props => [];
}

class CollectionDetailInitialState extends CollectionDetailState {}

class CollectionDetailLoadInProgress extends CollectionDetailState {
  final int id;

  const CollectionDetailLoadInProgress(this.id);

  @override
  List<Object?> get props => [id];
}

class CollectionDetailLoadSuccess extends CollectionDetailState {
  final Collection collection;
  final bool isUpdating;

  const CollectionDetailLoadSuccess(this.collection, {this.isUpdating = false});

  @override
  List<Object?> get props => [collection, isUpdating];

  CollectionDetailLoadSuccess copyWith(
          {Collection? collection, bool? isUpdating}) =>
      CollectionDetailLoadSuccess(collection ?? this.collection,
          isUpdating: isUpdating ?? this.isUpdating);
}

// todo: general failure state mixin
class CollectionDetailLoadFailure extends CollectionDetailState {
  final String errorMessage;

  const CollectionDetailLoadFailure(this.errorMessage);

  @override
  List<Object?> get props => [errorMessage];

  @override
  String toString() {
    return 'CollectionDetailLoadFailure { errorMessage: $errorMessage }';
  }
}
