import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../models/account.dart';
import '../paths/paths.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class SettingsNavigator extends InnerNavigator {
  SettingsNavigator(super.navigatorKey, {super.key, required super.onNotify});

  @override
  Widget build(BuildContext context) {
    final path = nav.state;
    return Navigator(
      key: navigatorKey,
      pages: [
        const FadeAnimationPage(
          child: SettingsScreen(),
          key: ValueKey('SettingsPage'),
        ),
        if (path is AccountCreatePath)
          CupertinoPage(
            child: _accountCreate(context),
            key: const ValueKey('AccountCreatePage'),
          ),
        if (path is AccountEditPath)
          CupertinoPage(
            child: _accountEdit(context, I<CurrentAccount>().state!),
            key: const ValueKey('AccountEditPage'),
          ),
      ],
      onPopPage: (route, result) {
        if (path is ChildPath) {
          nav.go((path as ChildPath).parent);
        }
        return popPage(route, result);
      },
    );
  }

  Widget _accountCreate(BuildContext ctx) =>
      EditAccountScreen(onSave: (context, entity) async {
        await I.get<EntitiesRepository<AccountEntity>>().create(entity);
        await I<CurrentAccount>().setEntity(entity);
        BlocProvider.of<AccountsBloc>(ctx)
            .add(EntitiesAdded<AccountEntity>([entity], persist: false));
      });

  Widget _accountEdit(BuildContext ctx, Account currentAccount) {
    final repository = I<EntitiesRepository<AccountEntity>>();
    final bloc = BlocProvider.of<AccountsBloc>(ctx);
    return EditAccountScreen(
        onSave: (context, entity) async {
          await repository.update(entity);
          await I<CurrentAccount>().setEntity(entity);
          BlocProvider.of<AccountsBloc>(ctx)
              .add(EntitiesUpdated<AccountEntity>([entity], persist: false));
        },
        onDelete: (context, entity) async {
          final deleted = entity.copyWith(deleted: true);
          I<AuthBloc>().state is AccessRemote
              ? await _deleteAccountRemote(entity)
              : await _deleteAccountLocal(entity);
          I<AccountsBloc>()
              .add(EntitiesUpdated<AccountEntity>([deleted], persist: false));
        },
        entity: currentAccount);
  }

  Future<void> _deleteAccountRemote(AccountEntity entity) async {
    await I<EntitiesRepository<AccountEntity>>()
        .update(entity.copyWith(deleted: true));
  }

  Future<void> _deleteAccountLocal(AccountEntity entity) async {
    I<EntitiesRepositoryHive<TagEntity>>().clear(entity.id);
    I<EntitiesRepositoryHive<ScoreEntity>>().clear(entity.id);
    I<EntitiesRepositoryHive<VerseEntity>>().clear(entity.id);
    I<EntitiesRepositoryHive<AccountEntity>>().delete(entity);
  }
}
