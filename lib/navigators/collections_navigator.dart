import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:share_plus/share_plus.dart';

import '../blocs/blocs.dart';
import '../models/collections/collections.dart';
import '../paths/paths.dart';
import '../screens/screens.dart';
import '../services/verse_service.dart';
import 'navigators.dart';

class CollectionsNavigator extends InnerNavigator {
  CollectionsNavigator(super.navigatorKey,
      {super.key, required super.onNotify});

  @override
  Widget build(BuildContext ctx) {
    return _collectionsProviders(
      child: Builder(builder: (ctx) {
        final nav = BlocProvider.of<NavigationCubit>(ctx);
        final path = nav.state;
        return Navigator(
            key: navigatorKey,
            pages: [
              FadeAnimationPage(
                child: _collectionsScreen(ctx),
                key: const ValueKey('CollectionsPage'),
              ),
              if (path is CollectionDetailPath)
                MaterialPage(
                    child: _detailScreen(ctx, path),
                    key: ValueKey('CollectionDetailPage-${path.id}')),
              if (path is CollectionFlashcardPath)
                MaterialPage(child: _flashcardScreen(path)),
              if (path is CollectionStudyPath)
                MaterialPage(child: _studyScreen())
            ],
            onPopPage: (route, result) {
              if (path is ChildPath) {
                nav.go((path as ChildPath).parent,
                    propagates: true); // propagates doesn't make a difference.
              }
              return popPage(route, result);
            });
      }),
    );
  }

  Widget _collectionsProviders({required Widget child}) {
    return BlocProvider<FlashcardBloc>(
            create: (ctx) => CollectionFlashcardBloc(
                collectionBloc: BlocProvider.of<CollectionDetailBloc>(ctx)),
            child: child);
  }

  Widget _collectionsScreen(BuildContext ctx) => BlocProvider<CollectionsBloc>(
      create: (ctx) {
        return CollectionsBloc()
          ..add(CollectionsInitiated(Query(
              language: I<CurrentAccount>().state?.language != null
                  ? I<CurrentAccount>().state?.language.languageCode
                  : L10n.current.locale
                      .languageCode))); // todo: improve for Chinese
      },
      child: const CollectionsScreen());

  Widget _importedScreen(BuildContext ctx) => BlocProvider<CollectionsBloc>(
      create: (ctx) {
        return ImportedBloc()
          ..add(CollectionsInitiated(Query(
              language: I
                  .get<CurrentAccount>()
                  .state
                  ?.language
                  .languageCode))); // todo: improve for Chinese
      },
      child: const ImportedScreen());

  Widget _detailScreen(BuildContext ctx, CollectionDetailPath path) {
    return const CollectionDetailScreen();
  }

  Widget _flashcardScreen(CollectionFlashcardPath path) {
    final nav = I<NavigationCubit>();
    return Builder(builder: (ctx) {
      /*if (path.verseId != null) {
        final flashcardBloc = BlocProvider.of<FlashcardBloc>(ctx);
        if(flashcardBloc.state is! FlashcardRun) {
          flashcardBloc.add(FlashcardInitiated(path.verseId!));
        }
      }*/
      return FlashcardScreen(
        onStudy: (verse) => nav.go(path.study),
        onShare: (verse) => Share.share(
            '${verse.passage}\n\n${VerseService().subject(verse)}'
            '\n$WEB_URL/collections/${path.id}/${verse.id}',
            subject: VerseService().subject(verse)),
      );
    });
  }

  Widget _studyScreen() {
    return Builder(builder: (context) {
      final flashcardBloc = BlocProvider.of<FlashcardBloc>(context);
      return FutureBuilder<FlashcardState>(
          future: flashcardBloc.stream
              .startWith(flashcardBloc.state)
              .firstWhere((state) => state is FlashcardRun),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final state = snapshot.data!;
              if (state is FlashcardRun) {
                return BlocProvider<StudyBloc>(
                    create: (ctx) => StudyBloc(flashcardBloc: flashcardBloc),
                    child: const StudyScreen());
              }
            }
            return const SizedBox.shrink();
          });
    });
  }
}
