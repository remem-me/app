import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';

import '../blocs/blocs.dart';

abstract class InnerNavigator extends StatelessWidget {
  final NavigationCubit nav = I<NavigationCubit>();
  final void Function() onNotify;
  final Key navigatorKey;

  InnerNavigator(this.navigatorKey, {super.key, required this.onNotify});

  bool popPage(Route route, result) {
    onNotify();
    return route.didPop(result);
  }
}
