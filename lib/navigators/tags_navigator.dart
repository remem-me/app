import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../paths/paths.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class TagsNavigator extends InnerNavigator {
  TagsNavigator(super.navigatorKey, {super.key, required super.onNotify});

  @override
  Widget build(BuildContext ctx) {
    final path = nav.state;
    return Navigator(
      key: navigatorKey,
      pages: [
        FadeAnimationPage(
          child: RepositoryProvider<DecksRepository>(
              create: (ctx) =>
                  DecksRepositoryHttp(I<AuthService>(), HttpService()),
              child: const TagsScreen()),
          key: ValueKey('TagsPage'),
        ),
        if (path is TagBinPath)
          FadeAnimationPage(
            child: _tagBin(ctx),
            key: const ValueKey('TagBinPage'),
          ),
      ],
      onPopPage: (route, result) {
        if (path is ChildPath) {
          nav.go((path as ChildPath).parent);
        }
        return popPage(route, result);
      },
    );
  }

  Widget _tagBin(BuildContext ctx) =>
      BlocProvider<BinBloc<TagEntity, TagEntity>>(
          create: (ctx) => BinBloc<TagEntity, TagEntity>(
                initialState: BinLoadInProgress<TagEntity>(),
                repository: RepositoryProvider.of<TagsRepository>(ctx),
                entitiesBloc: BlocProvider.of<TagsBloc>(ctx),
              )..add(BinLoaded<TagEntity>()),
          child: const BinScreen<TagEntity, TagEntity>(
              typeIcon: Icons.label_rounded));

}
