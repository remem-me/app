import 'package:app_core/app_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logging/logging.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository/repository.dart';
import 'package:share_plus/share_plus.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../paths/paths.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class HomeNavigator extends InnerNavigator {
  static final _log = Logger('HomeNavigator');
  late final FlashcardBloc _homeFlashcardBloc = I<HomeFlashcardBloc>();

  HomeNavigator(super.navigatorKey, {super.key, required super.onNotify});

  @override
  Widget build(BuildContext context) {
    final path = nav.state;
    return Navigator(
      key: navigatorKey,
      pages: [
        FadeAnimationPage(
          child: _home(),
          key: const ValueKey('HomePage'),
        ),
        if (path is HomeFlashcardPath) ...[
          MaterialPage(
            child: _flashcardScreen(context, path),
            key: const ValueKey('FlashcardPage'),
          ),
          if (path is HomeStudyPath)
            MaterialPage(
              child: _studyScreen(),
              key: const ValueKey('StudyPage'),
            ),
          if (path is HomeEditPath)
            CupertinoPage(
              child: _verseEditScreen(),
              key: const ValueKey('VerseEditPage'),
            ),
        ],
        if (path is HomeCreatePath)
          CupertinoPage(
            child: _verseAddScreen(context, path.data),
            key: const ValueKey('VerseAddPage'),
          )
      ],
      onPopPage: (route, result) {
        if (path is ChildPath) nav.go((path as ChildPath).parent);
        return popPage(route, result);
      },
    );
  }

  Widget _flashcardScreen(BuildContext ctx, HomeFlashcardPath path) {
    return BlocProvider<FlashcardBloc>.value(
      value: _homeFlashcardBloc,
      child: FlashcardScreen(
        onEdit: (verse) => nav.go(path.edit),
        onStudy: (verse) => nav.go(path.study),
        onShare: (verse) => Share.share(
            '${verse.passage}\n\n${VerseService().subject(verse)}\n$WEB_URL',
            subject: VerseService().subject(verse)),
      ),
    );
  }

  Widget _home() => Builder(builder: (ctx) {
        return MultiBlocProvider(
            providers: [
              BlocProvider<TabBloc>.value(value: I<TabBloc>()),
              BlocProvider<FilteredVersesBloc>.value(
                  value: I<FilteredVersesBloc>()),
              BlocProvider<FlashcardBloc>.value(value: _homeFlashcardBloc),
              BlocProvider<SelectionBloc>(
                  create: (ctx) => SelectionBloc(
                      tabBloc: I<TabBloc>(), versesBloc: I<VersesBloc>())),
            ],
            child: BlocBuilder<TabBloc, TabState>(
                buildWhen: (prev, next) =>
                    prev.isBoxed != next.isBoxed ||
                    prev.launches != next.launches,
                builder: (context, state) {
                  return HomeScreen(state.isBoxed,
                      key: ValueKey('${state.isBoxed}${state.launches}'));
                }));
      });

  Builder _studyScreen() {
    return Builder(builder: (context) {
      return BlocProvider<StudyBloc>(
          create: (ctx) => StudyBloc(flashcardBloc: _homeFlashcardBloc),
          child: const StudyScreen());
    });
  }

  Widget _verseAddScreen(BuildContext ctx, String? data) => EditVerseScreen(
        key: ValueKey(data),
        onSave: (context, created) async {
          final accountsBloc = BlocProvider.of<AccountsBloc>(ctx);
          var account = I<CurrentAccount>().state!;
          if (created.source != account.defaultSource) {
            if (created.source == null) {
              account = account.copyWith(nullValues: ['defaultSource']);
            } else {
              account = account.copyWith(defaultSource: created.source);
            }
            await RepositoryProvider.of<AccountsRepository>(ctx)
                .update(account);
            accountsBloc
                .add(EntitiesUpdated<Account>([account], persist: false));
          }
          await RepositoryProvider.of<VersesRepository>(ctx).create(created);
          BlocProvider.of<VersesBloc>(ctx)
              .add(EntitiesAdded<VerseEntity>([created], persist: false));
          I<UndoBloc>().add(UndoAdded([
            Undo(EntityAction.create, <VerseEntity>[Verse.fromEntity(created)]),
          ]));
        },
        sharedData: data,
      );

  Widget _verseEditScreen() {
    return Builder(builder: (ctx) {
      final flashcardState = _homeFlashcardBloc.state;
      if (flashcardState is FlashcardRun) {
        Verse origin = flashcardState.verse;
        return EditVerseScreen(
            onSave: (context, updated) async {
              await RepositoryProvider.of<VersesRepository>(ctx)
                  .update(updated);
              final verse = (await VerseService().fromEntities([updated]))[0];
              BlocProvider.of<VersesBloc>(ctx)
                  .add(EntitiesUpdated<Verse>([verse], persist: false));
              I<UndoBloc>().add(UndoAdded([
                Undo(EntityAction.update, <VerseEntity>[origin]),
              ]));
              _homeFlashcardBloc.add(FlashcardVerseUpdated(verse));
            },
            verse: flashcardState.verse);
      } else {
        return Scaffold(appBar: AppBar(), body: LoadingIndicator());
      }
    });
  }
}
