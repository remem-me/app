import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:repository/repository.dart';

import '../blocs/blocs.dart';
import '../models/models.dart';
import '../paths/paths.dart';
import '../screens/screens.dart';
import 'navigators.dart';

class SimpleNavigator extends InnerNavigator {

  SimpleNavigator(super.navigatorKey, {super.key, required super.onNotify});

  @override
  Widget build(BuildContext context) {
    final path = nav.state;
    return Navigator(
      key: navigatorKey,
      pages: [
        if (path is AccountBinPath)
          FadeAnimationPage(
            child: _accountBin(context),
            key: const ValueKey('AccountBinPage'),
          ),
        if (path is ScoresPath)
          const FadeAnimationPage(
            child: ScoresScreen(),
            key: ValueKey('ScoresPage'),
          ),
        if (path is StylesPath)
          const FadeAnimationPage(
            child: StylesScreen(),
            key: ValueKey('StylesPage'),
          ),
        if (path is VerseBinPath)
          FadeAnimationPage(
            child: _verseBin(context, I<CurrentAccount>().state!),
            key: const ValueKey('VerseBinPage'),
          ),
      ],
      onPopPage: popPage,
    );
  }

  Widget _accountBin(BuildContext ctx) =>
      BlocProvider<BinBloc<AccountEntity, AccountEntity>>(
          create: (ctx) => BinBloc<AccountEntity, AccountEntity>(
                initialState: BinLoadInProgress<AccountEntity>(),
                repository: I<EntitiesRepository<AccountEntity>>(),
                entitiesBloc: BlocProvider.of<AccountsBloc>(ctx),
              )..add(const BinLoaded<AccountEntity>()),
          child: const BinScreen<AccountEntity, AccountEntity>(
              typeIcon: Icons.folder_shared_rounded));

  Widget _verseBin(BuildContext ctx, Account account) =>
      BlocProvider<BinBloc<VerseEntity, Verse>>(
          create: (ctx) => BinBloc<VerseEntity, Verse>(
                initialState: BinLoadInProgress<VerseEntity>(),
                repository: RepositoryProvider.of<VersesRepository>(ctx),
                entitiesBloc: BlocProvider.of<VersesBloc>(ctx),
              )..add(BinLoaded<VerseEntity>()),
          child: const BinScreen<VerseEntity, Verse>(
              typeIcon: Icons.menu_book_rounded));
}
