import 'dart:ui';

import 'package:hive/hive.dart';

/// Adapter for Color
class ColorAdapter<T extends Color> extends TypeAdapter<T> {
  @override
  final int typeId = 9;

  ColorAdapter();

  @override
  T read(BinaryReader reader) {
    return Color(reader.readInt()) as T;
  }

  @override
  void write(BinaryWriter writer, Color obj) {
    writer.writeInt(obj.value);
  }
}