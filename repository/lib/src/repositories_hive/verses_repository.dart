import 'package:app_core/app_core.dart';

import '../../repository.dart';

class VersesRepositoryHive extends EntitiesRepositoryHive<VerseEntity>
    implements VersesRepository {
  static final VersesRepositoryHive _instance =
      VersesRepositoryHive._internal();
  @override
  final Type entityType = VerseEntity;

  factory VersesRepositoryHive() {
    return _instance;
  }

  VersesRepositoryHive._internal() : super();

  @override
  List<VerseEntity> sample({int? accountId}) {
    return [
      VerseEntity(
        'Joshua 1:8',
        'This Book of the Law shall not depart from your mouth, but you shall meditate on it day and night, so that you may be careful to do according to all that is written in it.'
            'For then you will make your way prosperous, and then you will have good success.',
        source: 'ESV',
        topic: 'Word of God',
        level: -1,
        id: 1,
        commit: DateService().today,
        tags: const {1: null, 2: null},
        accountId: accountId!,
      ),
      VerseEntity(
        'Isaiah 12:1',
        'In that day you will sing:'
            '“I will praise you, O Lord!'
            'You were angry with me, but not any more.'
            'Now you comfort me.',
        source: 'NIV',
        topic: 'Comfort',
        level: 3,
        id: 2,
        tags: const {2: null},
        accountId: accountId,
      ),
      VerseEntity(
        '1 Kings 18:21',
        'How long will you go limping between two different opinions? '
            'If the Lord is God, follow him;'
            'but if Baal, then follow him.',
        level: 0,
        id: 3,
        commit: DateService().today - 10,
        review: DateService().today,
        accountId: accountId,
      ),
      VerseEntity(
        'Psalm 16:11',
        'Lord, You will show me the way of life.',
        source: 'NIVUK',
        level: 23,
        id: 4,
        accountId: accountId,
      ),
      VerseEntity(
        'Luke 23:42',
        'And he said, “Jesus, remember me when you come into your kingdom.”',
        topic: 'Remember Me',
        level: 12,
        id: 5,
        commit: DateService().today - 10,
        review: DateService().today - 4,
        tags: const {2: null, 3: null},
        accountId: accountId,
      ),
    ];
  }

}
