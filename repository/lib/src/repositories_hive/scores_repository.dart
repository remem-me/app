import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:logging/logging.dart';
import 'package:repository/repository.dart';

class ScoresRepositoryHive extends EntitiesRepositoryHive<ScoreEntity>
    implements ScoresRepository {
  static final _log = Logger('ScoresRepositoryHive');
  static final ScoresRepositoryHive _instance =
      ScoresRepositoryHive._internal();
  @override
  final Type entityType = ScoreEntity;

  factory ScoresRepositoryHive() {
    return _instance;
  }

  ScoresRepositoryHive._internal() : super();

  @override
  Future<List<ScoreEntity>> updateList(List<ScoreEntity> scores) async {
    scores = await super.updateList(scores);
    if (scores.isNotEmpty && !scores[0].deleted) {
      _scorePurge(scores[0].accountId!);
    }
    return scores;
  }

  @override
  List<ScoreEntity> sample({int? accountId}) {
    return [];
  }

  Future<void> _scorePurge(int accountId) async {
    final box = await getBox(repositoryKey(entityType, id: accountId));
    final lastScoreDate = (box.getAt(box.length - 1))?.date;
    if (lastScoreDate != null && lastScoreDate < DateService().today) {
      final obsoleteScoreKeys =
          await compute(ScoreUtil.obsoleteScoreKeys, box.values.toList());
      _log.fine(
          '$runtimeType._scorePurge($accountId): ${obsoleteScoreKeys.length} obsoleteScoreKeys');
      await box.deleteAll(obsoleteScoreKeys);
    }
  }
}
