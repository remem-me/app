
import 'package:repository/repository.dart';

class TagsRepositoryHive extends EntitiesRepositoryHive<TagEntity> implements TagsRepository {
  static final TagsRepositoryHive _instance = TagsRepositoryHive._internal();
  @override
  final Type entityType = TagEntity;

  factory TagsRepositoryHive() {
    return _instance;
  }

  TagsRepositoryHive._internal(): super();

  @override
  List<TagEntity> sample({int? accountId}) {
    return [
      TagEntity(
        'Bible Study',
        id: 1,
        accountId: 0,
      ),
      TagEntity(
        'God\'s Unfailing Love',
        id: 2,
        accountId: 0,
      ),
      TagEntity(
        'Human Failure',
        id: 3,
        accountId: 0,
      ),
    ];

  }

}