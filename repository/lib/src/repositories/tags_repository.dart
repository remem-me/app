import 'dart:core';

import '../../repository.dart';



/// A class that Loads and Persists tags. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as tags_repository_simple or tags_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class TagsRepository extends EntitiesRepository<TagEntity> {}
