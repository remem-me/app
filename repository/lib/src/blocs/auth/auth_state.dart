part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => [];
}

abstract class Access extends AuthState {
  const Access();
}

class AuthInitialState extends AuthState {
  const AuthInitialState();
}

class NoAuth extends AuthState {
  const NoAuth();
}

class AccessRemote extends Access {
  final String accessToken;
  final String refreshToken;

  const AccessRemote({required this.accessToken, required this.refreshToken});

  @override
  // ignore accessToken to prevent account reloading in AccountsBloc()
  List<Object?> get props => [refreshToken];

  @override
  String toString() {
    return 'AccessRemote { accessToken: $accessToken, refreshToken: $refreshToken }';
  }
}

class AccessLocal extends Access {
  const AccessLocal();
}
