part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object?> get props => [];
}

class AuthLoadedFromSettings extends AuthEvent {}

class AuthTokenRefreshed extends AuthEvent {
  final String accessToken;
  final String refreshToken;

  const AuthTokenRefreshed({required this.accessToken, required this.refreshToken});

  @override
  List<Object?> get props => [accessToken, refreshToken];

  @override
  String toString() => 'AuthTokenRefreshed { authToken: $accessToken, '
      'refreshtoken: $refreshToken }';
}

class Granted extends AuthEvent {
  final String email;
  final AccessRemote access;
  const Granted(this.email, this.access);
}

class Permitted extends AuthEvent {
  const Permitted();
}

class Revoked extends AuthEvent {
  const Revoked();
}
