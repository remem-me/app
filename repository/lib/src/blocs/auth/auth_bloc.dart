import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:logging/logging.dart';
import 'package:equatable/equatable.dart';

import '../../../repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  static final _log = Logger('AuthBloc');
  late Settings settings;
  late StreamSubscription userSubscription;

  AuthBloc() : super(const AuthInitialState()) {
    _log.fine('AuthBloc constructor started');
    on<AuthEvent>(
        (event, emit) => emit.forEach(mapEventToState(event),
            onData: (AuthState result) => result),
        transformer: sequential()); // process events sequentially
    settings = I<Settings>();
  }

  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthLoadedFromSettings) {
      yield* mapAuthLoadedFromSettingsToState();
    } else if (event is Granted) {
      yield* mapGrantedToState(event);
    } else if (event is Permitted) {
      yield* mapPermittedToState(event);
    } else if (event is Revoked) {
      yield* mapRevokedToState();
    } else if (event is AuthTokenRefreshed) {
      yield* mapAuthTokenRefreshedToState(event);
    }
  }

  Stream<AuthState> mapAuthLoadedFromSettingsToState() async* {
    _log.fine('$runtimeType.mapAuthLoadedFromSettingsToState');
    var accessToken = I<Settings>().get(Settings.ACCESS_TOKEN);
    if (accessToken != null) {
      if (accessToken == 'local') {
        yield AccessLocal();
      } else {
        var refreshToken = settings.get(Settings.REFRESH_TOKEN)!;
        yield AccessRemote(
            accessToken: accessToken, refreshToken: refreshToken);
      }
    } else {
      yield const NoAuth();
    }
  }

  Stream<AuthState> mapGrantedToState(Granted event) async* {
    settings.put(Settings.ACCESS_TOKEN, event.access.accessToken);
    settings.put(Settings.REFRESH_TOKEN, event.access.refreshToken);
    settings.put(Settings.USER_EMAIL, event.email);
    if (settings.hasNewDbVersion) settings.updateDbVersion();
    yield event.access;
  }

  Stream<AuthState> mapPermittedToState(Permitted event) async* {
    settings.put(Settings.ACCESS_TOKEN, 'local');
    if (settings.hasNewDbVersion) settings.updateDbVersion();
    yield AccessLocal();
  }

  Stream<AuthState> mapAuthTokenRefreshedToState(
      AuthTokenRefreshed event) async* {
    settings.put(Settings.ACCESS_TOKEN, event.accessToken);
    settings.put(Settings.REFRESH_TOKEN, event.refreshToken);
    yield AccessRemote(
        accessToken: event.accessToken, refreshToken: event.refreshToken);
  }

  Stream<AuthState> mapRevokedToState() async* {
    settings.delete(Settings.ACCESS_TOKEN);
    settings.delete(Settings.REFRESH_TOKEN);
    settings.delete(Settings.USER_EMAIL);
    yield const NoAuth();
  }
}
