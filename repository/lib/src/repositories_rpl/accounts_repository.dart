import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

class AccountsRepositoryRpl extends EntitiesRepositoryRpl<AccountEntity>
    implements AccountsRepository {
  AccountsRepositoryRpl({required void Function() onUpdate})
      : super(I<EntitiesRepositoryHive<AccountEntity>>(),
            I<EntitiesRepositoryHttp<AccountEntity>>(),
            onUpdate: onUpdate);

  @override
  Future<AccountEntity> create(AccountEntity entity) async {
    entity = await httpRepo.create(entity);
    return hiveRepo.create(entity);
  }

  @override
  Future<AccountEntity> update(AccountEntity entity) async {
    final prev = await hiveRepo.read(entity.id);
    if (prev?.name == entity.name) {
      return (await super.updateList([entity])).first; // RETURN
    }
    entity = await httpRepo.update(entity);
    return hiveRepo.update(entity);
  }

  @override
  Future<List<AccountEntity>> createList(List<AccountEntity> entities) async {
    entities = await httpRepo.createList(entities);
    return hiveRepo.createList(entities);
  }

  @override
  Future<List<AccountEntity>> updateList(List<AccountEntity> entities,
      {bool isAlreadyStamped = false}) async {
    entities =
        await httpRepo.updateList(entities, isAlreadyStamped: isAlreadyStamped);
    return hiveRepo.updateList(entities);
  }
}
