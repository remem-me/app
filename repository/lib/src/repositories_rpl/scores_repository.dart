import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

class ScoresRepositoryRpl extends EntitiesRepositoryRpl<ScoreEntity>
    implements ScoresRepository {
  ScoresRepositoryRpl({void Function()? onUpdate})
      : super(I<EntitiesRepositoryHive<ScoreEntity>>(),
            I<EntitiesRepositoryHttp<ScoreEntity>>(),
            onUpdate: onUpdate);
}
