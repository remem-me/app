import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';

class TagsRepositoryRpl extends EntitiesRepositoryRpl<TagEntity>
    implements TagsRepository {
  TagsRepositoryRpl({required void Function() onUpdate})
      : super(I<EntitiesRepositoryHive<TagEntity>>(),
            I<EntitiesRepositoryHttp<TagEntity>>(),
            onUpdate: onUpdate);
}
