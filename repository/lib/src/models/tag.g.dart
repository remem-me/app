// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tag.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TagEntityAdapter extends TypeAdapter<TagEntity> {
  @override
  final int typeId = 2;

  @override
  TagEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TagEntity(
      fields[3] as String,
      included: fields[4] as bool?,
      published: fields[6] == null ? false : fields[6] as bool,
      accountId: fields[12] as int?,
      id: fields[0] as int?,
      modified: fields[1] as int?,
      deleted: fields[2] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, TagEntity obj) {
    writer
      ..writeByte(7)
      ..writeByte(3)
      ..write(obj.text)
      ..writeByte(4)
      ..write(obj.included)
      ..writeByte(6)
      ..write(obj.published)
      ..writeByte(12)
      ..write(obj.accountId)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.modified)
      ..writeByte(2)
      ..write(obj.deleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TagEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
