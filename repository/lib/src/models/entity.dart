import 'package:equatable/equatable.dart';
import 'package:app_core/app_core.dart';
import 'package:hive/hive.dart';

import '../../repository.dart';

/// generate entity adapters using
/// flutter packages pub run build_runner build (in directory repository)
class Entity extends Equatable {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final int modified;
  final int modifiedBy;
  @HiveField(2)
  final bool deleted;

  Entity({
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
  })  : id = id ?? IdGenerator().next(),
        modified = modified ?? DateService().now(),
        modifiedBy = modifiedBy ?? I<Settings>().deviceId,
        deleted = deleted ?? false;

  Entity copyWith({
    int? id,
    int? modified,
    int? modifiedBy,
    bool? deleted,
  }) {
    return Entity(
      id: id ?? this.id,
      modified: modified ?? this.modified,
      modifiedBy: modifiedBy ?? this.modifiedBy,
      deleted: deleted ?? this.deleted,
    );
  }

  @override
  List<Object?> get props => [id, modified, modifiedBy, deleted];

  @override
  String toString() {
    return 'Entity { id: $id, modified: $modified, '
        'modifiedBy: $modifiedBy, deleted: $deleted }';
  }

  String get label {
    return id.toString();
  }

  Map<String, Object?> toJson() {
    return {
      'id': id,
      'modified': modified,
      'modified_by': modifiedBy.toString(),
      'deleted': deleted,
    };
  }

  static Entity fromJson(Map<String, dynamic> json) {
    return Entity(
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      modifiedBy: modifiedByFromJson(json['modified_by']),
      deleted: json['deleted'] as bool?,
    );
  }

  static int? modifiedByFromJson(dynamic json) {
    return json != null && json is int ? json : null;
  }
}
