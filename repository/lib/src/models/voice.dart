import 'package:equatable/equatable.dart';

class Voice extends Equatable {
  final String name;
  final String idiom;

  const Voice({required this.name, required this.idiom});

  static Voice? parse(String str) {
    if (!str.contains('|')) return null;    // RETURN
    final parts = str.split('|');
    return Voice(name: parts[0].trim(), idiom: parts[1].trim());
  }

  @override
  String toString() => '$name | $idiom';

  @override
  List<Object?> get props => [name, idiom];
}