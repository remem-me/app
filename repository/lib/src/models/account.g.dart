// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class AccountEntityAdapter extends TypeAdapter<AccountEntity> {
  @override
  final int typeId = 3;

  @override
  AccountEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return AccountEntity(
      fields[3] as String,
      language: fields[4] == null ? const Locale('en') : fields[4] as Locale,
      langRef: fields[5] == null ? const Locale('en') : fields[5] as Locale,
      reviewFrequency: fields[6] == null ? 2.0 : fields[6] as double,
      reviewLimit: fields[7] == null ? 128 : fields[7] as int,
      dailyGoal: fields[8] == null ? 70 : fields[8] as int,
      reviewBatch: fields[20] == null ? 0 : fields[20] as int,
      inverseLimit: fields[19] == null ? 7 : fields[19] as int,
      referenceIncluded: fields[15] == null ? false : fields[15] as bool,
      topicPreferred: fields[16] == null ? false : fields[16] as bool,
      defaultSource: fields[17] as String?,
      canon: fields[9] == null ? Canon.protestant : fields[9] as Canon,
      fontType: fields[18] as FontType,
      orderNew: fields[10] == null ? VerseOrder.DATE : fields[10] as VerseOrder,
      orderDue:
          fields[11] == null ? VerseOrder.LEVEL : fields[11] as VerseOrder,
      orderKnown:
          fields[12] == null ? VerseOrder.RANDOM : fields[12] as VerseOrder,
      orderAll:
          fields[13] == null ? VerseOrder.CANON : fields[13] as VerseOrder,
      importedDecks: fields[14] == null ? [] : (fields[14] as List).cast<int>(),
      id: fields[0] as int?,
      modified: fields[1] as int?,
      deleted: fields[2] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, AccountEntity obj) {
    writer
      ..writeByte(21)
      ..writeByte(3)
      ..write(obj.name)
      ..writeByte(4)
      ..write(obj.language)
      ..writeByte(5)
      ..write(obj.langRef)
      ..writeByte(6)
      ..write(obj.reviewFrequency)
      ..writeByte(7)
      ..write(obj.reviewLimit)
      ..writeByte(8)
      ..write(obj.dailyGoal)
      ..writeByte(20)
      ..write(obj.reviewBatch)
      ..writeByte(19)
      ..write(obj.inverseLimit)
      ..writeByte(15)
      ..write(obj.referenceIncluded)
      ..writeByte(16)
      ..write(obj.topicPreferred)
      ..writeByte(17)
      ..write(obj.defaultSource)
      ..writeByte(9)
      ..write(obj.canon)
      ..writeByte(18)
      ..write(obj.fontType)
      ..writeByte(10)
      ..write(obj.orderNew)
      ..writeByte(11)
      ..write(obj.orderDue)
      ..writeByte(12)
      ..write(obj.orderKnown)
      ..writeByte(13)
      ..write(obj.orderAll)
      ..writeByte(14)
      ..write(obj.importedDecks)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.modified)
      ..writeByte(2)
      ..write(obj.deleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AccountEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
