import 'package:hive/hive.dart';

part 'verse_order.g.dart';

@HiveType(typeId: 8)
enum VerseOrder {
  @HiveField(0)
  ALPHABET,
  @HiveField(1)
  CANON,
  @HiveField(2)
  TOPIC,
  @HiveField(3)
  DATE,
  @HiveField(4)
  LEVEL,
  @HiveField(5)
  RANDOM,
  @HiveField(6)
  ID,
}
