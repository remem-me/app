// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'canon.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CanonAdapter extends TypeAdapter<Canon> {
  @override
  final int typeId = 6;

  @override
  Canon read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return Canon.protestant;
      case 1:
        return Canon.lutheran;
      case 2:
        return Canon.roman;
      case 3:
        return Canon.eastern;
      case 4:
        return Canon.oriental;
      case 5:
        return Canon.jewish;
      default:
        return Canon.protestant;
    }
  }

  @override
  void write(BinaryWriter writer, Canon obj) {
    switch (obj) {
      case Canon.protestant:
        writer.writeByte(0);
        break;
      case Canon.lutheran:
        writer.writeByte(1);
        break;
      case Canon.roman:
        writer.writeByte(2);
        break;
      case Canon.eastern:
        writer.writeByte(3);
        break;
      case Canon.oriental:
        writer.writeByte(4);
        break;
      case Canon.jewish:
        writer.writeByte(5);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CanonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
