import 'package:hive/hive.dart';

part 'canon.g.dart';

@HiveType(typeId: 6)
enum Canon {
  @HiveField(0)
  protestant,
  @HiveField(1)
  lutheran,
  @HiveField(2)
  roman,
  @HiveField(3)
  eastern,
  @HiveField(4)
  oriental,
  @HiveField(5)
  jewish
}