// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verse.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class VerseEntityAdapter extends TypeAdapter<VerseEntity> {
  @override
  final int typeId = 1;

  @override
  VerseEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return VerseEntity(
      fields[3] as String,
      fields[4] as String,
      source: fields[5] as String?,
      topic: fields[6] as String?,
      image: fields[7] as String?,
      commit: fields[8] as LocalDate?,
      review: fields[9] as LocalDate?,
      level: fields[10] == null ? -1 : fields[10] as int,
      tags: fields[11] == null ? {} : (fields[11] as Map).cast<int, String?>(),
      accountId: fields[12] as int?,
      id: fields[0] as int?,
      modified: fields[1] as int?,
      deleted: fields[2] as bool?,
    );
  }

  @override
  void write(BinaryWriter writer, VerseEntity obj) {
    writer
      ..writeByte(13)
      ..writeByte(3)
      ..write(obj.reference)
      ..writeByte(4)
      ..write(obj.passage)
      ..writeByte(5)
      ..write(obj.source)
      ..writeByte(6)
      ..write(obj.topic)
      ..writeByte(7)
      ..write(obj.image)
      ..writeByte(8)
      ..write(obj.commit)
      ..writeByte(9)
      ..write(obj.review)
      ..writeByte(10)
      ..write(obj.level)
      ..writeByte(11)
      ..write(obj.tags)
      ..writeByte(12)
      ..write(obj.accountId)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.modified)
      ..writeByte(2)
      ..write(obj.deleted);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VerseEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
