// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'font_type.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FontTypeAdapter extends TypeAdapter<FontType> {
  @override
  final int typeId = 7;

  @override
  FontType read(BinaryReader reader) {
    switch (reader.readByte()) {
      case 0:
        return FontType.Sans;
      case 1:
        return FontType.Serif;
      case 2:
        return FontType.Mono;
      default:
        return FontType.Sans;
    }
  }

  @override
  void write(BinaryWriter writer, FontType obj) {
    switch (obj) {
      case FontType.Sans:
        writer.writeByte(0);
        break;
      case FontType.Serif:
        writer.writeByte(1);
        break;
      case FontType.Mono:
        writer.writeByte(2);
        break;
    }
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FontTypeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
