import 'package:repository/repository.dart';

class ScoresRepositoryHttp extends EntitiesRepositoryHttp<ScoreEntity>
    implements ScoresRepository {
  static ScoresRepositoryHttp? _instance;

  factory ScoresRepositoryHttp() {
    _instance ??= ScoresRepositoryHttp._internal('scores/');
    return _instance!;
  }

  ScoresRepositoryHttp._internal(super.path);

  @override
  ScoreEntity parseItem(Map<String, dynamic>? json) {
    return ScoreEntity.fromJson(json!);
  }
}
