import 'package:repository/repository.dart';

class AccountsRepositoryHttp extends EntitiesRepositoryHttp<AccountEntity>
    implements AccountsRepository {
  static AccountsRepositoryHttp? _instance;

  factory AccountsRepositoryHttp() {
    _instance ??= AccountsRepositoryHttp._internal('accounts/');
    return _instance!;
  }

  AccountsRepositoryHttp._internal(super.path);

  @override
  AccountEntity parseItem(Map<String, dynamic>? json) {
    return AccountEntity.fromJson(json!);
  }

  @override
  List<AccountEntity> sample() {
    return [
      AccountEntity(
        'Test',
        id: 1,
      ),
      AccountEntity(
        'Demo',
        id: 2,
      ),
      AccountEntity(
        'My Verses',
        id: 3,
      ),
    ];
  }
}
