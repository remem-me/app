import 'package:repository/repository.dart';

class TagsRepositoryHttp extends EntitiesRepositoryHttp<TagEntity>
    implements TagsRepository {
  static TagsRepositoryHttp? _instance;

  factory TagsRepositoryHttp() {
    _instance ??= TagsRepositoryHttp._internal('tags/');
    return _instance!;
  }

  TagsRepositoryHttp._internal(super.path);

  @override
  TagEntity parseItem(Map<String, dynamic>? json) {
    return TagEntity.fromJson(json!);
  }

  @override
  List<TagEntity> sample() {
    return [
      TagEntity(
        'Bible Study',
        id: 1,
        accountId: 0,
      ),
      TagEntity(
        'God\'s Unfailing Love',
        id: 2,
        accountId: 0,
      ),
      TagEntity(
        'Human Failure',
        id: 3,
        accountId: 0,
      ),
    ];
  }
}
