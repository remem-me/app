import 'dart:convert';
import 'dart:math';

import 'package:app_core/app_core.dart';
import 'package:http/http.dart' as http;
import 'package:repository/repository.dart';
import 'package:logging/logging.dart';

class HttpService {
  static final _log = Logger('HttpService');
  static HttpService? _instance;

  factory HttpService() {
    _instance ??= HttpService._internal();
    return _instance!;
  }

  HttpService._internal();

  Future<http.Response> getList(String path,
      {int? accountId,
      bool? deleted,
      int? since,
      int? client,
      required Map<String, String> headers}) async {
    Map<String, String> params = {};
    if (deleted != null) params['deleted'] = deleted.toString();
    if (accountId != null) params['account'] = accountId.toString();
    if (since != null) params['since'] = since.toString();
    if (client != null) params['client'] = client.toString();
    final uri = await replicationUri('/v1/$path', params);
    final httpClient = I<http.Client>();
    try {
      final response = await httpClient.get(uri, headers: headers);
      _log.fine('Response status: ${response.statusCode}');
      _log.fine(
          'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
      return response;
    } finally {
      httpClient.close();
    }
  }

  Future<http.Response> getItem(
      String path, int id, Map<String, String> headers) async {
    final httpClient = I<http.Client>();
    try {
      final response = await httpClient
          .get(await replicationUri('/v1/$path$id/'), headers: headers);
      _log.fine('Response status: ${response.statusCode}');
      _log.fine(
          'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
      return response;
    } finally {
      httpClient.close();
    }
  }

  Future<http.Response?> postList(
      String path, List<Entity> entities, Map<String, String> headers) async {
    final response = await http.post(await replicationUri('/v1/$path'),
        body: jsonEncode(entities.map((entity) => entity.toJson()).toList()),
        headers: headers);
    _log.fine('Response status: ${response.statusCode}');
    _log.fine(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    return response;
  }

  Future<http.Response> postItem(
      String path, Entity entity, Map<String, String> headers) async {
    final response = await http.post(await replicationUri('/v1/$path'),
        body: jsonEncode(entity.toJson()), headers: headers);
    _log.fine('Response status: ${response.statusCode}');
    _log.fine(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    return response;
  }

  Future<http.Response?> patchList(
      String path, List<Entity> entities, Map<String, String> headers) async {
    final response = await http.patch(await replicationUri('/v1/$path'),
        body: jsonEncode(entities.map((entity) => entity.toJson()).toList()),
        headers: headers);
    _log.fine('Response status: ${response.statusCode}');
    _log.fine(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    return response;
  }

  Future<http.Response?> deleteList(
      String path, List<Entity> entities, Map<String, String> headers) async {
    http.Response? response;
    for (final entity in entities) {
      response = await deleteItem(path, entity, headers);
      if (response.statusCode != 204) break;
    }
    return response;
  }

  Future<http.Response> patchItem(
      String path, Entity entity, Map<String, String> headers) async {
    final response = await http.patch(
        await replicationUri('/v1/$path${entity.id}/'),
        body: jsonEncode(entity.toJson()),
        headers: headers);
    _log.fine('Response status: ${response.statusCode}');
    _log.fine(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    return response;
  }

  Future<http.Response> deleteItem(
      String path, Entity entity, Map<String, String> headers) async {
    final response = await http.delete(
        await replicationUri('/v1/$path${entity.id}/'),
        body: jsonEncode(entity.toJson()),
        headers: headers);
    _log.fine('Response status: ${response.statusCode}');
    print(
        'Response body: ${response.body.substring(0, min(response.body.length, 100))}');
    return response;
  }
}
