import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:app_core/app_core.dart';
import 'package:http/http.dart';

import '../../repository.dart';

typedef HttpCallback = Future<http.Response?> Function(Map<String, String>);

class AuthService {
  static AuthService? _instance;
  late AuthBloc _authBloc;

  factory AuthService() {
    _instance ??= AuthService._internal();
    return _instance!;
  }

  AuthService._internal() {
    _authBloc = I<AuthBloc>();
  }

  Future<http.Response> call(HttpCallback request) async {
    if(!I.isRegistered<L10n>()) await L10n.initialized;
    if (_authBloc.state is AccessRemote) {
      http.Response? response;
      try {
        response = await request(
            headers(accessToken: (_authBloc.state as AccessRemote).accessToken));
        if (response!.statusCode == 403) {
          String? accessToken = await _refreshAccessToken();
          if (accessToken != null) {
            response = await request(headers(accessToken: accessToken));
          } else {
            _authBloc.add(Revoked()); // todo: clear replication data
            throw MessageException({
              'messages': [L10n.current.t8('User.loggedOut.summary')]
            });
          }
        } else if (response.statusCode >= 500) {
          throw MessageException({
            'messages': [L10n.current.t8('Server.error.summary')]
          });
        } else if (response.statusCode >= 300) {
          throw MessageException(AuthService.parseMessages(response));
        }
      } on SocketException {
        throw MessageException({
          'messages': [L10n.current.t8('Connection.error.title')]
        });
      } on http.ClientException {
        throw MessageException({
          'messages': [L10n.current.t8('Connection.error.title')]
        });
      }
      return response!;
    } else {
      throw MessageException({
        'messages': [L10n.current.t8('User.login.required')]
      });
    }
  }

  Future<String?> _refreshAccessToken(/*http.Client client*/) async {
    if (_authBloc.state is! AccessRemote) return null; // RETURN
    var refreshToken = (_authBloc.state as AccessRemote).refreshToken;
    var body = jsonEncode({'refresh': refreshToken});
    var response = await http.post(await replicationUri('/v1/access/refresh/'),
        body: body, headers: headers());
    if (response.statusCode == 200) {
      var accessToken = jsonDecode(response.body)['access'];
      refreshToken = jsonDecode(response.body)['refresh'];
      _authBloc.add(AuthTokenRefreshed(
          accessToken: accessToken, refreshToken: refreshToken));
      return accessToken;
    } else {
      return null;
    }
  }

  static Map<String, String> headers(
      {String? accessToken} /*http.Client client*/) {
    final headers = <String, String>{
      // see https://www.django-rest-framework.org/api-guide/versioning/
      // HttpHeaders.acceptHeader: 'application/json; version=v1',
      HttpHeaders.acceptLanguageHeader: L10n.current.locale.toLanguageTag(),
      HttpHeaders.contentTypeHeader: 'application/json; charset=utf-8'
    };
    if (accessToken != null) {
      headers[HttpHeaders.authorizationHeader] = 'Bearer $accessToken';
    }
    return headers;
  }

  static Map<String, List<String>> parseMessages(Response response) {
    final obj = json.decode(utf8.decode(response.body.codeUnits));
    final Map<String, List<String>> result = <String, List<String>>{};
    for (var key in obj.keys) {
      if (key == 'detail') {
        // simplejwt error message format
        result['messages'] = [obj[key]];
        break;
      } else if (obj[key] is Iterable) {
        Iterable list = obj[key];
        result[key] = list.map((msg) => msg.toString()).toList();
      }
    }
    return result;
  }
}
