import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:repository/repository.dart';

class Queues {
  static final Queues _instance = Queues._internal();

  final String boxName = 'queues';
  late final Box<List<int>> _box;

  factory Queues() {
    return _instance;
  }

  Queues._internal() {
    _loadBox();
  }

  Future<void> _loadBox() async {
    debugPrint('Getting box $boxName');
    _box = await Hive.openBox(boxName);
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, `null` is returned.
  ///
  /// If [defaultValue] is specified, it is returned in case the key does not
  /// exist.
  Set<int> get(Type entityType, {int? accountId}) {
    return Set.from(_box.get(repositoryKey(entityType, id: accountId),
        defaultValue: const <int>[])!);
  }

  /// Saves the [key] - [value] pair.
  Future<void> put(Type entityType, Set<int> value, {int? accountId}) async {
    await _box.put(repositoryKey(entityType, id: accountId), value.toList());
  }

  /// Adds an id to the queue.
  Future<void> add(Type entityType, Set<int> value, {int? accountId}) async {
    final ids = {...get(entityType, accountId: accountId), ...value};
    return put(entityType, ids, accountId: accountId);
  }

  /// Deletes the [key] - [value] pair.
  Future delete(Type entityType, {int? accountId}) async {
    await _box.delete(repositoryKey(entityType, id: accountId));
  }

  Future<int> clear() async {
    return _box.clear();
  }
}
