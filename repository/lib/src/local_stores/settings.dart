import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:logging/logging.dart';

import '../../repository.dart';

class Settings {
  static final _log = Logger('Settings');
  static const CURRENT_RPL_DB_VERSION = 2;
  static const ACCESS_TOKEN = 'access_token';
  static const BIBLE_SERVER = 'bible_server';
  static const REFRESH_TOKEN = 'refresh_token';
  static const CURRENT_ACCOUNT = 'current_account';
  static const DOC_SERVER = 'doc_server';
  static const USER_EMAIL = 'user_email';
  static const IMAGE_SERVER = 'image_server';
  static const INCLUDE_NUMBERS = 'include_numbers';
  static const LEGACY_SERVER = 'legacy_server';
  static const REMINDER_ON = 'reminder_on';
  static const RATING_COUNT = 'rating_count';
  static const RATING_DATE = 'rating_date';
  static const REMINDER_TIME_H = 'reminder_time_h';
  static const REMINDER_TIME_M = 'reminder_time_m';
  static const RPL_DB_VERSION = 'rpl_db_version';
  static const RPL_DEVICE_ID = 'rpl_device_id';
  static const RPL_LAST_RECEIVED = 'rpl_last_received';
  static const RPL_SERVER = 'rpl_server';
  static const SPEECH_SPEED = 'speech_speed';
  static const SPEECH_PAUSE = 'speech_pause';
  static const SPEECH_PLAYLIST = 'speech_playlist';
  static const SPEECH_REPEAT = 'speech_repeat';
  static const SPEECH_MUSIC_OFF = 'speech_music_off';
  static const SPEECH_SCRIPT = 'speech_script';
  static const SPEECH_CAPTIONS = 'speech_captions';
  static const SPEECH_TIMER = 'speech_timer';
  static const SPEECH_VOLUME = 'speech_volume';
  static const STUDY_INITIALS_DEFAULT = 'study_initials_default';
  static const STUDY_PLACEHOLDERS_DEFAULT = 'study_placeholders_default';
  static const STUDY_SPEECH_ON = 'study_speech_on';
  static const STUDY_DICTATION_ON = 'study_dictation_on';
  static const STUDY_VIBRATION_ON = 'study_vibration_on';
  static const STUDY_PUZZLE_SIZE = 'study_puzzle_size';
  static const THEME_MODE = 'theme_mode';
  static const TIP_ACCOUNT_LOCAL = 'tip_account_local';
  static const TIP_COLLECTION_VERSE = 'tip_collection_verse';
  static const TIP_DUE_ACTION = 'tip_due_action';
  static const TIP_DUE_DONE = 'tip_due_done';
  static const TIP_DUE_EMPTY = 'tip_due_empty';
  static const TIP_KNOWN_ACTION = 'tip_known_action';
  static const TIP_KNOWN_EMPTY = 'tip_known_empty';
  static const TIP_NEW_ACTION = 'tip_new_action';
  static const TIP_NEW_EMPTY = 'tip_new_empty';
  static const TIP_TAG_PUBLISH = 'tip_tag_publish';
  static const TIP_TOOLTIP = 'tip_tooltip_on';
  static const TIP_VERSE_FETCH = 'tip_verse_fetch';
  static const TIP_VERSE_FORMAT = 'tip_verse_format';
  static const TIP_VERSE_LANGUAGE = 'tip_verse_language';
  static const TIP_VERSE_SELECT = 'tip_verse_select_on';
  static const TIP_VERSES_RECOVER = 'tip_verses_recover_on';
  static const ZOOM_SCALE_FACTOR = 'zoom_scale_factor';
  static const VERSES_LIST_TIPS = [
    TIP_ACCOUNT_LOCAL,
    TIP_COLLECTION_VERSE,
    TIP_DUE_ACTION,
    TIP_DUE_DONE,
    TIP_DUE_EMPTY,
    TIP_KNOWN_ACTION,
    TIP_KNOWN_EMPTY,
    TIP_NEW_ACTION,
    TIP_NEW_EMPTY,
    TIP_VERSE_SELECT,
  ];
  static const ALL_TIPS = [
    TIP_TAG_PUBLISH,
    TIP_TOOLTIP,
    TIP_VERSE_FETCH,
    TIP_VERSE_FORMAT,
    TIP_VERSE_LANGUAGE,
    TIP_VERSES_RECOVER,
    ...VERSES_LIST_TIPS,
  ];
  static const DEFAULTS = {
    BIBLE_SERVER: 'https://bible.remem.me',
    IMAGE_SERVER: 'https://bible.remem.me',
    LEGACY_SERVER: 'https://my.remem.me',
    RPL_SERVER: 'https://rpl.remem.me',
    DOC_SERVER: 'https://www.remem.me',
  };

  static final Settings _instance = Settings._internal();

  final String _boxName = 'settings';

  factory Settings() {
    return _instance;
  }

  Settings._internal() {
    _init = _loadBox();
  }

  late Future<void> _init;
  Box<String>? _box;

  Future<void> _loadBox() async {
    try {
      _box = await Hive.openBox(_boxName);
    } catch (e) {
      _log.fine('Settings not loaded: $e');
    }
  }

  Future<Settings> init() async {
    await _init;
    return this;
  }

  ValueListenable<Box<String>> listenable({List<String>? keys}) {
    return _box!.listenable(keys: keys);
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, `null` is returned.
  String? get(String key) {
    return _box?.get(key);
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, the default value is returned.
  /// Throws if the [key] is not registered in DEFAULTS.
  String getOrDefault(String key) {
    return get(key) ?? DEFAULTS[key]!;
  }

  /// Saves the [key] - [value] pair.
  /// Can be used synchronously.
  /// (https://docs.hivedb.dev/#/basics/read_write?id=write)
  Future<void> put(String key, String value) {
    return _box != null ? _box!.put(key, value) : Future.value();
  }

  int? getInt(String key) {
    final value = get(key);
    return value != null ? int.parse(value) : null;
  }

  Future<void> putInt(String key, int value) {
    return put(key, value.toString());
  }

  double? getDouble(String key) {
    final value = get(key);
    return value != null ? double.parse(value) : null;
  }

  Future<void> putDouble(String key, double value) {
    return put(key, value.toString());
  }

  bool? getBool(String key) {
    final value = get(key);
    return value != null ? value == 'true' : null;
  }

  Future<void> putBool(String key, bool value) {
    return put(key, value.toString());
  }

  LocalDate? getDate(String key) {
    final raw = get(key);
    return raw != null ? LocalDate.parse(raw) : null;
  }

  Future<void> putDate(String key, LocalDate value) {
    return put(key, value.formatIso());
  }

  Voice? getVoice(Locale locale) {
    final key = 'voice_$locale';
    final name = get(key);
    return name != null ? Voice.parse(name) : null;
  }

  Future<void> putVoice(Locale locale, Voice voice) {
    final key = 'voice_$locale';
    return put(key, voice.toString());
  }

  T? getEnum<T extends Enum>(String key, List<T> values) {
    return values.asNameMap()[get(key)];
  }

  Future<void> putEnum<T extends Enum>(String key, T value) {
    return put(key, value.name);
  }

  /// Deletes the [key] - [value] pair.
  Future<void> delete(String key) {
    return _box != null ? _box!.delete(key) : Future.value();
  }

  /// Retrieves device id or generates it
  int get deviceId {
    var id = getInt(RPL_DEVICE_ID);
    if (id == null) {
      id = IdGenerator().next();
      putInt(RPL_DEVICE_ID, id);
    }
    return id;
  }

  bool get hasNewDbVersion {
    return !kIsWeb &&
        CURRENT_RPL_DB_VERSION > (getInt(Settings.RPL_DB_VERSION) ?? 0);
  }

  Future updateDbVersion() async {
    if (!kIsWeb) {
      await putInt(Settings.RPL_DB_VERSION, Settings.CURRENT_RPL_DB_VERSION);
    }
  }
}
