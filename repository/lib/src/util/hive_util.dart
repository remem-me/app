import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:hive_flutter/hive_flutter.dart';


import '../../repository.dart';

/// Registers Hive adapters
/// Note: Since int keys are limited to 32 bit, convert them to String
///
class HiveUtil {
  static void init(String path) {
    Hive.init(path);
    Hive.registerAdapter<LocalDate>(LocalDateAdapter());     // #0
    Hive.registerAdapter<VerseEntity>(VerseEntityAdapter());   // #1
    Hive.registerAdapter<TagEntity>(TagEntityAdapter());     // #2
    Hive.registerAdapter<AccountEntity>(AccountEntityAdapter()); // #3
    Hive.registerAdapter<ScoreEntity>(ScoreEntityAdapter());   // #4
    Hive.registerAdapter<Locale>(LocaleAdapter());        // #5
    Hive.registerAdapter<Canon>(CanonAdapter());         // #6
    Hive.registerAdapter<FontType>(FontTypeAdapter());      // #7
    Hive.registerAdapter<VerseOrder>(VerseOrderAdapter());    // #8
    Hive.registerAdapter<Color>(ColorAdapter());    // #9
  }
}
