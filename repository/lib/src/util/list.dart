import 'package:collection/collection.dart';

import '../../repository.dart';

abstract class ListUtil {
  static List<T> update<T extends Entity>(
      ({Iterable<T> original, Iterable<T> updated}) items) {
    final updatedMap = {
      for (final item in items.updated) item.id: item,
    };

    final result = <T>[];
    for (final original in items.original) {
      final updated = updatedMap[original.id];
      final item = updated ?? original;
      if (!item.deleted) {
        result.add(item);
      }
    }
    return result;
  }

  static List<T> toSorted<T extends Entity, K extends Comparable<K>>(
      ({Iterable<T> items, K Function(T element)? sortedBy}) args) {
    return args.sortedBy != null
        ? args.items.toSet().sortedBy(args.sortedBy!)
        : args.items.toSet().toList();
  }

  static bool equal<T extends Entity>(Iterable<T> itemsA, Iterable<T> itemsB) {
    if (itemsA.length != itemsB.length) return false;
    final setA = Set<int>.from(itemsA.map((item) => item.id));
    final setB = Set<int>.from(itemsB.map((item) => item.id));
    return setA.length == setB.length && setA.containsAll(setB);
  }
}
