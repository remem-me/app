import 'package:app_core/app_core.dart';
import 'package:repository/repository.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';

class HasReachedGoalArgs {
  final List<ScoreEntity> scores;
  final LocalDate day;
  final int goal;

  HasReachedGoalArgs(this.scores, this.day, this.goal);
}

class DayScore extends Equatable {
  final LocalDate date;
  final int change;

  const DayScore(this.date, this.change);

  DayScore add(int value) {
    return DayScore(date, change + value);
  }

  @override
  List<Object?> get props => [date, change];

  @override
  String toString() {
    return 'DayScore { date: $date, change: $change }';
  }
}

abstract class ScoreUtil {
  static int dayScore(List<ScoreEntity> scores, LocalDate day) {
    return scores
        .where((score) => score.date == day)
        .fold<int>(0, (acc, score) => acc + score.change);
  }

  static bool hasReachedGoal(HasReachedGoalArgs args) {
    return args.goal > 0 && dayScore(args.scores, args.day) >= args.goal;
  }

  static List<DayScore> dayScores(Iterable<ScoreEntity> scores) {
    var result = sortByHighest(scoreDays(scores).values.toList());
    if (result.length > 7) result = result.sublist(0, 7);
    return result;
  }

  static Map<String, DayScore> scoreDays(Iterable<ScoreEntity> scores) {
    final result = <String, DayScore>{};
    for (var score in scores) {
      final key = score.date.formatIso();
      if (result[key] == null) {
        result[key] = DayScore(score.date, score.change);
      } else {
        result[key] = result[key]!.add(score.change);
      }
    }
    return result;
  }

  static List<DayScore> sortByHighest(List<DayScore> entries) {
    return entries.sorted((a, b) => -a.change.compareTo(b.change)).toList();
  }

  /// Keys of scores that are older than a year and not part of a highscore day.
  /// Keeps more unused scores than the replication server (9 days)
  /// because the user might change the device's date
  static List<String> obsoleteScoreKeys(Iterable<ScoreEntity> allScores) {
    final result = <String>[];
    final highScoreDates =
        ScoreUtil.dayScores(allScores).map((dayScore) => dayScore.date);
    final dateLimit = DateService().today.subtractMonths(12);
    for (final score in allScores) {
      if (score.date <= dateLimit && !highScoreDates.contains(score.date)) {
        result.add(score.id.toString());
      }
    }
    return result;
  }
}
