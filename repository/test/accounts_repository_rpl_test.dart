import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:repository/repository.dart';
import 'package:app_core/app_core.dart';

// Mock the dependencies
class MockSettings extends Mock implements Settings {}

class MockEntitiesRepositoryHive extends Mock
    implements EntitiesRepositoryHive<AccountEntity> {}

class MockEntitiesRepositoryHttp extends Mock
    implements EntitiesRepositoryHttp<AccountEntity> {}

class MockQueues extends Mock implements Queues {}

void main() {
  late AccountsRepositoryRpl repository;
  late MockEntitiesRepositoryHive mockHiveRepo;
  late MockEntitiesRepositoryHttp mockHttpRepo;

  setUpAll(() {
    registerFallbackValue(AccountEntity);
    final settings = MockSettings();
    when(() => settings.deviceId).thenReturn(1);
    I.registerSingleton<Settings>(settings);
    final queues = MockQueues();
    when(() => queues.add(any(), any(), accountId: null)).thenAnswer((_) async {});
    I.registerSingleton<Queues>(queues);
    registerFallbackValue(AccountEntity('fallback'));
  });

  setUp(() {
    if (I.isRegistered<EntitiesRepositoryHive<AccountEntity>>()) I.unregister<EntitiesRepositoryHive<AccountEntity>>();
    if (I.isRegistered<EntitiesRepositoryHttp<AccountEntity>>()) I.unregister<EntitiesRepositoryHttp<AccountEntity>>();
    mockHiveRepo = MockEntitiesRepositoryHive();
    mockHttpRepo = MockEntitiesRepositoryHttp();
    when(() => mockHiveRepo.updateList(any())).thenAnswer((_) async => <AccountEntity>[]);
    when(() => mockHiveRepo.entityType).thenReturn(AccountEntity);
    I.registerSingleton<EntitiesRepositoryHive<AccountEntity>>(mockHiveRepo);
    I.registerSingleton<EntitiesRepositoryHttp<AccountEntity>>(mockHttpRepo);
    repository = AccountsRepositoryRpl(onUpdate: () {});
  });

  group('AccountsRepositoryRpl', () {
    test('create should call httpRepo.create and hiveRepo.create', () async {
      final entity = AccountEntity('Test Account', id: 1);
      when(() => mockHttpRepo.create(entity)).thenAnswer((_) async => entity);
      when(() => mockHiveRepo.create(entity)).thenAnswer((_) async => entity);

      final result = await repository.create(entity);

      expect(result, entity);
      verify(() => mockHttpRepo.create(entity)).called(1);
      verify(() => mockHiveRepo.create(entity)).called(1);
    });

    test('update should skip httpRepo.update if name is unchanged', () async {
      final entity = AccountEntity('Test Account', id: 1);
      when(() => mockHiveRepo.read(1)).thenAnswer((_) async => entity);

      final result = await repository.update(entity);

      expect(result.name, entity.name);
      verifyNever(() => mockHttpRepo.update(any()));
      verify(() => mockHiveRepo.read(1)).called(1);
    });

    test(
        'update should call httpRepo.update and hiveRepo.update if name changed',
        () async {
      final entity = AccountEntity('Updated Account', id: 1);
      final prevEntity = AccountEntity('Test Account', id: 1);
      when(() => mockHiveRepo.read(1)).thenAnswer((_) async => prevEntity);
      when(() => mockHttpRepo.update(entity)).thenAnswer((_) async => entity);
      when(() => mockHiveRepo.update(entity)).thenAnswer((_) async => entity);

      final result = await repository.update(entity);

      expect(result, entity);
      verify(() => mockHttpRepo.update(entity)).called(1);
      verify(() => mockHiveRepo.update(entity)).called(1);
    });
  });
}
