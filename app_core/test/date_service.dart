import 'package:flutter_test/flutter_test.dart';
import 'package:app_core/app_core.dart';


void main() {
  group('DateService', () {
    late DateService dateService;

    setUp(() {
      dateService = DateService();
    });

    test('today is the same as LocalDate.today', () {
      final today = LocalDate.today;
      expect(dateService.today, today);
    });

    test('todayStream emits today', () {
      final today = LocalDate.today;
      expect(dateService.todayChangedStream, emits(today));
    });

    test('now returns the current time in milliseconds', () {
      final now = DateTime.now().millisecondsSinceEpoch;
      expect(dateService.now(), now);
    });

    test('daysFromToday returns the difference in days', () {
      final today = LocalDate.today;
      final tomorrow = today + 1;
      expect(dateService.daysFromToday(tomorrow), 1);
    });

    test('durationUntilDayChanged returns the duration until 02:01h', () {
      final duration = dateService.durationUntilDayChanged();
      final now = DateTime.now();
      final targetTime = DateTime(now.year, now.month, now.day, 2, 1);
      if (now.isAfter(targetTime)) {
        targetTime.add(Duration(days: 1));
      }
      final expected = targetTime.difference(now);
      expect(duration, expected);
    });

    test('updateToday updates the todayStream', () {
      final today = LocalDate.today;
      final tomorrow = today + 1;
      dateService.updateToday();
      expect(dateService.todayChangedStream, emits(today));
      dateService.updateToday();
      expect(dateService.todayChangedStream, emits(tomorrow));
    });
  });
}
