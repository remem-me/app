import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:app_core/app_core.dart';

void main() {
  group('Locale Transformations', () {
    final data = <String, Locale>{
      'zh_CN_#Hans': const Locale.fromSubtags(
          languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
      'zh-CN-Hans': const Locale.fromSubtags(
          languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
      'zh_CN_Hans': const Locale.fromSubtags(
          languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
      'zh_Hans_CN': const Locale.fromSubtags(
          languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
      'zh_CN': const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
      'zh_HK': const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant', countryCode: 'HK'),
      'zh_TW': const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant', countryCode: 'TW'),
      'zh_Hans': const Locale.fromSubtags(
        languageCode: 'zh',
        scriptCode: 'Hans',
      ),
      'zh': const Locale.fromSubtags(languageCode: 'zh'),
    };
    data.forEach((input, expected) {
      test("Should parse locales correctly: $input", () {
        expect(TextUtil.toLocale(input), expected);
      });
    });
  });
}
