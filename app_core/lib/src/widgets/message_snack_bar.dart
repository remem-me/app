import 'package:flutter/material.dart';

class MessageSnackBar extends StatelessWidget {
  final String message;

  const MessageSnackBar(this.message, {super.key});

  @override
  Widget build(BuildContext context) {
    return SnackBar(content: Text(message));
  }
}
