const MAX_INT = 9007199254740991; // pow(2, 53) - 1;
const MS_PER_DAY = 24 * 60 * 60 * 1000;
const WEB_URL = 'https://web.remem.me';
const APP_LOCALES = [
  'en',  // fallback locale
  'bg',
  'cs',
  'de',
  'es',
  'fr',
  'hu',
  'it',
  'ja',
  'ko',
  'nb', // Norwegian Bokmål
  'nl',
  'no', // Norwegian
  'pl',
  'pt',
  'ro',
  'ru',
  'sv',
  'uk',
  'ur', // Urdu
  'zh_Hans',
  'zh_Hant',
];
const REFERENCE_DEFAULTS = {
  'af': '1Joh 3:16-18',
  'ar': 'يو 3:16',
  'bg': '1Йн 3:16-18',
  'bm': '1 Yuhana 3:16-18',
  'cs': '1Jan 3:16-18',
  'da': '1Joh 3,16-18',
  'de': '1Joh 3,16-18',
  'en': '1Jn 3:16-18',
  'es': '1Jn 3:16-18',
  'fr': '1Jn 3:16-18',
  'he': 'יוחנן א 3:16',
  'hu': '1Jn 3:16-18',
  'id': '1Yo 3:16-18',
  'it': '1Gv 3:16-18',
  'ja': '一ヨハ3:16-18',
  'ko': '1요한3:16-18',
  'nb': '1Joh 3:16-18',
  'nl': '1Joh 3:16-18',
  'pl': '1J 3:16-18',
  'pt': '1Jo 3:16-18',
  'ro': '1In 3:16-18',
  'ru': '1Ин 3:16-18',
  'sv': '1Joh 3:16-18',
  'uk': '1Ів 3:16-18',
  'ur': 'یوحنا 3:16',
  'vi': '1Gi 3:16-18',
  'zh_Hans_CN': '约一 3:16-18',  // Simplified Chinese / Mandarin
  'zh_Hant_HK': '約一 3:16-18',  // Traditional Chinese / Cantonese
  'zh_Hant_TW': '約一 3:16-18',  // Traditional Chinese / Mandarin
};
const BIBLE_DEFAULTS = {
  'af': 'NLV',
  'am': 'AMB', // Amharic
  'ar': 'NAV',
  'bg': 'BPB', //Bulgarian
  'bm': 'BAMLSB',
  'cs': 'CEP',
  'da': 'BPH',
  'de': 'LUT',
  'el': 'SBL',
  'en': 'NIV',
  'es': 'NVIE',
  'fi': 'FB92',
  'fr': 'BDS',
  'ha': 'HAU', // Hausa
  'he': 'HHH',
  'hr': 'CKK',
  'hu': 'RUF',
  'id': 'BIMK',
  'ig': 'IGBOB', // Igbo
  'it': 'CEI',
  'ja': 'JAS',
  'ko': 'GAE',
  'la': 'LVB', // Latin
  'lt': 'LBD', //Lithuanian
  'mk': 'MKB',
  'ml': 'ML', // Malayalam
  'my': 'JBMLE', // Burmese
  'nb': 'N11BM',
  'nl': 'NBV',
  'pl': 'BW75',
  'pt': 'ARA',
  'ro': 'VDC',
  'ru': 'SRP2',
  'sr': 'SBERV',
  'sv': 'B2000',
  'sw': 'SUV', // Swahili
  'ta': 'TM', // Tamil
  'te': 'TEL', // Telugu
  'th': 'THSV11', // Thai
  'ti': 'TIB', // Tigrinya
  'tl': 'TLAB', // Tagalog
  'uk': 'CUB',
  'ur': 'URD', // Urdu
  'vi': 'NVB',
  'yo': 'YCE', // Yoruba
  'zh_Hans_CN': 'CNVS',  // Simplified Chinese / Mandarin
  'zh_Hant_HK': 'CNV',   // Traditional Chinese / Cantonese
  'zh_Hant_TW': 'CNV',   // Traditional Chinese / Mandarin
};
const RTL_LANGUAGES = [
  'ar',  // Arabic
  'dv',  // Divehi
  'fa',  // Persian
  'ha',  // Hausa
  'he',  // Hebrew
  'ks',  // Kashmiri
  'ku',  // Kurdish
  'ps',  // Pushto
  'sd',  // Sindhi
  'ur',  // Urdu
  'yi',  // Yiddish
];

