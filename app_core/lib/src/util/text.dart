import 'dart:ui';

import 'const.dart';
import 'package:collection/collection.dart';

class TextUtil {
  static final appLocales = APP_LOCALES.map((code) => TextUtil.toLocale(code));
  static final referenceLocales =
      REFERENCE_DEFAULTS.keys.map((code) => TextUtil.toLocale(code));
  static final bibleLocales =
      BIBLE_DEFAULTS.keys.map((code) => TextUtil.toLocale(code));

  static Locale toLocale(String code) {
    final segments = code.split(RegExp(r'[-_]#?'));
    final languageCode =
        segments.firstWhere((segment) => RegExp(r'^[a-z]+$').hasMatch(segment));
    final scriptCode = segments.firstWhereOrNull(
        (segment) => RegExp(r'^[A-Z][a-z]+$').hasMatch(segment));
    final countryCode = segments
        .firstWhereOrNull((segment) => RegExp(r'^[A-Z]+$').hasMatch(segment));
    return Locale.fromSubtags(
        languageCode: languageCode,
        scriptCode: scriptCode ??
            (countryCode == 'CN'
                ? 'Hans'
                : ['HK', 'TW'].contains(countryCode)
                    ? 'Hant'
                    : null),
        countryCode: countryCode);
  }

  static TextDirection textDirection(Locale locale) {
    return RTL_LANGUAGES.contains(locale.languageCode)
        ? TextDirection.rtl
        : TextDirection.ltr;
  }

  static Locale? appLocale(Locale intended) {
    return matchingLocale(appLocales, intended);
  }

  static Locale? bibleLocale(Locale intended) {
    return matchingLocale(bibleLocales, intended);
  }

  static Locale? referenceLocale(Locale intended) {
    return matchingLocale(referenceLocales, intended);
  }

  static Locale? matchingLocale(Iterable<Locale> locales, Locale intended) {
    if (['nn', 'no'].contains(intended.languageCode)) {
      intended = Locale('nb', intended.countryCode);
    }
    return locales.firstWhereOrNull(
            (locale) => localeScriptEqual(locale, intended)) ??
        locales.firstWhereOrNull(
            (locale) => languageScriptEqual(locale, intended)) ??
        locales.firstWhereOrNull((locale) => localeEqual(locale, intended)) ??
        locales.firstWhereOrNull((locale) => languageEqual(locale, intended));
  }

  static bool localeScriptEqual(Locale? a, Locale b) {
    return a != null &&
        a.languageCode == b.languageCode &&
        a.scriptCode == b.scriptCode &&
        a.countryCode == b.countryCode;
  }

  static bool languageScriptEqual(Locale? a, Locale b) {
    return a != null &&
        a.languageCode == b.languageCode &&
        a.scriptCode == b.scriptCode;
  }

  static bool localeEqual(Locale? a, Locale b) {
    return a != null &&
        a.languageCode == b.languageCode &&
        a.countryCode == b.countryCode;
  }

  static bool languageEqual(Locale? a, Locale b) {
    return a != null && a.languageCode == b.languageCode;
  }
}
