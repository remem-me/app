import 'package:flutter/material.dart';

class Flat {
  static const List<Color> wheel = [
    indigo,
    blue,
    azure,
    cyan,
    spring,
    green,
    chartreuse,
    amber,
    orange,
    scarlet,
    red,
    pink,
    purple,
  ];

  static const Color indigo = Color(0xFF6778c8);
  static const Color purple = Color(0xFF9B59B6);
  static const Color pink = Color(0xFFc15279);
  static const Color red = Color(0xFFE74C3C);
  static const Color scarlet = Color(0xFFe6652f);
  static const Color orange = Color(0xFFE67E22);
  static const Color amber = Color(0xFFeba118);
  static const Color chartreuse = Color(0xFF8fc840);
  static const Color green = Color(0xFF2ECC71);
  static const Color spring = Color(0xFF24c486);
  static const Color cyan = Color(0xFF1ABC9C);
  static const Color azure = Color(0xFF27aabb);
  static const Color blue = Color(0xFF3498DB);
  static const Color silver = Color(0xFFECF0F1);
  static const Color grey = Color(0xFF95A5A6);
  static const Color asphalt = Color(0xFF34495E);
}

class FlatDark {
  static const List<Color> wheel = [
    indigo,
    blue,
    azure,
    cyan,
    spring,
    green,
    chartreuse,
    amber,
    orange,
    scarlet,
    red,
    pink,
    purple,
  ];

  static const Color indigo = Color(0xFF5b62b3);
  static const Color purple = Color(0xFF8E44AD);
  static const Color pink = Color(0xFFa73e6c);
  static const Color red = Color(0xFFC0392B);
  static const Color scarlet = Color(0xFFc94615);
  static const Color orange = Color(0xFFD35400);
  static const Color amber = Color(0xFFe37809);
  static const Color chartreuse = Color(0xFF8da539);
  static const Color green = Color(0xFF27AE60);
  static const Color spring = Color(0xFF1ea772);
  static const Color cyan = Color(0xFF16A085);
  static const Color azure = Color(0xFF1f909f);
  static const Color blue = Color(0xFF2980B9);
  static const Color silver = Color(0xFFBDC3C7);
  static const Color grey = Color(0xFF7F8C8D);
  static const Color asphalt = Color(0xFF2C3E50);
  static const Color tar = Color(0xFF151515);
}

class AppColor {
  static const Color dark = Color(0xFFA30131);
  static const Color base = Color(0xFFCA0039);
  static const Color light = Color(0xFFFF6666);
}

class AppColorScheme {

  static final light = ColorScheme.fromSeed(
    seedColor: FlatDark.grey,
    dynamicSchemeVariant: DynamicSchemeVariant.neutral,
    brightness: Brightness.light,
    primary: AppColor.dark,
    primaryFixed: AppColor.dark,
    secondary: FlatDark.amber,
    secondaryFixed: FlatDark.amber,
    tertiary: FlatDark.indigo,
    tertiaryFixed: FlatDark.indigo,
    error: FlatDark.indigo,
  );

  static final dark = ColorScheme.fromSeed(
    seedColor: Flat.grey,
    dynamicSchemeVariant: DynamicSchemeVariant.neutral,
    brightness: Brightness.dark,
    primary: AppColor.light,
    primaryFixed: AppColor.dark,
    secondary: Flat.amber,
    secondaryFixed: FlatDark.amber,
    tertiary: Flat.indigo,
    tertiaryFixed: FlatDark.indigo,
    error: Flat.indigo,
  );
}