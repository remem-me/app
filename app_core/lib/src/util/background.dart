import 'package:flutter/material.dart';

abstract class BackgroundUtil {

  static Widget darkGradientLayer({required AlignmentGeometry begin, required AlignmentGeometry end}) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: const [
                Color(0x66331100),
                Color(0x55001122),
                Color(0x77000022)
              ],
              begin: begin,
              end: end,
            )));
  }

  static Widget lightGradientLayer({required AlignmentGeometry begin, required AlignmentGeometry end}) {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: const [
                Color(0x55330000),
                Color(0x33000011),
                Color(0x55000011)
              ],
              begin: begin,
              end: end,
            )));
  }
}