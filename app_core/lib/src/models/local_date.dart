import 'dart:ui';

import 'package:intl/intl.dart';

/// Represents a date without a time zone.
class LocalDate implements Comparable<LocalDate> {  final int epochDay;

  LocalDate(int year, int month, int day)
      : epochDay =
            DateTime(year, month, day).difference(DateTime(1970, 1, 1)).inDays;

  LocalDate.epoch(this.epochDay);

  // parses a string in the format 'yyyy-MM-dd' to a LocalDate
  LocalDate.parse(String isoString)
      : epochDay =
            DateTime.parse(isoString).difference(DateTime(1970, 1, 1)).inDays;

  LocalDate operator +(int days) => LocalDate.epoch(epochDay + days);

  LocalDate operator -(int days) => LocalDate.epoch(epochDay - days);

  LocalDate subtractMonths(int months) {
    final date = toDateTime();
    final newDate = DateTime(date.year, date.month - months, date.day);
    return LocalDate(newDate.year, newDate.month, newDate.day);
  }

  LocalDate addMonths(int months) {
    final date = toDateTime();
    final newDate = DateTime(date.year, date.month + months, date.day);
    return LocalDate(newDate.year, newDate.month, newDate.day);
  }

  int diff(LocalDate other) => epochDay - other.epochDay;

  bool operator <(LocalDate other) => epochDay < other.epochDay;
  bool operator >(LocalDate other) => epochDay > other.epochDay;
  bool operator <=(LocalDate other) => epochDay <= other.epochDay;
  bool operator >=(LocalDate other) => epochDay >= other.epochDay;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LocalDate &&
          runtimeType == other.runtimeType &&
          epochDay == other.epochDay;

  static LocalDate get today {
    final now = DateTime.now();
    final epoch = DateTime(1970, 1, 1).add(Duration(hours: 2));
    final diff = now.difference(epoch).inDays;
    return LocalDate.epoch(diff);
  }

  @override
  int get hashCode => epochDay.hashCode;

  // converts the LocalDate to a DateTime of the current timezone
  DateTime toDateTime() {
    return DateTime(1970, 1, 1).add(Duration(days: epochDay));
  }

  @override
  String toString() {
    return 'LocalDate { epochDay: $epochDay }';
  }

  // converts the LocalDate to a string in the format 'yyyy-MM-dd'
  String formatIso() {
    return toDateTime().toIso8601String().substring(0, 10);
  }

String formatVerbose(LocalDate date, Locale locale) {
  // Format the date using DateFormat.yMMMMd (e.g., "17 de julio de 2015")
  return DateFormat.yMMMMd(locale.toLanguageTag()).format(date.toDateTime());
}

String formatDayOfWeek(Locale locale) {
  // Format the day of the week using DateFormat.EEEE (e.g., "viernes")
  return DateFormat.EEEE(locale.toLanguageTag()).format(toDateTime());
}


  @override
  int compareTo(LocalDate other) => epochDay.compareTo(other.epochDay);
}
