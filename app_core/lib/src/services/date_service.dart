import 'package:rxdart/rxdart.dart';

import '../../app_core.dart';

class DateService {
  static final _instance = DateService._internal();

  late BehaviorSubject<LocalDate> _todaySubject;

  factory DateService() {
    return _instance;
  }

  DateService._internal() {
    _todaySubject = BehaviorSubject.seeded(today);
  }

  /// Today's date with an offset of 2h
  LocalDate get today => LocalDate.today;

  Stream<LocalDate> get todayChangedStream {
    return _todaySubject.stream.skip(1);
  }

  int now() {
    return DateTime.now().millisecondsSinceEpoch;
  }

  int daysFromToday(LocalDate date) {
    return date.diff(today);
  }

  /// Duration until next 02:01h
  Duration durationUntilDayChanged() {
    DateTime now = DateTime.now();
    DateTime targetTime = DateTime(now.year, now.month, now.day, 2, 1);
    if (now.isAfter(targetTime)) {
      targetTime = targetTime.add(Duration(days: 1));
    }
    return targetTime.difference(now);
  }

  void updateToday() {
    if (_todaySubject.value != today) {
      _todaySubject.add(today);
    }
  }
}
