library;

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

export 'src/widgets/widgets.dart';
export 'src/services/message_service.dart';
export 'src/services/date_service.dart';
export 'src/models/models.dart';
export 'src/l10n.dart';
export 'src/util/util.dart';

final I = GetIt.instance;
final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

