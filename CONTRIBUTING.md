## Contributing

First off, thank you for considering contributing to Remember Me. This is a great 
way to serve your sisters and brothers in Christ.

### Where do I go from here?

If you've noticed a bug or have a question that doesn't belong on the
[user forum][], [search the issue tracker][] to see if
someone else in the community has already created a ticket. If not, go ahead and
[make one][new issue]!

### Fork & create a branch

If this is something you think you can fix, then [fork Remember Me][]'s develop branch and
create a branch with a descriptive name.

A good branch name would be (where issue #325 is the ticket you're working on):

```sh
git checkout -b feature/325-add-japanese-translations
```

If it is a simple hotfix, you may fork the master branch, e.g. 
```sh
git checkout -b hotfix/326-add-missing-translation-of-word
```
Remember Me follows the [Gitflow workflow][].

### Run/build the web app
Use Flutter's [HTML renderer] (instead of the default CanvasKit) to prevent CORS issues with images
```
flutter run -d chrome --web-renderer html
flutter build web --web-renderer html   (and move svg folder to build/web/assets/svg)
```

### Get the test suite running

You can run the tests through the following command, detailed testing instructions will follow later.

`flutter test`

### Generate test coverage
Test coverage is automatically generated on each build in bitrise and can be found on [codecov]

### Generate code documentation
You can generate html code documentation with dartdoc, on windows use the following command

`%FLUTTER_INSTALL_FOLDER%\bin\cache\dart-sdk\bin\dartdoc.bat`

### Did you find a bug?

* **Ensure the bug was not already reported** by [searching all issues][].

* If you're unable to find an open issue addressing the problem,
  [open a new one][new issue]. Be sure to include a **title and clear
  description**, as much relevant information as possible, and a **code sample**
  or an **executable test case** demonstrating the expected behavior that is not
  occurring.

### Implement your fix or feature

At this point, you're ready to make your changes! Feel free to ask for help;
everyone is a beginner at first :smile_cat:

### Get the style right

Your patch should follow the same conventions & pass the same code quality
checks as the rest of the project. 

### Make a Pull Request

At this point, you should switch back to your develop branch and make sure it's
up to date with Remember Me's develop branch:

```sh
git remote add upstream git@gitlab.com:remem-me/flutter.git
git checkout develop
git pull upstream develop
```

Then update your feature branch from your local copy of develop, and push it!

```sh
git checkout feature/325-add-japanese-translations
git rebase develop
git push --set-upstream origin feature/325-add-japanese-translations
```

Finally, go to GitLab and [make a Pull Request][] :D

CI will run our test suite. We care
about quality, so your PR won't be merged until all tests pass. 

### Keeping your Pull Request updated

If a maintainer asks you to "rebase" your PR, they're saying that a lot of code
has changed, and that you need to update your branch so it's easier to merge.

To learn more about rebasing in Git, there are a lot of [good resources][git rebasing] 
but here's the suggested workflow:

```sh
git checkout feature/325-add-japanese-translations
git pull --rebase upstream develop
git push --force-with-lease feature/325-add-japanese-translations
```


[HTML renderer]: https://flutter.dev/docs/development/tools/web-renderers
[user forum]: https://groups.google.com/forum/#!forum/rememberapp
[search the issue tracker]: https://gitlab.com/remem-me/app/issues
[new issue]: https://gitlab.com/remem-me/app/issues/new
[fork Remember Me]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork
[searching all issues]: https://gitlab.com/remem-me/app/issues
[make a pull request]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream
[git rebasing]: http://git-scm.com/book/en/Git-Branching-Rebasing
[interactive rebase]: https://help.github.com/articles/interactive-rebase
[codecov]: https://codecov.io/gl/remem-me/app
[Gitflow workflow]: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow