import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockBloc extends Mock implements EntitiesBloc<VerseEntity, Verse> {
  @override
  Stream<EntitiesState<VerseEntity>> get stream => const Stream.empty();
}

class MockHive extends Mock implements EntitiesRepositoryHive<VerseEntity> {}

class MockHttp extends Mock implements EntitiesRepositoryHttp<VerseEntity> {}

class MockRpl extends Mock implements EntitiesRepositoryRpl<VerseEntity> {}

class FakeVerseEntity extends Fake implements VerseEntity {}

void main() {
  late CurrentAccount currentAccount;
  late EntitiesRepositoryHive<VerseEntity> repoHive;
  late EntitiesRepositoryHttp<VerseEntity> repoHttp;
  late EntitiesRepositoryRpl<VerseEntity> repoRpl;
  late EntitiesBloc<VerseEntity, Verse> bloc;

  group('receiveWhenReady', () {
    setUpAll(() {
      registerFallbackValue(const EntitiesUpdated<Verse>([]));
      registerFallbackValue(const EntitiesAdded<VerseEntity>([]));
      registerFallbackValue(FakeVerseEntity());
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(1);
      I.registerSingleton<Settings>(settings);
      currentAccount = MockCurrentAccount();
      when(() => currentAccount.langRef).thenReturn(const Locale('en'));
      I.registerSingleton<CurrentAccount>(currentAccount);
      when(() => settings.deviceId).thenReturn(1);
    });

    setUp(() {
      bloc = MockBloc();
      repoHttp = MockHttp();
      repoHive = MockHive();
      repoRpl = MockRpl();
      when(() => repoRpl.httpRepo).thenReturn(repoHttp);
      when(() => repoRpl.hiveRepo).thenReturn(repoHive);
      when(() => bloc.repository).thenReturn(repoRpl);
      List<VerseEntity> dataHttp = [
        VerseEntity(
          'ref',
          'http',
          modified: 3,
          id: 1,
          accountId: 1,
        )
      ];
      VerseEntity dataHive = VerseEntity(
        'ref',
        'hive',
        modified: 2,
        id: 1,
        accountId: 1,
      );
      Verse model = Verse(
        'ref',
        'hive',
        modified: 2,
        id: 1,
        accountId: 1,
      );
      when(() => currentAccount.id).thenReturn(1);
      when(() => repoHttp.readList(
          deleted: null,
          since: 1,
          client: any(named: 'client'))).thenAnswer((_) async => dataHttp);
      when(() => repoHive.read(1, accountId: 1))
          .thenAnswer((_) async => dataHive);
      when(() => repoHive.read(2, accountId: 1)).thenAnswer((_) async => null);
      when(() => repoHive.update(any())).thenAnswer((_) async => dataHive);
      when(() => repoHive.delete(any())).thenAnswer((_) async {});
      when(() => bloc.toModels(captureAny()))
          .thenAnswer((inv) async => [model]);
    });

    test('All entities should be added if bloc state is EntitiesLoadFailure',
        () async {
      when(() => bloc.state)
          .thenReturn(const EntitiesLoadFailure<VerseEntity>('test'));
      final replicator = Replicator<VerseEntity, Verse>(VerseEntity, bloc);
      await replicator.receiveWhenReady(1, 1);
      final captured = verify(() => bloc.add(captureAny())).captured;
      expect(captured.length, equals(1));
      expect(captured.last, isA<EntitiesAdded<VerseEntity>>());
    });

    test('All entities should be updated if bloc state is EntitiesLoadSuccess',
        () async {
      when(() => bloc.state).thenReturn(const EntitiesLoadSuccess<Verse>([]));
      final replicator = Replicator<VerseEntity, Verse>(VerseEntity, bloc);
      await replicator.receiveWhenReady(1, 1);
      final captured = verify(() => bloc.add(captureAny())).captured;
      expect(captured.length, equals(1));
      expect(captured.last, isA<EntitiesUpdated<Verse>>());
    });

    test('Deleted entities should be deleted', () async {
      List<VerseEntity> dataHttp = [
        VerseEntity(
          'ref1',
          'http',
          deleted: true,
          modified: 3,
          id: 1,
          accountId: 1,
        ),
        VerseEntity(
          'ref2',
          'http',
          deleted: true,
          modified: 3,
          id: 2,
          accountId: 1,
        ),
      ];
      when(() => repoHttp.readList(
          deleted: null,
          since: 1,
          client: any(named: 'client'))).thenAnswer((_) async => dataHttp);
      when(() => bloc.state).thenReturn(const EntitiesLoadSuccess<Verse>([]));
      when(() => bloc.toModels(captureAny()))
          .thenAnswer((inv) async => [Verse.fromEntity(dataHttp.first)]);
      final replicator = Replicator<VerseEntity, Verse>(VerseEntity, bloc);
      await replicator.receiveWhenReady(1, 1);
      verify(() => repoHive.delete(any())).called(1);
      final captured = verify(() => bloc.add(captureAny())).captured;
      expect(captured.length, equals(1));
      expect(captured.last, isA<EntitiesUpdated<Verse>>());
      expect((captured.last as EntitiesUpdated<Verse>).entities.first.deleted,
          true);
    });
  });
}
