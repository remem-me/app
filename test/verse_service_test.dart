import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:remem_me/util/verse_util.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';


class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockAuthBloc extends Mock implements AuthBloc {}

void main() {
  group('VerseService', () {
    // todo: wrap plugin flutter_native_timezone
    // https://docs.flutter.dev/development/packages-and-plugins/plugins-in-tests

    setUpAll(() {
      TestWidgetsFlutterBinding.ensureInitialized();
      final authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      when(() => authBloc.state).thenReturn(const AccessRemote(
          accessToken: 'accessToken', refreshToken: 'refreshToken'));
      I.registerSingleton<AuthBloc>(authBloc);
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      when(() => settings.put(captureAny(), captureAny())).thenAnswer((_) async {});
      I.registerSingleton<Settings>(settings);
      I.registerSingleton<CurrentAccount>(CurrentAccount());
    });

    test('Verse should be committed', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: null,
        review: null,
        level: -1,
        accountId: 0,
      );
      I<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().committed(data);
      expect(result.commit, DateService().today);
      expect(result.review, null);
      expect(result.due, DateService().today);
      expect(result.level, 0);
    });

    test('Verse should be forgotten', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      I<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().forgotten(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today);
      expect(result.level, 0);
    });

    test('Verse should be remembered', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 0,
        accountId: 0,
      );
      I<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today + 1);
      expect(result.level, 1);
    });

    test('Due date and level should advance until limit', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 2,
        accountId: 0,
      );
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          reviewLimit: 6));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today + 4);
      expect(result.level, 3);
    });

    test('Due date should not advance beyond limit', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          reviewLimit: 6));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today + 6);
      expect(result.level, 4);
    });

    test('Level should not advance beyond limit', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 4,
        accountId: 0,
      );
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          reviewLimit: 6));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today + 6);
      expect(result.level, 4);
    });

    test('Should respect reviewLimit of 1 day', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 0,
        accountId: 0,
      );
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          reviewLimit: 1));
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today);
      expect(result.due, DateService().today + 1);
      expect(result.level, 1);
    });

    test('Verse should be moved to due', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      I<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().movedToDue(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today - 2);
      expect(result.due, DateService().today);
      expect(result.level, 2);
    });

    test('Verse should be moved to due with review limit', () async {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 4,
        accountId: 0,
      );
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
      reviewLimit: 6));
      Verse result = VerseService().movedToDue(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, DateService().today - 6);
      expect(result.due, DateService().today);
      expect(result.level, 4);
    });

    test('Verse should be moved to new', () {
      Verse data = Verse(
        'reference',
        'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
        accountId: 0,
      );
      I<CurrentAccount>().setAccount(Account('Test'));
      Verse result = VerseService().movedToNew(data);
      expect(result.commit, null);
      expect(result.review, null);
      expect(result.due, null);
      expect(result.level, -1);
    });

    test('Max due should correspond to limit', () {
      final due = VerseUtil.due(
          level: 7,
          review: LocalDate(2021, 1, 1),
          reviewLimit: 10,
          reviewFrequency: I<CurrentAccount>().reviewFrequency);
      expect(due, LocalDate(2021, 1, 11));
    });

    test('Spoken reference', () {
      I<CurrentAccount>().setAccount(Account('Test'));
      expect(
          VerseService().spokenPassage(
              '1\ntest,\ntest: \ntest \nend', const Locale('en')),
          '1;\ntest,\ntest: \ntest;\nend');
    });

    test('Rank should match eastern canon', () async {
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          canon: Canon.eastern, langRef: const Locale('bg')));
      Verse v1 = Verse(
        'Варух 1:1',
        'passage',
        commit: null,
        review: null,
        level: -1,
        accountId: 0,
      );
      List<Verse> data = [
        v1,
        v1.copyWith(reference: 'вар1:1'),
        v1.copyWith(reference: 'Трета Макавейска 1:1')
      ];
      final result = await VerseService().fromEntities(data);
      expect(result[0].rank, 32001001);
      expect(result[1].rank, 32001001);
      expect(result[2].rank, 49001001);
    });

    test('Rank should match protestant canon', () async {
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          canon: Canon.protestant, langRef: const Locale('de')));
      Verse v1 = Verse(
        'Baruch 1:1',
        'passage',
        commit: null,
        review: null,
        level: -1,
        accountId: 0,
      );
      List<Verse> data = [v1, v1.copyWith(reference: 'bar1:1')];
      final result = await VerseService().fromEntities(data);
      expect(result[0].rank, 45001001);
      expect(result[1].rank, 45001001);
    });

    test('Missing book should be ignored', () async {
      await I<CurrentAccount>().setEntity(AccountEntity('Test',
          canon: Canon.protestant, langRef: const Locale('bg')));
      Verse v1 = Verse(
        '3Мак 1:1',
        'passage',
        commit: null,
        review: null,
        level: -1,
        accountId: 0,
      );
      List<Verse> data = [v1];
      final result = await VerseService().fromEntities(data);
      expect(result[0].rank, 1001);
    });
  });
}
