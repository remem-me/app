import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/config/config.dart';
import 'package:remem_me/services/rating_service.dart';
import 'package:repository/repository.dart';
import 'package:store_checker/store_checker.dart';

class MockSettings extends Mock implements Settings {}

void main() {
  group('RatingService', () {
    late MockSettings settings;

    setUpAll(() {
      registerFallbackValue(DateService().today);
      TestWidgetsFlutterBinding.ensureInitialized();
    });

    setUp(() {
      if (I.isRegistered<Settings>()) {
        I.unregister<Settings>();
      }
      settings = MockSettings();
      I.registerSingleton<Settings>(settings);
    });

    test('isReadyForRating() should be false if count not defined', () async {
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      expect(service.isReadyForRating(), false);
    });

    test('isReadyForRating() should be true if count is 0', () async {
      when(() => settings.getInt(Settings.RATING_COUNT)).thenReturn(0);
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      expect(service.isReadyForRating(), true);
    });

    test('isRatingDone() should be false if no date set', () async {
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      expect(service.isRatingDone(), false);
    });

    test('isRatingDone() should be false if date long ago', () async {
      when(() => settings.getDate(Settings.RATING_DATE))
          .thenReturn(DateService().today.subtractMonths(13));
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      expect(service.isRatingDone(), false);
    });

    test('isRatingDone() should be true if date recent', () async {
      when(() => settings.getDate(Settings.RATING_DATE))
          .thenReturn(DateService().today.subtractMonths(6));
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      expect(service.isRatingDone(), true);
    });

    test('countDownToRating() should modify count setting', () async {
      when(() => settings.putInt(Settings.RATING_COUNT, any()))
          .thenAnswer((_) => Future.value());
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      await service.countDownToRating();
      verify(() => settings.putInt(Settings.RATING_COUNT, 39)).called(1);
    });

    test('countDownToRating() should not modify 0 count', () async {
      when(() => settings.getInt(Settings.RATING_COUNT)).thenReturn(0);
      when(() => settings.putInt(Settings.RATING_COUNT, any()))
          .thenAnswer((_) => Future.value());
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      await service.countDownToRating();
      verifyNever(() => settings.putInt(Settings.RATING_COUNT, any()));
    });

    test('markRatingDone() should modify date setting', () async {
      when(() => settings.putDate(Settings.RATING_DATE, any()))
          .thenAnswer((_) => Future.value());
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      await service.markRatingDone();
      verify(() => settings.putDate(Settings.RATING_DATE, DateService().today))
          .called(1);
    });

    test('uriStore() should return the beta store URI', () async {
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      var uri = await service.uriStore();
      expect(uri.toString(), 'market://details?id=me.remem.app');
    });

    test('uriStore() should return the prod store URI', () async {
      AppConfig.appFlavor = Flavor.prod;
      var service =
          RatingService(Future.value(Source.IS_INSTALLED_FROM_PLAY_STORE));
      var uri = await service.uriStore();
      expect(uri.toString(), 'market://details?id=org.bible.remember_me');
    });
  });
}
