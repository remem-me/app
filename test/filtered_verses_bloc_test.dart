import 'package:app_core/app_core.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:test/test.dart';
import 'package:bloc_test/bloc_test.dart';

class MockSettings extends Mock implements Settings {}

class MockHomeWidgetService extends Mock implements HomeWidgetService {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockTaggedVersesBloc extends Mock implements TaggedVersesBloc {}

class MockTagsBloc extends Mock implements TagsBloc {}

void main() {
  late List<Verse> testVerses;
  late List<Tag> testTags;
  late TaggedVersesBloc versesBloc;

  setUpAll(() {
    final settings = MockSettings();
    when(() => settings.deviceId).thenReturn(0);
    I.registerSingleton<Settings>(settings);
    final homeWidgetService = MockHomeWidgetService();
    when(() => homeWidgetService.update(any()))
        .thenAnswer((_) => Future.value(false));
    I.registerSingleton<HomeWidgetService>(homeWidgetService);
    I.registerSingleton<CurrentAccount>(CurrentAccount());

    testVerses = [
      Verse(
        'ref3',
        'pas3',
        id: 3,
        modified: 0,
        accountId: 1,
      ),
      Verse(
        'ref1',
        'pas1',
        id: 1,
        modified: 0,
        accountId: 1,
        review: DateService().today,
        tags: const {1: 'tag1'},
      ),
      Verse(
        'ref2',
        'pas2',
        id: 2,
        modified: 0,
        accountId: 1,
        tags: const {2: 'tag2'},
      ),
    ];

    testTags = [
      Tag(
        'tag1',
        id: 1,
        modified: 0,
        accountId: 1,
        included: false,
        published: false,
        size: 1,
      ),
      Tag(
        'tag2',
        id: 2,
        modified: 0,
        accountId: 1,
        included: true,
        published: false,
        size: 1,
      ),
    ];
    versesBloc = MockTaggedVersesBloc();
    when(() => versesBloc.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<TaggedVersesBloc>(versesBloc);
  });

  group('Filter by tags and query', () {
    setUpAll(() {
      when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
        verses: testVerses,
        tags: testTags,
      ));
    });

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'emits [FilteredVersesLoadSuccess.isUpdating] when FilteredVersesReloaded is added',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(FilteredVersesReloaded()),
      expect: () =>
      [
        FilteredVersesSuccess(
          testVerses,
          date: DateService().today,
          queryFilter: const QueryFilter(),
          isUpdating: true,
        ),
        FilteredVersesSuccess(
          [
            Verse(
              'ref2',
              'pas2',
              id: 2,
              modified: 0,
              accountId: 1,
              tags: const {2: 'tag2'},
            ),
          ],
          date: DateService().today,
          queryFilter: const QueryFilter(),
          isUpdating: false,
        ),
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'does not filter by tag if search query is active',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const SearchFilterUpdated('pas3')),
      expect: () =>
      [
        FilteredVersesSuccess(
          [
            Verse(
              'ref3',
              'pas3',
              id: 3,
              modified: 0,
              accountId: 1,
            ),
          ],
          date: DateService().today,
          queryFilter: const QueryFilter('pas3'),
        ),
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'filtered only by tag in state FilterInProgress',
      build: () => FilteredVersesBloc(),
      seed: () => FilterInProgress(),
      act: (bloc) => bloc.add(const SearchFilterUpdated('pas3')),
      expect: () =>
      [
        FilteredVersesSuccess(
          [testVerses[2]],
          date: DateService().today,
          queryFilter: const QueryFilter(),
        ),
      ],
    );
  });

  group('Filter by tags and reviewed today', () {
    setUpAll(() {
      when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
        verses: testVerses,
        tags: const [],
      ));
    });

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'filtered by tags includes verses reviewed today ',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(true)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [
            Verse(
              'ref1',
              'pas1',
              id: 1,
              modified: 0,
              accountId: 1,
              review: DateService().today,
              tags: const {1: 'tag1'},
            )
          ],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(true),
        ),
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'filtered by tags excludes verses reviewed today ',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(false)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [
            Verse(
              'ref3',
              'pas3',
              id: 3,
              modified: 0,
              accountId: 1,
            ),
            Verse(
              'ref2',
              'pas2',
              id: 2,
              modified: 0,
              accountId: 1,
              tags: const {2: 'tag2'},
            ),
          ],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(false),
        ),
      ],
    );
  });

  group('Filter by reviewed today', () {
    setUpAll(() {
      when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
        verses: testVerses,
        tags: [
          Tag(
            'tag1',
            id: 1,
            modified: 0,
            accountId: 1,
            included: null,
            published: false,
            size: 1,
          ),
          Tag(
            'tag2',
            id: 2,
            modified: 0,
            accountId: 1,
            included: true,
            published: false,
            size: 1,
          ),
        ],
      ));
    });

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'includes verses reviewed today ',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(true)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [testVerses[1], testVerses[2]],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(true),
        ),
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'excludes verses reviewed today ',
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(false)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [testVerses[2]],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(false),
        ),
      ],
    );
  });


  group('Reviewed today filter removal', () {

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'removes reviewed today filter if no verses reviewed today',
      setUp: () {
        when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
          verses: testVerses.sublist(0, 1),
          tags: testTags,
        ));
      },
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(true)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(null),
        ),
      ],
    );

    blocTest<FilteredVersesBloc, FilteredVersesState>(
      'keeps reviewed today filter if verses reviewed today',
      setUp: () {
        when(() => versesBloc.state).thenReturn(TaggedVersesSuccess(
          verses: testVerses,
          tags: [],
        ));
      },
      build: () => FilteredVersesBloc(),
      act: (bloc) => bloc.add(const ReviewedTodayFilterUpdated(true)),
      expect: () =>
      [
        FilteredVersesSuccess(
          [testVerses[1]],
          date: DateService().today,
          reviewedTodayFilter: const ReviewedTodayFilter(true),
        ),
      ],
    );
  });
}
