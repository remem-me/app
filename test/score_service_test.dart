import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

void main() {
  group('ScoreService', () {
    late List<ScoreEntity> testScores;
    late LocalDate testDate;
    late String testKey;

    setUpAll(() {
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      testKey = '2014-12-02';
      testDate = LocalDate.parse(testKey);
      final baseScore = ScoreEntity(
          vkey: 'v1',
          date: testDate,
          accountId: 1,
          change: 1,
          id: 1,
          modified: 0,
          modifiedBy: 0);
      testScores = [
        baseScore.copyWith(),
        baseScore.copyWith(vkey: 'v2', id: 2, change: 2),
        baseScore.copyWith(id: 3, date: testDate + 1),
        baseScore.copyWith(id: 4, date: testDate + 2),
        baseScore.copyWith(id: 5, date: testDate + 3),
        baseScore.copyWith(id: 6, date: testDate + 4),
        baseScore.copyWith(id: 7, date: testDate + 5),
        baseScore.copyWith(id: 8, date: testDate + 6),
        baseScore.copyWith(id: 9, date: testDate + 7),
        baseScore.copyWith(id: 11, date: DateService().today - 2),
      ];
      //{  vkey: Psalm 16,8-11LUT,  date: Tuesday, 02 December 2014, change: 69, account: 998, id: 78503425843733, modified: 1639904742723, modifiedBy: 380628903367170, deleted: false }]
    });

    test('scoreDays() should add scores', () async {
      Map<String, DayScore> result = ScoreUtil.scoreDays(testScores);
      expect(result[testKey], DayScore(testDate, 3));
    });

    test('obsoleteScoreKeys() should list obsolete score keys', () async {
      final result = ScoreUtil.obsoleteScoreKeys(testScores);
      expect(result, ['9']);
    });
  });
}
