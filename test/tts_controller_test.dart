import 'dart:async';
import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_controller.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockTtsService extends Mock implements TtsService {}
class MockVerseService extends Mock implements VerseService {}

class MockTranscriptionCubit extends MockCubit<Transcription?>
    implements TranscriptionCubit {}

final List<Verse> testVerses = [
  Verse('r0', 'p0', topic: 't1', commit: null, review: null, level: -1, accountId: 0),
  Verse('r1', 'p1', commit: null, review: null, level: -1, accountId: 0),
  Verse('r2', 'p2', commit: null, review: null, level: -1, accountId: 0),
  Verse('r3', 'p3', commit: null, review: null, level: -1, accountId: 0),
];

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  late MockTtsService ttsService;
  late MockVerseService verseService;
  late MockCurrentAccount currentAccount;
  late MockSettings settings;
  late MockTranscriptionCubit transcriptionCubit;

  setUp(() {
    if(I.isRegistered<Settings>()) I.unregister<Settings>();
    if(I.isRegistered<CurrentAccount>()) I.unregister<CurrentAccount>();
    if(I.isRegistered<TtsService>()) I.unregister<TtsService>();
    if(I.isRegistered<VerseService>()) I.unregister<VerseService>();
    if(I.isRegistered<TranscriptionCubit>()) I.unregister<TranscriptionCubit>();
    registerFallbackValue(const Locale('en'));
    ttsService = MockTtsService();
    verseService = MockVerseService();
    currentAccount = MockCurrentAccount();
    settings = MockSettings();
    transcriptionCubit = MockTranscriptionCubit();
    when(() => settings.deviceId).thenReturn(0);
    when(() => settings.getBool(any())).thenReturn(true);
    when(() => settings.getDouble(any())).thenReturn(1.0);
    when(() => currentAccount.language).thenReturn(const Locale('en'));
    when(() => currentAccount.langRef).thenReturn(const Locale('en'));
    when(() => currentAccount.topicPreferred).thenReturn(true);
    when(() => currentAccount.referenceIncluded).thenReturn(true);
    when(() => transcriptionCubit.state).thenReturn(null);
    when(() => ttsService.registerCompleteHandler(any(), any()))
        .thenAnswer((_) async {});
    when(() => ttsService.registerCancelHandler(any(), any()))
        .thenAnswer((_) async {});
    when(() => ttsService.stop()).thenAnswer((_) async {});
    when(() => ttsService.speakText(any(), any(), any(), isResumed: any(named: 'isResumed')))
        .thenAnswer((_) async {});
    when(() => verseService.spokenReference(any())).thenAnswer((_) => Future.value('spoken reference'));
    I.registerSingleton<Settings>(settings);
    I.registerSingleton<CurrentAccount>(currentAccount);
    I.registerSingleton<TtsService>(ttsService);
    I.registerSingleton<VerseService>(verseService);
    I.registerSingleton<TranscriptionCubit>(transcriptionCubit);
  });

  group('TtsController', () {
    late TtsController controller;

    setUp(() async {
      controller = TtsController();
    });

    test('initial state is Speaking.completed', () {
      expect(controller.state, Speaking.completed);
    });

    group('speakVerse', () {
      test('should call TtsService.speakText with correct parameters', () async {
        controller.speakVerse(testVerses[0]);
        verify(() => ttsService.speakText(
            'home',
            testVerses[0].topic!,
            const Locale('en'),
            isResumed: false)).called(1);
      });

      test('should emit Speaking.started', () async {
        expectLater(controller.stream, emits(Speaking.started));
        controller.speakVerse(testVerses[0]);
      });

      test('should emit Speaking.started when resumed', () async {
        controller.silence = Timer(const Duration(seconds: 1), () {});
        controller.speakVerse(testVerses[0]);
        expectLater(controller.stream, emitsInOrder([Speaking.paused, Speaking.started]));
        await controller.pause();
        controller.speakVerse(testVerses[0]);
      });
    });

    group('stop', () {
      test('should call TtsService.stop', () async {
        await controller.stop();
        verify(() => ttsService.stop()).called(1);
      });
    });

    group('pause', () {
      test('should call TtsService.stop', () async {
        await controller.pause();
        verify(() => ttsService.stop()).called(1);
      });
    });

    group('complete handler', () {
      test('should call _proceed', () async {
        controller.speakVerse(testVerses[0]);
        await Future.delayed(const Duration(milliseconds: 100));
        verify(() => ttsService.registerCompleteHandler('home', any())).called(1);
        verify(() => ttsService.speakText('home', 't1', const Locale('en'), isResumed: false)).called(1);
      });
    });

    group('cancel handler', () {
      test('should call _proceed', () async {
        controller.speakVerse(testVerses[0]);
        await Future.delayed(const Duration(milliseconds: 100));
        await controller.stop();
        verify(() => ttsService.registerCancelHandler('home', any())).called(1);
        verify(() => ttsService.stop()).called(1);
      });
    });

    group('stages', () {
      test('should return correct stages', () {
        expect(controller.stages, equals([
          Stage.topic,
          Stage.breather,
          Stage.reference,
          Stage.breather,
          Stage.passage,
          Stage.breather,
          Stage.reference,
          Stage.rest,
        ]));
      });

      test('should return correct stages when topicPreferred is false', () {
        when(() => currentAccount.topicPreferred).thenReturn(false);
        expect(controller.stages, equals([
          Stage.reference,
          Stage.breather,
          Stage.passage,
          Stage.breather,
          Stage.reference,
          Stage.rest,
        ]));
      });

      test('should return correct stages when referenceIncluded is false', () {
        when(() => currentAccount.referenceIncluded).thenReturn(false);
        expect(controller.stages, equals([
          Stage.topic,
          Stage.breather,
          Stage.reference,
          Stage.breather,
          Stage.passage,
          Stage.rest,
        ]));
      });
    });

    group('speakCurrentStage', () {
      test('should call _speakReference when stage is Stage.reference', () async {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.progress = 2;
        controller.speakCurrentStage(false);
        verify(() => verseService.spokenReference('r0')).called(1);
      });

      test('should call _speakPassage when stage is Stage.passage', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.progress = 4;
        controller.speakCurrentStage(false);
        verify(() => ttsService.speakText(
            'home',
            testVerses[0].passage,
            const Locale('en'),
            isResumed: false)).called(1);
      });

      test('should call _speakShortSilence when stage is Stage.breather', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.progress = 1;
        controller.speakCurrentStage(false);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });

      test('should call _speakCustomSilence when stage is Stage.rest', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.progress = 7;
        controller.speakCurrentStage(false);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });
    });

    group('_speakReference', () {
      test('should call TtsService.speakText with correct parameters', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakReference(false);
        verify(() => verseService.spokenReference('r0')).called(1);
      });
    });

    group('_speakPassage', () {
      test('should call TtsService.speakText with correct parameters', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakPassage(false);
        verify(() => ttsService.speakText(
            'home',
            testVerses[0].passage,
            const Locale('en'),
            isResumed: false)).called(1);
      });

      test('should call TranscriptionCubit.flash with correct parameters', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakPassage(false);
        verify(() => transcriptionCubit.flash(
            Transcription(testVerses[0].passage, mode: TranscriptionMode.script)))
            .called(1);
      });
    });

    group('_speakTopic', () {

      test('should call _proceed when topic is empty', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakTopic(false);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });
    });

    group('_speakShortSilence', () {
      test('should call _speakSilence with correct parameters', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakShortSilence(false);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });
    });

    group('_speakCustomSilence', () {
      test('should call _speakSilence with correct parameters', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakCustomSilence(false);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });
    });

    group('_speakSilence', () {
      test('should call _proceed when isResumed is true', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakSilence(true, 1.0);
        verifyNever(() => ttsService.speakText(any(), any(), any(),
            isResumed: any(named: 'isResumed')));
      });

      test('should start a timer when isResumed is false', () {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakSilence(false, 1.0);
        expect(controller.silence, isNotNull);
      });

      test('should call _proceed when timer completes', () async {
        controller.verse = testVerses[0].copyWith(topic: '');
        controller.speakSilence(false, 1.0);
        await Future.delayed(const Duration(seconds: 1));
        verify(() => ttsService.speakText('home', 'spoken reference', const Locale('en'),
            isResumed: false));
      });
    });
  });
}