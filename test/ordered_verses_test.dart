import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';


class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockOrderedVersesBloc extends Mock implements OrderedVersesBloc {}

class MockScoresBloc extends Mock implements ScoresBloc {}

class MockSelectionBloc extends Mock implements SelectionBloc {}

class MockTtsService extends Mock implements TtsService {}

class MockTtsHandler extends Mock implements TtsHandler {}

void main() {
  late MockSettings settings;
  late MockScoresBloc scoresBloc;
  late MockOrderedVersesBloc versesBloc;
  late MockSelectionBloc selectionBloc;

  List<Verse> verses() => [
        Verse('r1', 'p1', id: 1, modified: 0, accountId: 1),
      ];

  Widget boilerplate(Widget child) => MultiBlocProvider(providers: [
        BlocProvider<OrderedVersesBloc>.value(value: versesBloc),
        BlocProvider<SelectionBloc>.value(value: selectionBloc),
      ], child: MaterialApp(home: Scaffold(body: child)));

  setUpAll(() {
    // dependency injection
    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    I.registerSingleton<L10n>(l10n);

    final currentAccount = MockCurrentAccount();
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.topicPreferred).thenReturn(false);
    when(() => currentAccount.dailyGoal).thenReturn(1);
    I.registerSingleton<CurrentAccount>(currentAccount);

    final ttsService = MockTtsService();
    I.registerSingleton<TtsService>(ttsService);

    final ttsHandler = MockTtsHandler();
    when(() => ttsHandler.playbackState).thenAnswer((_) => BehaviorSubject());
    when(() => ttsHandler.mediaProcessing).thenAnswer((_) => NeverStream());
    I.registerSingleton<TtsHandler>(ttsHandler);


    // context
    selectionBloc = MockSelectionBloc();
    when(() => selectionBloc.state).thenReturn(const SelectionState({}));
    when(() => selectionBloc.stream).thenAnswer((_) => NeverStream());
  });

  setUp(() {
    // dependency injection
    if (I.isRegistered<Settings>()) I.unregister<Settings>();
    settings = MockSettings();
    when(() => settings.getBool(any())).thenReturn(false);
    when(() => settings.getBool(Settings.TIP_VERSE_SELECT)).thenReturn(true);
    when(() => settings.putBool(any(), any())).thenAnswer((_) async {});
    when(() => settings.deviceId).thenReturn(1);
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(MockBox()));
    I.registerSingleton<Settings>(settings);

    if (I.isRegistered<ScoresBloc>()) I.unregister<ScoresBloc>();
    scoresBloc = MockScoresBloc();
    I.registerSingleton<ScoresBloc>(scoresBloc);

    // context
    versesBloc = MockOrderedVersesBloc();
    when(() => versesBloc.state)
        .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
    when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
          const OrderSuccess(VerseOrder.ALPHABET, []),
        ]));
  });

  group('Box NEW', () {
    setUp(() {
      when(() => settings.getBool(Settings.TIP_NEW_EMPTY)).thenReturn(true);
      when(() => settings.getBool(Settings.TIP_NEW_ACTION)).thenReturn(true);
      when(() => versesBloc.box).thenReturn(VerseBox.NEW);
      when(() => scoresBloc.goalReachedStream)
          .thenAnswer((_) => Stream.fromIterable([DateService().today]));
    });

    testWidgets('Shows Tip.new.action', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, verses()),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.new.action');
      final text2Finder = find.text('Tip.verse.select');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsExactly(2));
      expect(text1Finder, findsOneWidget);
      expect(text2Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_NEW_ACTION, false)).called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, false))
          .called(1);
    });

    testWidgets('Shows Tip.new.empty', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            const OrderSuccess(VerseOrder.ALPHABET, []),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.new.empty');
      expect(verseFinder, findsNothing);
      expect(tipFinder, findsOneWidget);
      expect(text1Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_NEW_EMPTY, false)).called(1);
    });
  });

  group('Box DUE goal not reached', () {
    setUp(() {
      when(() => settings.getBool(Settings.TIP_DUE_EMPTY)).thenReturn(true);
      when(() => settings.getBool(Settings.TIP_DUE_ACTION)).thenReturn(true);
      when(() => versesBloc.box).thenReturn(VerseBox.DUE);
      when(() => scoresBloc.goalReachedStream)
          .thenAnswer((_) => Stream.fromIterable([LocalDate(2010, 3, 24)]));
    });

    testWidgets('Shows Tip.due.action', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, verses()),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.due.action');
      final text2Finder = find.text('Tip.verse.select');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsExactly(2));
      expect(text1Finder, findsOneWidget);
      expect(text2Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_DUE_ACTION, false)).called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, false))
          .called(1);
    });

    testWidgets('Shows Tip.due.empty', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            const OrderSuccess(VerseOrder.ALPHABET, []),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.due.empty');
      expect(verseFinder, findsNothing);
      expect(tipFinder, findsOneWidget);
      expect(text1Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_DUE_EMPTY, false)).called(1);
    });
  });

  group('Box DUE goal reached', () {
    setUp(() {
      when(() => settings.getBool(Settings.TIP_DUE_DONE)).thenReturn(true);
      when(() => versesBloc.box).thenReturn(VerseBox.DUE);
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, verses()),
          ]));
    });

    testWidgets('Shows Tip.due.done', (tester) async {
      when(() => scoresBloc.goalReachedStream)
          .thenAnswer((_) => Stream.fromIterable([
                LocalDate(2010, 3, 24),
                DateService().today,
              ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.due.done');
      final text2Finder = find.text('Tip.verse.select');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsExactly(2));
      expect(text1Finder, findsOneWidget);
      expect(text2Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_DUE_DONE, false)).called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, false))
          .called(1);
      verify(() => settings.getBool(Settings.TIP_DUE_DONE)).called(3);
    });

    testWidgets('Hides Tip.due.done on date change', (tester) async {
      when(() => scoresBloc.goalReachedStream)
          .thenAnswer((_) => Stream.fromIterable([
                DateService().today,
                LocalDate(2010, 3, 24),
              ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.due.done');
      final text2Finder = find.text('Tip.verse.select');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsOneWidget);
      expect(text1Finder, findsNothing);
      expect(text2Finder, findsOneWidget);
      verify(() => settings.getBool(Settings.TIP_DUE_DONE)).called(3);
    });
  });

  group('Box KNOWN', () {
    setUp(() {
      when(() => settings.getBool(Settings.TIP_KNOWN_EMPTY)).thenReturn(true);
      when(() => settings.getBool(Settings.TIP_KNOWN_ACTION)).thenReturn(true);
      when(() => versesBloc.box).thenReturn(VerseBox.KNOWN);
      when(() => scoresBloc.goalReachedStream)
          .thenAnswer((_) => Stream.fromIterable([DateService().today]));
    });

    testWidgets('Shows Tip.known.action', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, verses()),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.known.action');
      final text2Finder = find.text('Tip.verse.select');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsExactly(2));
      expect(text1Finder, findsOneWidget);
      expect(text2Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_KNOWN_ACTION, false))
          .called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, false))
          .called(1);
    });

    testWidgets('Shows Tip.known.empty', (tester) async {
      when(() => versesBloc.stream).thenAnswer((_) => Stream.fromIterable([
            const OrderSuccess(VerseOrder.ALPHABET, []),
          ]));
      await tester.pumpWidget(boilerplate(const OrderedVerses()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SelectableVerseTile);
      final tipFinder = find.byType(TipTile);
      final text1Finder = find.text('Tip.known.empty');
      expect(verseFinder, findsNothing);
      expect(tipFinder, findsOneWidget);
      expect(text1Finder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_KNOWN_EMPTY, false)).called(1);
    });
  });
}
