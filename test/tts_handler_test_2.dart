import 'dart:async';

import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_controller.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/default_art.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockDefaultArt extends Mock implements DefaultArt {}

class MockTtsController extends MockCubit<Speaking> implements TtsController {}

final List<Verse> testVerses = [
  Verse('r0', 'p0', commit: null, review: null, level: -1, accountId: 0),
  Verse('r1', 'p1', commit: null, review: null, level: -1, accountId: 0),
  Verse('r2', 'p2', commit: null, review: null, level: -1, accountId: 0),
  Verse('r3', 'p3', commit: null, review: null, level: -1, accountId: 0),
];

void main() {
  late MockTtsController controller;
  late MockDefaultArt defaultArt;
  late StreamController<Speaking> speaking;

  group('play', () {

    setUpAll(() {
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      // when(() => settings.getInt(any())).thenReturn(0);
      // when(() => settings.putInt(any(), any())).thenAnswer((_) async {});
      I.registerSingleton<Settings>(settings);
      defaultArt = MockDefaultArt();
      when(() => defaultArt.init).thenAnswer((_) async {});
      when(() => defaultArt.uri).thenAnswer((_) => Uri());
    });

    setUp(() {
      TestWidgetsFlutterBinding.ensureInitialized();
      controller = MockTtsController();
      when(() => controller.stop()).thenAnswer((_) async {});
      speaking = StreamController<Speaking>();
      whenListen(controller, speaking.stream, initialState: Speaking.completed);
    });

    test('TtsHandler should skip verse', () async {
      final ttsHandler =
          TtsHandler(controller: controller, defaultArt: defaultArt);
      await ttsHandler.init;
      ttsHandler.setUp(testVerses);
      expectLater(
          controller.stream,
          emitsInOrder([
            Speaking.cancelled,
            Speaking.started,
          ])).then((_) {
        verify(() => controller.speakVerse(testVerses[0])).called(1);
        verify(() => controller.speakVerse(testVerses[1])).called(1);
        verifyNever(() => controller.speakVerse(testVerses[2]));
        expect(ttsHandler.index, equals(1));
      });
      await ttsHandler.play();
      await ttsHandler.skipToNext();
      speaking.sink.add(Speaking.cancelled);
      await Future.delayed(const Duration(milliseconds: 500), () {});
      speaking.sink.add(Speaking.started);
    });

  });
}
