import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockSettings extends Mock implements Settings {}
class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockAuthBloc extends Mock implements AuthBloc {}
class MockUndoBloc extends Mock implements UndoBloc {}
class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockAccountsRepository extends Mock implements AccountsRepository {}
//class MockTts extends Mock implements AccountsRepository {}

void main() {
  group('AccountsBloc', () {
    late AccountsBloc accountsBloc;
    late List<AccountEntity> testAccounts;
    late MockAuthBloc authBloc;

    setUpAll(() {
      TestWidgetsFlutterBinding.ensureInitialized();

      authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      I.registerSingleton<AuthBloc>(authBloc);

      final orderedVersesDueBloc = MockOrderedVersesDueBloc();
      when(() => orderedVersesDueBloc.stream).thenAnswer((_) => Stream.fromIterable([OrderFailure()]));
      when(() => orderedVersesDueBloc.completed).thenReturn((_) => true);
      I.registerSingleton<OrderedVersesDueBloc>(orderedVersesDueBloc);

      final replicationBloc = MockReplicationBloc();
      when(() => replicationBloc.stream).thenAnswer((_) => NeverStream());
      when(() => replicationBloc.state).thenReturn(ReplicationInitial());
      I.registerSingleton<ReplicationBloc>(replicationBloc);

      final connectivityCubit = MockConnectivityCubit();
      when(() => connectivityCubit.stream).thenAnswer((_) => NeverStream());
      I.registerSingleton<ConnectivityCubit>(connectivityCubit);

      // final currentAccount = MockCurrentAccount();
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      when(() => settings.put(captureAny(), captureAny())).thenAnswer((_) async {});
      I.registerSingleton<Settings>(settings);


      final currentAccount = MockCurrentAccount();
      when(() => currentAccount.selected()).thenAnswer((_) => NeverStream());
      when(() => currentAccount.dailyGoal).thenReturn(0);
      when(() => currentAccount.id).thenReturn(0);
      I.registerSingleton<CurrentAccount>(currentAccount);

      //di.registerSingleton<CurrentAccount>(CurrentAccount());
      I.registerSingleton<EntitiesRepository<AccountEntity>>(
          MockAccountsRepository());
      I.registerSingleton<UndoBloc>(MockUndoBloc());
      testAccounts = [
        AccountEntity('acc1', id: 1, modified: 0),
      ];
    });

    setUp(() {
      accountsBloc = AccountsBloc(EntitiesLoadSuccess(testAccounts));
    });

    blocTest<AccountsBloc, EntitiesState<AccountEntity>>(
      'emits [EntitiesLoadSuccess()] when account is added',
      build: () => accountsBloc,
      act: (bloc) => bloc.add(EntitiesAdded(
          [AccountEntity('acc2', id: 2, modified: 0)],
          persist: false)),
      expect: () => [
        EntitiesLoadSuccess(
          [
            AccountEntity('acc1', id: 1, modified: 0),
            AccountEntity('acc2', id: 2, modified: 0)
          ],
        ),
      ],
    );

    blocTest<AccountsBloc, EntitiesState<AccountEntity>>(
      'ignores when identical account is added',
      build: () => accountsBloc,
      act: (bloc) => bloc.add(EntitiesAdded(
          [AccountEntity('acc1', id: 1, modified: 0)],
          persist: false)),
      expect: () => [
        EntitiesLoadSuccess(
          [
            AccountEntity('acc1', id: 1, modified: 0),
          ],
        ),
      ],
    );

    blocTest<AccountsBloc, EntitiesState<AccountEntity>>(
      'emits InitialEntitiesState when auth changes to NoAuth',
      build: () => accountsBloc,
      act: (bloc) {
        when(() => authBloc.stream).thenAnswer((_) => Stream.value(const NoAuth()));
        bloc.subscribeToAuthChange();
      },
      expect: () => [
        const InitialEntitiesState<AccountEntity>(),
      ],
    );
  });
}
