import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:repository/repository.dart';


class MockSettings extends Mock implements Settings {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockCollectionService extends Mock implements CollectionService {}

class MockMessageService extends Mock implements MessageService {}

void main() {
  final LocalDate initialDate = LocalDate(2010, 3, 24);

  group('CollectionsBloc', () {
    late MockCollectionService service;
    late MockCurrentAccount currentAccount;

    setUpAll(() {
      registerFallbackValue(const Query());
      TestWidgetsFlutterBinding.ensureInitialized();
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      service = MockCollectionService();
      when(() => service.fetchCollections(any()))
          .thenAnswer((_) => Future.value([]));
      when(() => service.hasReachedMax(any())).thenReturn(false);
      I.registerSingleton<CollectionService>(service);
    });

    setUp(() {
      if (I.isRegistered<CurrentAccount>()) I.unregister<CurrentAccount>();
      currentAccount = MockCurrentAccount();
      when(() => currentAccount.selected()).thenAnswer((invocation) =>
          Stream.fromIterable(
              [Account.fromEntity(AccountEntity('test', id: 1))]));
      when(() => currentAccount.id).thenReturn(-1);
      I.registerSingleton<CurrentAccount>(currentAccount);
      if (I.isRegistered<MessageService>()) I.unregister<MessageService>();
    });

    blocTest<CollectionsBloc, CollectionsState>(
      'emits [CollectionsState(fetchStatus: Status.done)] on empty initiation',
      build: () => CollectionsBloc(),
      act: (bloc) => bloc.add(CollectionsInitiated(const Query())),
      wait: const Duration(milliseconds: 500),
      expect: () => [
        const CollectionsState(),
        const CollectionsState(fetchStatus: Status.done),
      ],
    );

    blocTest<CollectionsBloc, CollectionsState>(
      'search sets searchbar to visible',
      build: () => CollectionsBloc(),
      act: (bloc) => bloc.add(CollectionsInitiated(
        const Query(search: 'search'),
      )),
      wait: const Duration(milliseconds: 500),
      expect: () => [
        const CollectionsState(query: Query(search: 'search')),
        const CollectionsState(query: Query(search: 'search'), fetchStatus: Status.done),
      ],
    );

    blocTest<CollectionsBloc, CollectionsState>(
      'closing search sets searchbar to invisible',
      build: () => CollectionsBloc(initialState: const CollectionsState(query: Query(search: 'search'))),
      act: (bloc) {
        bloc.add(CollectionsInitiated(const Query(search: null)));
      },
      wait: const Duration(milliseconds: 500),
      expect: () => [
        const CollectionsState(query: Query(search: null)),
        const CollectionsState(query: Query(search: null), fetchStatus: Status.done),
      ],
    );
  });
}
