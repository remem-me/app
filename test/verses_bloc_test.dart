import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/home_widget_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockSettings extends Mock implements Settings {}

class MockHomeWidgetService extends Mock implements HomeWidgetService {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockVersesRepository extends Mock implements VersesRepository {}

class MockAuthBloc extends Mock implements AuthBloc {}


void main() {
  group('VersesBloc', () {
    late VersesBloc versesBloc;
    late List<Verse> testVerses;

    setUpAll(() {
      TestWidgetsFlutterBinding.ensureInitialized();
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      final authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      I.registerSingleton<AuthBloc>(authBloc);
      final homeWidgetService = MockHomeWidgetService();
      when(() => homeWidgetService.update(any()))
          .thenAnswer((_) => Future.value(false));
      I.registerSingleton<HomeWidgetService>(homeWidgetService);
      I.registerSingleton<CurrentAccount>(CurrentAccount());
      final versesRepository = MockVersesRepository();
      I.registerSingleton<EntitiesRepository<VerseEntity>>(versesRepository);

      testVerses = [
        Verse(
          'ref3',
          'pas3',
          id: 3,
          modified: 0,
          accountId: 1,
          review: DateService().today,
          level: 2,
          due: DateService().today,
        ),
        Verse(
          'ref1',
          'pas1',
          id: 1,
          modified: 0,
          accountId: 1,
        ),
        Verse(
          'ref2',
          'pas2',
          id: 2,
          modified: 0,
          accountId: 1,
        ),
      ];
    });

    setUp(() {
      versesBloc = VersesBloc();
    });

    blocTest<VersesBloc, EntitiesState<VerseEntity>>(
      'emits updated [VersesLoadSuccess] when AccountModified is added',
      build: () {
        versesBloc =
            VersesBloc(initialState: EntitiesLoadSuccess<Verse>(testVerses));
        return versesBloc;
      },
      act: (bloc) => bloc.add(AccountModified()),
      expect: () => [
        EntitiesLoadSuccess<Verse>(
          testVerses,
          true,
        ),
        EntitiesLoadSuccess<Verse>(
          [
            Verse(
              'ref3',
              'pas3',
              id: 3,
              modified: 0,
              accountId: 1,
              rank: 3000,
              review: DateService().today,
              level: 2,
              due: DateService().today + 2,
            ),
            Verse(
              'ref1',
              'pas1',
              id: 1,
              modified: 0,
              accountId: 1,
              rank: 1000,
            ),
            Verse(
              'ref2',
              'pas2',
              id: 2,
              modified: 0,
              accountId: 1,
              rank: 2000,
            ),
          ],
          false,
        ),
      ],
    );
  });
}
