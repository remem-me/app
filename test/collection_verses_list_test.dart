import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collection.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

// Mock dependencies
class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

void main() {
  late MockSettings settings;
  late MockNavigationCubit nav;
  late MockBox mockBox;

  setUpAll(() {
    registerFallbackValue(CollectionsPath());
  });

  setUp(() {
    settings = MockSettings();
    when(() => settings.getBool(any())).thenReturn(false);
    when(() => settings.putBool(any(), any())).thenAnswer((_) async {});
    when(() => settings.deviceId).thenReturn(1);
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(MockBox()));
    I.registerSingleton<Settings>(settings);

    nav = MockNavigationCubit();
    when(() => nav.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<NavigationCubit>(nav);

    mockBox = MockBox();

    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    when(() => l10n.t8(any(), any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    when(() => l10n.locale).thenReturn(const Locale('en'));
    I.registerSingleton<L10n>(l10n);

    final currentAccount = MockCurrentAccount();
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.topicPreferred).thenReturn(false);
    when(() => currentAccount.dailyGoal).thenReturn(1);
    I.registerSingleton<CurrentAccount>(currentAccount);
  });

  tearDown(() {
    I.reset();
  });

  group('CollectionVersesList', () {
    late Collection testCollection;

    setUp(() {
      testCollection = Collection(
        1,
        'coll1',
        'coll1',
        verses: [
          Verse('ref1', 'pas1', id: 1),
          Verse('ref2', 'pas2', id: 2),
        ],
        description: '',
        publisher:
            Publisher(1, 'pub', language: Locale('en'), langRef: Locale('en')),
        created: DateTime.now(),
      );
    });

    testWidgets('renders verses and tips', (WidgetTester tester) async {
      // Arrange
      when(() => settings.getBool(Settings.TIP_COLLECTION_VERSE))
          .thenReturn(true);
      when(() => settings.listenable(keys: any(named: 'keys')))
          .thenReturn(ValueNotifier(mockBox));
      //when(() => mockBox.get(Settings.VERSES_LIST_TIPS)).thenReturn(true);

      // Act
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<NavigationCubit>.value(
            value: nav,
            child: Material(
                child: CollectionVersesList(collection: testCollection)),
          ),
        ),
      );

      // Assert
      expect(find.byType(TipTile), findsOneWidget);
      expect(find.byType(SimpleVerseTile), findsNWidgets(2));
      expect(find.text('ref1'), findsOneWidget);
      expect(find.text('ref2'), findsOneWidget);
    });

    testWidgets('does not render tip when setting is false',
        (WidgetTester tester) async {
      // Arrange
      when(() => settings.getBool(Settings.TIP_COLLECTION_VERSE))
          .thenReturn(false);
      when(() => settings.listenable(keys: any(named: 'keys')))
          .thenReturn(ValueNotifier(mockBox));

      // Act
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<NavigationCubit>.value(
            value: nav,
            child: Material(
                child: CollectionVersesList(collection: testCollection)),
          ),
        ),
      );

      // Assert
      expect(find.byType(TipTile), findsNothing);
      expect(find.byType(SimpleVerseTile), findsNWidgets(2));
    });

    testWidgets('navigates to flashcard when verse is tapped',
        (WidgetTester tester) async {
      // Arrange
      when(() => settings.getBool(Settings.TIP_COLLECTION_VERSE))
          .thenReturn(true);
      when(() => settings.listenable(keys: any(named: 'keys')))
          .thenReturn(ValueNotifier(mockBox));
      when(() => nav.go(any(), propagates: true)).thenReturn(null);

      // Act
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<NavigationCubit>.value(
            value: nav,
            child: Material(
                child: CollectionVersesList(collection: testCollection)),
          ),
        ),
      );
      await tester.pumpAndSettle();
      await tester.tap(find.text('ref1'));

      // Assert
      verify(() => nav.go(
            CollectionFlashcardPath(id: 1, verseId: 1),
            propagates: true,
          )).called(1);
    });

    testWidgets('renders empty list when collection has no verses',
        (WidgetTester tester) async {
      // Arrange
      final emptyCollection = Collection(
          2, 'Empty Collection', 'Empty Collection',
          verses: [],
          description: '',
          publisher: Publisher(1, 'pub',
              language: Locale('en'), langRef: Locale('en')),
          created: DateTime.now());
      when(() => settings.getBool(Settings.TIP_COLLECTION_VERSE))
          .thenReturn(true);
      when(() => settings.listenable(keys: any(named: 'keys')))
          .thenReturn(ValueNotifier(mockBox));

      // Act
      await tester.pumpWidget(
        MaterialApp(
          home: BlocProvider<NavigationCubit>.value(
            value: nav,
            child: Material(
                child: CollectionVersesList(collection: emptyCollection)),
          ),
        ),
      );

      // Assert
      expect(find.byType(TipTile), findsNothing);
      expect(find.byType(SimpleVerseTile), findsNothing);
    });
  });
}
