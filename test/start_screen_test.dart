import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/screens/screens.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:remem_me/widgets/start/basic_info.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockBox extends Mock implements Box<String> {}

class MockStartBloc extends Mock implements StartBloc {}

void main() {
  MockStartBloc startBloc = MockStartBloc();

  setUpAll(() {
    registerFallbackValue(LogInInitiated());
    final l10n = MockL10n();
    I.registerSingleton<L10n>(l10n);
    when(() => l10n.t8(any())).thenReturn('mocked translation');
    when(() => l10n.t8(any(), any())).thenReturn('mocked translation');
    final box = MockBox();
    final settings = MockSettings();
    I.registerSingleton<Settings>(settings);
    when(() => settings.deviceId).thenReturn(0);
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(box));
  });

  setUp(() {
    startBloc = MockStartBloc();
    I.registerSingleton<StartBloc>(startBloc);
    when(() => startBloc.stream).thenAnswer((_) => NeverStream());
  });

  tearDown(() {
    I.unregister<StartBloc>();
  });

  group('Start dialogs', () {
    testWidgets('StartInitialState should not display any dialog',
        (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final dialogFinder = find.byType(Dialog);
      expect(dialogFinder, findsNothing);
    });

    testWidgets('LoginFeedback should not display login dialog',
        (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      expect(find.byType(UserForm), findsNothing);
    });

    testWidgets('LoginFeedback should display login dialog', (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      when(() => startBloc.stream).thenAnswer((_) => Stream.fromIterable([
            LogInFeedback({}, messages: {
              'email': ['feedback']
            })
          ]));
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final UserForm form = tester.widget(find.byType(UserForm));
      expect(form.titleL10nKey, 'User.login.title');
    });

    testWidgets('ForgotPasswordFeedback should display login dialog',
        (tester) async {
          when(() => startBloc.state).thenReturn(StartInitialState());
      when(() => startBloc.stream).thenAnswer((_) => Stream.fromIterable([
            ForgotPasswordFeedback({}, messages: {
              'email': ['feedback']
            })
          ]));
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final UserForm form = tester.widget(find.byType(UserForm));
      expect(form.titleL10nKey, 'User.login.title');
    });

    testWidgets('SignUpFeedback should display register dialog',
        (tester) async {
          when(() => startBloc.state).thenReturn(StartInitialState());
      when(() => startBloc.stream).thenAnswer((_) => Stream.fromIterable([
            SignUpFeedback({}, messages: {
              'email': ['feedback']
            })
          ]));
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final UserForm form = tester.widget(find.byType(UserForm));
      expect(form.titleL10nKey, 'User.register.title');
    });

    testWidgets('ResetPasswordFeedback should display register dialog',
        (tester) async {
          when(() => startBloc.state).thenReturn(StartInitialState());
      when(() => startBloc.stream).thenAnswer((_) => Stream.fromIterable([
            ResetPasswordFeedback({},
                messages: {}, uidb64: 'uidb64', token: 'token')
          ]));
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final UserForm form = tester.widget(find.byType(UserForm));
      expect(form.titleL10nKey, 'User.resetPassword.title');
    });

    testWidgets('StartInfo should display info dialog', (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      when(() => startBloc.stream).thenAnswer((_) => Stream.fromIterable(
          [StartInfo({}, dialogId: '', title: 'title', message: 'message')]));
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      final AlertDialog dialog = tester.widget(find.byType(AlertDialog));
      expect((dialog.title as Text).data, 'title');
    });

    testWidgets('StartInProgress should display loading indicator',
        (tester) async {
      when(() => startBloc.state).thenReturn(StartInProgress());
      when(() => startBloc.stream).thenAnswer((_) => NeverStream());
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pump(const Duration(milliseconds: 100));
      final infoFinder = find.byType(BasicInfo);
      final loadingFinder = find.byType(LoadingIndicator);
      final Stack stack = tester.widget(find.byType(Stack).first);
      expect(infoFinder, findsOneWidget);
      expect(loadingFinder, findsOneWidget);
      expect(stack.children, hasLength(2));
    });
  });

  group('Start buttons', () {
    testWidgets('Login button should update state', (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('loginButton')));
      await tester.pumpAndSettle();
      verify(() => startBloc.add(any(that: isA<LogInInitiated>()))).called(1);
    });

    testWidgets('Register button should update state', (tester) async {
      when(() => startBloc.state).thenReturn(StartInitialState());
      await tester.pumpWidget(_boilerplate(const StartScreen()));
      await tester.pumpAndSettle();
      await tester.tap(find.byKey(const ValueKey('registerButton')));
      await tester.pumpAndSettle();
      verify(() => startBloc.add(any(that: isA<SignUpInitiated>()))).called(1);
    });
  });
}

Widget _boilerplate(Widget child) => MultiBlocProvider(
        providers: [
          BlocProvider<StartBloc>.value(value: I<StartBloc>()),
        ],
        child: MaterialApp(
          localizationsDelegates: const [
            LocaleNamesLocalizationsDelegate(),
          ],
          home: child,
        ));
