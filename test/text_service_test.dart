import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:mocktail/mocktail.dart';

class MockCurrentAccount extends Mock implements CurrentAccount {}

void main() {
  late TextService textService;
  late MockCurrentAccount currentAccount;

  setUp(() {
    if (I.isRegistered<CurrentAccount>()) I.unregister<CurrentAccount>();
    textService = TextService();
    currentAccount = MockCurrentAccount();
    when(() => currentAccount.language).thenReturn(const Locale('en'));
    I.registerSingleton<CurrentAccount>(currentAccount);
  });

  group('TextService', () {
    group('lines', () {
      test('should split text into lines', () {
        const text = 'Line 1\n\nLine 2\u2028Line 3';
        final lines = textService.lines(text);
        expect(lines, ['Line 1\n\n', 'Line 2\n', 'Line 3']);
      });
    });

    group('words', () {
      test('should split text into words', () {
        const text = 'Line 1\nLine 2\nLine 3';
        final words = textService.words(text);
        expect(words, [
          ['Line ', '1\n'],
          ['Line ', '2\n'],
          ['Line ', '3']
        ]);
      });

      test('should handle punctuation correctly', () {
        const text = 'Word 1, Word 2. Word 3!';
        final words = textService.words(text);
        expect(words, [
          ['Word ', '1, ', 'Word ', '2. ', 'Word ', '3!']
        ]);
      });

      test('should handle multiple spaces correctly', () {
        const text = 'Word 1  Word 2   Word 3';
        final words = textService.words(text);
        expect(words, [
          ['Word ', '1  ', 'Word ', '2   ', 'Word ', '3']
        ]);
      });
    });

    group('splitText', () {
      test('should split text into words for English', () {
        const text = 'This is a test.';
        final words = textService.splitText(text);
        expect(words, ['This ', 'is ', 'a ', 'test.']);
      });

      test('should split text into words for Japanese', () {
        when(() => currentAccount.language).thenReturn(const Locale('ja'));
        const text = 'これはテストです。';
        final words = textService.splitText(text);
        expect(words, ['これ', 'は', 'テスト', 'です。']);
      });
    });

    group('splitReference', () {
      test('should split reference into parts for English', () {
        const reference = 'John 3:16';
        final parts = textService.splitReference(reference);
        expect(parts, ['John ', '3:', '16']);
      });

      test('should split reference into parts for Japanese', () {
        when(() => currentAccount.language).thenReturn(const Locale('ja'));
        const reference = 'ヨハネ 3・。16';
        final parts = textService.splitReference(reference);
        expect(parts, ['ヨハネ ', '3・。', '16']);
      });
    });

    group('wordsToText', () {
      test('should return text from words for a single word', () {
        final lines = [
          ['This', 'is', 'a', 'test']
        ];
        const start = Pos(0, 2);
        final text = textService.wordsToText(lines, start, false);
        expect(text, 'a');
      });

      test('should return text from words for multiple words', () {
        final lines = [
          ['This ', 'is ', 'a ', 'test']
        ];
        const start = Pos(0, 1);
        final text = textService.wordsToText(lines, start, true);
        expect(text, 'is a test');
      });
    });

    group('stripPunctuation', () {
      test('should remove punctuation from a word', () {
        const word = 'Test,';
        final strippedWord = textService.stripPunctuation(word);
        expect(strippedWord, 'test');
      });

      test('should handle multiple punctuation marks', () {
        const word = 'Test...!';
        final strippedWord = textService.stripPunctuation(word);
        expect(strippedWord, 'test');
      });

      test('should handle leading and trailing punctuation', () {
        const word = '-test.';
        final strippedWord = textService.stripPunctuation(word);
        expect(strippedWord, 'test');
      });

      test('should handle empty word', () {
        const word = '';
        final strippedWord = textService.stripPunctuation(word);
        expect(strippedWord, '   ');
      });
    });

    group('stripLinebreaks', () {
      test('should remove line breaks from a word', () {
        const word = '\nTest.';
        final strippedWord = textService.stripLinebreaks(word);
        expect(strippedWord, 'Test.');
      });

      test('should handle multiple line breaks', () {
        const word = '\n\nTest.\n';
        final strippedWord = textService.stripLinebreaks(word);
        expect(strippedWord, 'Test.');
      });
    });

    group('punctuationBefore', () {
      test('should return the number of punctuation marks before a word', () {
        const word = ',This is a test.';
        final punctuationCount = textService.punctuationBefore(word);
        expect(punctuationCount, 1);
      });

      test('should handle multiple punctuation marks', () {
        const word = '...This is a test.';
        final punctuationCount = textService.punctuationBefore(word);
        expect(punctuationCount, 3);
      });

      test('should handle no punctuation marks', () {
        const word = 'This is a test.';
        final punctuationCount = textService.punctuationBefore(word);
        expect(punctuationCount, 0);
      });
    });

    group('punctuationAfter', () {
      test('should return the number of punctuation marks after a word', () {
        const word = 'This is a test.';
        final punctuationCount = textService.punctuationAfter(word);
        expect(punctuationCount, 1);
      });

      test('should handle multiple punctuation marks', () {
        const word = 'This is a test...';
        final punctuationCount = textService.punctuationAfter(word);
        expect(punctuationCount, 3);
      });

      test('should handle no punctuation marks', () {
        const word = 'This is a test';
        final punctuationCount = textService.punctuationAfter(word);
        expect(punctuationCount, 0);
      });
    });

    group('lastPos', () {
      test('should return the last position of the words', () {
        final lines = [
          ['This', 'is', 'a', 'test'],
          ['Another', 'line']
        ];
        final lastPosition = textService.lastPos(lines);
        expect(lastPosition, const Pos(1, 1));
      });
    });

    group('wordsToMarks', () {
      test('should create a list of marks for the words', () {
        final lines = [
          ['This', 'is', 'a', 'test'],
          ['Another', 'line']
        ];
        final marks = textService.wordsToMarks(lines);
        expect(marks, [
          [false, false, false, false],
          [false, false]
        ]);
      });
    });

    group('copyItems', () {
      test('should copy a list of items', () {
        final rows = [
          ['This', 'is', 'a', 'test'],
          ['Another', 'line']
        ];
        final copiedRows = textService.copyItems(rows);
        expect(copiedRows, [
          ['This', 'is', 'a', 'test'],
          ['Another', 'line']
        ]);
        expect(copiedRows, isNot(same(rows)));
      });
    });

    group('countWordsOfText', () {
      test('should count the number of words in a text', () {
        const text = 'This is a test.';
        final wordCount = textService.countWordsOfText(text);
        expect(wordCount, 4);
      });

      test('should handle empty text', () {
        const text = '';
        final wordCount = textService.countWordsOfText(text);
        expect(wordCount, 1);
      });
    });

    group('countWordsOfRows', () {
      test('should count the number of words in a list of rows', () {
        final rows = [
          ['This', 'is', 'a', 'test'],
          ['Another', 'line']
        ];
        final wordCount = textService.countWordsOfRows(rows);
        expect(wordCount, 6);
      });

      test('should handle empty rows', () {
        final rows = <List<String>>[];
        final wordCount = textService.countWordsOfRows(rows);
        expect(wordCount, 0);
      });
    });

    group('countMarks', () {
      test('should count the number of marked words', () {
        final marks = [
          [false, true, false, true],
          [true, false]
        ];
        final markCount = textService.countMarks(marks);
        expect(markCount, 3);
      });

      test('should handle empty marks', () {
        final marks = <List<bool>>[];
        final markCount = textService.countMarks(marks);
        expect(markCount, 0);
      });
    });

    group('startsWith', () {
      test('should return true if a word starts with the given letters', () {
        const word = 'This is a test.';
        const letters = 'This';
        final startsWith = textService.startsWith(word, letters);
        expect(startsWith, true);
      });

      test(
          'should return false if a word does not start with the given letters',
          () {
        const word = 'This is a test.';
        const letters = 'Test';
        final startsWith = textService.startsWith(word, letters);
        expect(startsWith, false);
      });

      test('should handle empty letters', () {
        const word = 'This is a test.';
        const letters = '';
        final startsWith = textService.startsWith(word, letters);
        expect(startsWith, false);
      });
    });

    group('beginning', () {
      test('should return the beginning of the text', () {
        const text = 'This is a test. This is another line.';
        final beginning = textService.beginning(text);
        expect(beginning, 'This is a …');
      });

      test('should handle short text', () {
        const text = 'A test.';
        final beginning = textService.beginning(text);
        expect(beginning, 'A …');
      });
    });

    group('mark', () {
      test('should mark the word at the given index', () {
        final marked = [
          [false, false, false, false],
          [false, false]
        ];
        textService.mark(2, marked);
        expect(marked, [
          [false, false, true, false],
          [false, false]
        ]);
      });

      test('should handle index out of bounds', () {
        final marked = [
          [false, false, false, false],
          [false, false]
        ];
        textService.mark(6, marked);
        expect(marked, [
          [false, false, false, false],
          [false, false]
        ]);
      });
    });

    group('wordDelimiter', () {
      test('should return the correct word delimiter for English', () {
        final delimiter = textService.wordDelimiter;
        expect(delimiter, TextService.wordDelimiterEn);
      });

      test('should return the correct word delimiter for Chinese', () {
        when(() => currentAccount.language).thenReturn(const Locale('zh'));
        final delimiter = textService.wordDelimiter;
        expect(delimiter, TextService.wordDelimiterZh);
      });
    });

    group('referenceDelimiter', () {
      test('should return the correct reference delimiter for English', () {
        final delimiter = textService.referenceDelimiter;
        expect(delimiter, TextService.referenceDelimiterEn);
      });

      test('should return the correct reference delimiter for Chinese', () {
        when(() => currentAccount.language).thenReturn(const Locale('zh'));
        final delimiter = textService.referenceDelimiter;
        expect(delimiter, TextService.referenceDelimiterZh);
      });
    });

    group('_languageCode', () {
      test('should return the correct language code', () {
        final languageCode = textService.languageCode;
        expect(languageCode, 'en');
      });
    });

    group('newText', () {
      test('should return the new text from the next text', () {
        const prev = 'This is a test.';
        const next = 'This is a test. This is another line.';
        final newText = textService.newText(prev, next);
        expect(newText, ['this', 'is', 'another', 'line']);
        expect(newText, ['this', 'is', 'another', 'line']);
      });

      test('should handle empty next text', () {
        const prev = 'This is a test.';
        const next = '';
        final newText = textService.newText(prev, next);
        expect(newText, ['   ']);
      });
    });

    group('differingPos', () {
      test('should return the position where two strings differ', () {
        const a = 'This is a test.';
        const b = 'This is a test. This is another line.';
        final differingPos = textService.differingPos(a, b);
        expect(differingPos, 15);
      });

      test('should handle identical strings', () {
        const a = 'This is a test.';
        const b = 'This is a test.';
        final differingPos = textService.differingPos(a, b);
        expect(differingPos, a.length);
      });
    });

    group('removeVowels', () {
      test('should remove vowels from a string', () {
        const s = 'בְּרֵאשִׁית';
        final result = textService.removeVowels(s);
        expect(result, 'בראשית');
      });

      test('should handle empty string', () {
        const s = '';
        final result = textService.removeVowels(s);
        expect(result, '');
      });
    });
  });

  group('beginning', () {
    test('should return the beginning of the text', () {
      const text = 'This is a test.\nThis is another line.';
      final beginning = textService.beginning(text);
      expect(beginning, 'This is …');
    });

    test('should handle one word', () {
      const text = 'test.';
      final beginning = textService.beginning(text);
      expect(beginning, 't…');
    });

    test('should handle short text', () {
      const text = 'A test.';
      final beginning = textService.beginning(text);
      expect(beginning, 'A …');
    });

    test('should handle text with less than 5 words', () {
      const text = 'This is test.';
      final beginning = textService.beginning(text);
      expect(beginning, 'This …');
    });

    test('should handle text with exactly 5 words', () {
      const text = 'This is a simple test.';
      final beginning = textService.beginning(text);
      expect(beginning, 'This is …');
    });

    test('should handle text with more than 5 words', () {
      const text = 'This is a simple test for the beginning method.';
      final beginning = textService.beginning(text);
      expect(beginning, 'This is a …');
    });
  });

  group('wrapDifferences', () {
    test('should wrap additional words or numbers in square brackets', () {
      const excluded = 'abc 11 def 12 ghi';
      const included = '1 abc 11 def 12 13 ghi 11';
      final result = textService.wrapDifferences(excluded, included);
      expect(result, '[1] abc 11 def 12 [13] ghi [11]');
    });

    test('should handle Chinese text', () {
      when(() => currentAccount.language).thenReturn(const Locale('zh'));
      const excluded = '这是一个测试。这是另一行。';
      const included = '15这是一个测试。6 这是另一行。';
      final result = textService.wrapDifferences(excluded, included);
      expect(result, '[15]这是一个测试。[6] 这是另一行。');
    });
  });

  group('stripMarkdown', () {
    test('should remove bold markers', () {
      const text = 'This is **bold** text.';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is bold text.');
    });

    test('should remove italic markers', () {
      const text = 'This is *italic* text.';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is italic text.');
    });

    test('should remove bold and italic markers', () {
      const text = 'This is ***bold and italic*** text.';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is bold and italic text.');
    });

    test('should remove square brackets around numbers', () {
      const text = 'This is a reference [1].';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is a reference 1.');
    });

    test('should remove heading markers from beginning of line', () {
      const text = '# This is a heading';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is a heading');
    });

    test('should handle empty text', () {
      const text = '';
      final result = textService.stripMarkdown(text);
      expect(result, '');
    });

    test('should handle text without markdown', () {
      const text = 'This is plain text.';
      final result = textService.stripMarkdown(text);
      expect(result, 'This is plain text.');
    });
  });

  group('lineBreaksToMarkdown', () {
    test('should replace single line break with two spaces and a line break', () {
      const text = 'Line 1\nLine 2';
      final result = textService.lineBreaksToMarkdown(text);
      expect(result, 'Line 1  \nLine 2');
    });

    test('should replace consecutive line breaks with appropriate markdown', () {
      const text = 'Line 1\n\nLine 2\n\n\nLine 3';
      final result = textService.lineBreaksToMarkdown(text);
      expect(result, 'Line 1&nbsp;\n\nLine 2  \n&nbsp;\n\nLine 3');
    });

    test('should handle text without line breaks', () {
      const text = 'Line 1 Line 2';
      final result = textService.lineBreaksToMarkdown(text);
      expect(result, 'Line 1 Line 2');
    });

    test('should handle empty text', () {
      const text = '';
      final result = textService.lineBreaksToMarkdown(text);
      expect(result, '');
    });

    test('should handle text with only line breaks', () {
      const text = '\n\n\n';
      final result = textService.lineBreaksToMarkdown(text);
      expect(result, '  \n&nbsp;\n\n');
    });
  });
}
