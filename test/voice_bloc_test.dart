import 'package:flutter_test/flutter_test.dart';
import 'package:just_audio/just_audio.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/status.dart';
import 'package:record/record.dart';

class MockFlashcardBloc extends Mock implements FlashcardBloc {}

class MockRecord extends Mock implements Record {}

class MockAudioPlayer extends Mock implements AudioPlayer {}

void main() {
  late VoiceBloc voiceBloc;
  late MockFlashcardBloc mockFlashcardBloc;
  late MockRecord mockRecord;
  late MockAudioPlayer mockAudioPlayer;

  setUpAll(() {
    registerFallbackValue(SilenceAudioSource(duration: Duration(seconds: 0)));
  });

  setUp(() {
    TestWidgetsFlutterBinding.ensureInitialized();
    mockFlashcardBloc = MockFlashcardBloc();
    when(() => mockFlashcardBloc.stream).thenAnswer((_) => Stream.empty());
    mockRecord = MockRecord();
    mockAudioPlayer = MockAudioPlayer();
    when(() => mockAudioPlayer.play()).thenAnswer((_) async {});
    when(() => mockAudioPlayer.stop()).thenAnswer((_) async {});
    voiceBloc = VoiceBloc(mockFlashcardBloc)
      ..recorder = mockRecord
      ..player = mockAudioPlayer;
  });

  group('VoiceBloc', () {
    test(
        'emits Status.ready when RecordPrepared event is added and permission is granted',
        () async {
      when(() => mockRecord.hasPermission()).thenAnswer((_) async => true);

      voiceBloc.add(RecordPrepared());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.ready, Status.none),
        ]),
      );
    });

    test(
        'emits Status.forbidden when RecordPrepared event is added and permission is denied',
        () async {
      when(() => mockRecord.hasPermission()).thenAnswer((_) async => false);

      voiceBloc.add(RecordPrepared());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.forbidden, Status.none),
        ]),
      );
    });

    test(
        'emits Status.active when RecordStarted event is added and recording starts successfully',
        () async {
      when(() => mockRecord.hasPermission()).thenAnswer((_) async => true);
      when(() => mockRecord.start()).thenAnswer((_) async {});
      when(() => mockRecord.isRecording()).thenAnswer((_) async => true);

      voiceBloc.add(RecordStarted());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.active, Status.none),
        ]),
      );
    });

    test(
        'emits Status.broken when RecordStarted event is added and recording fails',
        () async {
      when(() => mockRecord.hasPermission()).thenAnswer((_) async => true);
      when(() => mockRecord.start())
          .thenThrow(Exception('No valid recording session'));

      voiceBloc.add(RecordStarted());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.broken, Status.none),
        ]),
      );
    });

    test(
        'emits Status.ready when RecordStopped event is added and recording stops successfully',
        () async {
      when(() => mockRecord.stop())
          .thenAnswer((_) async => 'path/to/recording');

      voiceBloc.add(RecordStopped());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.ready, Status.none),
        ]),
      );
    });

    test(
        'emits Status.broken when RecordStopped event is added and recording fails to stop',
        () async {
      when(() => mockRecord.stop()).thenAnswer((_) async => null);

      voiceBloc.add(RecordStopped());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.broken, Status.none),
        ]),
      );
    });

    test(
        'emits Status.ready when PlayPrepared event is added and audio source is set',
        () async {
      voiceBloc.source = AudioSource.uri(Uri.parse('path/to/recording'));
      when(() => mockAudioPlayer.setAudioSource(any()))
          .thenAnswer((_) async => null);

      voiceBloc.add(PlayPrepared());
      await expectLater(
        voiceBloc.stream,
        emitsInOrder([
          VoiceState(Status.none, Status.ready),
        ]),
      );
    });
  });
}
