import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/screens/home_screen.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

import 'tts_controller_test.dart';

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

class MockTranscriptionCubit extends Mock implements TranscriptionCubit {}

class MockTabBloc extends Mock implements TabBloc {}

class MockSelectionBloc extends Mock implements SelectionBloc {}

class MockAccountsBloc extends Mock implements AccountsBloc {}

class MockUndoBloc extends Mock implements UndoBloc {}

class MockVersesBloc extends Mock implements VersesBloc {}

class MockFilteredVersesBloc extends Mock implements FilteredVersesBloc {}

class MockTaggedVersesBloc extends Mock implements TaggedVersesBloc {}

class MockOrderedVersesNewBloc extends Mock implements OrderedVersesNewBloc {}

class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}

class MockOrderedVersesKnownBloc extends Mock
    implements OrderedVersesKnownBloc {}

class MockOrderedVersesAllBloc extends Mock implements OrderedVersesAllBloc {}

class MockScoresBloc extends Mock implements ScoresBloc {}

class MockTtsService extends Mock implements TtsService {}

class MockTtsHandler extends Mock implements TtsHandler {}

class LocaleFake extends Fake implements Locale {}

void main() {
  group('HomeScreen', () {
    late MockAuthBloc authBloc;
    late TabBloc tabBloc;
    late SelectionBloc selectionBloc;
    late FilteredVersesBloc filteredVersesBloc;
    late BehaviorSubject<PlaybackState> playback;

    setUpAll(() {
      authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      when(() => authBloc.state).thenReturn(const NoAuth());
      I.registerSingleton<AuthBloc>(authBloc);
      final l10n = MockL10n();
      final settings = MockSettings();
      final box = MockBox();
      final currentAccount = MockCurrentAccount();
      final nav = MockNavigationCubit();
      final transcriptionCubit = MockTranscriptionCubit();
      final accountsBloc = MockAccountsBloc();
      final undoBloc = MockUndoBloc();
      final versesBloc = MockVersesBloc();
      tabBloc = MockTabBloc();
      selectionBloc = MockSelectionBloc();
      filteredVersesBloc = MockFilteredVersesBloc();
      final taggedVersesBloc = MockTaggedVersesBloc();
      final versesNewBloc = MockOrderedVersesNewBloc();
      final versesDueBloc = MockOrderedVersesDueBloc();
      final versesKnownBloc = MockOrderedVersesKnownBloc();
      final versesAllBloc = MockOrderedVersesAllBloc();
      final scoresBloc = MockScoresBloc();
      final ttsService = MockTtsService();
      final ttsHandler = MockTtsHandler();
      playback = BehaviorSubject<PlaybackState>();
      registerFallbackValue(LocaleFake());
      I.registerSingleton<L10n>(l10n);
      I.registerSingleton<Settings>(settings);
      I.registerSingleton<CurrentAccount>(currentAccount);
      I.registerSingleton<NavigationCubit>(nav);
      I.registerSingleton<TranscriptionCubit>(transcriptionCubit);
      I.registerSingleton<AccountsBloc>(accountsBloc);
      I.registerSingleton<TabBloc>(tabBloc);
      I.registerSingleton<SelectionBloc>(selectionBloc);
      I.registerSingleton<FilteredVersesBloc>(filteredVersesBloc);
      I.registerSingleton<TaggedVersesBloc>(taggedVersesBloc);
      I.registerSingleton<OrderedVersesNewBloc>(versesNewBloc);
      I.registerSingleton<OrderedVersesDueBloc>(versesDueBloc);
      I.registerSingleton<OrderedVersesKnownBloc>(versesKnownBloc);
      I.registerSingleton<OrderedVersesAllBloc>(versesAllBloc);
      I.registerSingleton<UndoBloc>(undoBloc);
      I.registerSingleton<VersesBloc>(versesBloc);
      I.registerSingleton<ScoresBloc>(scoresBloc);
      I.registerSingleton<TtsService>(ttsService);
      I.registerSingleton<TtsHandler>(ttsHandler);
      when(() => l10n.t8(any())).thenReturn('mocked translation');
      when(() => l10n.t8(any(), any())).thenReturn('mocked translation');
      when(() => settings.deviceId).thenReturn(0);
      when(() => settings.listenable(keys: any(named: 'keys')))
          .thenReturn(ValueNotifier(box));
      when(() => currentAccount.state)
          .thenReturn(Account.fromEntity(AccountEntity('test', id: 1)));
      when(() => currentAccount.stream).thenAnswer((_) => Stream.fromIterable([
            Account.fromEntity(AccountEntity('test', id: 1)),
          ]));
      when(() => currentAccount.selected()).thenAnswer((_) => NeverStream());
      when(() => currentAccount.dailyGoal).thenReturn(0);
      when(() => currentAccount.id).thenReturn(1);
      when(() => currentAccount.langRef).thenReturn(const Locale('en'));
      when(() => currentAccount.language).thenReturn(const Locale('en'));
      when(() => currentAccount.topicPreferred).thenReturn(false);
      when(() => currentAccount.font).thenReturn('serif');
      when(() => nav.stream).thenAnswer((_) => NeverStream());
      when(() => nav.propagateStream).thenAnswer((_) => NeverStream());
      when(() => transcriptionCubit.stream).thenAnswer((_) =>
          Stream.fromIterable([
            const Transcription('transcription', mode: TranscriptionMode.script)
          ]));
      when(() => accountsBloc.stream).thenAnswer((_) => Stream.fromIterable([
            EntitiesLoadSuccess<AccountEntity>([AccountEntity('test', id: 1)]),
          ]));
      when(() => accountsBloc.state).thenReturn(
          EntitiesLoadSuccess<AccountEntity>([AccountEntity('test', id: 1)]));
      when(() => undoBloc.stream).thenAnswer((_) => NeverStream());
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.NEW));
      when(() => tabBloc.stream).thenAnswer((_) => NeverStream());
      when(() => tabBloc.propagateStream).thenAnswer((_) => NeverStream());
      when(() => selectionBloc.state).thenReturn(const SelectionState({}));
      when(() => selectionBloc.stream).thenAnswer((_) => NeverStream());
      when(() => filteredVersesBloc.state).thenReturn(FilterInProgress());
      when(() => filteredVersesBloc.stream)
          .thenAnswer((_) => Stream.fromIterable([
                FilteredVersesSuccess(
                  _testVerses(),
                  date: DateService().today,
                  queryFilter: const QueryFilter(),
                ),
              ]));
      when(() => taggedVersesBloc.state).thenReturn(TaggedVersesInProgress());
      when(() => taggedVersesBloc.stream)
          .thenAnswer((_) => Stream.fromIterable([
                TaggedVersesSuccess(
                  verses: _testVerses(),
                  tags: const [],
                ),
              ]));
      when(() => versesNewBloc.state)
          .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
      when(() => versesNewBloc.box).thenReturn(VerseBox.NEW);
      when(() => versesNewBloc.stream).thenAnswer((_) => Stream.fromIterable([
            const OrderSuccess(VerseOrder.ALPHABET, []),
          ]));
      when(() => versesDueBloc.state).thenReturn(
          OrderSuccess(VerseOrder.DATE, _testVerses().sublist(0, 1)));
      when(() => versesDueBloc.box).thenReturn(VerseBox.DUE);
      when(() => versesDueBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _testVerses().sublist(0, 1)),
          ]));
      when(() => versesKnownBloc.state).thenReturn(
          OrderSuccess(VerseOrder.LEVEL, _testVerses().sublist(1, 3)));
      when(() => versesKnownBloc.box).thenReturn(VerseBox.KNOWN);
      when(() => versesKnownBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _testVerses().sublist(1, 3)),
          ]));
      when(() => versesAllBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.RANDOM, _testVerses()));
      when(() => versesAllBloc.box).thenReturn(VerseBox.ALL);
      when(() => versesAllBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _testVerses()),
          ]));
      when(() => scoresBloc.goalReachedStream).thenAnswer((_) => Stream.fromIterable([DateService().today]));
      when(() => ttsService.speakText(any(), any(), any()))
          .thenAnswer((_) async {});
      when(() => ttsService.stop()).thenAnswer((_) async {});
      when(() => ttsHandler.playbackState).thenAnswer((_) => playback);
      when(() => ttsHandler.mediaProcessing).thenAnswer((_) => NeverStream());
    });

    testWidgets('TabBarView should be initiated', (tester) async {
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      final titleFinder = find.text('test');
      expect(titleFinder, findsOneWidget);
      TabBarView tabBarView =
          tester.widget(find.byKey(const ValueKey('tabBarView')));
      expect(tabBarView.controller?.index, 0);
    });

    testWidgets('Tab controller should be updated on TabEvent', (tester) async {
      when(() => tabBloc.propagateStream)
          .thenAnswer((_) => Stream.fromIterable([
                const TabState(true, VerseBox.KNOWN),
              ]));
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      TabBarView tabBarView =
          tester.widget(find.byKey(const ValueKey('tabBarView')));
      expect(tabBarView.controller?.index, 2);
    });

    testWidgets('Tab view should be updated on TabEvent', (tester) async {
      when(() => tabBloc.propagateStream)
          .thenAnswer((_) => Stream.fromIterable([
                const TabState(true, VerseBox.KNOWN),
              ]));
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      expect(find.byType(OrderedVerses), findsNWidgets(1));
      await tester.pumpAndSettle();
      expect(find.byType(SelectableVerseTile), findsNWidgets(2));
    });

    testWidgets('Tabs should not be visible when not boxed', (tester) async {
      when(() => tabBloc.propagateStream)
          .thenAnswer((_) => Stream.fromIterable([
                const TabState(false, VerseBox.KNOWN),
              ]));
      await tester.pumpWidget(_boilerplate(const HomeScreen(false)));
      expect(find.byType(OrderedVerses), findsNWidgets(1));
      expect(find.byType(BoxAllBar), findsNWidgets(1));
      await tester.pumpAndSettle();
      expect(find.byType(SelectableVerseTile), findsNWidgets(3));
    });

    testWidgets('SearchVersesBar is visible', (tester) async {
      when(() => filteredVersesBloc.state).thenReturn(FilteredVersesSuccess(
        _testVerses(),
        date: DateService().today,
        queryFilter: const QueryFilter(),
      ));
      when(() => selectionBloc.state).thenReturn(const SelectionState(
        {},
        isSearchActive: true,
      ));
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      await tester.pumpAndSettle();

      final searchBarFinder = find.byType(SearchVersesBar);
      final selectionBarFinder = find.byType(SelectionBar);

      expect(searchBarFinder, findsOneWidget);
      expect(selectionBarFinder, findsNothing);
    });

    testWidgets('SearchVersesBar does not cover SelectionBar', (tester) async {
      when(() => filteredVersesBloc.state).thenReturn(FilteredVersesSuccess(
        _testVerses(),
        date: DateService().today,
        queryFilter: const QueryFilter(),
      ));
      when(() => selectionBloc.state).thenReturn(SelectionState(
        {1: testVerses[0]},
        isSearchActive: true,
      ));
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      await tester.pumpAndSettle();

      final searchBarFinder = find.byType(SearchVersesBar);
      final selectionBarFinder = find.byType(SelectionBar);

      expect(searchBarFinder, findsNothing);
      expect(selectionBarFinder, findsOneWidget);
    });

    testWidgets('Transcription is displayed', (tester) async {
      await tester.pumpWidget(_boilerplate(const HomeScreen(true)));
      await tester.pumpAndSettle();

      playback.add(PlaybackState(processingState: AudioProcessingState.ready));
      await tester.pumpAndSettle();

      final fabFinder = find.byType(TtsFabRow);
      final transcriptionDialogFinder = find.byType(TranscriptionDialog);
      final transcriptionFinder = find.text('transcription');
      expect(fabFinder, findsOneWidget);
      expect(transcriptionDialogFinder, findsOneWidget);
      expect(transcriptionFinder, findsOneWidget);
    });
  });
}

List<Verse> _testVerses() => [
      Verse(
        'ref3',
        'pas3',
        id: 3,
        modified: 0,
        accountId: 1,
      ),
      Verse(
        'ref1',
        'pas1',
        id: 1,
        modified: 0,
        accountId: 1,
        tags: const {1: 'tag1'},
      ),
      Verse(
        'ref2',
        'pas2',
        id: 2,
        modified: 0,
        accountId: 1,
        tags: const {2: 'tag2'},
      ),
    ];

Widget _boilerplate(Widget child) => MultiBlocProvider(
        providers: [
          BlocProvider<AccountsBloc>.value(value: I<AccountsBloc>()),
          BlocProvider<UndoBloc>.value(value: I<UndoBloc>()),
          BlocProvider<FilteredVersesBloc>.value(
              value: I<FilteredVersesBloc>()),
          BlocProvider<TaggedVersesBloc>.value(value: I<TaggedVersesBloc>()),
          BlocProvider<TabBloc>.value(value: I<TabBloc>()),
          BlocProvider<SelectionBloc>.value(value: I<SelectionBloc>()),
          BlocProvider<TranscriptionCubit>.value(
              value: I<TranscriptionCubit>()),
        ],
        child: MediaQuery(
            data: const MediaQueryData(),
            child: Localizations(
                locale: const Locale('en', 'gb'),
                delegates: const <LocalizationsDelegate<dynamic>>[
                  DefaultWidgetsLocalizations.delegate,
                  DefaultMaterialLocalizations.delegate,
                ],
                child: Directionality(
                    textDirection: TextDirection.ltr,
                    child: Overlay(initialEntries: [
                      OverlayEntry(builder: (context) {
                        return child;
                      }),
                    ])))));
