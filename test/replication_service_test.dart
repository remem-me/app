import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/replication_service.dart';
import 'package:remem_me/services/replicator.dart';
import 'package:repository/repository.dart';

class MockAuthBloc extends Mock implements AuthBloc {}

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockBloc extends Mock
    implements EntitiesBloc<VerseEntity, Verse> {}

class MockReplicator extends Mock implements Replicator {}

class FakeVerseEntity extends Fake implements VerseEntity {}

void main() {
  late CurrentAccount currentAccount;
  late Replicator accountReplicator;
  late Replicator verseReplicator;

  group('receive', () {

    setUpAll(() {
      registerFallbackValue(FakeVerseEntity());
      final authBloc = MockAuthBloc();
      when(() => authBloc.state).thenReturn(const AccessRemote(
          accessToken: 'accessToken', refreshToken: 'refreshToken'));
      I.registerSingleton<AuthBloc>(authBloc);
      final settings = MockSettings();
      when(() => settings.getInt(any())).thenReturn(0);
      when(() => settings.putInt(any(), any())).thenAnswer((_) async {});
      I.registerSingleton<Settings>(settings);
      currentAccount = MockCurrentAccount();
      I.registerSingleton<CurrentAccount>(currentAccount);
      accountReplicator = MockReplicator();
      when(() => accountReplicator.receiveWhenReady(null, null)).thenAnswer((
          _) async {});
      verseReplicator = MockReplicator();
      when(() => verseReplicator.receiveWhenReady(null, null)).thenAnswer((
          _) async {});
    });

    setUp(() {
      when(() => currentAccount.id).thenReturn(1);
    });

    test('All replicators should be called', () async {
      final service = ReplicationService();
      service.register(AccountEntity, accountReplicator);
      service.register(VerseEntity, verseReplicator);

      await service.receive(all: true);

      verify(() => accountReplicator.receiveWhenReady(null, null)).called(1);
      verify(() => verseReplicator.receiveWhenReady(null, null)).called(1);
    });

  });
}
