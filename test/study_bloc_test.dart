import 'dart:ui';

import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockTtsService extends Mock implements TtsService {}

class MockFlashcardBloc extends Mock implements FlashcardBloc {}

void main() {
  late StudyBloc studyBloc;
  late FlashcardBloc flashcardBloc;
  late Verse testVerseEn;
  late List<List<String>> testLinesEn;
  late Verse testVerseZh;
  late List<List<String>> testLinesZh;
  late MockCurrentAccount currentAccount;

  setUpAll(() {
    TestWidgetsFlutterBinding.ensureInitialized();
    registerFallbackValue(const Locale('en'));
    flashcardBloc = MockFlashcardBloc();
    when(() => flashcardBloc.stream).thenAnswer((_) => NeverStream());
    when(() => flashcardBloc.state).thenReturn(const FlashcardLoadInProgress());
    final settings = MockSettings();
    when(() => settings.deviceId).thenReturn(0);
    I.registerSingleton<Settings>(settings);
    final ttsService = MockTtsService();
    when(() => ttsService.speakText(any(), any(), any()))
        .thenAnswer((_) async {});
    when(() => ttsService.stop()).thenAnswer((_) async {});
    I.registerSingleton<TtsService>(ttsService);
    testVerseEn =
        // 'aword bword cword'
        Verse('aref', 'aword bword cword', id: 1, modified: 0, accountId: 1);
    testLinesEn = [
      ['aword ', 'bword ', 'cword'],
    ];
    testVerseZh = Verse('箴言 6:6', '懶惰人哪！你去', id: 1, modified: 0, accountId: 1);
    testLinesZh = [
      ['懶', '惰', '人', '哪！', '你', '去'],
    ];
  });

  setUp(() {
    currentAccount = MockCurrentAccount();
    when(() => currentAccount.selected()).thenAnswer((invocation) =>
        Stream.fromIterable(
            [Account.fromEntity(AccountEntity('test', id: 1))]));
    when(() => currentAccount.id).thenReturn(-1);
    when(() => currentAccount.language).thenReturn(Locale('en'));
    when(() => currentAccount.langRef).thenReturn(Locale('en'));
    when(() => currentAccount.referenceIncluded).thenReturn(false);
    I.registerSingleton<CurrentAccount>(currentAccount);
    studyBloc = StudyBloc(flashcardBloc: flashcardBloc);
  });

  tearDown(() {
    I.unregister<CurrentAccount>();
  });

  group('Typing', () {
    blocTest<StudyBloc, StudyState>(
      'emits Typing state after Typed event',
      build: () => studyBloc,
      act: (bloc) {
        bloc.add(StudyStarted(testVerseEn));
        bloc.add(const Typed());
        bloc.add(const Typed(letters: 'a'));
        bloc.add(const Typed(letters: 'x'));
        bloc.add(const Typed(letters: 'b'));
        // bloc.add(Typed(letters: 'b'));
      },
      expect: () => [
        Study(testVerseEn, testLinesEn),
        Typing(
          testVerseEn,
          testLinesEn,
          pos: const Pos(0, 2),
          placeholders: const Pos(0, 2),
          nextWord: 'aword ',
        ),
        Typing(
          testVerseEn,
          testLinesEn,
          pos: const Pos(0, 2),
          placeholders: const Pos(0, 2),
          revealed: const Pos(0, 0),
          letters: 'a',
          isCorrect: true,
          nextWord: 'bword ',
          countRight: 1,
        ),
        Typing(
          testVerseEn,
          testLinesEn,
          pos: const Pos(0, 2),
          placeholders: const Pos(0, 2),
          revealed: const Pos(0, 0),
          letters: 'x',
          isCorrect: false,
          nextWord: 'bword ',
          countWrong: 1,
          countRight: 1,
        ),
        Typing(
          testVerseEn,
          testLinesEn,
          pos: const Pos(0, 2),
          placeholders: const Pos(0, 2),
          revealed: const Pos(0, 1),
          letters: 'b',
          isCorrect: true,
          nextWord: 'cword',
          countWrong: 1,
          countRight: 2,
        ),
      ],
    );

    blocTest<StudyBloc, StudyState>(
      'Typing state after Typed event in Chinese',
      setUp: () => when(() => currentAccount.language).thenReturn(
          Locale.fromSubtags(
              languageCode: 'zh', scriptCode: 'Hant', countryCode: 'HK')),
      build: () => studyBloc,
      act: (bloc) {
        bloc.add(StudyStarted(testVerseZh));
        bloc.add(const Typed());
        bloc.add(const Typed(letters: '懶'));
        bloc.add(const Typed(letters: '惰'));
        bloc.add(const Typed(letters: '人'));
        },
      expect: () => [
        Study(testVerseZh, testLinesZh),
        Typing(
          testVerseZh,
          testLinesZh,
          pos: const Pos(0, 5),
          placeholders: const Pos(0, 5),
          nextWord: '懶',
        ),
        Typing(
          testVerseZh,
          testLinesZh,
          pos: const Pos(0, 5),
          placeholders: const Pos(0, 5),
          revealed: const Pos(0, 0),
          letters: '懶',
          isCorrect: true,
          nextWord: '惰',
          countRight: 1,
        ),
        Typing(
          testVerseZh,
          testLinesZh,
          pos: const Pos(0, 5),
          placeholders: const Pos(0, 5),
          revealed: const Pos(0, 1),
          letters: '惰',
          isCorrect: true,
          nextWord: '人',
          countRight: 2,
        ),
        Typing(
          testVerseZh,
          testLinesZh,
          pos: const Pos(0, 5),
          placeholders: const Pos(0, 5),
          revealed: const Pos(0, 2),
          letters: '人',
          isCorrect: true,
          nextWord: '哪！',
          countRight: 3,
        ),
      ],
    );
  });

  group('Obfuscating', () {
    blocTest<StudyBloc, StudyState>(
      'emits Obfuscation state after Obfuscated and Revealed events',
      build: () => studyBloc,
      act: (bloc) {
        bloc.add(StudyStarted(testVerseEn));
        bloc.add(const Obfuscated());
        bloc.add(const Revealed(Pos(0, 0)));
        bloc.add(const Obfuscated());
        bloc.add(const Obfuscated());
        bloc.add(const Obfuscated());
        bloc.add(const Revealed(Pos(0, 0)));
        bloc.add(const Obfuscated());
      },
      expect: () => [
        Study(testVerseEn, testLinesEn),
        isA<Obfuscation>().having(
            (o) => o.obfuscated[0].where((value) => value),
            'obfuscated',
            hasLength(1)),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(1))
            .having((o) => o.revealed[0].where((value) => value), 'revealed',
                hasLength(1)),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(1))
            .having((o) => o.revealed[0].where((value) => value), 'revealed',
                hasLength(0)),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(2))
            .having((o) => o.isComplete, 'isComplete', false),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(3))
            .having((o) => o.isComplete, 'isComplete', true),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(3))
            .having((o) => o.revealed[0].where((value) => value), 'revealed',
                hasLength(1))
            .having((o) => o.isComplete, 'isComplete', false),
        isA<Obfuscation>()
            .having((o) => o.obfuscated[0].where((value) => value),
                'obfuscated', hasLength(3))
            .having((o) => o.isComplete, 'isComplete', true),
      ],
    );
  });

  group('Puzzle', () {
    blocTest<StudyBloc, StudyState>(
      'emits Puzzle state after Puzzled event',
      build: () => studyBloc,
      act: (bloc) {
        bloc.add(StudyStarted(testVerseEn));
        bloc.add(const Puzzled(true));
        bloc.add(const Puzzled(false));
      },
      expect: () => [
        Study(testVerseEn, testLinesEn),
        isA<Puzzle>().having((p) => p.choice, 'choice', isNotNull),
        isA<Puzzle>()
            .having((p) => p.choice, 'choice', isNotNull)
            .having((p) => p.countWrong, 'countWrong', 1),
      ],
    );
  });
}
