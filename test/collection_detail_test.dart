import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/screens/collection_detail_screen.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockCollectionDetailBloc extends Mock implements CollectionDetailBloc {}

class MockTtsService extends Mock implements TtsService {}

void main() {
  late MockSettings settings;
  late MockAuthBloc authBloc;
  late MockCollectionDetailBloc versesBloc;

  Widget boilerplate(Widget child) => MultiBlocProvider(
          providers: [
            BlocProvider<AuthBloc>.value(value: authBloc),
            BlocProvider<CollectionDetailBloc>.value(value: versesBloc),
          ],
          child: MaterialApp(
            localizationsDelegates: const [
              LocaleNamesLocalizationsDelegate(),
            ],
            home: child,
          ));

  setUpAll(() {
    initializeDateFormatting();
    // dependency injection
    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    when(() => l10n.t8(any(), any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    when(() => l10n.locale).thenReturn(const Locale('en'));
    I.registerSingleton<L10n>(l10n);

    final currentAccount = MockCurrentAccount();
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.topicPreferred).thenReturn(false);
    when(() => currentAccount.dailyGoal).thenReturn(1);
    I.registerSingleton<CurrentAccount>(currentAccount);

    authBloc = MockAuthBloc();
    when(() => authBloc.stream).thenAnswer((_) => NeverStream());
    when(() => authBloc.state).thenReturn(const NoAuth());
    I.registerSingleton<AuthBloc>(authBloc);

    final ttsService = MockTtsService();
    I.registerSingleton<TtsService>(ttsService);
  });

  setUp(() {
    // dependency injection
    if (I.isRegistered<Settings>()) I.unregister<Settings>();
    settings = MockSettings();
    when(() => settings.getBool(any())).thenReturn(false);
    when(() => settings.putBool(any(), any())).thenAnswer((_) async {});
    when(() => settings.deviceId).thenReturn(1);
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(MockBox()));
    I.registerSingleton<Settings>(settings);

    // context
    versesBloc = MockCollectionDetailBloc();
    final collectionSuccess = CollectionDetailLoadSuccess(Collection(
      1,
      'Test',
      'Test Collection',
      description: 'This is a test collection.',
      publisher: const Publisher(1, 'test',
          language: Locale('en'), langRef: Locale('en')),
      created: DateTime(2010, 3, 24),
      verses: [
        Verse('r1', 'p1', id: 1, modified: 0, accountId: 1),
      ],
    ));
    when(() => versesBloc.state).thenReturn(collectionSuccess);
    when(() => versesBloc.stream)
        .thenAnswer((_) => Stream.fromIterable([collectionSuccess]));
  });

  group('Collection Detail Tip', () {
    setUp(() {
      when(() => settings.getBool(Settings.TIP_COLLECTION_VERSE))
          .thenReturn(true);
    });

    testWidgets('Shows Tip.collection.verse', (tester) async {
      await tester.pumpWidget(boilerplate(const CollectionDetailScreen()));
      await tester.pumpAndSettle();
      final verseFinder = find.byType(SimpleVerseTile);
      final tipFinder = find.byType(TipTile);
      final textFinder = find.text('Tip.collection.verse');
      expect(verseFinder, findsOneWidget);
      expect(tipFinder, findsOneWidget);
      expect(textFinder, findsOneWidget);
      for (var i = 0; i < tipFinder.evaluate().length; i++) {
        final trailingIconFinder = find.descendant(
          of: tipFinder.at(i),
          matching: find.byIcon(Icons.close_rounded),
        );
        await tester.tap(trailingIconFinder);
      }
      await tester.pump();
      verify(() => settings.putBool(Settings.TIP_COLLECTION_VERSE, false))
          .called(1);
    });
  });
}
