import 'package:app_core/app_core.dart';
import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/routers/routers.dart';
import 'package:remem_me/screens/screens.dart';
import 'package:remem_me/services/bible_service.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:remem_me/services/notification_service.dart';
import 'package:remem_me/services/tts_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockL10n extends Mock implements L10n {}

class MockBox extends Mock implements Box<String> {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockStartBloc extends Mock implements StartBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockSettings extends Mock implements Settings {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockHomeFlashcardBloc extends Mock implements HomeFlashcardBloc {}

class MockAccountsRepository extends Mock implements AccountsRepository {}

class MockVersesRepository extends Mock implements VersesRepository {}

class MockTagsRepository extends Mock implements TagsRepository {}

class MockTranscriptionCubit extends Mock implements TranscriptionCubit {}

class MockTabBloc extends Mock implements TabBloc {}

class MockSelectionBloc extends Mock implements SelectionBloc {}

class MockAccountsBloc extends Mock implements AccountsBloc {}

class MockUndoBloc extends Mock implements UndoBloc {}

class MockVersesBloc extends Mock implements VersesBloc {}

class MockFilteredVersesBloc extends Mock implements FilteredVersesBloc {}

class MockTaggedVersesBloc extends Mock implements TaggedVersesBloc {}

class MockOrderedVersesNewBloc extends Mock implements OrderedVersesNewBloc {}

class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}

class MockOrderedVersesKnownBloc extends Mock
    implements OrderedVersesKnownBloc {}

class MockOrderedVersesAllBloc extends Mock implements OrderedVersesAllBloc {}

class MockScoresBloc extends Mock implements ScoresBloc {}

class MockTagsBloc extends Mock implements TagsBloc {}

class MockTtsHandler extends Mock implements TtsHandler {}

class MockTtsService extends Mock implements TtsService {}

class MockVerse extends Mock implements Verse {}

class MockBibleService extends Mock implements BibleService {}

class MockNotificationService extends Mock implements NotificationService {}

class MockCollectionService extends Mock implements CollectionService {}

class MockCollection extends Mock implements Collection {}

class MockClient extends Mock implements http.Client {}

void main() {
  MockClient client = MockClient();
  late OuterRouterDelegate routerDelegate;
  late MockNavigationCubit mockNavCubit;
  late MockAuthBloc mockAuthBloc;
  late MockStartBloc mockStartBloc;
  late MockHomeFlashcardBloc homeFlashcardBloc;

  setUpAll(() {
    registerFallbackValue(StartBasePath());
    registerFallbackValue(const Locale('en'));

    final l10n = MockL10n();
    I.registerSingleton<L10n>(l10n);
    when(() => l10n.t8(any())).thenReturn('mocked translation');
    when(() => l10n.t8(any(), any())).thenReturn('mocked translation');
    when(() => l10n.locale).thenReturn(Locale('en'));

    final connectivityCubit = MockConnectivityCubit();
    when(() => connectivityCubit.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<ConnectivityCubit>(connectivityCubit);

    final box = MockBox();
    final settings = MockSettings();
    I.registerSingleton<Settings>(settings);
    when(() => settings.deviceId).thenReturn(0);
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(box));

    final currentAccount = MockCurrentAccount();
    I.registerSingleton<CurrentAccount>(currentAccount);
    when(() => currentAccount.state)
        .thenReturn(Account.fromEntity(AccountEntity('test', id: 1)));
    when(() => currentAccount.stream).thenAnswer((_) => Stream.fromIterable([
          Account.fromEntity(AccountEntity('test', id: 1)),
        ]));
    when(() => currentAccount.selected()).thenAnswer((_) => NeverStream());
    when(() => currentAccount.setPublisher(any())).thenAnswer((_) async {});
    when(() => currentAccount.dailyGoal).thenReturn(0);
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.langRef).thenReturn(const Locale('en'));
    when(() => currentAccount.language).thenReturn(const Locale('en'));
    when(() => currentAccount.topicPreferred).thenReturn(false);
    when(() => currentAccount.referenceIncluded).thenReturn(false);
    when(() => currentAccount.languageDirection).thenReturn(TextDirection.ltr);
    when(() => currentAccount.langRefDirection).thenReturn(TextDirection.ltr);
    when(() => currentAccount.font).thenReturn('serif');
    when(() => currentAccount.name).thenReturn('account');
    when(() => currentAccount.books).thenReturn({});

    final replicationBloc = MockReplicationBloc();
    when(() => replicationBloc.stream).thenAnswer((_) => NeverStream());
    when(() => replicationBloc.state).thenReturn(ReplicationInitial());
    I.registerSingleton<ReplicationBloc>(replicationBloc);

    final playback = BehaviorSubject<PlaybackState>();
    final ttsHandler = MockTtsHandler();
    when(() => ttsHandler.play()).thenAnswer(((_) async {}));
    when(() => ttsHandler.playbackState).thenAnswer((_) => playback);
    I.registerSingleton<TtsHandler>(ttsHandler);

    final ttsService = MockTtsService();
    when(() => ttsService.speakText(any(), any(), any()))
        .thenAnswer((_) async {});
    when(() => ttsService.stop()).thenAnswer((_) async {});
    I.registerSingleton<TtsService>(ttsService);

    final bibleService = MockBibleService();
    when(() => bibleService.init(any())).thenAnswer((_) async {});
    when(() => bibleService.query(any(), any(), any())).thenReturn(null);
    I.registerSingleton<BibleService>(bibleService);

    final notificationService = MockNotificationService();
    when(() => notificationService.pendingNotificationRequests())
        .thenAnswer((_) async => []);
    I.registerSingleton<NotificationService>(notificationService);

    final accountsRepository = MockAccountsRepository();
    I.registerSingleton<EntitiesRepository<AccountEntity>>(accountsRepository);
    when(() => accountsRepository.readList(deleted: true))
        .thenAnswer((_) async {
      return [];
    });
    final versesRepository = MockVersesRepository();
    I.registerSingleton<EntitiesRepository<VerseEntity>>(versesRepository);
    when(() => versesRepository.readList(
        accountId: any(named: 'accountId'),
        deleted: true)).thenAnswer((_) async {
      return [];
    });
    final tagsRepository = MockTagsRepository();
    I.registerSingleton<EntitiesRepository<TagEntity>>(tagsRepository);
    when(() => tagsRepository.readList(
        accountId: any(named: 'accountId'),
        deleted: true)).thenAnswer((_) async {
      return [];
    });

    final transcriptionCubit = MockTranscriptionCubit();
    final accountsBloc = MockAccountsBloc();
    final undoBloc = MockUndoBloc();
    final versesBloc = MockVersesBloc();
    final tabBloc = MockTabBloc();
    final selectionBloc = MockSelectionBloc();
    final filteredVersesBloc = MockFilteredVersesBloc();
    final taggedVersesBloc = MockTaggedVersesBloc();
    final versesNewBloc = MockOrderedVersesNewBloc();
    final versesDueBloc = MockOrderedVersesDueBloc();
    final versesKnownBloc = MockOrderedVersesKnownBloc();
    final versesAllBloc = MockOrderedVersesAllBloc();
    final scoresBloc = MockScoresBloc();
    final tagsBloc = MockTagsBloc();

    I.registerSingleton<TranscriptionCubit>(transcriptionCubit);
    I.registerSingleton<AccountsBloc>(accountsBloc);
    I.registerSingleton<TabBloc>(tabBloc);
    I.registerSingleton<SelectionBloc>(selectionBloc);
    I.registerSingleton<FilteredVersesBloc>(filteredVersesBloc);
    I.registerSingleton<TaggedVersesBloc>(taggedVersesBloc);
    I.registerSingleton<OrderedVersesNewBloc>(versesNewBloc);
    I.registerSingleton<OrderedVersesDueBloc>(versesDueBloc);
    I.registerSingleton<OrderedVersesKnownBloc>(versesKnownBloc);
    I.registerSingleton<OrderedVersesAllBloc>(versesAllBloc);
    I.registerSingleton<UndoBloc>(undoBloc);
    I.registerSingleton<VersesBloc>(versesBloc);
    I.registerSingleton<ScoresBloc>(scoresBloc);
    I.registerSingleton<TagsBloc>(tagsBloc);

    when(() => tabBloc.stream).thenAnswer((_) => NeverStream());
    when(() => tabBloc.propagateStream).thenAnswer((_) => NeverStream());
    when(() => tabBloc.state).thenReturn(TabState(true, VerseBox.NEW));
    when(() => filteredVersesBloc.stream).thenAnswer((_) => NeverStream());
    when(() => filteredVersesBloc.state).thenReturn(FilterInProgress());
    when(() => undoBloc.stream).thenAnswer((_) => NeverStream());
    when(() => undoBloc.state).thenReturn([]);
    when(() => taggedVersesBloc.stream).thenAnswer((_) => NeverStream());
    when(() => taggedVersesBloc.state).thenReturn(TaggedVersesInProgress());
    when(() => versesBloc.stream).thenAnswer((_) => NeverStream());
    when(() => versesBloc.state).thenReturn(EntitiesLoadInProgress());
    when(() => accountsBloc.stream).thenAnswer((_) => NeverStream());
    when(() => accountsBloc.state).thenReturn(EntitiesLoadInProgress());
    when(() => versesNewBloc.stream).thenAnswer((_) => NeverStream());
    when(() => versesNewBloc.state).thenReturn(OrderInProgress());
    when(() => versesDueBloc.stream).thenAnswer((_) => NeverStream());
    when(() => versesDueBloc.state).thenReturn(OrderInProgress());
    when(() => versesKnownBloc.stream).thenAnswer((_) => NeverStream());
    when(() => versesKnownBloc.state).thenReturn(OrderInProgress());
    when(() => versesAllBloc.stream).thenAnswer((_) => NeverStream());
    when(() => versesAllBloc.state).thenReturn(OrderInProgress());
    when(() => tagsBloc.stream).thenAnswer((_) => NeverStream());
    when(() => tagsBloc.state).thenReturn(EntitiesLoadInProgress());
    when(() => scoresBloc.stream).thenAnswer((_) => NeverStream());
    when(() => scoresBloc.state).thenReturn(EntitiesLoadInProgress());
  });
  setUp(() {
    final verse = MockVerse();
    when(() => verse.reference).thenReturn('reference');
    when(() => verse.passage).thenReturn('passage');
    when(() => verse.tags).thenReturn({});
    when(() => verse.level).thenReturn(0);
    when(() => verse.toEntity()).thenReturn(verse);
    homeFlashcardBloc = MockHomeFlashcardBloc();
    when(() => homeFlashcardBloc.stream).thenAnswer((_) => NeverStream());
    when(() => homeFlashcardBloc.state)
        .thenReturn(FlashcardRun(verses: [verse], index: 0, inverse: false));
    I.registerSingleton<HomeFlashcardBloc>(homeFlashcardBloc);

    mockNavCubit = MockNavigationCubit();
    mockAuthBloc = MockAuthBloc();
    mockStartBloc = MockStartBloc();

    I.registerSingleton<NavigationCubit>(mockNavCubit);
    I.registerSingleton<AuthBloc>(mockAuthBloc);
    I.registerSingleton<StartBloc>(mockStartBloc);

    when(() => mockNavCubit.stream).thenAnswer((_) => NeverStream());
    when(() => mockNavCubit.propagateStream).thenAnswer((_) => NeverStream());
    when(() => mockAuthBloc.stream).thenAnswer((_) => NeverStream());
    when(() => mockStartBloc.stream).thenAnswer((_) => NeverStream());
    when(() => mockStartBloc.state).thenReturn(StartInitialState());

    routerDelegate = OuterRouterDelegate();
  });

  tearDown(() {
    I.unregister<NavigationCubit>();
    I.unregister<AuthBloc>();
    I.unregister<StartBloc>();
    I.unregister<HomeFlashcardBloc>();
  });

  group('OuterRouterDelegate', () {
    test('initial state', () {
      when(() => mockNavCubit.state).thenReturn(HomePath());
      expect(routerDelegate.currentConfiguration, isA<AppRoutePath>());
    });

    testWidgets('builds StartScreen for initial state',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomePath());
      when(() => mockAuthBloc.state).thenReturn(NoAuth());

      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));

      expect(find.byType(StartScreen), findsOneWidget);
    });

    testWidgets('builds BaseScreen for authenticated user',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomePath());
      when(() => mockAuthBloc.state).thenReturn(AccessRemote(
          accessToken: 'accessToken', refreshToken: 'refreshToken'));

      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));

      expect(find.byType(BaseScreen), findsOneWidget);
    });

    testWidgets('redirects to login for unauthenticated user',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomePath());
      when(() => mockAuthBloc.state).thenReturn(NoAuth());

      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));

      verify(() => mockStartBloc.add(LogInInitiated())).called(1);
      verify(() =>
              mockNavCubit.go(any(that: isA<LoginPath>()), propagates: true))
          .called(1);
    });

    test('setNewRoutePath updates navigation state', () async {
      final newPath = LoginPath();
      await routerDelegate.setNewRoutePath(newPath);
      verify(() => mockNavCubit.go(newPath, propagates: true)).called(1);
    });
  });

  group('InnerRouterDelegate: Home', () {
    setUp(() {
      when(() => mockAuthBloc.state).thenReturn(AccessRemote(
          accessToken: 'accessToken', refreshToken: 'refreshToken'));
    });

    testWidgets('HomePath builds HomeScreen', (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomePath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(HomeScreen), findsOneWidget);
    });

    testWidgets('HomeFlashcardPath builds FlashcardScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomeFlashcardPath(id: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(FlashcardScreen), findsOneWidget);
    });

    testWidgets('HomeStudyPath builds StudyScreen',
        (WidgetTester tester) async {
      when(() => homeFlashcardBloc.state).thenReturn(FlashcardLoadInProgress());
      when(() => mockNavCubit.state).thenReturn(HomeStudyPath(id: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(StudyScreen), findsOneWidget);
    });

    testWidgets('HomeEditPath builds EditVerseScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomeEditPath(id: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(EditVerseScreen), findsOneWidget);
    });

    testWidgets('HomeCreatePath builds EditVerseScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(HomeCreatePath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(EditVerseScreen), findsOneWidget);
    });
  });

  group('InnerRouterDelegate: Collection', () {
    setUpAll(() {
      final collectionService = MockCollectionService();
      I.registerSingleton<CollectionService>(collectionService);
      when(() => collectionService.fetchCollection(any())).thenAnswer((_) =>
          Future.value(Collection(1, 'label', 'name',
              description: 'descr',
              publisher: Publisher(1, 'pub',
                  language: Locale('en'), langRef: Locale('en')),
              created: DateTime.timestamp())));
    });

    setUp(() {
      I.registerSingleton<http.Client>(client);
      when(() => mockAuthBloc.state).thenReturn(AccessRemote(
          accessToken: 'accessToken', refreshToken: 'refreshToken'));
    });
    tearDown(() {
      I.unregister<http.Client>();
    });

    testWidgets('CollectionsPath builds CollectionsScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(CollectionsPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(CollectionsScreen), findsOneWidget);
    });

    testWidgets('CollectionDetailPath builds CollectionDetailScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(CollectionDetailPath(id: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(CollectionDetailScreen), findsOneWidget);
    });

    testWidgets('CollectionFlashcardPath builds FlashcardScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state)
          .thenReturn(CollectionFlashcardPath(id: 1, verseId: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(FlashcardScreen), findsOneWidget);
    });

    /*testWidgets('CollectionStudyPath builds FlashcardScreen',
        (WidgetTester tester) async {
      when(() => mockNavCubit.state).thenReturn(CollectionStudyPath(id: 1, verseId: 1));
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(StudyScreen), findsOneWidget);
    });*/
  });

  group('InnerRouterDelegate: Settings', () {
    testWidgets('SettingsPath builds SettingsScreen',
        (WidgetTester tester) async {
      when(() => mockAuthBloc.state).thenReturn(NoAuth());
      when(() => mockNavCubit.state).thenReturn(SettingsPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(SettingsScreen), findsOneWidget);
    });

    testWidgets('AccountCreatePath builds EditAccountScreen',
        (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(AccountCreatePath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(EditAccountScreen), findsOneWidget);
    });

    testWidgets('AccountEditPath builds EditAccountScreen',
        (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(AccountEditPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(EditAccountScreen), findsOneWidget);
    });
  });

  group('InnerRouterDelegate: Tags', () {
    testWidgets('TagsPath builds TagsScreen', (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(TagsPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(TagsScreen), findsOneWidget);
    });

    testWidgets('TagBinPath builds BinScreen', (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(TagBinPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(BinScreen<TagEntity, TagEntity>), findsOneWidget);
    });
  });

  group('InnerRouterDelegate: Simple', () {
    testWidgets('AccountBinPath builds BinScreen', (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(AccountBinPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(
          find.byType(BinScreen<AccountEntity, AccountEntity>), findsOneWidget);
    });

    testWidgets('VerseBinPath builds BinScreen', (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(VerseBinPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(BinScreen<VerseEntity, Verse>), findsOneWidget);
    });

    testWidgets('ScoresPath builds ScoresScreen', (WidgetTester tester) async {
      when(() => mockAuthBloc.state)
          .thenReturn(AccessRemote(accessToken: '', refreshToken: ''));
      when(() => mockNavCubit.state).thenReturn(ScoresPath());
      await tester.pumpWidget(MaterialApp.router(
        routerDelegate: routerDelegate,
        routeInformationParser: AppRouteInformationParser(),
      ));
      expect(find.byType(ScoresScreen), findsOneWidget);
    });
  });
}
