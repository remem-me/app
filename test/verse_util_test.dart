import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/current_account/current_account.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/book_service.dart';
import 'package:remem_me/util/verse_util.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockAuthBloc extends Mock implements AuthBloc {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('VerseUtil Korean books', ()  {
    late Map<String, Book> books;

    setUpAll(() async {
      books = await BookService().loadBooks(const Locale('ko'), Canon.protestant);
    });

    test('Korean book with chapter should be ranked', () async {
      expect(VerseUtil.rank("루가1", books), 56001000);
      expect(VerseUtil.rank("요한1", books), 57001000);
      expect(VerseUtil.rank("요한복음1", books), 57001000);
      expect(VerseUtil.rank("1요한1", books), 76001000);
      expect(VerseUtil.rank("요한1서1", books), 76001000);
    });

    test('Korean book with chapter and verse should be ranked', () async {
      expect(VerseUtil.rank("루가1:1", books), 56001001);
      expect(VerseUtil.rank("요한1:1", books), 57001001);
      expect(VerseUtil.rank("요한복음1:1", books), 57001001);
      expect(VerseUtil.rank("1요한1:1", books), 76001001);
      expect(VerseUtil.rank("요한1서1:1", books), 76001001);
    });
  });

  group('VerseUtil German books', () {
    late Map<String, Book> books;

    setUpAll(() async {
      books = await BookService().loadBooks(const Locale('de'), Canon.protestant);
    });

    test('German book with chapter should be ranked', () async {
      expect(VerseUtil.rank("Luk. 1", books), 56001000);
      expect(VerseUtil.rank("Joh 1", books), 57001000);
      expect(VerseUtil.rank("Johannes 1", books), 57001000);
      expect(VerseUtil.rank("1.Korinther 11", books), 60011000);
      expect(VerseUtil.rank("1.Joh 1", books), 76001000);
      expect(VerseUtil.rank("1Johannes 1", books), 76001000);
    });

    test('German book with chapter and verse should be ranked', () async {
      expect(VerseUtil.rank("Luk. 1, 1", books), 56001001);
      expect(VerseUtil.rank("Joh 1,1", books), 57001001);
      expect(VerseUtil.rank("Johannes 1,1", books), 57001001);
      expect(VerseUtil.rank("1.Korinther 11,23-26", books), 60011023);
      expect(VerseUtil.rank("1.Joh 1,1", books), 76001001);
      expect(VerseUtil.rank("1Johannes 1,1", books), 76001001);
    });
  });

  group('VerseUtil Japanese books', () {
    late Map<String, Book> books;

    setUpAll(() async {
      books = await BookService().loadBooks(const Locale('ja'), Canon.protestant);
    });

    test('Japanese book with chapter should be ranked', () async {
      expect(VerseUtil.rank("ルカ1", books), 56001000);
      expect(VerseUtil.rank("ヨハ11", books), 57011000);
      expect(VerseUtil.rank("ヨハネによる福音書1", books), 57001000);
      expect(VerseUtil.rank("ヨハ1 1", books), 76001000);
      expect(VerseUtil.rank("一ヨハ1", books), 76001000);
    });

    test('Japanese book with chapter and verse should be ranked', () async {
      expect(VerseUtil.rank("ルカによる福1:1", books), 56001001);
      expect(VerseUtil.rank("ヨハネ11:1", books), 57011001);
      expect(VerseUtil.rank("ヨハネによる福音1:1", books), 57001001);
      expect(VerseUtil.rank("ヨハ1 1:1", books), 76001001);
      expect(VerseUtil.rank("ヨハネの手紙一1:1", books), 76001001);
    });
  });

  group('VerseUtil Chinese books', () {
    late Map<String, Book> books;

    setUpAll(() async {
      books = await BookService().loadBooks(const Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hans'), Canon.protestant);
    });

    test('Chinese book with chapter should be ranked', () async {
      expect(VerseUtil.rank("路 1", books), 56001000);
      expect(VerseUtil.rank("约翰 1", books), 57001000);
      expect(VerseUtil.rank("林前 11", books), 60011000);
      expect(VerseUtil.rank("约一 1", books), 76001000);
    });

    test('Chinese book with chapter and verse should be ranked', () async {
      expect(VerseUtil.rank("路加福音 1:1", books), 56001001);
      expect(VerseUtil.rank("约翰福音 1:1", books), 57001001);
      expect(VerseUtil.rank("哥林多前书 11:23-26", books), 60011023);
      expect(VerseUtil.rank("约翰一书 1:1", books), 76001001);
    });
  });
}
