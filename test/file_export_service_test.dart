import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/services/file_export_service.dart';
import 'package:repository/repository.dart';


class MockSettings extends Mock implements Settings {}

void main() {
  group('FileExportService', () {
    late MockSettings settings;

    setUpAll(() {
      registerFallbackValue(DateService().today);
      TestWidgetsFlutterBinding.ensureInitialized();
      settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
    });

    test('encodeVerse() should encode all properties', () async {
      var verse = VerseEntity(
        '1. Mose 3,1-3',
        'In the beginning, God...',
        source: 'S',
        topic: 'T',
        image: 'I',
        commit: LocalDate.epoch(0),
        review: LocalDate.epoch(1),
        level: 1,
        tags: const {1: 'L1', 2: 'L2'}
      );
      var service = FileExportService();
      expect(service.encodeVerse(verse), [
        '1. Mose 3,1-3',
        'In the beginning, God...',
        'S',
        'T',
        '1970-01-01',
        '1970-01-02',
        '1',
        'L1; L2',
        'I'
      ]);
    });

    test('encodeVerse() should keep symbols', () async {
      var verse = VerseEntity("'", '"—#%&()*,./:;@[^_`{|~¡¦§¹»¿‐‒–—―');
      var service = FileExportService();
      expect(service.encodeVerse(verse), [
        "'",
        '"—#%&()*,./:;@[^_`{|~¡¦§¹»¿‐‒–—―',
        '',
        '',
        '',
        '',
        '-1',
        '',
        ''
      ]);
    });
  });
}
