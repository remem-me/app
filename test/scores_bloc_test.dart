import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';


class MockSettings extends Mock implements Settings {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockReplicationBloc extends Mock implements ReplicationBloc {}

class MockConnectivityCubit extends Mock implements ConnectivityCubit {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockScoresRepository extends Mock implements ScoresRepository {}

class MockMessageService extends Mock implements MessageService {}

void main() {
  final LocalDate initialDate = LocalDate(2010, 3, 24);

  group('ScoresBloc', () {
    late ScoresBloc scoresBloc;
    late MockScoresRepository repository;
    late List<ScoreEntity> testScores;
    late LocalDate testDate;
    late MockCurrentAccount currentAccount;
    late MockMessageService messageService;

    setUpAll(() {
      registerFallbackValue(const SnackBar(content: SizedBox.shrink()));
      TestWidgetsFlutterBinding.ensureInitialized();
      final authBloc = MockAuthBloc();
      when(() => authBloc.stream).thenAnswer((_) => NeverStream());
      I.registerSingleton<AuthBloc>(authBloc);
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      testDate = DateService().today; //LocalDatePattern.iso.parse(testKey).value;
      final baseScore = ScoreEntity(
          vkey: 'v',
          date: testDate,
          accountId: 1,
          change: 1,
          modified: 0,
          modifiedBy: 0);
      testScores = [
        baseScore.copyWith(vkey: 'v3', date: testDate - 1, id: 3),
        baseScore.copyWith(vkey: 'v1', id: 1, change: 2),
        baseScore.copyWith(vkey: 'v2', id: 2),
        baseScore.copyWith(vkey: 'v2', id: 2),
        baseScore.copyWith(vkey: 'v3', id: 3),
      ];
      repository = MockScoresRepository();
      when(() => repository.readList(accountId: captureAny(named: 'accountId')))
          .thenAnswer((_) => Future.value(testScores.slice(0,4)));
      I.registerSingleton<EntitiesRepository<ScoreEntity>>(repository);
    });

    setUp(() {
      if(I.isRegistered<CurrentAccount>()) I.unregister<CurrentAccount>();
      currentAccount = MockCurrentAccount();
      when(() => currentAccount.selected()).thenAnswer((invocation) =>
          Stream.fromIterable([Account.fromEntity(AccountEntity('test', id: 1))]));
      when(() => currentAccount.id).thenReturn(1);
      I.registerSingleton<CurrentAccount>(currentAccount);
      if(I.isRegistered<MessageService>()) I.unregister<MessageService>();
      messageService = MockMessageService();
      I.registerSingleton<MessageService>(messageService);
      scoresBloc = ScoresBloc();
    });

    blocTest<ScoresBloc, EntitiesState<ScoreEntity>>(
      'emits [EntitiesLoadSuccess()] when score is added, no message when daily goal is not reached',
      setUp: () {
        when(() => currentAccount.dailyGoal).thenReturn(5);
      },
      build: () => scoresBloc,
      act: (bloc) => bloc.add(
        EntitiesAdded<ScoreEntity>([testScores[4]], persist: false),
      ),
      expect: () => [
        EntitiesLoadInProgress<ScoreEntity>(),
        EntitiesLoadSuccess<ScoreEntity>(
          [testScores[0], testScores[1], testScores[2]],
        ),
        EntitiesLoadSuccess<ScoreEntity>(
          [testScores[0], testScores[1], testScores[2], testScores[4]],
        ),
      ],
      verify: (bloc) => expectLater(bloc.goalReachedStream, emitsInOrder([initialDate])).then((_) {
        verifyNever(() {
          messageService.show(any());
        });
      }),
    );

    test('displays message on Scored event when goal is reached', () async {
      when(() => currentAccount.dailyGoal).thenReturn(4);
      expectLater(scoresBloc.goalReachedStream, emitsInOrder([initialDate, DateService().today])).then((_) {
        verify(() {
          messageService.show(any());
        }).called(1);
      });
      scoresBloc.add(
        EntitiesAdded([testScores[4]], persist: false),
      );
    });

  });
}
