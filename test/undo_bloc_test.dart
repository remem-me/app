import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';


class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockVersesBloc extends Mock implements VersesBloc {}

void main() {
  group('UndoBloc', () {
    late UndoBloc undoBloc;
    late VersesBloc versesBloc;

    setUpAll(() {
      registerFallbackValue(const EntitiesUpdated(<VerseEntity>[]));
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      final currentAccount = MockCurrentAccount();
      I.registerSingleton<CurrentAccount>(currentAccount);
    });

    setUp(() {
      if (I.isRegistered<VersesBloc>()) I.unregister<VersesBloc>();
      versesBloc = MockVersesBloc();
      I.registerSingleton<VersesBloc>(versesBloc);
    });

    blocTest<UndoBloc, List<Undo>?>(
      'collects undos as they are added',
      build: () {
        undoBloc = UndoBloc();
        return undoBloc;
      },
      act: (bloc) {
        bloc.add(UndoAdded([
          Undo(EntityAction.create,
              [VerseEntity('ref1', 'pas1', id: 1, modified: 0)])
        ]));
        bloc.add(UndoAdded([
          Undo(EntityAction.update, [
            ScoreEntity(
                vkey: 'v',
                date: LocalDate(2021, 1, 1),
                id: 1,
                change: 1,
                modified: 0)
          ])
        ]));
        bloc.add(UndoAdded([
          Undo(EntityAction.delete, [TagEntity('tag1', id: 1, modified: 0)])
        ]));
      },
      expect: () => [
        [
          Undo(EntityAction.create,
              [VerseEntity('ref1', 'pas1', id: 1, modified: 0)])
        ],
        [
          Undo(EntityAction.update, [
            ScoreEntity(
                vkey: 'v',
                date: LocalDate(2021, 1, 1),
                id: 1,
                change: 1,
                modified: 0)
          ])
        ],
        [
          Undo(EntityAction.delete, [TagEntity('tag1', id: 1, modified: 0)])
        ],
      ],
    );

    blocTest<UndoBloc, List<Undo>?>(
      'undoes creation by setting entities as deleted',
      build: () {
        undoBloc = UndoBloc(initialState: [
          Undo(EntityAction.create,
              <VerseEntity>[VerseEntity('ref1', 'pas1', id: 1, modified: 0)])
        ]);
        return undoBloc;
      },
      act: (bloc) {
        bloc.add(UndoApplied());
      },
      expect: () => [null],
      verify: (_) =>
          verify(() => versesBloc.add(any(that: isA<EntitiesUpdated>())))
              .called(1),
    );
  });
}
