import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts_handler.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockTabBloc extends Mock implements TabBloc {}

class MockOrderedVersesNewBloc extends Mock implements OrderedVersesNewBloc {}

class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}

class MockOrderedVersesKnownBloc extends Mock
    implements OrderedVersesKnownBloc {}

class MockOrderedVersesAllBloc extends Mock implements OrderedVersesAllBloc {}

class MockFlashcardBloc extends Mock implements FlashcardBloc {}

class MockSelectionBloc extends Mock implements SelectionBloc {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockTtsHandler extends Mock implements TtsHandler {}

void main() {
  late MockSettings settings;
  late TabBloc tabBloc;
  late NavigationCubit nav;
  late FlashcardBloc flashcardBloc;
  late SelectionBloc selectionBloc;
  late MockOrderedVersesNewBloc versesNewBloc;
  late MockOrderedVersesDueBloc versesDueBloc;
  late MockOrderedVersesKnownBloc versesKnownBloc;
  late MockOrderedVersesAllBloc versesAllBloc;

  setUpAll(() {
    registerFallbackValue(const FlashcardInitiated(0));
    registerFallbackValue(HomePath());

    settings = MockSettings();
    when(() => settings.deviceId).thenReturn(1);
    I.registerSingleton<Settings>(settings);
  });

  setUp(() {
    if (I.isRegistered<CurrentAccount>()) I.unregister<CurrentAccount>();
    final currentAccount = MockCurrentAccount();
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.inverseLimit).thenReturn(5);
    when(() => currentAccount.reviewBatch).thenReturn(0);
    I.registerSingleton<CurrentAccount>(currentAccount);

    if (I.isRegistered<TabBloc>()) I.unregister<TabBloc>();
    tabBloc = MockTabBloc();
    when(() => tabBloc.stream).thenAnswer((_) => NeverStream());
    when(() => tabBloc.propagateStream).thenAnswer((_) => NeverStream());
    I.registerSingleton<TabBloc>(tabBloc);

    if (I.isRegistered<FlashcardBloc>()) I.unregister<FlashcardBloc>();
    flashcardBloc = MockFlashcardBloc();
    when(() => flashcardBloc.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<FlashcardBloc>(flashcardBloc);

    if (I.isRegistered<SelectionBloc>()) I.unregister<SelectionBloc>();
    selectionBloc = MockSelectionBloc();
    when(() => selectionBloc.state).thenReturn(const SelectionState({}));
    when(() => selectionBloc.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<SelectionBloc>(selectionBloc);

    if (I.isRegistered<NavigationCubit>()) I.unregister<NavigationCubit>();
    nav = MockNavigationCubit();
    when(() => nav.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<NavigationCubit>(nav);

    if (I.isRegistered<L10n>()) I.unregister<L10n>();
    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenReturn('mocked translation');
    when(() => l10n.t8(any(), any())).thenReturn('mocked translation');
    I.registerSingleton<L10n>(l10n);

    if (I.isRegistered<OrderedVersesNewBloc>()) {
      I.unregister<OrderedVersesNewBloc>();
    }
    if (I.isRegistered<OrderedVersesDueBloc>()) {
      I.unregister<OrderedVersesDueBloc>();
    }
    if (I.isRegistered<OrderedVersesKnownBloc>()) {
      I.unregister<OrderedVersesKnownBloc>();
    }
    if (I.isRegistered<OrderedVersesAllBloc>()) {
      I.unregister<OrderedVersesAllBloc>();
    }
    versesNewBloc = MockOrderedVersesNewBloc();
    versesDueBloc = MockOrderedVersesDueBloc();
    versesKnownBloc = MockOrderedVersesKnownBloc();
    versesAllBloc = MockOrderedVersesAllBloc();
    I.registerSingleton<OrderedVersesNewBloc>(versesNewBloc);
    I.registerSingleton<OrderedVersesDueBloc>(versesDueBloc);
    I.registerSingleton<OrderedVersesKnownBloc>(versesKnownBloc);
    I.registerSingleton<OrderedVersesAllBloc>(versesAllBloc);
    when(() => versesNewBloc.box).thenReturn(VerseBox.NEW);
    when(() => versesDueBloc.box).thenReturn(VerseBox.DUE);
    when(() => versesKnownBloc.box).thenReturn(VerseBox.KNOWN);
    when(() => versesAllBloc.box).thenReturn(VerseBox.ALL);
  });

  group('BoxFab actions', () {
    setUp(() {
      when(() => versesNewBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.CANON, _newVerses));
      when(() => versesNewBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _newVerses),
          ]));
      when(() => versesDueBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.DATE, _dueVerses));
      when(() => versesDueBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _dueVerses),
          ]));
      when(() => versesKnownBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.LEVEL, _knownVerses));
      when(() => versesKnownBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _knownVerses),
          ]));
      when(() => versesAllBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.RANDOM, _allVerses));
      when(() => versesAllBloc.stream).thenAnswer((_) => Stream.fromIterable([
            OrderSuccess(VerseOrder.ALPHABET, _allVerses),
          ]));
    });

    testWidgets('Navigates to verse editor when pressed', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.NEW));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('addBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      verify(() => nav.go(HomeCreatePath()))
          .called(1);
    });

    testWidgets('Launches flashcards with all verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.DUE));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('reviewBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      final capturedAdd =
          verify(() => flashcardBloc.add(captureAny())).captured;
      final flashcardLoaded = capturedAdd[0] as FlashcardLoaded;
      expect(flashcardLoaded.verses, hasLength(2));
      expect(flashcardLoaded.box, VerseBox.DUE);
      expect(flashcardLoaded.inverse, isFalse);
      verify(() =>
              nav.go(HomeFlashcardPath(id: 3), propagates: false))
          .called(1);
    });

    testWidgets('Launches flashcards with selected verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.DUE));
      when(() => selectionBloc.state)
          .thenReturn(SelectionState({1: _dueVerses[1]}));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('reviewBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      final captured =
          verify(() => flashcardBloc.add(captureAny())).captured.first;
      expect(captured, isA<FlashcardLoaded>());
      final verses = (captured as FlashcardLoaded).verses;
      expect(verses, hasLength(1));
      expect(verses[0].id, equals(4));
      verify(() =>
              nav.go(HomeFlashcardPath(id: 4), propagates: false))
          .called(1);
    });

    testWidgets('Launches inverse flashcards with all verses', (tester) async {
      when(() => tabBloc.state)
          .thenReturn(const TabState(true, VerseBox.KNOWN));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('inverseBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      final capturedAdd =
          verify(() => flashcardBloc.add(captureAny())).captured;
      final flashcardLoaded = capturedAdd[0] as FlashcardLoaded;
      expect(flashcardLoaded.verses, hasLength(2));
      expect(flashcardLoaded.box, VerseBox.KNOWN);
      expect(flashcardLoaded.inverse, isTrue);
      final capturedGo = verify(() =>
              nav.go(captureAny(), propagates: captureAny(named: 'propagates')))
          .captured;
      expect(capturedGo[0], isA<HomeFlashcardPath>());
      final path = capturedGo[0] as HomeFlashcardPath;
      expect(path.id, isIn([5,6]));
    });

    testWidgets('Launches inverse flashcards with selected verses',
        (tester) async {
      when(() => tabBloc.state)
          .thenReturn(const TabState(true, VerseBox.KNOWN));
      when(() => selectionBloc.state)
          .thenReturn(SelectionState({1: _knownVerses[1]}));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('inverseBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      final captured =
          verify(() => flashcardBloc.add(captureAny())).captured.first;
      expect(captured, isA<FlashcardLoaded>());
      final verses = (captured as FlashcardLoaded).verses;
      expect(verses, hasLength(1));
      expect(verses[0].id, equals(6));
      verify(() => nav.go(HomeFlashcardPath(id: 6),
          propagates: false)).called(1);
    });
  });

  group('BoxFab visibility', () {
    setUp(() {
      when(() => versesNewBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesDueBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesKnownBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesAllBloc.stream).thenAnswer((_) => NeverStream());
    });

    testWidgets('Hides action buttons when NEW is empty', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.NEW));
      when(() => versesNewBloc.state)
          .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabAddFinder = find.byType(BoxFabAdd);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabAddFinder, findsOneWidget);
      expect(fabPlayFinder, findsNothing);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Displays action buttons when NEW has verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.NEW));
      when(() => versesNewBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.CANON, _newVerses));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabAddFinder = find.byType(BoxFabAdd);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabAddFinder, findsOneWidget);
      expect(fabPlayFinder, findsOneWidget);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Hides action buttons when DUE is empty', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.DUE));
      when(() => versesDueBloc.state)
          .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabReviewFinder = find.byType(BoxFabReview);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabReviewFinder, findsNothing);
      expect(fabPlayFinder, findsNothing);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Displays action buttons when DUE has verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.DUE));
      when(() => versesDueBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.CANON, _dueVerses));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabReviewFinder = find.byType(BoxFabReview);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabReviewFinder, findsOneWidget);
      expect(fabPlayFinder, findsOneWidget);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Hides action buttons when KNOWN is empty', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.KNOWN));
      when(() => versesKnownBloc.state)
          .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabInverseFinder = find.byType(BoxFabInverse);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabInverseFinder, findsNothing);
      expect(fabPlayFinder, findsNothing);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Displays action buttons when KNOWN has verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.KNOWN));
      when(() => versesKnownBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.CANON, _knownVerses));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabInverseFinder = find.byType(BoxFabInverse);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabInverseFinder, findsOneWidget);
      expect(fabPlayFinder, findsOneWidget);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Hides action buttons when ALL is empty', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.ALL));
      when(() => versesAllBloc.state)
          .thenReturn(const OrderSuccess(VerseOrder.CANON, []));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabAddFinder = find.byType(BoxFabAdd);
      final fabReviewFinder = find.byType(BoxFabReview);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabAddFinder, findsNothing);
      expect(fabReviewFinder, findsNothing);
      expect(fabPlayFinder, findsNothing);
      expect(fabHelpFinder, findsOneWidget);
    });

    testWidgets('Displays action buttons when ALL has verses', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.ALL));
      when(() => versesAllBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.CANON, _allVerses));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabAddFinder = find.byType(BoxFabAdd);
      final fabReviewFinder = find.byType(BoxFabReview);
      final fabPlayFinder = find.byType(BoxFabTts);
      final fabHelpFinder = find.byType(HelpButton);
      expect(fabAddFinder, findsOneWidget);
      expect(fabReviewFinder, findsOneWidget);
      expect(fabPlayFinder, findsOneWidget);
      expect(fabHelpFinder, findsNothing);
    });
  });

  group('FabTts', () {
    late TtsHandler ttsHandler;

    setUp(() {
      if (I.isRegistered<TtsHandler>()) I.unregister<TtsHandler>();
      ttsHandler = MockTtsHandler();
      when(() => ttsHandler.play()).thenAnswer(((_) async {}));
      I.registerSingleton<TtsHandler>(ttsHandler);
      when(() => versesAllBloc.state)
          .thenReturn(OrderSuccess(VerseOrder.RANDOM, _allVerses));
      when(() => versesAllBloc.stream).thenAnswer((_) => Stream.fromIterable([
        OrderSuccess(VerseOrder.ALPHABET, _allVerses),
      ]));
    });

    testWidgets('Starts playback with first verse of box', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.ALL));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('ttsBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      verify(() => ttsHandler.setUp(_allVerses, index: 0, box: VerseBox.ALL))
          .called(1);
      verify(() => ttsHandler.play()).called(1);
    });

    testWidgets('Starts playback with first selected verse', (tester) async {
      when(() => tabBloc.state).thenReturn(const TabState(true, VerseBox.ALL));
      when(() => selectionBloc.state)
          .thenReturn(SelectionState({4: _allVerses[1]}));
      when(() => selectionBloc.stream).thenAnswer((_) => Stream.fromIterable([
            SelectionState({1: _allVerses[1]})
          ]));
      await tester.pumpWidget(_boilerplate(const BoxFabRow()));
      final fabFinder = find.byKey(const Key('ttsBtn'));
      expect(fabFinder, findsOneWidget);
      await tester.tap(fabFinder);
      verify(() => ttsHandler.setUp(_allVerses, index: 1, box: VerseBox.ALL))
          .called(1);
      verify(() => ttsHandler.play()).called(1);
    });
  });
}

List<Verse> _newVerses = [
  Verse('new1', 'p1', id: 1, modified: 0, accountId: 1),
];
List<Verse> _dueVerses = [
  Verse('due3', 'p3', id: 3, modified: 0, accountId: 1),
  Verse('due4', 'p4', id: 4, modified: 0, accountId: 1),
];
List<Verse> _knownVerses = [
  Verse('known5', 'p5', id: 5, modified: 0, accountId: 1),
  Verse('known6', 'p6', id: 6, modified: 0, accountId: 1),
];
List<Verse> _allVerses = _dueVerses + _knownVerses;

Widget _boilerplate(Widget child) => MultiBlocProvider(providers: [
      BlocProvider<TabBloc>.value(value: I<TabBloc>()),
      BlocProvider<FlashcardBloc>.value(value: I<FlashcardBloc>()),
      BlocProvider<SelectionBloc>.value(value: I<SelectionBloc>()),
      BlocProvider<NavigationCubit>.value(value: I<NavigationCubit>()),
    ], child: MaterialApp(home: Scaffold(body: child)));
