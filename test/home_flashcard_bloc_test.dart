import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockVersesBloc extends Mock implements VersesBloc {}

class MockScoresBloc extends Mock implements ScoresBloc {}

class MockVersesRepository extends Mock
    implements EntitiesRepository<VerseEntity> {}

class MockSettings extends Mock implements Settings {}

class MockUndoBloc extends Mock implements UndoBloc {}

class MockTabBloc extends Mock implements TabBloc {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

class MockOrderedVersesNewBloc extends Mock implements OrderedVersesNewBloc {}

class MockOrderedVersesDueBloc extends Mock implements OrderedVersesDueBloc {}

class MockOrderedVersesKnownBloc extends Mock
    implements OrderedVersesKnownBloc {}

class MockOrderedVersesAllBloc extends Mock implements OrderedVersesAllBloc {}

class AppRoutePathFake extends Fake implements AppRoutePath {}

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  MockOrderedVersesNewBloc versesNewBloc;
  MockOrderedVersesDueBloc versesDueBloc;
  MockOrderedVersesKnownBloc versesKnownBloc;
  MockCurrentAccount currentAccount = MockCurrentAccount();

  setUpAll(() {
    final settings = MockSettings();
    final tabBloc = MockTabBloc();
    registerFallbackValue(AppRoutePathFake());
    I.registerSingleton<UndoBloc>(MockUndoBloc());
    I.registerSingleton<VersesBloc>(MockVersesBloc());
    I.registerSingleton<ScoresBloc>(MockScoresBloc());
    I.registerSingleton<EntitiesRepository<VerseEntity>>(
        MockVersesRepository());
    I.registerSingleton<Settings>(settings);
    when(() => currentAccount.selected()).thenAnswer((_) => NeverStream());
    when(() => currentAccount.id).thenReturn(0);
    when(() => currentAccount.reviewBatch).thenReturn(2);
    when(() => currentAccount.inverseLimit).thenReturn(2);
    when(() => currentAccount.referenceIncluded).thenReturn(false);
    I.registerSingleton<CurrentAccount>(currentAccount);
    I.registerSingleton<TabBloc>(tabBloc);
    when(() => settings.deviceId).thenReturn(0);
    when(() => tabBloc.state)
        .thenReturn(const TabState(true, VerseBox.KNOWN));
  });

  group('HomeFlashcardBloc DUE box', () {
    late MockNavigationCubit nav;

    setUpAll(() {
      if (I.isRegistered<OrderedVersesNewBloc>()) I.unregister<OrderedVersesNewBloc>();
      if (I.isRegistered<OrderedVersesDueBloc>()) I.unregister<OrderedVersesDueBloc>();
      if (I.isRegistered<OrderedVersesKnownBloc>()) I.unregister<OrderedVersesKnownBloc>();
      versesNewBloc = MockOrderedVersesNewBloc();
      versesDueBloc = MockOrderedVersesDueBloc();
      versesKnownBloc = MockOrderedVersesKnownBloc();
      I.registerSingleton<OrderedVersesNewBloc>(versesNewBloc);
      I.registerSingleton<OrderedVersesDueBloc>(versesDueBloc);
      I.registerSingleton<OrderedVersesKnownBloc>(versesKnownBloc);
      when(() => versesNewBloc.state).thenReturn(OrderInProgress());
      when(() => versesNewBloc.stream).thenAnswer((_) => Stream.fromIterable([
        const OrderSuccess(VerseOrder.ALPHABET, []),
      ]));
      when(() => versesDueBloc.state).thenReturn(OrderInProgress());
      when(() => versesDueBloc.stream).thenAnswer((_) => Stream.fromIterable([
        OrderSuccess(VerseOrder.ALPHABET, _testVerses()),
      ]));
      when(() => versesKnownBloc.state).thenReturn(OrderInProgress());
      when(() => versesKnownBloc.stream).thenAnswer((_) => Stream.fromIterable([
        const OrderSuccess(VerseOrder.ALPHABET, []),
      ]));
    });

    setUp(() {
      // rebuild objects that are modified by tests
      if (I.isRegistered<NavigationCubit>()) I.unregister<NavigationCubit>();
      nav = MockNavigationCubit();
      when(() => nav.stream).thenAnswer((_) => NeverStream());
      when(() => nav.propagateStream).thenAnswer((_) => NeverStream());
      I.registerSingleton<NavigationCubit>(nav);
      when(() => currentAccount.inverseLimit).thenReturn(2);
    });

    blocTest<HomeFlashcardBloc, FlashcardState>(
        'updates navigation when flashcards are cancelled',
        build: () => HomeFlashcardBloc(),
        act: (bloc) {
          bloc.add(FlashcardLoaded(
              verses: _testVerses(),
              box: VerseBox.DUE,
              index: 0,
              inverse: false));
          bloc.add(FlashcardCancelled());
        },
        expect: () => [
              isA<FlashcardRun>(),
              isA<FlashcardLoadInProgress>(),
            ],
        verify: (bloc) => verify(() => nav.goHome()).called(1));

    blocTest<HomeFlashcardBloc, FlashcardState>(
      'emits [FlashcardRun()] when passage review session is started by navigation event',
      setUp: () {
        if (I.isRegistered<NavigationCubit>()) {
          I.unregister<NavigationCubit>();
        }
        nav = MockNavigationCubit();
        when(() => nav.propagateStream).thenAnswer((_) => Stream.fromIterable([
              HomePath(),
              HomeFlashcardPath(id: 3),
            ]));
        I.registerSingleton<NavigationCubit>(nav);
      },
      build: () => HomeFlashcardBloc(),
      expect: () => [
        FlashcardRun(
            verses: _testVerses().sublist(0, 2),
            box: VerseBox.DUE,
            inverse: false,
            reverse: false,
            flipping: false,
            visible: false,
            hinting: {FlashcardHint.subtitle, FlashcardHint.tags},
            revealLines: 0,
            index: 0,
            countRemembered: 0,
            countForgotten: 0,
            previous: null),
      ],
    );

    blocTest<HomeFlashcardBloc, FlashcardState>(
      'emits [FlashcardRun()] when reference review session is started by navigation event',
      setUp: () {
        if (I.isRegistered<NavigationCubit>()) {
          I.unregister<NavigationCubit>();
        }
        nav = MockNavigationCubit();
        when(() => nav.propagateStream).thenAnswer((_) => Stream.fromIterable([
              HomePath(),
              HomeFlashcardPath(id: 3),
            ]));
        I.registerSingleton<NavigationCubit>(nav);
      },
      build: () => HomeFlashcardBloc(),
      expect: () => [
        FlashcardRun(
            verses: _testVerses().sublist(0, 2),
            box: VerseBox.DUE,
            inverse: false,
            reverse: false,
            flipping: false,
            visible: false,
            hinting: {FlashcardHint.subtitle, FlashcardHint.tags},
            revealLines: 0,
            index: 0,
            countRemembered: 0,
            countForgotten: 0,
            previous: null),
      ],
    );

    blocTest<HomeFlashcardBloc, FlashcardState>(
        'adds FlashcardCancelled on change from HomeFlashcardPath to HomePath',
        setUp: () {
          if (I.isRegistered<NavigationCubit>()) {
            I.unregister<NavigationCubit>();
          }
          nav = MockNavigationCubit();
          when(() => nav.stream).thenAnswer((_) => NeverStream());
          when(() => nav.propagateStream).thenAnswer((_) =>
              Stream.fromIterable([HomeFlashcardPath(id: 3), HomePath()]));
          I.registerSingleton<NavigationCubit>(nav);
        },
        build: () =>
            HomeFlashcardBloc(const FlashcardRun(verses: [], inverse: false)),
        expect: () => [
              isA<FlashcardLoadInProgress>(),
              isA<FlashcardRun>(),
            ],
        verify: (bloc) => verify(() => nav.goHome()).called(1));

    blocTest<HomeFlashcardBloc, FlashcardState>(
        'does not add FlashcardCancelled from any other state than HomeFlashcardPath to HomePath.',
        setUp: () {
          if (I.isRegistered<NavigationCubit>()) {
            I.unregister<NavigationCubit>();
          }
          nav = MockNavigationCubit();
          when(() => nav.stream).thenAnswer((_) => NeverStream());
          when(() => nav.propagateStream).thenAnswer(
              (_) => Stream.fromIterable([CollectionsPath(), HomePath()]));
          I.registerSingleton<NavigationCubit>(nav);
        },
        build: () =>
            HomeFlashcardBloc(const FlashcardRun(verses: [], inverse: false)),
        expect: () => [
              isA<FlashcardLoadInProgress>(),
            ],
        verify: (bloc) => verify(() => nav.goHome()).called(1));
  });

  
  group('HomeFlashcardBloc KNOWN box', () {
    late MockNavigationCubit nav;

    setUpAll(() {
      if (I.isRegistered<OrderedVersesNewBloc>()) I.unregister<OrderedVersesNewBloc>();
      if (I.isRegistered<OrderedVersesDueBloc>()) I.unregister<OrderedVersesDueBloc>();
      if (I.isRegistered<OrderedVersesKnownBloc>()) I.unregister<OrderedVersesKnownBloc>();
      versesNewBloc = MockOrderedVersesNewBloc();
      versesDueBloc = MockOrderedVersesDueBloc();
      versesKnownBloc = MockOrderedVersesKnownBloc();
      I.registerSingleton<OrderedVersesNewBloc>(versesNewBloc);
      I.registerSingleton<OrderedVersesDueBloc>(versesDueBloc);
      I.registerSingleton<OrderedVersesKnownBloc>(versesKnownBloc);
      when(() => versesNewBloc.state).thenReturn(OrderInProgress());
      when(() => versesNewBloc.stream).thenAnswer((_) => Stream.fromIterable([
        const OrderSuccess(VerseOrder.ALPHABET, []),
      ]));
      when(() => versesDueBloc.state).thenReturn(OrderInProgress());
      when(() => versesDueBloc.stream).thenAnswer((_) => Stream.fromIterable([
        const OrderSuccess(VerseOrder.ALPHABET, []),
      ]));
      when(() => versesKnownBloc.state).thenReturn(OrderInProgress());
      when(() => versesKnownBloc.stream).thenAnswer((_) => Stream.fromIterable([
        OrderSuccess(VerseOrder.ALPHABET, _testVerses()),
      ]));
    });

    setUp(() {
      if (I.isRegistered<NavigationCubit>()) I.unregister<NavigationCubit>();
      nav = MockNavigationCubit();
      when(() => nav.stream).thenAnswer((_) => NeverStream());
      when(() => nav.propagateStream).thenAnswer((_) => NeverStream());
      I.registerSingleton<NavigationCubit>(nav);
    });

    blocTest<HomeFlashcardBloc, FlashcardState>(
      'reference review session with limited flashcards',
      setUp: () {
        if (I.isRegistered<NavigationCubit>()) {
          I.unregister<NavigationCubit>();
        }
        nav = MockNavigationCubit();
        when(() => nav.propagateStream).thenAnswer((_) => Stream.fromIterable([
          HomePath(),
          HomeFlashcardPath(id: 3),
        ]));
        I.registerSingleton<NavigationCubit>(nav);
      },
      build: () => HomeFlashcardBloc(),
      expect: () => [
        isA<FlashcardRun>()
            .having((run) => run.verses, 'verses', hasLength(2))
            .having((run) => run.box, 'box', equals(VerseBox.KNOWN))
            .having((run) => run.inverse, 'inverse', isTrue)
            .having((run) => run.hinting, 'hinting', isNull),
      ],
    );

    blocTest<HomeFlashcardBloc, FlashcardState>(
      'reference review session with unlimited flashcards',
      setUp: () {
        when(() => currentAccount.inverseLimit).thenReturn(0);
        if (I.isRegistered<NavigationCubit>()) {
          I.unregister<NavigationCubit>();
        }
        nav = MockNavigationCubit();
        when(() => nav.propagateStream).thenAnswer((_) => Stream.fromIterable([
          HomePath(),
          HomeFlashcardPath(id: 3),
        ]));
        I.registerSingleton<NavigationCubit>(nav);
      },
      build: () => HomeFlashcardBloc(),
      expect: () => [
        isA<FlashcardRun>()
            .having((run) => run.verses, 'verses', hasLength(3))
            .having((run) => run.box, 'box', equals(VerseBox.KNOWN))
            .having((run) => run.inverse, 'inverse', isTrue)
            .having((run) => run.hinting, 'hinting', isNull),
      ],
    );
  });

}

List<Verse> _testVerses() => [
      Verse(
        'ref3',
        'pas3',
        id: 3,
        modified: 0,
        accountId: 1,
      ),
      Verse(
        'ref1',
        'pas1',
        id: 1,
        modified: 0,
        accountId: 1,
        tags: const {1: 'tag1'},
      ),
      Verse(
        'ref2',
        'pas2',
        id: 2,
        modified: 0,
        accountId: 1,
        tags: const {2: 'tag2'},
      ),
    ];
