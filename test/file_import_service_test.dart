import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:file_selector/file_selector.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/file_import_service.dart';
import 'package:repository/repository.dart';

class MockSettings extends Mock implements Settings {}

class MockTagsBloc
    extends MockBloc<EntitiesEvent<TagEntity>, EntitiesState<TagEntity>>
    implements TagsBloc {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockXFile extends Mock implements XFile {}

void main() {
  late FileImportService fileImportService;
  late TagsBloc mockTagsBloc;
  late CurrentAccount mockCurrentAccount;

  setUp(() {
    final settings = MockSettings();
    I.registerSingleton<Settings>(settings);
    when(() => settings.deviceId).thenReturn(1);
    mockCurrentAccount = MockCurrentAccount();
    I.registerSingleton<CurrentAccount>(mockCurrentAccount);
    when(() => mockCurrentAccount.id).thenReturn(1);
    mockTagsBloc = MockTagsBloc();
    I.registerSingleton<TagsBloc>(mockTagsBloc);
    when(() => mockTagsBloc.state)
        .thenReturn(EntitiesLoadSuccess<TagEntity>([]));
    fileImportService = FileImportService();
  });

  tearDown(() {
    I.reset();
  });

  group('FileImportService', () {
    test('FileImportService returns the same instance', () {
      final instance1 = FileImportService();
      final instance2 = FileImportService();
      expect(instance1, same(instance2));
    });

    group('decodeCsv', () {
      test('should decode CSV text into VerseEntities', () {
        const csvText =
            'reference,passage,source,topic,commit,review,level,tags,image\r\n'
            'John 3:16,"For God so loved the world...",Bible,Love,2023-01-01,2023-01-02,1,tag1; tag2,image.jpg\r\n'
            'Romans 8:28,"And we know that in all things...",Bible,Faith,2023-02-01,2023-02-02,2,tag3,';
        final newTags = <TagEntity>[];
        final result = fileImportService.decodeCsv(csvText, newTags);
        expect(result.length, 2);
        expect(result[0].reference, 'John 3:16');
        expect(result[0].passage, 'For God so loved the world...');
        expect(result[0].source, 'Bible');
        expect(result[0].topic, 'Love');
        expect(result[0].commit, LocalDate.parse('2023-01-01'));
        expect(result[0].review, LocalDate.parse('2023-01-02'));
        expect(result[0].level, 1);
        expect(result[0].tags.length, 2);
        expect(result[0].image, 'image.jpg');
        expect(result[0].accountId, 1);
        expect(result[1].reference, 'Romans 8:28');
        expect(result[1].passage, 'And we know that in all things...');
        expect(result[1].source, 'Bible');
        expect(result[1].topic, 'Faith');
        expect(result[1].commit, LocalDate.parse('2023-02-01'));
        expect(result[1].review, LocalDate.parse('2023-02-02'));
        expect(result[1].level, 2);
        expect(result[1].tags.length, 1);
        expect(result[1].image, null);
        expect(result[1].accountId, 1);
      });
    });

    group('decodeLegacy', () {
      test('should decode legacy text into VerseEntities', () {
        const legacyText =
            'R: John 3:16\nQ: Bible\nT: Love\nP: For God so loved the world...\nV: 2023-01-02\nL: 1\nC: 2023-01-01\nS: tag1; tag2\n'
            'R: Romans 8:28\nQ: Bible\nT: Faith\nP: And we know that in all things...\nV: 2023-02-02\nL: 2\nC: 2023-02-01\nS: tag3\n';
        final newTags = <TagEntity>[];
        final result = fileImportService.decodeLegacy(legacyText, newTags);
        expect(result.length, 2);
        expect(result[0].reference, 'John 3:16');
        expect(result[0].passage, 'For God so loved the world...');
        expect(result[0].source, 'Bible');
        expect(result[0].topic, 'Love');
        expect(result[0].commit, LocalDate.parse('2023-01-01'));
        expect(result[0].review, LocalDate.parse('2023-01-02'));
        expect(result[0].level, 1);
        expect(result[0].tags.length, 2);
        expect(result[0].accountId, 1);
        expect(result[1].reference, 'Romans 8:28');
        expect(result[1].passage, 'And we know that in all things...\n');
        expect(result[1].source, 'Bible');
        expect(result[1].topic, 'Faith');
        expect(result[1].commit, LocalDate.parse('2023-02-01'));
        expect(result[1].review, LocalDate.parse('2023-02-02'));
        expect(result[1].level, 2);
        expect(result[1].tags.length, 1);
        expect(result[1].accountId, 1);
      });

      test('should handle multi-line passages in legacy text', () {
        const legacyText =
            'R: John 3:16\nQ: Bible\nT: Love\nP: For God so loved the world,\nthat he gave his only Son,\nthat whoever believes in him shall not perish but have eternal life.\nV: 2023-01-02\nL: 1\nC: 2023-01-01\nS: tag1; tag2\n';
        final newTags = <TagEntity>[];
        final result = fileImportService.decodeLegacy(legacyText, newTags);
        expect(result.length, 1);
        expect(result[0].passage,
            'For God so loved the world,\nthat he gave his only Son,\nthat whoever believes in him shall not perish but have eternal life.\n');
      });

      test('should handle empty blocks in legacy text', () {
        const legacyText = '';
        final newTags = <TagEntity>[];
        final result = fileImportService.decodeLegacy(legacyText, newTags);
        expect(result.isEmpty, true);
      });

      test('should handle null verse in legacy text', () {
        const legacyText =
            'Q: Bible\nT: Love\nP: For God so loved the world...\nV: 2023-01-02\nL: 1\nC: 2023-01-01\nS: tag1; tag2\n';
        final newTags = <TagEntity>[];
        final result = fileImportService.decodeLegacy(legacyText, newTags);
        expect(result.isEmpty, true);
      });
    });

    group('decodeVerse', () {
      test('should decode a row into a VerseEntity', () {
        final row = [
          'John 3:16',
          'For God so loved the world...',
          'Bible',
          'Love',
          '2023-01-01',
          '2023-01-02',
          '1',
          'tag1; tag2',
          'image.jpg'
        ];
        final newTags = <TagEntity>[];
        when(() => mockTagsBloc.state)
            .thenReturn(EntitiesLoadSuccess<TagEntity>([]));
        final result = fileImportService.decodeVerse(row, newTags);
        expect(result.reference, 'John 3:16');
        expect(result.passage, 'For God so loved the world...');
        expect(result.source, 'Bible');
        expect(result.topic, 'Love');
        expect(result.commit, LocalDate.parse('2023-01-01'));
        expect(result.review, LocalDate.parse('2023-01-02'));
        expect(result.level, 1);
        expect(result.tags.length, 2);
        expect(result.tags.containsValue('tag1'), true);
        expect(result.tags.containsValue('tag2'), true);
        expect(result.image, 'image.jpg');
        expect(result.accountId, 1);
      });

      test('should handle empty tags', () {
        final row = [
          'Romans 8:28',
          'And we know that in all things...',
          'Bible',
          'Faith',
          '2023-02-01',
          '2023-02-02',
          '2',
          '',
          ''
        ];
        final newTags = <TagEntity>[];
        when(() => mockTagsBloc.state)
            .thenReturn(EntitiesLoadSuccess<TagEntity>([]));
        final result = fileImportService.decodeVerse(row, newTags);
        expect(result.reference, 'Romans 8:28');
        expect(result.passage, 'And we know that in all things...');
        expect(result.source, 'Bible');
        expect(result.topic, 'Faith');
        expect(result.commit, LocalDate.parse('2023-02-01'));
        expect(result.review, LocalDate.parse('2023-02-02'));
        expect(result.level, 2);
        expect(result.tags.isEmpty, true);
        expect(result.image, null);
        expect(result.accountId, 1);
      });

      test('should handle missing rows', () {
        final row = [
          'Romans 8:28',
          'And we know that in all things...',
          'Bible',
          'Faith',
        ];
        final newTags = <TagEntity>[];
        when(() => mockTagsBloc.state)
            .thenReturn(EntitiesLoadSuccess<TagEntity>([]));
        final result = fileImportService.decodeVerse(row, newTags);
        expect(result.reference, 'Romans 8:28');
        expect(result.passage, 'And we know that in all things...');
        expect(result.source, 'Bible');
        expect(result.topic, 'Faith');
        expect(result.commit, null);
        expect(result.review, null);
        expect(result.level, -1);
        expect(result.tags.isEmpty, true);
        expect(result.image, null);
        expect(result.accountId, 1);
      });

      test('should handle new tags', () {
        final row = [
          'Romans 8:28',
          'And we know that in all things...',
          'Bible',
          'Faith',
          '2023-02-01',
          '2023-02-02',
          '2',
          'tag1;tag2;tag3',
          ''
        ];
        final newTags = <TagEntity>[];
        when(() => mockTagsBloc.state)
            .thenReturn(EntitiesLoadSuccess<TagEntity>([]));
        final result = fileImportService.decodeVerse(row, newTags);
        expect(result.reference, 'Romans 8:28');
        expect(result.passage, 'And we know that in all things...');
        expect(result.source, 'Bible');
        expect(result.topic, 'Faith');
        expect(result.commit, LocalDate.parse('2023-02-01'));
        expect(result.review, LocalDate.parse('2023-02-02'));
        expect(result.level, 2);
        expect(result.tags.length, 3);
        expect(result.tags.containsValue('tag1'), true);
        expect(result.tags.containsValue('tag2'), true);
        expect(result.tags.containsValue('tag3'), true);
        expect(newTags.length, 3);
        expect(newTags[0].text, 'tag1');
        expect(newTags[1].text, 'tag2');
        expect(newTags[2].text, 'tag3');
        expect(result.image, null);
        expect(result.accountId, 1);
      });
    });
  });
}
