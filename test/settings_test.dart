import 'package:app_core/app_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/widgets/settings/general_setting.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:remem_me/widgets/zoom/zoom.dart';
import 'package:repository/repository.dart';

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockBox extends Mock implements Box<String> {}

class MockZoomCubit extends Mock implements ZoomCubit {}

void main() {
  late MockSettings settings;

  setUp(() {
    if (I.isRegistered<L10n>()) I.unregister<L10n>();
    if (I.isRegistered<Settings>()) I.unregister<Settings>();
    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenReturn('mocked translation');
    when(() => l10n.t8(any(), any())).thenReturn('mocked translation');
    I.registerSingleton<L10n>(l10n);
    final box = MockBox();
    settings = MockSettings();
    when(() => settings.listenable(keys: any(named: 'keys')))
        .thenReturn(ValueNotifier(box));
    when(() => settings.putDouble(any(), any())).thenAnswer((_) async {});
    when(() => settings.putBool(any(), any())).thenAnswer((_) async {});
    I.registerSingleton<Settings>(settings);
  });

  group('GeneralSetting', () {
    setUp(() {
      if (I.isRegistered<ZoomCubit>()) I.unregister<ZoomCubit>();
      final zoomCubit = MockZoomCubit();
      I.registerSingleton<ZoomCubit>(zoomCubit);
    });

    testWidgets('Tip is visible', (tester) async {
      when(() => settings.getBool(Settings.TIP_TOOLTIP)).thenReturn(true);
      await tester.pumpWidget(_boilerplate(const GeneralSettingForm()));
      final tipRowFinder = find.byType(TipRow);
      expect(tipRowFinder, findsOneWidget);
    });

    testWidgets('Tip is hidden', (tester) async {
      when(() => settings.getBool(Settings.TIP_TOOLTIP)).thenReturn(false);
      await tester.pumpWidget(_boilerplate(const GeneralSettingForm()));
      final tipRowFinder = find.byType(TipRow);
      expect(tipRowFinder, findsNothing);
    });

    testWidgets('Zoom gets set', (tester) async {
      await tester.pumpWidget(_boilerplate(const GeneralSettingForm()));
      final sliderFinder = find.byType(SettingSlider);
      expect(sliderFinder, findsOneWidget);
      await tester.drag(sliderFinder, const Offset(100, 0));
      final captured = verify(() =>
              settings.putDouble(Settings.ZOOM_SCALE_FACTOR, captureAny()))
          .captured;
      expect(captured, contains(1.5));
    });

    testWidgets('Tips get turned on', (tester) async {
      when(() => settings.getBool(Settings.TIP_TOOLTIP)).thenReturn(false);
      await tester.pumpWidget(_boilerplate(const GeneralSettingForm()));
      final switchFinder = find.byType(Switch);
      expect(switchFinder, findsOneWidget);
      await tester.tap(switchFinder);
      verify(() => settings.putBool(Settings.TIP_TOOLTIP, true)).called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, true))
          .called(1);
      verify(() => settings.putBool(Settings.TIP_VERSES_RECOVER, true))
          .called(1);
    });

    testWidgets('Tips get turned off', (tester) async {
      await tester.pumpWidget(_boilerplate(const GeneralSettingForm()));
      final switchFinder = find.byType(Switch);
      expect(switchFinder, findsOneWidget);
      await tester.tap(switchFinder);
      verify(() => settings.putBool(Settings.TIP_TOOLTIP, false)).called(1);
      verify(() => settings.putBool(Settings.TIP_VERSE_SELECT, false))
          .called(1);
      verify(() => settings.putBool(Settings.TIP_VERSES_RECOVER, false))
          .called(1);
    });
  });
}

Widget _boilerplate(Widget child) => Dialog(
      child: Directionality(
          textDirection: TextDirection.ltr,
          child: Overlay(initialEntries: [
            OverlayEntry(builder: (context) {
              return child;
            }),
          ])),
    );
