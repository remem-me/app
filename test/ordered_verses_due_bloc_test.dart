import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';

class MockL10n extends Mock implements L10n {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockFilteredVerseBloc
    extends MockBloc<FilteredVersesEvent, FilteredVersesState>
    implements FilteredVersesBloc {}

class MockVerse extends Mock implements Verse {}
class MockAccount extends Mock implements Account {}

void main() {
  final today = DateService().today;
  final yesterday = today - 1;
  late MockFilteredVerseBloc filteredBloc;
  late MockVerse verse;

  setUpAll(() {
    // dependency injection
    final l10n = MockL10n();
    when(() => l10n.t8(any())).thenAnswer((invocation) {
      final key = invocation.positionalArguments[0] as String;
      return key;
    });
    I.registerSingleton<L10n>(l10n);

    final account = MockAccount();
    when(() => account.orderDue).thenReturn(VerseOrder.ALPHABET);
    final currentAccount = MockCurrentAccount();
    when(() => currentAccount.id).thenReturn(1);
    when(() => currentAccount.topicPreferred).thenReturn(false);
    when(() => currentAccount.state).thenReturn(account);
    I.registerSingleton<CurrentAccount>(currentAccount);
  });


  setUp(() {
    // Create a mock instance
    if(I.isRegistered<FilteredVersesBloc>()) I.unregister<FilteredVersesBloc>();
    filteredBloc = MockFilteredVerseBloc();
    verse = MockVerse();
    when(() => verse.reference).thenReturn('ref');
    when(() => verse.due).thenReturn(yesterday);

    // Stub the state stream
    whenListen(
      filteredBloc,
      Stream.fromIterable([
        FilteredVersesSuccess([verse], date: yesterday),
        FilteredVersesSuccess([verse], date: today),
      ]),
      initialState: FilterInProgress(),
    );
    I.registerSingleton<FilteredVersesBloc>(filteredBloc);
  });

  group('OrderedVersesDueBloc', () {
    blocTest(
      'does not emit on date change',
      build: () => OrderedVersesDueBloc(),
      expect: () => [
        OrderInProgress(),
        OrderSuccess(VerseOrder.ALPHABET, [verse], isUpdating: false),
      ],
    );
  });
}
