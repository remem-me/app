import 'package:app_core/app_core.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/paths/paths.dart';
import 'package:remem_me/services/start_service.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';

class MockStartService extends Mock implements StartService {}

class MockL10n extends Mock implements L10n {}

class MockSettings extends Mock implements Settings {}

class MockAuthBloc extends Mock implements AuthBloc {}

class MockNavigationCubit extends Mock implements NavigationCubit {}

void main() {
  late StartBloc startBloc;
  late AuthBloc authBloc;
  late NavigationCubit nav;
  StartService startService = MockStartService();

  setUpAll(() {
    registerFallbackValue(AuthLoadedFromSettings());
    final l10n = MockL10n();
    I.registerSingleton<L10n>(l10n);
    when(() => l10n.t8(any())).thenAnswer((invocation) {
      final arg = invocation.positionalArguments[0];
      return arg;
    });
    when(() => l10n.t8(any(), any())).thenAnswer((invocation) {
      final arg1 = invocation.positionalArguments[0];
      return arg1;
    });

    final settings = MockSettings();
    when(() => settings.deviceId).thenReturn(0);
    I.registerSingleton<Settings>(settings);
    I.registerSingleton<CurrentAccount>(CurrentAccount());

    authBloc = MockAuthBloc();
    when(() => authBloc.state).thenReturn(NoAuth());
    when(() => authBloc.stream).thenAnswer((_) => NeverStream());
    I.registerSingleton<AuthBloc>(authBloc);

    nav = MockNavigationCubit();
    when(() => nav.state).thenReturn(StartBasePath());
    when(() => nav.stream).thenAnswer((_) => NeverStream());
    when(() => nav.propagateStream).thenAnswer((_) => NeverStream());
    I.registerSingleton<NavigationCubit>(nav);
  });

  setUp(() {
    startService = MockStartService();
    I.registerSingleton<StartService>(startService);
    when(() => startService.register(
        email: any(named: 'email'),
        password: 'correct')).thenAnswer((invocation) async {
      final email = invocation.namedArguments[#email] as String;
      return StartInfo(
        {'email': email, 'password': 'correct'},
        dialogId: 'register-sent',
        title: 'title',
        message: 'message',
      );
    });
    when(() => startService.register(
        email: any(named: 'email'),
        password: 'error')).thenAnswer((invocation) async {
      final email = invocation.namedArguments[#email] as String;
      return SignUpFeedback(
        {'email': email, 'password': 'error'},
        messages: {
          'messages': ['error message']
        },
      );
    });
    when(() =>
            startService.login(email: any(named: 'email'), password: 'correct'))
        .thenAnswer((invocation) async {
      final email = invocation.namedArguments[#email] as String;
      return LogInSuccess(Granted(email,
          AccessRemote(accessToken: 'accessToken', refreshToken: 'refreshToken')));
    });
    when(() =>
            startService.login(email: any(named: 'email'), password: 'wrong'))
        .thenAnswer((invocation) async {
      final email = invocation.namedArguments[#email] as String;
      return LogInFeedback(
        {'email': email, 'password': 'wrong'},
        messages: {
          'messages': ['error message']
        },
      );
    });
    when(() => startService.forgotPassword(email: 'email'))
        .thenAnswer((invocation) async {
      return StartInfo({'email': 'email'},
          dialogId: 'dialog', title: 'title', message: 'sent');
    });
    when(() => startService.forgotPassword(email: 'wrong'))
        .thenAnswer((invocation) async {
      return LogInFeedback(
        {'email': 'wrong'},
        messages: {
          'messages': ['not sent']
        },
      );
    });
    when(() => startService.submitPassword(
        password: 'pw',
        uidb64: any(named: 'uidb64'),
        token: any(named: 'token'))).thenAnswer((invocation) async {
      return StartInfo({'email': 'email'},
          dialogId: 'dialog', title: 'title', message: 'changed');
    });
    when(() => startService.submitPassword(
        password: 'wrong',
        uidb64: any(named: 'uidb64'),
        token: any(named: 'token'))).thenAnswer((invocation) async {
      final uidb64 = invocation.namedArguments[#uidb64] as String;
      final token = invocation.namedArguments[#token] as String;
      return ResetPasswordFeedback(
        {'password': 'wrong'},
        messages: {
          'messages': ['not changed']
        },
        uidb64: uidb64,
        token: token,
      );
    });

    startBloc = StartBloc();
  });

  tearDown(() {
    I.unregister<StartService>();
  });

  group('Credentials', () {
    blocTest<StartBloc, StartState>('keeps credentials until user is logged in',
        build: () => startBloc,
        act: (bloc) {
          bloc.add(SignUpInitiated());
          bloc.add(SignedUp(email: 'email', password: 'error'));
          bloc.add(DialogDismissed('sign-up'));
          bloc.add(SignUpInitiated());
          bloc.add(SignedUp(email: 'email', password: 'correct'));
          bloc.add(ActivateFailed());
          bloc.add(Activated());
          bloc.add(LogInInitiated());
          bloc.add(LoggedIn(email: 'email', password: 'wrong'));
          bloc.add(DialogDismissed('log-in'));
          bloc.add(LogInInitiated());
          bloc.add(LoggedIn(email: 'email', password: 'correct'));
        },
        expect: () => [
              SignUpFeedback({}, messages: {}),
              StartInProgress(),
              SignUpFeedback({
                'email': 'email',
                'password': 'error'
              }, messages: {
                'messages': ['error message']
              }),
              StartInitialState({'email': 'email', 'password': 'error'}),
              SignUpFeedback({'email': 'email', 'password': 'error'},
                  messages: {}),
              StartInProgress(),
              StartInfo({'email': 'email', 'password': 'correct'},
                  dialogId: 'register-sent',
                  title: 'title',
                  message: 'message'),
              StartInfo(
                {'email': 'email', 'password': 'correct'},
                dialogId: 'register-error',
                title: 'User.register.error.title',
                message: 'User.register.error.summary',
              ),
              StartInfo(
                {'email': 'email', 'password': 'correct'},
                dialogId: 'register-success',
                title: 'User.register.success.title',
                message: 'User.register.success.summary',
              ),
              LogInFeedback({'email': 'email', 'password': 'correct'},
                  messages: {}),
              StartInProgress(),
              LogInFeedback(
                {'email': 'email', 'password': 'wrong'},
                messages: {
                  'messages': ['error message']
                },
              ),
              StartInitialState({'email': 'email', 'password': 'wrong'}),
              LogInFeedback({'email': 'email', 'password': 'wrong'},
                  messages: {}),
              StartInProgress(),
              LogInSuccess(Granted(
                  'email',
                  AccessRemote(
                      accessToken: 'accessToken',
                      refreshToken: 'refreshToken')))
            ],
        verify: (_) {
          verify(() => authBloc.add(any(that: isA<Granted>()))).called(1);
        });
  });

  group('Tokens', () {
    blocTest<StartBloc, StartState>('keeps tokens until password is changed',
        build: () => startBloc,
        act: (bloc) {
          bloc.add(LogInInitiated());
          bloc.add(PasswordForgotten(email: 'wrong'));
          bloc.add(PasswordForgotten(email: 'email'));
          bloc.add(ResetPasswordInitiated('uidb64', 'token'));
          bloc.add(PasswordSubmitted(password: 'wrong'));
          bloc.add(PasswordSubmitted(password: 'pw'));
        },
        expect: () => [
              LogInFeedback({}, messages: {}),
              StartInProgress(),
              LogInFeedback({
                'email': 'wrong'
              }, messages: {
                'messages': ['not sent']
              }),
              StartInProgress(),
              StartInfo({'email': 'email'},
                  dialogId: 'dialog', title: 'title', message: 'sent'),
              ResetPasswordFeedback({'email': 'email'},
                  messages: {}, uidb64: 'uidb64', token: 'token'),
              StartInProgress(),
              ResetPasswordFeedback({'password': 'wrong'},
                  messages: {'messages': ['not changed']}, uidb64: 'uidb64', token: 'token'),
              StartInProgress(),
              StartInfo({'email': 'email'},
                  dialogId: 'dialog', title: 'title', message: 'changed'),
            ]);
  });
}
