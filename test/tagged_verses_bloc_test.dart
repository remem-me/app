import 'package:app_core/app_core.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository/repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:bloc_test/bloc_test.dart';

class MockSettings extends Mock implements Settings {}

class MockCurrentAccount extends Mock implements CurrentAccount {}

class MockVersesBloc extends Mock implements VersesBloc {}

class MockTagsBloc extends Mock implements TagsBloc {}

void main() {
  group('TaggedVersesBloc', () {
    late List<Verse> testVerses;
    late List<TagEntity> testTags;
    late TagsBloc tagsBloc;
    late VersesBloc versesBloc;

    setUpAll(() {
      // final currentAccount = MockCurrentAccount();
      final settings = MockSettings();
      when(() => settings.deviceId).thenReturn(0);
      I.registerSingleton<Settings>(settings);
      I.registerSingleton<CurrentAccount>(CurrentAccount());

      testVerses = [
        Verse.fromEntity(VerseEntity('ref3', 'pas3', id: 3, modified: 0, accountId: 1), rank: 3000),
        Verse.fromEntity(VerseEntity('ref1', 'pas1',
            id: 1, modified: 0, accountId: 1, tags: const {1: null}), rank: 2000),
            Verse.fromEntity(VerseEntity('ref2', 'pas2',
            id: 2, modified: 0, accountId: 1, tags: const {2: null}), rank: 1000),
      ];

      testTags = [
        TagEntity('tag1', id: 1, modified: 0, accountId: 1, included: false),
        TagEntity('tag2', id: 2, modified: 0, accountId: 1, included: true),
      ];
    });

    setUp(() {
      if(I.isRegistered<VersesBloc>()) I.unregister<VersesBloc>();
      versesBloc = MockVersesBloc();
      when(() => versesBloc.stream).thenAnswer((_) => NeverStream());
      when(() => versesBloc.state).thenReturn(EntitiesLoadSuccess<Verse>(testVerses));
      I.registerSingleton<VersesBloc>(versesBloc);
      if(I.isRegistered<TagsBloc>()) I.unregister<TagsBloc>();
      tagsBloc = MockTagsBloc();
      when(() => tagsBloc.stream).thenAnswer((_) => NeverStream());
      when(() => tagsBloc.state).thenReturn(EntitiesLoadSuccess<TagEntity>(testTags));
      I.registerSingleton<TagsBloc>(tagsBloc);
    });

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesSuccess()]',
      setUp: () => when(() => tagsBloc.state).thenReturn(EntitiesLoadSuccess<TagEntity>(testTags)),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesInProgress(),
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1, rank: 3000),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: const {1: 'tag1'}, rank: 2000),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: const {2: 'tag2'}, rank: 1000),
          ],
          tags: testTags.map((t) => Tag.fromEntity(t)).toList(),
        ),
        TaggedVersesInProgress(),
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1, rank: 3000),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: const {1: 'tag1'}, rank: 2000),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: const {2: 'tag2'}, rank: 1000),
          ],
          tags: testTags.map((t) => Tag.fromEntity(t)).toList(),
        ),
      ],
    );

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesFailure()] if versesBloc fails',
      setUp: () => when(() => versesBloc.state).thenReturn(const EntitiesLoadFailure("error")),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesFailure(),
      ],
    );

    blocTest<TaggedVersesBloc, TaggedVersesState>(
      'emits [TaggedVersesSuccess()] if tagsBloc fails',
      setUp: () => when(() => tagsBloc.state).thenReturn(const EntitiesLoadFailure("error")),
      build: () => TaggedVersesBloc(),
      act: (bloc) {},
      expect: () => [
        TaggedVersesInProgress(),
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1, rank: 3000),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: const {1: null}, rank: 2000),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: const {2: null}, rank: 1000),
          ],
          tags: const [],
        ),
        TaggedVersesInProgress(),
        TaggedVersesSuccess(
          verses: [
            Verse('ref3', 'pas3',
                id: 3, modified: 0, accountId: 1, rank: 3000),
            Verse('ref1', 'pas1',
                id: 1, modified: 0, accountId: 1, tags: const {1: null}, rank: 2000),
            Verse('ref2', 'pas2',
                id: 2, modified: 0, accountId: 1, tags: const {2: null}, rank: 1000),
          ],
          tags: const [],
        ),
      ],
    );
  });
}
