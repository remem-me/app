import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/study_service.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:mocktail/mocktail.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:app_core/app_core.dart';

const isOutOfWordsException = TypeMatcher<OutOfWordsException>();

const _testLines = [
  ['one'],
  ['two', 'three'],
  ['four', 'five', 'six'],
];

class MockCurrentAccount extends Mock implements CurrentAccount {}

void main() {
  final account = MockCurrentAccount();
  I.registerSingleton<CurrentAccount>(account);

  setUp(() {
    when(() => account.language).thenReturn(const Locale('en'));
  });

  group('passageToWords', () {
    test('Should handle simple phrase', () {
      String passage = 'One, two "three".';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One, ', 'two ', '"three".']
          ]));
    });
    test('Should handle multiple lines', () {
      String passage = 'One\n two "three".';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One\n'],
            [' two ', '"three".']
          ]));
    });
    test('Should handle multiple Japanese lines', () {
      when(() => account.language).thenReturn(const Locale('ja'));
      String passage = '主は私の羊飼いですから、\n必要なものはみな与えてくださいます。';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            [
              '主',
              'は',
              '私',
              'の',
              '羊飼',
              'い',
              'です',
              'から、\n'
            ],
            ['必要', 'なもの', 'はみ', 'な', '与え', 'て', 'く', 'ださい', 'ます。']
          ]));
    });
    test('Should add subsequent non-word to preceding word', () {
      String passage = 'One - two, "three".';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One - ', 'two, ', '"three".']
          ]));
    });
    test('Should add preceding non-word to subsequent word', () {
      String passage = 'One\n- two, "three".';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One\n'],
            ['- two, ', '"three".']
          ]));
    });
    test('Should add non-word before line break to preceding word', () {
      String passage = 'One -\ntwo, "three".';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One -\n'],
            ['two, ', '"three".']
          ]));
    });
    test('Should handle three dots', () {
      String passage = 'One, two, ...\nthree';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['One, ', 'two, ...\n'],
            ['three']
          ]));
    });
    test('Should handle three dots in separate line', () {
      String passage = '"One",\n...\nthree';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['"One",\n'],
            ['...\n'],
            ['three']
          ]));
    });
    test('Should list item with three dots', () {
      String passage = '"One",\n- ...\nthree';
      List<List<String>> result = TextService().words(passage);
      expect(
          result,
          equals([
            ['"One",\n'],
            ['- ...\n'],
            ['three']
          ]));
    });
  });

  group('punctuation', () {
    test('Should handle word with punctuation and space', () {
      String word = 'word", ';
      int after = TextService().punctuationAfter(word);
      expect(after, equals(3));
    });
    test('Should handle number with space', () {
      String word = '1 ';
      int after = TextService().punctuationAfter(word);
      expect(after, equals(1));
    });
    test('Should handle list item with space and three dots', () {
      String word = '- ...\n';
      int before = TextService().punctuationBefore(word);
      int after = TextService().punctuationAfter(word);
      expect(before, equals(2));
      expect(after, equals(4));
    });
    test('Should handle list item with three dots', () {
      String word = '-...\n';
      int before = TextService().punctuationBefore(word);
      int after = TextService().punctuationAfter(word);
      expect(before, equals(0));
      expect(after, equals(5));
    });
  });

  group('LineUp', () {
    test('Should advance to first word', () {
      Pos result = StudyService().advanceByWord(_testLines, const Pos(0, -1));
      expect(result, const Pos(0, 0));
    });

    test('Should advance to last word', () {
      Pos result = StudyService().advanceByWord(_testLines, const Pos(2, 1));
      expect(result, const Pos(2, 2));
    });

    test('Should advance to next line', () {
      Pos result = StudyService().advanceByWord(_testLines, const Pos(1, 1));
      expect(result, const Pos(2, 0));
    });

    test('Advancing by word should throw exception', () {
      expect(() => StudyService().advanceByWord(_testLines, const Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });

    test('Should advance to end of line', () {
      Pos result = StudyService().advanceByLine(_testLines, const Pos(0, 1));
      expect(result, const Pos(1, 1));
    });

    test('Should advance to last line', () {
      Pos result = StudyService().advanceByLine(_testLines, const Pos(1, 1));
      expect(result, const Pos(2, 2));
    });

    test('Advancing by line should throw exception', () {
      expect(() => StudyService().advanceByLine(_testLines, const Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });

    test('Should retract word from last word', () {
      Pos result = StudyService().retractWord(_testLines, const Pos(2, 2));
      expect(result, const Pos(2, 1));
    });

    test('Should retract word to previous line', () {
      Pos result = StudyService().retractWord(_testLines, const Pos(2, 0));
      expect(result, const Pos(1, 1));
    });

    test('Should retract word before first word', () {
      Pos result = StudyService().retractWord(_testLines, const Pos(0, 0));
      expect(result, const Pos(0, -1));
    });

    test('Should not retract word', () {
      Pos result = StudyService().retractWord(_testLines, const Pos(0, -1));
      expect(result, const Pos(0, -1));
    });

    test('Should retract line', () {
      Pos result = StudyService().retractLine(_testLines, const Pos(2, 2));
      expect(result, const Pos(1, 1));
    });

    test('Should retract line to beginning', () {
      Pos result = StudyService().retractLine(_testLines, const Pos(0, 1));
      expect(result, const Pos(0, -1));
    });

    test('Should not retract line', () {
      Pos result = StudyService().retractLine(_testLines, const Pos(0, -1));
      expect(result, const Pos(0, -1));
    });

    test('Should look ahead', () {
      Pos result = StudyService().lookAhead(_testLines, const Pos(0, -1));
      expect(result, const Pos(0, 0));
    });

    test('Should look line ahead', () {
      Pos result = StudyService().lookAhead(_testLines, const Pos(1, 1));
      expect(result, const Pos(2, 2));
    });

    test('Looking ahead should throw exception', () {
      expect(() => StudyService().lookAhead(_testLines, const Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });
  });

  group('Typing', () {
    test('Should accept single character', () {
      bool result = TextService().startsWith('word', 'w');
      expect(result, isTrue);
    });
    test('Should accept multiple characters', () {
      bool result = TextService().startsWith('word', 'wo');
      expect(result, isTrue);
    });
    test('Should deny wrong beginning', () {
      bool result = TextService().startsWith('word', 'o');
      expect(result, isFalse);
    });
    test('Should skip quote', () {
      bool result = TextService().startsWith('"word', 'w');
      expect(result, isTrue);
    });
  });

  group('Puzzle', () {
    test('Choice should contain one at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, const Pos(0, 0), 5);
      expect(result.solution, equals(result.options.indexOf('one')));
    });
    test('Choice should contain three at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, const Pos(1, 1), 5);
      expect(result.solution, equals(result.options.indexOf('three')));
    });
    test('Choice should contain five at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, const Pos(2, 1), 5);
      expect(result.solution, equals(result.options.indexOf('five')));
    });
    test('Choice should display empty item', () {
      Choice result = StudyService().puzzle([
        ['...']
      ], const Pos(0, 0), 1);
      expect(result.solution, equals(result.options.indexOf('   ')));
    });
  });

  group('Other TextService methods', () {
    test('newText should return the difference', () async {
      const prev = 'There';
      const next = 'There is';
      List<String> result = TextService().newText(prev, next);
      expect(result, equals(['is']));
    });
  });
}
